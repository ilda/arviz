﻿using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UVRPN.Core;

public class ScenePlacement : MonoBehaviour {

    public Transform CameraHololens;
    public Transform CameraVicon;

    public Vector3 SceneOffset = new Vector3(-0.003f, -0.046f, 1.23447f);
    private Vector3 sceneOffset = new Vector3();

    private Quaternion rotationReferential = new Quaternion(-0.707107f, 0.0f, 0.0f, 0.707107f);
    private Quaternion rotationReferential2 = new Quaternion(-1f, 0f, 0f, 0f);
    
    private Vector3 WallVicon = new Vector3();    

    // Start is called before the first frame update
    void Start()
    {
        this.sceneOffset = new Vector3(-this.SceneOffset.x, this.SceneOffset.z, this.SceneOffset.y);
    }

    private void Update()
    {
        if (this.CameraHololens != null && this.CameraVicon != null)
        {
            Vector3 tmp = this.CameraVicon.rotation.eulerAngles;
            Quaternion corectedVicon = Quaternion.Euler(tmp.x, tmp.y, tmp.z);
            
            /* use rotation referential because of hat, now not useful */                                 
            //Vector3 eulertmp = (rotationReferential * CameraVicon.rotation).eulerAngles;
            Vector3 eulertmp = (this.CameraVicon.rotation).eulerAngles;
            
            Quaternion newAngle = this.CameraHololens.rotation * Quaternion.Euler(-eulertmp.x, eulertmp.z, -eulertmp.y);
            this.transform.position = this.CameraHololens.position + newAngle * (-this.CameraVicon.position);
            this.transform.rotation = newAngle;
        }
    }
    
    [Button]
    public void DistancePoint()
    {
        Debug.Log(Vector3.Distance(this.transform.position, this.CameraHololens.transform.position).ToString());
    }
}
