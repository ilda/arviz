﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ScenePlacementTest : MonoBehaviour
{
    public Transform CameraHololens;
    public Transform CameraVicon;

    public Vector3 SceneOffset = new Vector3(-0.003f, -0.046f, 1.23447f);
    private Vector3 sceneOffset = new Vector3();

    private Quaternion rotationReferential = new Quaternion(-0.707107f, 0.0f, 0.0f, 0.707107f);
    private Quaternion rotationReferential2 = new Quaternion(-1f, 0f, 0f, 0f);

    private Vector3 WallVicon = new Vector3();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = this.CameraHololens.position + this.rotationReferential * this.CameraHololens.rotation * this.CameraVicon.rotation * (-this.CameraVicon.position);
        this.transform.rotation = this.rotationReferential * this.CameraHololens.rotation * this.CameraVicon.rotation;
    }
}
