﻿using Synchro;
using System;
using System.Collections.Generic;
using UnityEngine;

public enum PrivacyState
{
    Foreign,
    Private,
    Public,
    Shared
};

public class Permissions
{
    // -1 : foreign
    // 0 : private
    // 1 : shared with collaborators
    // 2 : public

    private int permission;
    private List<int> collaborators;

    public Permissions()
    {
        this.permission = -1;
        this.collaborators = new List<int>();
    }

    public PrivacyState PermissionState()
    {
        return (this.permission == -1) ? PrivacyState.Foreign : (this.permission == 0) ? PrivacyState.Private : (this.permission == 1) ? PrivacyState.Shared : PrivacyState.Public;
    }

    public int GetPermission()
    {
        return this.permission;
    }

    public void SetCollaborators(List<int> collaboratorsIDs)
    {
        if (collaboratorsIDs.Count == 0)
        {
            this.permission = 0;
            this.collaborators.Clear();
        }
        else
        {
            this.permission = 1;
            this.collaborators.Clear();
            this.collaborators = new List<int>(collaboratorsIDs);
        }
    }

    public void SetCollaborator(int collaboratorID)
    {
        this.permission = 1;
        this.collaborators.Clear();
        this.collaborators.Add(collaboratorID);        
    }

    public List<int> GetCollaborators()
    {
        return this.collaborators;
    }

    public void SwitchShared(List<int> collaborators)
    {
        this.permission = 1;
        this.collaborators = new List<int>(collaborators);
    }
    public void SwitchPublic()
    {
        SetCollaborators(SynchroManager.Instance.fullOwners);
        this.permission = 2;
    }
    public void SwitchPrivate()
    {
        this.permission = 0;
        this.collaborators.Clear();
    }
    public void SwitchForeign()
    {
        this.permission = -1;
        this.collaborators.Clear();
    }

    public static PrivacyState PrivacyConverter(int p)
    {
        return (p == -1) ? PrivacyState.Foreign : (p == 0) ? PrivacyState.Private : (p == 1) ? PrivacyState.Shared : (p == 2) ? PrivacyState.Public : throw new Exception("Invalid permission value " + p );
    }

    public static int PrivacyConverter(PrivacyState p)
    {
        return (p == PrivacyState.Foreign) ? -1  : (p == PrivacyState.Private) ? 0 : (p == PrivacyState.Shared) ? 1 : (p == PrivacyState.Public) ? 2 : throw new Exception("Invalid PrivacyState value " + p);
    }
}