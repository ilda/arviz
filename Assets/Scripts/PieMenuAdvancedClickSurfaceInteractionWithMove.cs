﻿using System.Collections;

using System.Linq;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;
using Microsoft.MixedReality.Toolkit;
using Synchro;
using System.Collections.Generic;
using NaughtyAttributes;

public class PieMenuAdvancedClickSurfaceInteractionWithMove : InteractionScript, IMixedRealitySourceStateHandler
{
    private bool activeMenu = false;
    private bool waitForAction = false;

    IMixedRealityPointer pointer;

    Transform cameraTr;

    bool moveOrderedCenterHead = false;
    bool batchMoving = false;
    public bool isAttached = false;


    Vector3 deltaDistance;
    float itemDistance;
    DeleteObject delObj = new DeleteObject();

    public AudioClip PressClip;
    new AudioSource audio;

    public GameObject ButtonDelete;

    private bool expandMove = false;

    protected override void Start()
    {
        base.Start();
        this.changeSurfaceMessage.isWindow = true;
        this.cameraTr = Camera.main.transform;
        this.delObj.owner = SynchroManager.Instance.ownerId;
        this.delObj.name = this.hostTransform.name;

        this.audio = this.gameObject.AddComponent<AudioSource>();
        UpdateContent();
    }

    public void DiffStart()
    {
        this.changeSurfaceMessage = new ChangeSurface
        {
            isWindow = true
        };

        this.delObj.owner = SynchroManager.Instance.ownerId;
        this.delObj.name = this.hostTransform.name;
    }

    public void StartMove(bool expand = false)
    {
        this.cameraTr = Camera.main.transform;
        this.expandMove = expand;
        if (this.expandMove)
            this.hostTransform.GetComponent<AuthorViewRights>().MakeShared(SynchroManager.Instance.fullOwners);
        StartHeadMove();
    }

    public void UpdateContent()
    {
        if (this.hostTransform.childCount <= 1)
        {
            this.ButtonDelete.SetActive(true);
        }
        else
        {
            this.ButtonDelete.SetActive(false);
        }
    }

    protected override void Update()
    {
        base.Update();
        //Debug.Log(hostTransform.name + " " + moveOrderedCenterHead + " " + batchMoving + " " + isMagnetic);
        if (this.moveOrderedCenterHead)
        {
            Quaternion targetRotation = this.transform.rotation;
            Vector3 targetPosition;

            targetPosition = this.gazeProvider.HitPosition;
            targetRotation = this.cameraTr.rotation;

            // Apply magnetism effect when getting inside a defined effect area
            if (this.isMagnetic)
            {
                Magnetism(this.max_dist, ref targetPosition, ref targetRotation);
            }

            float lerpAmount = GetLerpAmount();
            targetPosition = targetPosition - new Vector3(0f, this.hostTransform.transform.localScale.y * 1.925f / 2f, 0f);
            Quaternion smoothedRotation = Quaternion.Lerp(this.hostTransform.rotation, targetRotation, lerpAmount);
            Vector3 smoothedPosition = Vector3.Lerp(this.hostTransform.position, targetPosition, lerpAmount);
            this.hostTransform.SetPositionAndRotation(smoothedPosition, smoothedRotation);
        }
        else if (this.batchMoving)
        {
            Quaternion targetRotation = this.transform.rotation;
            Vector3 targetPosition;

            targetPosition = this.gazeProvider.HitPosition;
            targetRotation = this.cameraTr.rotation;

            // Apply magnetism effect when getting inside a defined effect area
            if (this.isMagnetic)
            {
                //Debug.Log("magnet batch");
                BatchMagnetism(this.max_dist, ref targetPosition, ref targetRotation);
            }

            float lerpAmount = GetLerpAmount();
            //targetPosition = targetPosition - new Vector3(0f, hostTransform.transform.localScale.y * 1.925f / 2f, 0f);
            Quaternion smoothedRotation = Quaternion.Lerp(this.hostTransform.rotation, targetRotation, lerpAmount);
            Vector3 smoothedPosition = Vector3.Lerp(this.hostTransform.position, targetPosition, lerpAmount);
            this.hostTransform.SetPositionAndRotation(smoothedPosition, smoothedRotation);
        }

    }

    public override void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        eventData.Use();
    }

    public override void OnPointerDragged(MixedRealityPointerEventData eventData)
    {

        eventData.Use();
    }

    public override void OnPointerUp(MixedRealityPointerEventData eventData)
    {
        eventData.Use();
    }

    public override void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
        //Debug.Log(this.batchMoving + "  " + this.moveOrderedCenterHead + " " + this.expandMove);
        this.audio.PlayOneShot(this.PressClip);

        if (this.batchMoving)
        {
            this.batchMoving = false;

            for (int i = 0; i < this.hostTransform.childCount; i++)
            {
                Transform t2 = this.hostTransform.GetChild(i);
                if (!t2.name.Contains("Window"))
                {
                    //Debug.Log(this.hostTransform.parent.gameObject);
                    t2.GetComponent<SlateAuthorViewRights>().LetAuthoring();
                    t2.GetComponent<PieMenuClickInteraction>().SwitchSurfaceTo(this.hostTransform.parent.gameObject);
                    i--;
                }
            }
            CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);
            Delete();
        }
        else if (!this.moveOrderedCenterHead)
        {
            Vector3 menuScale;
            Vector3 menuPos;

            //Debug.Log(Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position));
            if (Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position) > 1.2f)
            {
                menuScale = Vector3.one * 0.007f * Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position) / 2.5f;
                menuPos = (this.gazeProvider.HitPosition + Camera.main.transform.position) / 2f;
            }
            else
            {
                menuScale = Vector3.one * 0.007f * (Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position) / 1.2f);
                menuPos = this.gazeProvider.HitPosition + Camera.main.transform.forward * (-0.05f);
            }

            this.pointer = this.pointer = eventData.InputSource.Pointers.First();

            StartHeadMove();
        }
        else if (this.moveOrderedCenterHead)
        {
            this.moveOrderedCenterHead = false;

            foreach (Collider c in this.hostTransform.GetComponentsInChildren<Collider>())
            {
                c.enabled = true;
            }

            if (this.expandMove)
            {
                for (int i = 0; i < this.hostTransform.childCount; i++)
                {
                    Transform t2 = this.hostTransform.GetChild(i);
                    if (!t2.name.Contains("Window"))
                    {
                        t2.GetComponent<SlateAuthorViewRights>().LetAuthoring();
                    }
                }
                this.expandMove = false;
            }

            CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);
        }

        eventData.Use();
    }

    private void StartPieMenu(MixedRealityPointerEventData eventData)
    {
        string[] funcName = new string[] { "Move On Boundaries", null, null, null };
        if (this.hostTransform.childCount <= 1)
        {
            funcName[1] = "Delete";
        }

        this.activeMenu = true;


        Vector3 menuScale;
        Vector3 menuPos;

        //Debug.Log(Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position));
        if (Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position) > 1.2f)
        {
            menuScale = Vector3.one * 0.007f * Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position) / 2.5f;
            menuPos = (this.gazeProvider.HitPosition + Camera.main.transform.position) / 2f;
        }
        else
        {
            menuScale = Vector3.one * 0.007f * (Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position) / 1.2f);
            menuPos = this.gazeProvider.HitPosition + Camera.main.transform.forward * (-0.05f);
        }

        PieMenu.Instance.Call(menuPos, Camera.main.transform.rotation, menuScale, funcName);

        PieMenu.Instance.OnAction1 += StartHeadMove;
        PieMenu.Instance.OnAction2 += Delete;

        this.pointer = this.pointer = eventData.InputSource.Pointers.First();

        CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
    }

    private void StopPieMenu()
    {
        CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= Delete;

        PieMenu.Instance.Destroy();
    }

    private void CancelPieMenu()
    {
        CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);

        this.activeMenu = false;
        this.moveOrderedCenterHead = false;
        this.pointer = null;

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= Delete;

        PieMenu.Instance.Destroy();
    }

    private void StartHeadMove()
    {
        this.waitForAction = false;

        this.hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();

        this.moveOrderedCenterHead = true;
        this.activeMenu = false;

        this.deltaDistance = Camera.main.transform.forward * 0.2f;
        this.itemDistance = Vector3.Distance(this.transform.position, this.cameraTr.position);

        foreach (Collider c in this.hostTransform.GetComponentsInChildren<Collider>())
        {
            c.enabled = false;
        }

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= Delete;

        PieMenu.Instance.Hide();

        CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
    }


    // Add a delete action    
    public void Delete()
    {
        if (this.activeMenu)
            StopPieMenu();

        //SynchroServer.Instance.SendCommand("H", this.delObj);
        SynchroManager.Instance.CommandInstance.Cmd_DeleteObject(delObj.owner, delObj.name);
        Destroy(this.hostTransform.gameObject);
    }

    public void DeleteRemote()
    {
        StopPieMenu();

        WallGenerator.Instance.RemoveWindow(this.hostTransform.gameObject);
        Destroy(this.hostTransform.gameObject);
    }

    IEnumerator WaitForAction()
    {
        yield return new WaitForSeconds(0.5f);
        if (this.waitForAction)
            CancelPieMenu();
        this.waitForAction = false;
    }


    public void ReLayout()
    {

        // Get all cards inside the window
        List<Transform> cards = new List<Transform>();
        for (int i = 0; i < this.HostTransform.childCount; i++)
        {
            if (this.HostTransform.GetChild(i).name.Contains("Window"))
                continue;

            cards.Add(this.HostTransform.GetChild(i));
        }

        float w_window = this.transform.parent.localScale.x;
        float h_window = this.transform.parent.localScale.y;
        float w_step = OrbitalPersonalRing.Instance.stepX;
        float h_step = OrbitalPersonalRing.Instance.stepY;
        Vector3 baseOffset = new Vector3(w_window / 2f, h_window / 2f, 0f);

        int w_grid_count = Mathf.FloorToInt(w_window / w_step);
        int h_grid_count = Mathf.FloorToInt(h_window / h_step);
        float w_grid = w_window / w_grid_count;
        float h_grid = h_window / h_grid_count;

        int origin_w_grid_count = w_grid_count;
        w_grid_count = Mathf.CeilToInt(cards.Count / (float)h_grid_count);
        int resize_factor = origin_w_grid_count;

        List<List<Transform>> grid = Enumerable.Repeat<List<Transform>>(null, h_grid_count).ToList();
        // Create a grid to welcome the cards in fixed places
        // This grid has a fixed height, and variable width
        // Calculate grid capacity to resize window if the capacity is too small

        int w_index = 0;
        int h_index = 0;
        List<Transform> positioning = new List<Transform>();

        for (h_index = 0; h_index < h_grid_count; h_index++)
        {
            grid[h_index] = new List<Transform>();
            List<Transform> line = new List<Transform>();

            // Get every elements fitting a line in the grid
            for (int i = cards.Count - 1; i >= 0; i--)
            {
                Vector3 testPos = cards[i].localPosition + baseOffset;
                if ((h_grid_count - h_index) * h_grid > testPos.y && testPos.y > (h_grid_count - h_index - 1) * h_grid)
                {
                    line.Add(cards[i]);
                    cards.RemoveAt(i);
                }
            }

            // Fit the max width of each line to find out the max, and sort each line
            w_grid_count = Mathf.Max(w_grid_count, grid[h_index].Count);
            positioning.AddRange(line.OrderBy(o => o.localPosition.x).ToList());
        }
        h_index--;


        // If some element are outside, fit them in the last line
        if (cards.Count > 0)
            positioning.AddRange(cards.OrderBy(o => o.localPosition.x).ToList());


        // Determine every cards right position
        List<List<Vector3>> gridPositions = Enumerable.Repeat<List<Vector3>>(null, h_grid_count).ToList();
        int maxSize = 0;
        for (h_index = 0; h_index < h_grid_count; h_index++)
        {
            w_index = 0;
            gridPositions[h_index] = new List<Vector3>();

            while (positioning.Count > 0 && w_index < w_grid_count)
            {
                float xPos = (2 * w_index + 1) * w_grid / 2f;
                float yPos = (2 * h_index + 1) * h_grid / 2f;

                grid[h_index].Add(positioning[0]);
                gridPositions[h_index].Add(new Vector3(xPos, -yPos, 0f));
                positioning.RemoveAt(0);

                w_index++;
            }
        }
        maxSize = w_grid_count;
        // Resize the window
        this.transform.parent.localScale = new Vector3(((float)Mathf.Max(Mathf.FloorToInt(1.925f / w_step), maxSize) / resize_factor) * this.transform.parent.localScale.x, this.transform.parent.localScale.y, this.transform.parent.localScale.z);
        baseOffset = new Vector3(((float)Mathf.Max(Mathf.FloorToInt(1.925f / w_step), maxSize) / resize_factor) * baseOffset.x, -baseOffset.y, baseOffset.z);

        for (h_index = 0; h_index < h_grid_count; h_index++)
        {
            for (w_index = 0; w_index < grid[h_index].Count; w_index++)
            {
                grid[h_index][w_index].localPosition = gridPositions[h_index][w_index] - baseOffset;
                grid[h_index][w_index].hasChanged = true;
            }
            grid[h_index].Clear();
        }
        cards.Clear();
        grid.Clear();
    }


    public void Relayout_2()
    {

        // Get all cards inside the window
        List<Transform> cards = new List<Transform>();
        for (int i = 0; i < this.HostTransform.childCount; i++)
        {
            if (this.HostTransform.GetChild(i).name.Contains("Window"))
                continue;

            cards.Add(this.HostTransform.GetChild(i));
        }

        float w_window = this.transform.parent.localScale.x;
        float h_window = this.transform.parent.localScale.y;
        float w_step = OrbitalPersonalRing.Instance.stepX;
        float h_step = OrbitalPersonalRing.Instance.stepY;
        Vector3 baseOffset = new Vector3(w_window / 2f, h_window / 2f, 0f);

        int w_grid_count = Mathf.FloorToInt(w_window / w_step);
        int h_grid_count = Mathf.FloorToInt(h_window / h_step);
        float w_grid = w_window / w_grid_count;
        float h_grid = h_window / h_grid_count;
        int resize_factor = w_grid_count;

        List<List<Transform>> grid = Enumerable.Repeat<List<Transform>>(null, h_grid_count).ToList();
        // Create a grid to welcome the cards in fixed places
        // This grid has a fixed height, and variable width
        // Calculate grid capacity to resize window if the capacity is too small

        int w_index = 0;
        int h_index = 0;

        for (h_index = 0; h_index < h_grid_count; h_index++)
        {
            grid[h_index] = new List<Transform>();

            // Get every elements fitting a line in the grid
            for (int i = cards.Count - 1; i >= 0; i--)
            {
                Vector3 testPos = cards[i].localPosition + baseOffset;
                if ((h_grid_count - h_index) * h_grid > testPos.y && testPos.y > (h_grid_count - h_index - 1) * h_grid)
                {
                    grid[h_index].Add(cards[i]);
                    cards.RemoveAt(i);
                }
            }

            // Fit the max width of each line to find out the max, and sort each line
            w_grid_count = Mathf.Max(w_grid_count, grid[h_index].Count);
            grid[h_index] = grid[h_index].OrderBy(o => o.localPosition.x).ToList();
        }
        h_index--;


        // If some element are outside, fit them in the last line
        if (cards.Count > 0)
            grid[h_index].AddRange(cards);
        w_grid_count = Mathf.Max(w_grid_count, grid[h_index].Count);
        grid[h_index] = grid[h_index].OrderBy(o => o.localPosition.x).ToList();

        // Determine every cards right position
        List<List<Vector3>> gridPositions = Enumerable.Repeat<List<Vector3>>(null, w_grid_count).ToList();
        int maxSize = 0;
        for (h_index = 0; h_index < h_grid_count; h_index++)
        {
            w_index = 0;
            int xPosIndex = w_index;
            gridPositions[h_index] = new List<Vector3>();

            while (w_index < grid[h_index].Count)
            {
                while (grid[h_index][w_index].localPosition.x > (xPosIndex + 1) * w_grid - baseOffset.x)
                    xPosIndex++;

                float xPos = (2 * xPosIndex + 1) * w_grid / 2f;
                float yPos = (2 * h_index + 1) * h_grid / 2f;

                gridPositions[h_index].Add(new Vector3(xPos, -yPos, 0f));

                w_index++;
                xPosIndex++;
                maxSize = Mathf.Max(maxSize, xPosIndex);
            }
        }

        // Resize the window
        this.transform.parent.localScale = new Vector3(((float)Mathf.Max(Mathf.FloorToInt(1.925f / w_step), maxSize) / resize_factor) * this.transform.parent.localScale.x, this.transform.parent.localScale.y, this.transform.parent.localScale.z);
        baseOffset = new Vector3(((float)Mathf.Max(Mathf.FloorToInt(1.925f / w_step), maxSize) / resize_factor) * baseOffset.x, -baseOffset.y, baseOffset.z);

        for (h_index = 0; h_index < h_grid_count; h_index++)
        {
            for (w_index = 0; w_index < grid[h_index].Count; w_index++)
            {
                grid[h_index][w_index].localPosition = gridPositions[h_index][w_index] - baseOffset;
                grid[h_index][w_index].hasChanged = true;
            }
            grid[h_index].Clear();
        }
        cards.Clear();
        grid.Clear();
    }

    [Button]
    public void BatchMove()
    {
        Transform ts = this.HostTransform;

        int childCountInWindow = this.HostTransform.childCount;
        float minX = this.HostTransform.GetChild(0).transform.localPosition.x,
               minY = this.HostTransform.GetChild(0).transform.localPosition.y,
               maxX = this.HostTransform.GetChild(0).transform.localPosition.x,
               maxY = this.HostTransform.GetChild(0).transform.localPosition.x;

        for (int i = 0; i < childCountInWindow; i++)
        {
            Transform t = this.HostTransform.GetChild(i);
            if (t.name.Contains("Window"))
                continue;

            minX = Mathf.Min(minX, t.transform.localPosition.x);
            minY = Mathf.Min(minY, t.transform.localPosition.y);
            maxX = Mathf.Max(maxX, t.transform.localPosition.x);
            maxY = Mathf.Max(maxY, t.transform.localPosition.y);
        }

        Vector3 cursorCentroidAlt = new Vector3((maxX + minX) / 2, (maxY + minY) / 2, 0);

        for (int i = this.HostTransform.childCount - 1; i >= 0; i--)
        {
            Transform t = this.HostTransform.GetChild(i);
            Vector3 v = t.localPosition;
            if (t.name.Contains("Window"))
                continue;

            t.parent = SynchroManager.Instance.transform;
            t.GetComponent<PieMenuClickInteraction>().BatchHeadMove(v - cursorCentroidAlt, childCountInWindow, Mathf.Abs(maxY - minY), Mathf.Abs(maxX - minX), i);
        }
    }

    public void OnSourceDetected(SourceStateEventData eventData)
    {
        if (this.moveOrderedCenterHead)
        {
            this.pointer = eventData.InputSource.Pointers.First();
        }
    }

    public void OnSourceLost(SourceStateEventData eventData)
    {
        this.pointer = null;        
    }

    public override void ChangeMagnet(GameObject g)
    {
        if(g == null)
        {

            this.isAttracted = false;
            this.Magnet = null;

        }
        else
        {
            this.isAttracted = true;
            this.Magnet = g;
        }

    }

    protected override void Magnetism(float max_dist, ref Vector3 position, ref Quaternion rotation)
    {

        // Manage magnetism when multiple magnet exists 
        int minIndex = 0;
        float distance;
        Vector3 ClosestToCenter;

        GameObject[] Magnets = GameObject.FindGameObjectsWithTag("MagneticBoundary");
        Vector3 OnMagnet = Magnets[0].GetComponent<Collider>().ClosestPoint(position);
        float minDist = Vector3.Distance(OnMagnet, position);

        // Find the magnet closest to the transform 
        for (int i = 1; i < Magnets.Length; i++)
        {
            OnMagnet = Magnets[i].GetComponent<Collider>().ClosestPoint(position);

            distance = Vector3.Distance(OnMagnet, position); ;
            if (distance < minDist)
            {
                minDist = distance;
                minIndex = i;
            }
        }


        // Calculate the true position for item
        GameObject ClosestMagnet = Magnets[minIndex];
        OnMagnet = ClosestMagnet.GetComponent<Collider>().ClosestPoint(position);
        ClosestToCenter = ClosestMagnet.transform.position - OnMagnet;

        Vector3 objOnMagnet = Vector3.Project(ClosestToCenter, ClosestMagnet.transform.forward) + OnMagnet;

        // Operate Magnet effect on the object
        if (Vector3.Distance(objOnMagnet, position) <= max_dist)
        {
            // Case where item goes off a surface
            if (this.isAttracted == false)
            {
                this.Magnet = ClosestMagnet;
                this.hostTransform.GetComponent<SurfaceAuthorViewRights>().OnSurface(ClosestMagnet);

                this.changeSurfaceMessage.surfaceName = ClosestMagnet.name;
                //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }
            // Case where item changes surface without going through the 3D space
            else if (this.isAttracted == true && ClosestMagnet != this.Magnet)
            {
                this.hostTransform.GetComponent<SurfaceAuthorViewRights>().SwitchSurface(this.Magnet, ClosestMagnet);
                this.Magnet = ClosestMagnet;

                this.changeSurfaceMessage.surfaceName = ClosestMagnet.name;
                //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }

            this.isAttracted = true;
            position = objOnMagnet + (Camera.main.transform.position - this.Magnet.transform.position).normalized * 0.01f;
            rotation = (Vector3.Dot(ClosestMagnet.transform.forward, this.hostTransform.forward) > 0f) ? ClosestMagnet.transform.rotation : Quaternion.Inverse(ClosestMagnet.transform.rotation);
        }
        else
        {
            // Case where item is in 3D space and goes on a surface
            if (this.isAttracted == true)
            {
                this.hostTransform.GetComponent<SurfaceAuthorViewRights>().OffSurface(this.Magnet);

                this.Magnet = null;

                this.changeSurfaceMessage.surfaceName = null;
                //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }
            this.isAttracted = false;
        }
    }

    private void BatchMagnetism(float max_dist, ref Vector3 position, ref Quaternion rotation)
    {
        // Manage magnetism when multiple magnet exists
        int minIndex = 0;
        float distance;
        Vector3 ClosestToCenter;

        GameObject[] Magnets = GameObject.FindGameObjectsWithTag("MagneticSurface");
        Vector3 OnMagnet = Magnets[0].GetComponent<Collider>().ClosestPoint(position);
        float minDist = Vector3.Distance(OnMagnet, position);

        // Find the magnet closest to the transform 
        for (int i = 1; i < Magnets.Length; i++)
        {
            //Debug.Log(Magnets[i] + " " + this.hostTransform.name);
            if (Magnets[i].name == this.hostTransform.name)
                continue;

            OnMagnet = Magnets[i].GetComponentInChildren<Collider>().ClosestPoint(position);

            distance = Vector3.Distance(OnMagnet, position); ;
            if (distance < minDist)
            {
                minDist = distance;
                minIndex = i;
            }
        }


        // Calculate the true position for item
        GameObject ClosestMagnet = Magnets[minIndex];
        OnMagnet = ClosestMagnet.GetComponentInChildren<Collider>().ClosestPoint(position);
        ClosestToCenter = ClosestMagnet.transform.position - OnMagnet;

        Vector3 objOnMagnet = Vector3.Project(ClosestToCenter, ClosestMagnet.transform.forward) + OnMagnet;

        // Operate Magnet effect on the object
        if (Vector3.Distance(objOnMagnet, position) <= max_dist)
        {
            // Case where item goes off a surface
            if (this.isAttracted == false)
            {
                this.Magnet = ClosestMagnet;
                this.hostTransform.GetComponent<SurfaceAuthorViewRights>().OnSurface(ClosestMagnet);

                this.changeSurfaceMessage.surfaceName = ClosestMagnet.name;
                //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }
            // Case where item changes surface without going through the 3D space
            else if (this.isAttracted == true && ClosestMagnet != this.Magnet)
            {
                this.hostTransform.GetComponent<SurfaceAuthorViewRights>().SwitchSurface(this.Magnet, ClosestMagnet);
                this.Magnet = ClosestMagnet;

                this.changeSurfaceMessage.surfaceName = ClosestMagnet.name;
                //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }

            this.isAttracted = true;
            position = objOnMagnet + (Camera.main.transform.position - this.Magnet.transform.position).normalized * 0.01f;
            rotation = (Vector3.Dot(ClosestMagnet.transform.forward, this.hostTransform.forward) > 0f) ? ClosestMagnet.transform.rotation : Quaternion.Inverse(ClosestMagnet.transform.rotation);
        }
        else
        {
            // Case where item is in 3D space and goes on a surface
            if (this.isAttracted == true)
            {
                this.hostTransform.GetComponent<SurfaceAuthorViewRights>().OffSurface(this.Magnet);

                this.Magnet = null;

                this.changeSurfaceMessage.surfaceName = null;
                //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }
            this.isAttracted = false;
        }
    }

    // Collapse/Expand the personal space window (window to bag mode and reverse)   
    public void Empty()
    {
        // Oops doesnt work that way
        //OrbitalPersonalRing.Instance.gameObject.GetComponent<PersonalSpaceManagement>().Empty(this);
        this.ButtonDelete.SetActive(true);
    }

    public void Expand()
    {

    }
}

