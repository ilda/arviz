﻿using UnityEngine;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using System.Collections.Generic;
using System.Collections;
using Synchro;
using NaughtyAttributes;

public class VersioningNotifications : MonoBehaviour
{
    [BoxGroup("Textures")]
    public Material basic;
    [BoxGroup("Textures")]
    public Material notifNew;
    [BoxGroup("Textures")]
    public Material notifMissing;
    [BoxGroup("Textures")]
    public Material invisible;

    [BoxGroup("ObjectStatus")]
    public Transform plane;
    [BoxGroup("ObjectStatus")]
    public bool isAttracted;
    [BoxGroup("ObjectStatus")]
    public bool isOnWall;

    [BoxGroup("Ownership Items")]
    public GameObject permissionPanel;
    [BoxGroup("Ownership Items")]
    public GameObject ownA;
    [BoxGroup("Ownership Items")]
    public GameObject ownB;
    [BoxGroup("Ownership Items")]
    public GameObject ownC;

    public bool ToDestroy { get; private set; } = false;

    public bool ShowId = false;

    public Bind OwnerObjectBind;
    private Bind oobObject;
    
    private bool isNotified = false;
    private bool hasDisappeared = false;
    private bool focus = false;
    private bool selection = false;
    private int memOwner = -1;

    private Material Action0;
    private Material Action1;
    private Material Action2;

    private Material Focus0;
    private Material Focus1;
    private Material Focus2;

    private Material SelectionDefault;
    private Material Selection0;
    private Material Selection1;
    private Material Selection2;


    public GameObject SelectionOutline;


    // Start is called before the first frame update
    void Awake()
    {
        this.plane = this.transform.GetChild(0);
    }

    void Start()
    {
        this.Action0 = Resources.Load("Action0") as Material;
        this.Action1 = Resources.Load("Action1") as Material;
        this.Action2 = Resources.Load("Action2") as Material;

        this.Focus0 = Resources.Load("Focus0") as Material;
        this.Focus1 = Resources.Load("Focus1") as Material;
        this.Focus2 = Resources.Load("Focus2") as Material;

        this.SelectionDefault = Resources.Load("SelectionDefault") as Material;
        this.Selection0 = Resources.Load("Selection0") as Material;
        this.Selection1 = Resources.Load("Selection1") as Material;
        this.Selection2 = Resources.Load("Selection2") as Material;

        if (!this.ToDestroy && this.isAttracted && (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS))
            this.plane.GetComponent<Renderer>().material = this.invisible;

        if (this.isOnWall)
            SetWallVisibility();
        else 
            SetStandardVisibility(true, true, true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetStandardVisibility(bool c, bool b, bool a)
    {
        this.isOnWall = false;

        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS || SynchroManager.Instance.IsDevice() == Synchro.DeviceType.OBSERVER)
        {
            SetOwnSelf();
        }
        else if(SynchroManager.Instance.IsDevice() == Synchro.DeviceType.WALL)
        {
            SetPublicSelf();
        }

    }

    public void SetWallVisibility()
    {
        this.isOnWall = true;

        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS || SynchroManager.Instance.IsDevice() == Synchro.DeviceType.OBSERVER)
            SetPublicSelf();
        else if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.WALL)
            SetOwnSelf();

    }

    public void Deactivate()
    {
        //Debug.Log("isDeactivated");
        Destroy(this.oobObject);
        this.oobObject = null;
        this.gameObject.SetActive(false);
    }

    public void Activate()
    {
        this.gameObject.SetActive(true);
    }

    public void SendMissing()
    {
        //Debug.Log("Send Take");
        SynchroManager.Instance.SharedToOwn(this.name);
        SynchroManager.Instance.SharedToOwn(this.GetComponent<BoundingBox>().Target.name);
        /*
        SynchroServer.Instance.SendCommand("H", new TakeOwnership
        {
            objectName = this.name,
            owner = SynchroManager.Instance.ownerId
        });
        */
        SynchroManager.Instance.CommandInstance.Cmd_TakeOwnership(this.name, SynchroManager.Instance.ownerId);
    }

    public void SendNew()
    {
        //Debug.Log("Send Public");
        SynchroManager.Instance.OwnToShared(this.name);
        SynchroManager.Instance.OwnToShared(this.GetComponent<BoundingBox>().Target.name);
        /*
        SynchroServer.Instance.SendCommand("H", new MakePublic
        {
            objectName = this.name,
            owner = SynchroManager.Instance.ownerId
        });
        */
        SynchroManager.Instance.CommandInstance.Cmd_MakePublic(this.name, SynchroManager.Instance.ownerId);
    }

    public void Remove()
    {
        this.plane.GetComponent<Renderer>().enabled = false;
        foreach (Transform child in this.plane)
        {
            child.gameObject.SetActive(false);
        }
        this.hasDisappeared = true;
    }

    public void Add()
    {
        this.plane.GetComponent<Renderer>().enabled = true;
        this.plane.GetComponent<Renderer>().material = this.basic;
        foreach (Transform child in this.plane)
        {
            child.gameObject.SetActive(true);
        }
        this.hasDisappeared = false;
    }

    // Option to disable / enable the ABC marking on the slate
    public void OwnershipDisplay(bool on)
    {
        this.permissionPanel.SetActive(on);
    }

    public void HasSelection(int id)
    {        
        //Debug.Log("Selection Color !");

        if (this.SelectionOutline == null)
        {
            if (this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.WALL
                || !this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS
                || !this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.OBSERVER
                || SynchroManager.Instance.IsDevice() == Synchro.DeviceType.MASTER)
            {
                Material m;

                switch (id)
                {
                    case 0:
                        m = this.Action0;
                        break;
                    case 1:
                        m = this.Action1;
                        break;
                    case 2:
                        m = this.Action2;
                        break;
                    default:
                        m = this.Action0;
                        break;
                }
                this.transform.GetChild(0).GetComponent<Renderer>().materials = new Material[2] { this.transform.GetChild(0).GetComponent<Renderer>().material, m };
            }
        }
        else
        {
            if (this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.WALL
                || !this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS
                || !this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.OBSERVER
                || SynchroManager.Instance.IsDevice() == Synchro.DeviceType.MASTER)
            {
                Material m;

                switch (id)
                {
                    case 0:
                        m = this.Selection0;
                        break;
                    case 1:
                        m = this.Selection1;
                        break;
                    case 2:
                        m = this.Selection2;
                        break;
                    default:
                        m = this.Selection0;
                        break;
                }
                this.SelectionOutline.SetActive(true);
                this.SelectionOutline.GetComponent<Renderer>().material = m;
            }
        }
        //Debug.Log("Color = " + id);
        //Debug.Log("Focus has Selection !");

        this.memOwner = id;
        this.selection = true;
    }

    public void LoseSelection()
    {
        if (this.SelectionOutline == null)
        {
            if (this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.WALL
            || !this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS
            || !this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.OBSERVER
            || SynchroManager.Instance.IsDevice() == Synchro.DeviceType.MASTER)
            {
                Material m = this.transform.GetChild(0).GetComponent<Renderer>().material;
                this.transform.GetChild(0).GetComponent<Renderer>().materials = new Material[1] { this.transform.GetChild(0).GetComponent<Renderer>().materials[0] = m };
            }
        }
        else
        {
            if (this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.WALL
               || !this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS
               || !this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.OBSERVER
               || SynchroManager.Instance.IsDevice() == Synchro.DeviceType.MASTER)
            {
                //this.SelectionOutline.SetActive(false);
                this.SelectionOutline.GetComponent<Renderer>().material = this.SelectionDefault;
            }
        }
        //Debug.Log("Focus Lose Selection !");

        this.selection = false;
    }

    public void HasFocus()
    {
        /*
        Material m;

        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS
            || SynchroManager.Instance.IsDevice() == Synchro.DeviceType.MASTER)
        {
            switch (SynchroManager.Instance.fullOwners.IndexOf(int.Parse(SynchroManager.Instance.ownerId)))
            {
                case 0:
                    m = this.Focus0;
                    break;
                case 1:
                    m = this.Focus1;
                    break;
                case 2:
                    m = this.Focus2;
                    break;
                default:
                    m = this.Focus0;
                    break;
            }
            this.transform.GetChild(0).GetComponent<Renderer>().materials = new Material[2] { this.transform.GetChild(0).GetComponent<Renderer>().material, m };
        }
        //Debug.Log("Focus has Color !");
        */

        this.focus = true;
    }

    public void LoseFocus()
    {
        /*
        Material m;

        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS
            || SynchroManager.Instance.IsDevice() == Synchro.DeviceType.MASTER)
        {
            if (SynchroManager.Instance.IsLocked(this.name, SynchroManager.Instance.ownerId))
            {
                if (this.selection
                    && (this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.WALL
                    || !this.isOnWall && SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS
                    || SynchroManager.Instance.IsDevice() == Synchro.DeviceType.MASTER)
                    && this.SelectionOutline == null)
                {
                    switch (SynchroManager.Instance.fullOwners.IndexOf(int.Parse(SynchroManager.Instance.ownerId)))
                    {
                        case 0:
                            m = this.Action0;
                            break;
                        case 1:
                            m = this.Action1;
                            break;
                        case 2:
                            m = this.Action2;
                            break;
                        default:
                            m = this.Action0;
                            break;
                    }
                    this.transform.GetChild(0).GetComponent<Renderer>().materials = new Material[2] { this.transform.GetChild(0).GetComponent<Renderer>().material, m };
                }
                else
                {
                    m = this.transform.GetChild(0).GetComponent<Renderer>().material;
                    this.transform.GetChild(0).GetComponent<Renderer>().materials = new Material[1] { this.transform.GetChild(0).GetComponent<Renderer>().materials[0] = m };
                }
                //Debug.Log("Focus Lost Color !");
                    
            }
            else
            {
                m = this.transform.GetChild(0).GetComponent<Renderer>().material;
                this.transform.GetChild(0).GetComponent<Renderer>().materials = new Material[1] { this.transform.GetChild(0).GetComponent<Renderer>().materials[0] = m };
            }
        }
        */
        this.focus = false;
    }

    public void CancelVisual()
    {
        Material m;
        m = this.transform.GetChild(0).GetComponent<Renderer>().material;
        this.transform.GetChild(0).GetComponent<Renderer>().materials = new Material[1] { this.transform.GetChild(0).GetComponent<Renderer>().materials[0] = m };
        //Debug.Log("Cancel Visual");
        this.selection = false;
        this.focus = false;
    }

    #region PrivacySetters

    public void SetOwn()
    {
        this.gameObject.SetActive(true);
        this.isNotified = false;
        this.plane.GetComponent<Renderer>().material = this.basic;
        this.plane.GetComponent<Renderer>().enabled = true;
        foreach (Transform child in this.plane)
        {
            child.gameObject.SetActive(true);
        }
        this.hasDisappeared = false;

        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
        {
            this.GetComponent<BoundingBox>().enabled = true;
            this.GetComponent<InteractionScript>().enabled = true;
        }
    }

    public void SetOwnSelf()
    {
        this.gameObject.SetActive(true);
        this.isNotified = false;
        this.plane.GetComponent<Renderer>().material = this.basic;
        this.plane.GetComponent<Renderer>().enabled = true;
        foreach (Transform child in this.plane)
        {
            child.gameObject.SetActive(true);
        }

        this.hasDisappeared = false;

        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
        {
            this.GetComponent<BoundingBox>().enabled = true;
            this.GetComponent<InteractionScript>().enabled = true;
        }

        if (this.focus)
            HasFocus();
        else
            LoseFocus();

        if (this.selection)
            HasSelection(this.memOwner);
        else
            LoseSelection();

        }

    public void SetPublic(string owner)
    {
        this.gameObject.SetActive(true);
        this.isNotified = true;
        this.plane.GetComponent<Renderer>().material = this.notifNew;
        foreach (Transform child in this.plane)
        {
            child.gameObject.SetActive(false);
        }
        this.hasDisappeared = false;

        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
        {
            this.GetComponent<BoundingBox>().enabled = true;
            this.GetComponent<InteractionScript>().enabled = true;
        }

        this.oobObject = Instantiate(this.OwnerObjectBind) as Bind;
        this.oobObject.node1 = this.transform;
        this.oobObject.node2 = GameObject.Find("Op" + owner).transform;
        this.oobObject.NewBind();
    }

    public void SetPublicSelf()
    {
        this.gameObject.SetActive(true);
        this.isNotified = true;        
        foreach (Transform child in this.plane)
        {
            child.gameObject.SetActive(false);
        }

        this.hasDisappeared = false;

        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
        {
            this.GetComponent<BoundingBox>().enabled = true;
            this.GetComponent<InteractionScript>().enabled = true;
        }

        this.plane.GetComponent<Renderer>().material = this.invisible;


        if (this.focus)
            HasFocus();
        else
            LoseFocus();

        if (this.selection)
            HasSelection(this.memOwner);
        else
            LoseSelection();
    }

    public void SetForeign(string owner)
    {
        GameObject Shadow = Instantiate(this.gameObject);        
        Shadow.transform.position = this.transform.position;
        Shadow.transform.parent = SynchroManager.Instance.transform;
        Shadow.transform.localRotation = Quaternion.identity;
        Shadow.GetComponent<VersioningNotifications>().SetForeignSelf(owner);
        Destroy(this.oobObject);
        this.oobObject = null;
        this.gameObject.SetActive(false);
    }

    public void SetForeignSelf(string owner)
    {
        this.gameObject.SetActive(true);
        this.isNotified = true;
        this.ToDestroy = true;

        this.plane.GetComponent<Renderer>().material = this.notifMissing;
        this.plane.GetComponent<Renderer>().enabled = true;
        foreach (Transform child in this.plane)
        {
            child.gameObject.SetActive(false);
        }
        this.hasDisappeared = true;

        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
        {
            this.GetComponent<BoundingBox>().enabled = false;
            this.GetComponent<InteractionScript>().enabled = false;
        }

        this.oobObject = Instantiate(this.OwnerObjectBind) as Bind;
        this.oobObject.node1 = this.transform;
        this.oobObject.node2 = GameObject.Find("Op" + owner).transform;
        this.oobObject.MissingBind();
    }

    #endregion

    void BackToBase()
    {
        if (this.isNotified)
        {
            this.plane.GetComponent<Renderer>().enabled = false;

            if (this.oobObject != null)
            {
                Destroy(this.oobObject.gameObject);
                this.oobObject = null;
            }

            if (this.hasDisappeared)
            {
                this.hasDisappeared = false;
                this.gameObject.SetActive(false);
            }

            if (this.ToDestroy)
            {
                Destroy(this.gameObject);
            }
        }
    }
   
    [Button]
    public void OnBecomeIn()
    {
        this.transform.GetChild(0).GetComponent<Renderer>().enabled = false;
    }

    [Button]
    public void BecomeForeign()
    {
        this.SetForeign("241");
    }

    [Button]
    public void BecomePublic()
    {
        GameObject a = new GameObject
        {
            name = "Op241"
        };
        this.SetPublic("241");
    }

    [Button]
    public void GraphicalStatus()
    {
        Debug.Log("F : " + this.focus + " S : " + this.selection + " memOwn " + this.memOwner);
    }
}
