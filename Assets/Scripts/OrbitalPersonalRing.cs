﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System;
using Synchro;

public class OrbitalPersonalRing : Singleton<OrbitalPersonalRing>
{
    protected List<OrbitalPersonal> personalCollection = new List<OrbitalPersonal>();
    protected GameObject head;

    [BoxGroup("Belt Settings")] public float definedAngularSize;
    [BoxGroup("Belt Settings")] public float downDelta;
    [BoxGroup("Belt Settings")] public float distanceToUser = 1f;

    [BoxGroup("Window Layout Settings")] public float lineSize;
    [BoxGroup("Window Layout Settings")] public float heightSize;
    [BoxGroup("Window Layout Settings")] public float stepX;
    [BoxGroup("Window Layout Settings")] public float stepY;

    public virtual PieMenuAdvancedClickSurfaceInteraction PersonalWindow { get; set; }
    protected float deltaAngle = 0f;

    private void Start()
    {
        this.head = Camera.main.gameObject;
    }

    public virtual void AddToCollection(OrbitalPersonal item)
    {        
        item.enabled = true;
        this.personalCollection.Add(item);
        RepositionItems(this.deltaAngle);
    }

    public virtual void AddToCollection(OrbitalPersonal item, Vector3 pos)
    {
        item.enabled = true;
        this.personalCollection.Add(item);
        RepositionItems(this.deltaAngle);
    }

    public virtual void RemoveCollection(OrbitalPersonal item)
    {
        item.enabled = false;
        this.personalCollection.Remove(item);
        RepositionItems(this.deltaAngle);
    }

    protected virtual void RepositionItems(float deltaAngle, float startAngle = 0)
    {
        List<Tuple<float,Vector3>> newPos = SurfaceCalculation(this.personalCollection.Count, this.definedAngularSize, deltaAngle, startAngle);

        for (int i = 0; i < this.personalCollection.Count; i++)
        {
            this.personalCollection[i].localAngle = newPos[i].Item1;
            this.personalCollection[i].deltaPosition = newPos[i].Item2 * this.distanceToUser;
            this.personalCollection[i].downDelta = this.downDelta;
        }
    }

    // Surface Calculation to sort cards from left to right
    protected List<Tuple<float, Vector3>> SurfaceCalculationReadingOrder(int itemNumber, float itemAngularSize, float deltaAngleFixed)
    {
        Vector3 headPos = this.head.transform.position;
        Vector3 front = Vector3.forward;
        
        float startAngle = - ((itemNumber - 1) * itemAngularSize) / 2f + deltaAngleFixed;
        List<Tuple<float,Vector3>> onSurfacePlacements = new List<Tuple<float,Vector3>>();       

        for (int i = 0; i < itemNumber; i++)
        {
            Vector3 angle = new Vector3(Mathf.Sin(Mathf.Deg2Rad * (startAngle + i * itemAngularSize)), 0f, Mathf.Cos(Mathf.Deg2Rad * (startAngle + i * itemAngularSize)));

            float localAngle = i * itemAngularSize - (itemNumber - 1) * itemAngularSize / 2f + deltaAngleFixed;
            onSurfacePlacements.Add(new Tuple<float, Vector3>(localAngle, angle));
        }

        return onSurfacePlacements;
    }

    // Surface calculation with last card taken as center, other cards pushed left and right evenly
    protected List<Tuple<float, Vector3>> SurfaceCalculation(int itemNumber, float itemAngularSize, float deltaAngleFixed, float startAngle)
    {
        Vector3 front = Vector3.forward;
        
        List<Tuple<float, Vector3>> onSurfacePlacements = new List<Tuple<float, Vector3>>();

        for (int i = 0; i < itemNumber; i++)
        {
            float angle = (i % 2 == 0) ?
                Mathf.Deg2Rad * (startAngle - Mathf.CeilToInt((itemNumber - i) / 2) * itemAngularSize) :
                Mathf.Deg2Rad * (startAngle + Mathf.CeilToInt((itemNumber - i) / 2) * itemAngularSize);

            Vector3 anglePosition = new Vector3(Mathf.Sin(angle), 0f, Mathf.Cos(angle));

            float localAngle = (i % 2 == 0) ?
                startAngle - Mathf.CeilToInt((itemNumber - i) / 2) * itemAngularSize :
                startAngle + Mathf.CeilToInt((itemNumber - i) / 2) * itemAngularSize;

            onSurfacePlacements.Add(new Tuple<float, Vector3>(localAngle, anglePosition));
        }

        return onSurfacePlacements;
    }

    protected virtual void ResetPosition()
    {

    }

    public virtual void BatchMove()
    { }

    public virtual void Expand()
    { }

    [Button]
    public void Repos()
    {
        RepositionItems(this.deltaAngle);
    }
    
    [Button]
    public void TestButton()
    {
        this.head = Camera.main.gameObject;
        int itemNumber = 5;
        float itemAngularSize = 20;
        Vector3 headPos = this.head.transform.position;
        Vector3 front = Vector3.ProjectOnPlane(this.head.transform.forward, Vector3.up).normalized;

        float startAngleReal = Mathf.Acos(front.x) * Mathf.Rad2Deg;
        float startAngle = startAngleReal - ( (itemNumber-1) * itemAngularSize) / 2f;
        Debug.Log(itemNumber * itemAngularSize);
        List<Vector3> onSurfacePlacements = new List<Vector3>();

        for (int i = 0; i < itemNumber; i++)
        {
            Vector3 angle = new Vector3( - Mathf.Cos(Mathf.Deg2Rad * (startAngle + i*itemAngularSize)), 0, Mathf.Sin(Mathf.Deg2Rad * (startAngle + i * itemAngularSize)));
            onSurfacePlacements.Add(angle);
            Debug.Log(angle + " " + (startAngle + i * itemAngularSize) + " " + (i*itemAngularSize - (itemNumber - 1)*itemAngularSize/2f ) + " " + startAngleReal + " " + front);
            GameObject.CreatePrimitive(PrimitiveType.Sphere).transform.position = angle;
        }
    }

    public virtual void ResetState()
    {
        int i = 0;
        for (i = this.personalCollection.Count - 1; i >= 0; i--)
        {
            RemoveCollection(personalCollection[i]);
        }
    }
}
