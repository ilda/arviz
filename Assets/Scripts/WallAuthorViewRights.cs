﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallAuthorViewRights : AuthorViewRights
{
    protected override void Awake()
    {
        base.Awake();

        viewPermissions.SwitchPublic();
    }

    #region ChangePermissions
    public override void MakeForeign()
    {
        throw new System.NotImplementedException();
    }

    public override void MakePrivate()
    {
        throw new System.NotImplementedException();
    }

    public override void MakePublic()
    {
        throw new System.NotImplementedException();
    }

    public override void MakeShared(List<int> owners)
    {
        throw new System.NotImplementedException();
    }

    public override void RemoteMakeForeign()
    {
        throw new System.NotImplementedException();
    }

    public override void RemoteMakePrivate()
    {
        throw new System.NotImplementedException();
    }

    public override void RemoteMakePublic()
    {
        throw new System.NotImplementedException();
    }

    public override void RemoteMakeShared(List<int> owners)
    {
        throw new System.NotImplementedException();
    }    

    public override void SlatePermissionDisplay(bool value)
    {
        throw new System.NotImplementedException();
    }
    #endregion
}
