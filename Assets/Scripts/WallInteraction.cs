﻿using Synchro;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WallInteraction : MonoBehaviour
{

    public float w_step;
    public float h_step;

    // Start is called before the first frame update
    void Start()
    {
        if(this.w_step == 0)
            this.w_step = OrbitalPersonalRing.Instance.stepX;
        if(this.h_step == 0)
            this.h_step = OrbitalPersonalRing.Instance.stepY;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Relayout_Wall()
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
            Measurement.Instance.RelayoutPlus();

        // Get all cards inside the window
        List<Transform> cards = new List<Transform>();
        for (int i = 0; i < this.transform.childCount; i++)
        {
            if (this.transform.GetChild(i).name.Contains("ReLayout"))
                continue;

            cards.Add(this.transform.GetChild(i));
        }

        float w_window = GetComponent<BoxCollider>().size.x;
        float h_window = GetComponent<BoxCollider>().size.y;        
        Vector3 baseOffset = new Vector3(w_window / 2f, h_window / 2f, 0f);

        int w_grid_count = Mathf.FloorToInt(w_window / this.w_step);
        int h_grid_count = Mathf.FloorToInt(h_window / this.h_step);
        float w_grid = w_window / w_grid_count;
        float h_grid = h_window / h_grid_count;

        List<List<Transform>> grid = Enumerable.Repeat<List<Transform>>(null, h_grid_count).ToList();
        // Create a grid to welcome the cards in fixed places
        // This grid has a fixed height, and variable width
        // Calculate grid capacity to resize window if the capacity is too small

        int w_index = 0;
        int h_index = 0;
        List<Transform> positioning = new List<Transform>();

        for (h_index = 0; h_index < h_grid_count; h_index++)
        {
            grid[h_index] = new List<Transform>();
            List<Transform> line = new List<Transform>();

            // Get every elements fitting a line in the grid
            for (int i = cards.Count - 1; i >= 0; i--)
            {
                Vector3 testPos = cards[i].localPosition + baseOffset;
                if ((h_grid_count - h_index) * h_grid > testPos.y && testPos.y > (h_grid_count - h_index - 1) * h_grid)
                {
                    line.Add(cards[i]);
                    cards.RemoveAt(i);
                }
            }

            // Fit the max width of each line to find out the max, and sort each line
            w_grid_count = Mathf.Max(w_grid_count, grid[h_index].Count);
            positioning.AddRange(line.OrderBy(o => o.localPosition.x).ToList());
        }
        h_index--;


        // If some element are outside, fit them in the last line
        if (cards.Count > 0)
            positioning.AddRange(cards.OrderBy(o => o.localPosition.x).ToList());


        // Determine every cards right position
        List<List<Vector3>> gridPositions = Enumerable.Repeat<List<Vector3>>(null, h_grid_count).ToList();
        int maxSize = 0;
        for (h_index = 0; h_index < h_grid_count; h_index++)
        {
            w_index = 0;
            gridPositions[h_index] = new List<Vector3>();

            while (positioning.Count > 0 && w_index < w_grid_count)
            {
                float xPos = (2 * w_index + 1) * w_grid / 2f;
                float yPos = (2 * h_index + 1) * h_grid / 2f;

                grid[h_index].Add(positioning[0]);
                gridPositions[h_index].Add(new Vector3(xPos, -yPos, 0f));
                positioning.RemoveAt(0);

                w_index++;
            }
        }
        maxSize = w_grid_count;
        // Resize the window        
        baseOffset = new Vector3(baseOffset.x, -baseOffset.y, baseOffset.z);

        for (h_index = 0; h_index < h_grid_count; h_index++)
        {
            for (w_index = 0; w_index < grid[h_index].Count; w_index++)
            {
                grid[h_index][w_index].localPosition = gridPositions[h_index][w_index] - baseOffset;
                grid[h_index][w_index].hasChanged = true;
            }
            grid[h_index].Clear();
        }
        cards.Clear();
        grid.Clear();
    }


}
