﻿using NaughtyAttributes;
using Synchro;
using System.Collections.Generic;
using UnityEngine;


public abstract class AuthorViewRights : MonoBehaviour
{
    public Permissions viewPermissions;
    protected ChangePermission changePermission;
    public bool isGenerated = false;

    #region MonoBehaviour Methods
    protected virtual void Awake()
    {
        this.viewPermissions = new Permissions();
        this.changePermission = new ChangePermission
        {
            owner = SynchroManager.Instance.ownerId
        };
    }
    #endregion

    #region Helpers Methods
    public virtual void AnticipateStart() { }

    public abstract void SlatePermissionDisplay(bool value);

    [Button]
    public void ViewPermissions()
    {
        if (this.viewPermissions.PermissionState() == PrivacyState.Shared)
        {
            string s = "";
            foreach (int i in this.viewPermissions.GetCollaborators())
                s += i.ToString() + " - ";
            Debug.Log(this.viewPermissions.PermissionState() + " " + s);
        }
        else
            Debug.Log(this.viewPermissions.PermissionState());
    }

    #endregion

    public virtual void HasFocus()
    {

    }
    public virtual void LoseFocus()
    {

    }

    #region Authoring Methods
    public virtual void GetAuthoring()
    {        
        if (!SynchroManager.Instance.Lock(this.name, SynchroManager.Instance.ownerId))
        {
            //throw new System.AccessViolationException();
        }
    }
    public virtual void GetAuthoring(bool selectionState)
    {
        if (!SynchroManager.Instance.Lock(this.name, SynchroManager.Instance.ownerId))
        {
            //throw new System.AccessViolationException();
        }
    }
    public virtual void RemoteGetAuthoring(int owner, bool selectionState)
    {
        if(!SynchroManager.Instance.Lock(this.name, owner.ToString()))
        {
            //throw new System.AccessViolationException();
        }
    }
    public virtual void LetAuthoring()
    {
        if (!SynchroManager.Instance.Unlock(this.name))
        {
            //throw new System.AccessViolationException();
        }
    }
    public virtual void LetAuthoring(bool affectSelectionState)
    {
        if (!SynchroManager.Instance.Unlock(this.name))
        {
            //throw new System.AccessViolationException();
        }
    }
    public virtual void RemoteLetAuthoring(int owner, bool affectSelectionState)
    {
        if (!SynchroManager.Instance.Unlock(this.name, owner.ToString()))
        {
            //throw new System.AccessViolationException();
        }
    }
    #endregion

    #region Privacy Methods
    public abstract void MakePublic();
    public abstract void MakePrivate();
    public abstract void MakeShared(List<int> owners);
    public abstract void MakeForeign();

    public abstract void RemoteMakePublic();
    public abstract void RemoteMakePrivate();
    public abstract void RemoteMakeShared(List<int> owners);
    public abstract void RemoteMakeForeign();
    #endregion
}
