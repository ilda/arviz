﻿using NaughtyAttributes;
using Synchro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionController : Singleton<SelectionController>
{
    private List<PieMenuClickInteraction> selection = new List<PieMenuClickInteraction>();

    AnimatedSelectionCursor cursor;

    // Start is called before the first frame update
    void Start()
    {
        this.cursor = GameObject.Find("CrossSelectionCursor(Clone)")?.GetComponent<AnimatedSelectionCursor>();
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void IsDragging(bool isBusy)
    {
        this.cursor.IsDragged(isBusy);
    }

    public void AddToSelection(GameObject g)
    {
        this.selection.Add(g.GetComponent<PieMenuClickInteraction>());
    }

    public void RemoveFromSelection(GameObject g)
    {
        this.selection.Remove(g.GetComponent<PieMenuClickInteraction>());
    }

    public void MoveSelection(GameObject caller)
    {
        Transform ts = caller.transform.parent;

        float minX = this.selection[0].transform.localPosition.x, 
            minY = this.selection[0].transform.localPosition.y,
            maxX = this.selection[0].transform.localPosition.x,
            maxY = this.selection[0].transform.localPosition.y;


        int childCountInWindow = this.selection.Count;

        for (int i = 0; i < childCountInWindow; i++)
        {
            minX = Mathf.Min(minX, this.selection[i].transform.localPosition.x);
            minY = Mathf.Min(minY, this.selection[i].transform.localPosition.y);
            maxX = Mathf.Max(maxX, this.selection[i].transform.localPosition.x);
            maxY = Mathf.Max(maxY, this.selection[i].transform.localPosition.y);
        }

        Vector3 cursorCentroidAlt = new Vector3((maxX + minX) / 2, (maxY + minY) / 2, 0);

        for (int i = this.selection.Count - 1; i >= 0; i--)
        {
            Transform t = this.selection[i].transform;
            Vector3 v = t.localPosition;
            t.GetComponent<PieMenuClickInteraction>().BatchHeadMove(v - cursorCentroidAlt, childCountInWindow, Mathf.Abs(maxY - minY), Mathf.Abs(maxX - minX), i);
        }
    }

    public void StoreSelectionToPS()
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
            Measurement.Instance.EmptySelectionPlus();

        OrbitalPersonalRing.Instance.gameObject.GetComponent<PersonalSpaceManagement>().EmptySelection(this.selection);
    }

    public void ResetSelection()
    {
        if (this.selection.Count > 0)
        {
            for (int i = this.selection.Count - 1; i >= 0; i--)
            {
                this.selection[i].SelectItem();
            }
        }
    }
}
