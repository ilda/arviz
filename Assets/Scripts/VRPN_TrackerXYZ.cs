﻿using System.Collections;
using System.Collections.Generic;
using UVRPN;
using UnityEngine;
using UVRPN.Core;

public class VRPN_TrackerXYZ : VRPN_Tracker
{
    //private Quaternion rotationReferential = new Quaternion(-0.707107f, 0.0f, 0.0f, 0.707107f);
    public string Tracker
    {
        get { return tracker; }
        set { tracker = value; }
    }


    protected override Vector3 Process(Vector3 input)
    {        
        return new Vector3(-input.x, input.z, -input.y);
    }

    protected override Quaternion Process(Quaternion input)
    {
        return input;// * rotationReferential;
    }
}
