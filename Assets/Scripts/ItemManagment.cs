﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Synchro;
using NaughtyAttributes;


public class ItemManagment : Singleton<ItemManagment>
{
    private Hashtable interactables;

    [ReorderableList]
    public GameObject[] startItems;

    // Start is called before the first frame update
    void Start()
    {
        interactables = new Hashtable();
        for(int i = 0; i < startItems.Length; i++)
        {
            interactables.Add(startItems[i].name, startItems[i]);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddInteractable(GameObject g)
    {
        interactables.Add(g.name, g);
    }

    public void RemoveInteractable(GameObject g)
    {
        interactables.Remove(g.name);
    }
    
    public GameObject GetInteractable(string name)
    {
        return (GameObject)interactables[name];
    }
}
