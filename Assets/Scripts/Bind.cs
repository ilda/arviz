﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bind : MonoBehaviour
{
    public LineRenderer lr;
    public Transform node1;
    public Transform node2;
    public Material newMat;
    public Material missingMat;

    // Start is called before the first frame update
    void Start()
    {        
        lr.positionCount = 2;
        lr.SetPositions(new Vector3[] { node1.position, node2.position });
    }

    // Update is called once per frame
    void Update()
    {
        lr.SetPositions(new Vector3[] { node1.position, node2.position });
    }

    public void NewBind()
    {
        lr.material = newMat;
    }

    public void MissingBind()
    {
        lr.material = missingMat;
    }
}
