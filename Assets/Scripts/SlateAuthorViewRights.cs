﻿using NaughtyAttributes;
using Synchro;

using System.Collections.Generic;
using UnityEngine;

public class SlateAuthorViewRights : AuthorViewRights
{
    private VersioningNotifications vn;
    private Synchro.DeviceType device;
    public bool defaultPublic = false;

    private Material Action0;
    private Material Action1;
    private Material Action2;
    private Material Action3;

    private Material Focus0;
    private Material Focus1;
    private Material Focus2;
    private Material Focus3;

    public TakeObject to = new TakeObject();
    public LetObject lo = new LetObject();

    // Start is called before the first frame update
    protected override void Awake()
    {
        base.Awake();
        this.vn = this.GetComponent<VersioningNotifications>();
        this.device = SynchroManager.Instance.IsDevice();
    }

    protected void Start()
    {
        this.to.owner = SynchroManager.Instance.ownerId;
        this.to.objectName = this.name;
        this.lo.owner = SynchroManager.Instance.ownerId;
        this.lo.objectName = this.name;

        if (this.defaultPublic)
        {
            this.viewPermissions.SwitchPublic();
        }

        int idAction = SynchroManager.Instance.fullOwners.IndexOf(int.Parse(SynchroManager.Instance.ownerId));
        this.Action0 = Resources.Load("Action0") as Material;
        this.Action1 = Resources.Load("Action1") as Material;
        this.Action2 = Resources.Load("Action2") as Material;
        this.Action3 = Resources.Load("Action3") as Material;

        this.Focus0 = Resources.Load("Focus0") as Material;
        this.Focus1 = Resources.Load("Focus1") as Material;
        this.Focus2 = Resources.Load("Focus2") as Material;
        this.Focus3 = Resources.Load("Focus3") as Material;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(this.GetComponentInChildren<Collider>().bounds + " Current bounds " + this.name);
    }


    private void OnDestroy()
    {
        SynchroManager.Instance.RemoveObject(this.transform.GetChild(0).name);
        SynchroManager.Instance.RemoveObject(this.gameObject.name);
    }

    // **Surface : Change item surface and privacy | **SurfaceLite : Change only item privacy
    public void OffSurface(GameObject surface)
    {
        if (surface == null)
        {
            return;
        }
        else if (surface.name == "Wall")
        {
            this.vn.SetStandardVisibility(true, true, true);
        }
        else
        {
            if (surface.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>())
            {
                surface.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().UpdateContent(-1);
                surface.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().Deactivate();
            }
        }

        this.transform.parent = SynchroManager.Instance.transform;
    }
    public void OffSurfaceLite(GameObject surface)
    {
        if (surface == null)
        {
            return;
        }
        else if (surface.name == "Wall")
        {
            this.vn.SetStandardVisibility(true, true, true);
        }
        else
        {
            //Debug.Log(surface.name + " " + surface.GetComponent<PieMenuAdvancedClickSurfaceInteraction>());
            surface.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().UpdateContent(-1);
        }
    }
    public void OnSurface(GameObject surface)
    {
        AuthorViewRights savr = surface.GetComponent<AuthorViewRights>();
        this.ChangePermission(savr);

        this.transform.parent = surface.transform;

        if (surface.name != "Wall" && !surface.name.Contains("Op"))
        {
            surface.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().UpdateContent(0);
            surface.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().Activate();
        }
    }
    public void OnSurfaceLite(GameObject surface)
    {
        AuthorViewRights savr = surface.GetComponent<AuthorViewRights>();
        this.ChangePermission(savr);

        if (surface.name != "Wall" && !surface.name.Contains("Op"))
            surface.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().UpdateContent(0);
    }
    public void SwitchSurface(GameObject oldSurface, GameObject newSurface)
    {
        AuthorViewRights savr = newSurface.GetComponent<AuthorViewRights>();
        this.ChangePermission(savr);

        this.transform.parent = newSurface.transform;

        if (oldSurface.name != "Wall" && !oldSurface.name.Contains("Op"))
        {
            oldSurface.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().UpdateContent(-1);
            oldSurface.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().Deactivate();
        }
        if (newSurface.name != "Wall" && !newSurface.name.Contains("Op"))
        {
            newSurface.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().UpdateContent(0);
            newSurface.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().Activate();
        }
    }
    public void SwitchSurfaceLite(GameObject oldSurface, GameObject newSurface)
    {
        AuthorViewRights savr = newSurface.GetComponent<AuthorViewRights>();
        this.ChangePermission(savr);

        if (oldSurface.name != "Wall")
            oldSurface.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().UpdateContent(-1);
        if (newSurface.name != "Wall")
            newSurface.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().UpdateContent(0);
    }


    public override void GetAuthoring()
    {
        base.GetAuthoring();
        this.to.selectionState = false;
        //SynchroServer.Instance.SendCommand("H", this.to);
        SynchroManager.Instance.CommandInstance.Cmd_TakeObject(to.owner, to.objectName, to.selectionState);
    }
    public override void GetAuthoring(bool selectionState)
    {
        base.GetAuthoring();
        this.to.selectionState = selectionState;
        //SynchroServer.Instance.SendCommand("H", this.to);
        SynchroManager.Instance.CommandInstance.Cmd_TakeObject(to.owner, to.objectName, to.selectionState);
    }
    public override void RemoteGetAuthoring(int owner, bool selectionState)
    {
        base.RemoteGetAuthoring(owner, selectionState);
        int id = SynchroManager.Instance.fullOwners.IndexOf(owner);
        //Debug.Log("color = " + owner);
        //WALL AND MASTER DO NOT HAVE THE SELECTION §§§§§
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
            this.GetComponent<PieMenuClickInteraction>().RemoteSelectItem(selectionState, owner.ToString());
        else
        {
            if (selectionState)
                this.vn.HasSelection(id);
        }
    }
    public override void LetAuthoring()
    {
        //Debug.Log("LetAuthoring");
        base.LetAuthoring();
        this.lo.affectSelectionState = false;
        //SynchroServer.Instance.SendCommand("H", this.lo);
        SynchroManager.Instance.CommandInstance.Cmd_LetObject(lo.owner, lo.objectName, lo.affectSelectionState);
    }
    public override void LetAuthoring(bool affectSelectionState)
    {
        //Debug.Log("LetAuthoring");
        base.LetAuthoring(affectSelectionState);
        this.lo.affectSelectionState = affectSelectionState;
        //SynchroServer.Instance.SendCommand("H", this.lo);
        SynchroManager.Instance.CommandInstance.Cmd_LetObject(lo.owner, lo.objectName, lo.affectSelectionState);
    }
    public override void RemoteLetAuthoring(int owner, bool affectSelectionState)
    {
        base.RemoteLetAuthoring(owner, affectSelectionState);
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS && affectSelectionState)
            this.GetComponent<InteractionScript>().RemoteSelectItem(false, owner.ToString());
        else if(affectSelectionState)
            this.vn.LoseSelection();
    }

    public void ChangePermission(AuthorViewRights savr)
    {
        //Debug.Log(this.name + " " + savr.viewPermissions.PermissionState().ToString());
        switch (savr.viewPermissions.PermissionState())
        {
            case PrivacyState.Foreign:
                MakeForeign();
                break;
            case PrivacyState.Private:
                MakePrivate();
                break;
            case PrivacyState.Public:
                MakePublic();
                break;
            case PrivacyState.Shared:
                MakeShared(savr.viewPermissions.GetCollaborators());
                break;
        }

        this.changePermission.objectName = this.name;
        this.changePermission.owners = new List<int>(savr.viewPermissions.GetCollaborators());
        this.changePermission.permissionState = Permissions.PrivacyConverter(savr.viewPermissions.PermissionState());
        //SynchroServer.Instance.SendCommand("H", this.changePermission);
        SynchroManager.Instance.CommandInstance.Cmd_ChangePermission(changePermission.owner, changePermission.objectName, changePermission.permissionState, changePermission.owners.ToArray());
    }
    
    public override void MakePrivate()
    {
        PrivacyState oldState = this.viewPermissions.PermissionState();

        SynchroManager.Instance.ToOwn(this.name, oldState);
        this.viewPermissions.SwitchPrivate();
        this.vn.SetStandardVisibility(true, false, false);
    }
    public override void MakePublic()
    {
        PrivacyState oldState = this.viewPermissions.PermissionState();

        SynchroManager.Instance.ToShared(this.name, oldState);
        this.viewPermissions.SwitchPublic();
        this.vn.SetWallVisibility();
    }
    public override void MakeShared(List<int> owners)
    {
        PrivacyState oldState = this.viewPermissions.PermissionState();

        SynchroManager.Instance.ToShared(this.name, oldState);
        this.viewPermissions.SwitchShared(owners);

        List<int> fullOwnersList = new List<int>(SynchroManager.Instance.fullOwners);
        List<bool> isIn = new List<bool>();
        for (int i = 0; i < fullOwnersList.Count; i++)
        {
            isIn.Add(owners.Contains(fullOwnersList[i]));
        }

        this.vn.SetStandardVisibility(isIn[0], isIn[1], isIn[2]);
    }
    public override void MakeForeign()
    {
        PrivacyState oldState = this.viewPermissions.PermissionState();

        SynchroManager.Instance.ToForeign(this.name, oldState);
        this.viewPermissions.SwitchForeign();
    }

    public override void RemoteMakePublic()
    {
        PrivacyState oldState = this.viewPermissions.PermissionState();
        SynchroManager.Instance.ToShared(this.name, oldState);

        this.vn.Activate();
        this.vn.SetWallVisibility();
    }
    public override void RemoteMakePrivate()
    {
        PrivacyState oldState = this.viewPermissions.PermissionState();
        SynchroManager.Instance.ToOwn(this.name, oldState);

        this.vn.Activate();
        this.vn.SetStandardVisibility(true, false, false);
    }
    public override void RemoteMakeShared(List<int> owners)
    {
        PrivacyState oldState = this.viewPermissions.PermissionState();
        SynchroManager.Instance.ToShared(this.name, oldState);

        List<int> fullOwnersList = new List<int>(SynchroManager.Instance.fullOwners);
        List<bool> isIn = new List<bool>();

        for (int i = 0; i < fullOwnersList.Count; i++)
        {
            isIn.Add(owners.Contains(fullOwnersList[i]));
        }

        this.viewPermissions.SwitchShared(owners);
        this.vn.Activate();
        this.vn.SetStandardVisibility(isIn[0], isIn[1], isIn[2]);
    }
    public override void RemoteMakeForeign()
    {
        MakeForeign();
        this.vn.Deactivate();
    }

    public override void SlatePermissionDisplay(bool value)
    {
        this.vn.OwnershipDisplay(value);
    }

    public override void HasFocus()
    {
        base.HasFocus();
        this.vn.HasFocus();
    }

    public override void LoseFocus()
    {
        base.LoseFocus();
        this.vn.LoseFocus();       
    }

    [Button]
    public void SelectBy()
    {
        SynchroClient.Instance.TESTOWNE(180, this.name);
    }
}
