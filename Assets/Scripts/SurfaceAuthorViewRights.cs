﻿using Synchro;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Collections;
using Microsoft.MixedReality.Toolkit.UI;

public class SurfaceAuthorViewRights : AuthorViewRights
{
    public TextMesh sharedToA;
    public TextMesh sharedToB;
    public GameObject buttonPanel;

    public GameObject panel;

    public PermissionDisplayMode permissionMode { get; private set; }

    private int AId;
    private int BId;

    public bool isPersonal;

    private SpawnObject so;
    private bool anticipatedStart = false;
    public bool endTransition = false;


    protected override void Awake()
    {
        base.Awake();        
    }

    private void Start()
    {
        if (!this.anticipatedStart)
        {
            int own = int.Parse(SynchroManager.Instance.ownerId);
            List<int> full = SynchroManager.Instance.fullOwners;

            this.so = new SpawnObject();
            this.transform.GetChild(0).name = "Window-" + this.name;

            if (this.isPersonal)
            {
                this.viewPermissions.SwitchPrivate();
                SynchroManager.Instance.AddOwn(this.gameObject);
                SynchroManager.Instance.AddOwn(this.transform.GetChild(0).gameObject);

                if (!this.isGenerated)
                {
                    InstantiateWallOnNetwork();
                }
                this.buttonPanel.GetComponent<InteractableToggleCollection>().SetSelection((this.viewPermissions.PermissionState().Equals(PrivacyState.Private)) ? 0 : 3);
            }
            else
            {
                this.viewPermissions.SwitchShared(full);

                SynchroManager.Instance.AddShared(this.gameObject);
                SynchroManager.Instance.AddShared(this.transform.GetChild(0).gameObject);

                if (!this.isGenerated)
                {
                    InstantiateWallOnNetwork();
                    SharedToAll();
                    GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().StartMove();
                }
            }
        }
    }

    public override void AnticipateStart()
    {
        int own = int.Parse(SynchroManager.Instance.ownerId);
        List<int> full = SynchroManager.Instance.fullOwners;

        this.so = new SpawnObject();
        this.transform.GetChild(0).name = "Window-" + this.name;

        if (this.isPersonal)
        {
            this.viewPermissions.SwitchPrivate();
            SynchroManager.Instance.AddOwn(this.gameObject);
            SynchroManager.Instance.AddOwn(this.transform.GetChild(0).gameObject);

            if (!this.isGenerated)
            {
                InstantiateWallOnNetwork();
            }
            this.buttonPanel.GetComponent<InteractableToggleCollection>().SetSelection((this.viewPermissions.PermissionState().Equals(PrivacyState.Private)) ? 0 : 3);
        }
        else
        {
            this.viewPermissions.SwitchShared(full);

            SynchroManager.Instance.AddShared(this.gameObject);
            SynchroManager.Instance.AddShared(this.transform.GetChild(0).gameObject);

            if (!this.isGenerated)
            {
                InstantiateWallOnNetwork();
                SharedToAll();
                GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().StartMove();
            }
        }

        this.anticipatedStart = true;
    }

    private void OnDestroy()
    {
        WallGenerator.Instance.RemoveWindow(this.gameObject);

        SynchroManager.Instance.RemoveObject(this.panel.name);
        SynchroManager.Instance.RemoveObject(this.gameObject.name);
    }

    public override void MakePublic()
    {
        SynchroManager.Instance.ToShared(this.name, this.viewPermissions.PermissionState());
        this.viewPermissions.SwitchPublic();
        this.gameObject.SetActive(true);
        SetPermission(true);

        GetComponent<Collider>().enabled = true;

        foreach (SlateAuthorViewRights savr in GetComponentsInChildren<SlateAuthorViewRights>())
        {
            savr.ChangePermission(this);
        }

        this.changePermission.objectName = this.name;
        this.changePermission.owners = new List<int>(this.viewPermissions.GetCollaborators());
        this.changePermission.permissionState = Permissions.PrivacyConverter(this.viewPermissions.PermissionState());
        //SynchroServer.Instance.SendCommand("H", this.changePermission);
        SynchroManager.Instance.CommandInstance.Cmd_ChangePermission(changePermission.owner, changePermission.objectName, changePermission.permissionState, changePermission.owners.ToArray());
    }
    public override void MakePrivate()
    {
        SynchroManager.Instance.ToOwn(this.name, this.viewPermissions.PermissionState());
        this.viewPermissions.SwitchPrivate();
        this.gameObject.SetActive(true);

        SetPermission(false);

        GetComponentInChildren<Collider>().enabled = true;

        foreach (SlateAuthorViewRights savr in GetComponentsInChildren<SlateAuthorViewRights>())
        {
            savr.ChangePermission(this);
        }

        this.changePermission.objectName = this.name;
        this.changePermission.owners = new List<int>(this.viewPermissions.GetCollaborators());
        this.changePermission.permissionState = Permissions.PrivacyConverter(this.viewPermissions.PermissionState());
        //SynchroServer.Instance.SendCommand("H", this.changePermission);
        SynchroManager.Instance.CommandInstance.Cmd_ChangePermission(changePermission.owner, changePermission.objectName, changePermission.permissionState, changePermission.owners.ToArray());
    }
    public override void MakeShared(List<int> owners)
    {
        SynchroManager.Instance.ToShared(this.name, this.viewPermissions.PermissionState());
        this.viewPermissions.SetCollaborators(owners);
        this.gameObject.SetActive(true);

        SetPermission(owners);

        foreach (SlateAuthorViewRights savr in GetComponentsInChildren<SlateAuthorViewRights>())
        {
            savr.ChangePermission(this);
        }

        this.changePermission.objectName = this.name;
        this.changePermission.owners = new List<int>(this.viewPermissions.GetCollaborators());
        this.changePermission.permissionState = Permissions.PrivacyConverter(this.viewPermissions.PermissionState());
        //SynchroServer.Instance.SendCommand("H", this.changePermission);
        SynchroManager.Instance.CommandInstance.Cmd_ChangePermission(changePermission.owner, changePermission.objectName, changePermission.permissionState, changePermission.owners.ToArray());
    }
    public override void MakeForeign()
    {
        //Debug.Log("Foreign");
        SynchroManager.Instance.ToForeign(this.name, this.viewPermissions.PermissionState());

        if (SynchroManager.Instance.IsDevice() != Synchro.DeviceType.MASTER || SynchroManager.Instance.IsDevice() != Synchro.DeviceType.OBSERVER)
            this.gameObject.SetActive(false);

        SetPermission(false);

        foreach (SlateAuthorViewRights savr in GetComponentsInChildren<SlateAuthorViewRights>())
        {
            savr.ChangePermission(this);
        }

        this.changePermission.objectName = this.name;
        this.changePermission.owners = new List<int>(this.viewPermissions.GetCollaborators());
        this.changePermission.permissionState = Permissions.PrivacyConverter(this.viewPermissions.PermissionState());
        //SynchroServer.Instance.SendCommand("H", this.changePermission);
        SynchroManager.Instance.CommandInstance.Cmd_ChangePermission(changePermission.owner, changePermission.objectName, changePermission.permissionState, changePermission.owners.ToArray());
    }

    public void ShareToA()
    {
        MakeShared(new List<int>() { int.Parse(SynchroManager.Instance.ownerId), this.AId });
        //buttonPanel.SetActive(false);

        GetComponentInChildren<Collider>().enabled = true;
    }
    public void ShareToB()
    {
        MakeShared(new List<int>() { int.Parse(SynchroManager.Instance.ownerId), this.BId });
        //buttonPanel.SetActive(false);

        GetComponentInChildren<Collider>().enabled = true;
    }
    public void SharedToAll()
    {
        MakeShared(SynchroManager.Instance.fullOwners);
        //buttonPanel.SetActive(false);

        GetComponentInChildren<Collider>().enabled = true;
    }

    public override void RemoteMakePublic()
    {
        SynchroManager.Instance.ToShared(this.name, this.viewPermissions.PermissionState());
        if (SynchroManager.Instance.IsDevice() != Synchro.DeviceType.MASTER)// || SynchroManager.Instance.IsDevice() != Synchro.DeviceType.OBSERVER)
        {
            this.gameObject.SetActive(true);

            foreach (SlateAuthorViewRights savr in GetComponentsInChildren<SlateAuthorViewRights>())
            {
                savr.RemoteMakePublic();
            }
        }
    }
    public override void RemoteMakePrivate()
    {
        SynchroManager.Instance.ToOwn(this.name, this.viewPermissions.PermissionState());
        if (SynchroManager.Instance.IsDevice() != Synchro.DeviceType.MASTER || SynchroManager.Instance.IsDevice() != Synchro.DeviceType.OBSERVER)
        {
            this.gameObject.SetActive(true);

            foreach (SlateAuthorViewRights savr in GetComponentsInChildren<SlateAuthorViewRights>())
            {
                savr.RemoteMakePrivate();
            }
        }
    }
    public override void RemoteMakeShared(List<int> owners)
    {
        SynchroManager.Instance.ToShared(this.name, this.viewPermissions.PermissionState());
        this.viewPermissions.SetCollaborators(owners);
        if (SynchroManager.Instance.IsDevice() != Synchro.DeviceType.MASTER || SynchroManager.Instance.IsDevice() != Synchro.DeviceType.OBSERVER)
        {
            this.gameObject.SetActive(true);

            foreach (SlateAuthorViewRights savr in GetComponentsInChildren<SlateAuthorViewRights>())
            {
                savr.RemoteMakeShared(owners);
            }
        }
        SetPermissionIcons(owners);
    }
    public override void RemoteMakeForeign()
    {
        //Debug.Log("Foreign");
        SynchroManager.Instance.ToForeign(this.name, this.viewPermissions.PermissionState());
        if (SynchroManager.Instance.IsDevice() != Synchro.DeviceType.MASTER || SynchroManager.Instance.IsDevice() != Synchro.DeviceType.OBSERVER)
        {
            this.gameObject.SetActive(false);

            foreach (SlateAuthorViewRights savr in GetComponentsInChildren<SlateAuthorViewRights>())
            {
                savr.RemoteMakeForeign();
            }
        }
    }

    private void InstantiateWallOnNetwork()
    {
        this.so.owner = SynchroManager.Instance.ownerId;
        this.so.name = this.name;
        this.so.parentName = null;
        this.so.prefabName = "FloatingWindowQuad";
        this.so.startPos = this.gameObject.transform.localPosition;
        this.so.startRot = this.gameObject.transform.localRotation;
        this.so.startScale = this.gameObject.transform.localScale;
        this.so.privacy = Permissions.PrivacyConverter(this.viewPermissions.PermissionState());
        this.so.owners = this.viewPermissions.GetCollaborators();

        //SynchroServer.Instance.SendCommand("H", this.so);
        SynchroManager.Instance.CommandInstance.Cmd_SpawnObject(so.owner, so.name, so.prefabName, so.parentName, so.startPos, so.startRot, so.startScale, so.privacy, so.owners.ToArray());
    }

    // Remote change of permission for windows
    public void ChangePermissionDisplay(PermissionDisplayMode newMode)
    {
        this.permissionMode = newMode;
        if (this.viewPermissions.PermissionState() == PrivacyState.Shared && this.viewPermissions.GetCollaborators().Count == 3)
        {
            SetPermission(true);
        }
        else if (this.viewPermissions.PermissionState() == PrivacyState.Private)
        {
            SetPermission(false);
        }
        else if(this.viewPermissions.PermissionState() == PrivacyState.Shared)
        {
            SetPermission(this.viewPermissions.GetCollaborators());
        }
        else
        {
            
        }
    }

    // Set permission display
    private void SetPermission(List<int> ip_list)
    {
        switch (this.permissionMode)
        {
            case PermissionDisplayMode.WindowColorDisplay:
                SetPermissionColors(ip_list);
                break;
            case PermissionDisplayMode.WindowIconDisplay:
                SetPermissionIcons(ip_list);
                break;
            case PermissionDisplayMode.WindowIconFullDisplay:
                SetPermissionIconsFull(ip_list);
                break;
            default:
                break;
        }
    }
    private void SetPermission(bool isPublic)
    {
        switch (this.permissionMode)
        {
            case PermissionDisplayMode.WindowColorDisplay:
                SetPermissionColors(isPublic);
                break;
            case PermissionDisplayMode.WindowIconDisplay:
                SetPermissionIcons(isPublic);
                break;
            case PermissionDisplayMode.WindowIconFullDisplay:
                SetPermissionIconsFull(isPublic);
                break;
            default:
                break;
        }
    }

    // Set permission display for icons 
    private void SetPermissionIcons(List<int> ip_list)
    {
    }
    private void SetPermissionIcons(bool isPublic)
    {
    }

    // Set permission display for icons, every person displayed
    private void SetPermissionIconsFull(List<int> ip_list)
    {
    }
    private void SetPermissionIconsFull(bool isPublic)
    {
    }

    // Set permission display for colors
    private void SetPermissionColors(List<int> ip_list)
    {
    }
    private void SetPermissionColors(bool isPublic)
    {
    }

    // Set all gameObjects of a list at Active : false
    private void DisableGameObjectList(GameObject[] gameObjectList)
    {
        foreach(GameObject g in gameObjectList)
        {
            g.SetActive(false);
        }
    }

    public void OffSurface(GameObject surface)
    {
    }
    public void OnSurface(GameObject surface)
    {
        //Debug.Log(surface.name);
        this.transform.parent = surface.transform;
    }
    public void SwitchSurface(GameObject oldSurface, GameObject newSurface)
    {
        //Debug.Log(newSurface.name);
        this.transform.parent = newSurface.transform;
    }

    [Button]
    public void NetworkMakeForeign()
    {
        RemoteMakeForeign();
    }

    [Button]
    public void Test()
    {
        this.buttonPanel.GetComponent<InteractableToggleCollection>().SetSelection((this.viewPermissions.PermissionState().Equals(PrivacyState.Private)) ? 0 : 3);
    }

    public void Appear()
    {
        Material m = this.panel.GetComponent<Renderer>().material;

        StartCoroutine(ProgressiveAppearance(Time.time, 2.0f, 0f, 1.5f, 2f, 32f, m));
    }

    IEnumerator ProgressiveAppearance(float startTime, float duration, float startRimPower, float endRimPower, float startGlowPower, float endGlowPower, Material m)
    {
        float rimValue;
        float glowValue;

        while (Time.time - startTime < duration)
        {
            rimValue = Mathf.Lerp(startRimPower, endRimPower, (Time.time - startTime) / duration);
            glowValue = Mathf.Lerp(startGlowPower, endGlowPower, (Time.time - startTime) / duration);

            m.SetFloat(Shader.PropertyToID("_RimPower"), rimValue);
            m.SetFloat(Shader.PropertyToID("_InnerGlowPower"), glowValue);

            yield return new WaitForEndOfFrame();
        }

        m.SetFloat(Shader.PropertyToID("_RimPower"), endRimPower);
        m.SetFloat(Shader.PropertyToID("_InnerGlowPower"), endGlowPower);

        this.endTransition = true;
    }

    public override void SlatePermissionDisplay(bool value)
    {
        throw new System.NotImplementedException();
    }
}
