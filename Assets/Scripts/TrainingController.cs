﻿using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using NaughtyAttributes;
using Synchro;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class TrainingController : MonoBehaviour, IMixedRealityPointerHandler
{

    public GameObject slate;
    public GameObject imageSlate;
    public int slateNumber;

    [BoxGroup("Material Parameters")] public Material isMissing;
    [BoxGroup("Material Parameters")] public Material isNew;

    [BoxGroup("Material Parameters")] public Material basic;
    [BoxGroup("Material Parameters")] public Material invisible;

    [BoxGroup("Content Parameters")] public TextAsset textAsset;
    [BoxGroup("Content Parameters")] public TextAsset imageAsset;

    [BoxGroup("Training Resources")] public ToolTip tooltip;
    [BoxGroup("Training Resources")] public GameObject HighLightCard;
    [BoxGroup("Training Resources")] public GameObject HighLightSurface;
    [BoxGroup("Training Resources")] public Canvas PieMenuOptions;

    GameObject wall;
    bool isHololens;
    string[] nameList;

    List<Vector3> posImages = new List<Vector3>()
    {
        new Vector3(-0.974f, -0.330f, 0.000f),
        new Vector3(-2.278f, 0.074f, 0.000f),
        new Vector3(1.400f, 0.601f, 0.000f),
        new Vector3(-2.262f, -0.476f, 0.000f),
        new Vector3(0.851f, 0.617f, 0.000f),
        new Vector3(-0.064f, -0.599f, 0.000f),
        new Vector3(0.399f, -0.560f, 0.000f),
        new Vector3(2.225f, 0.003f, 0.000f),
    };

    [BoxGroup("Slate Parameters")] public bool icons;
    [BoxGroup("Slate Parameters")] public Material MoveCard;
    [BoxGroup("Slate Parameters")] public Material MoveCardToPersonal;
    [BoxGroup("Slate Parameters")] public Material MovePersonalCardsToWindow;
    [BoxGroup("Slate Parameters")] public Material MoveWindowCardsToPersonal;
    [BoxGroup("Slate Parameters")] public Material ExpandPersonalCardsAsWindow;
    [BoxGroup("Slate Parameters")] public Material MoveWindowCards;

    private List<GameObject> slatesG = new List<GameObject>();

    private State state = State.ExplainMoveCard;

    string explainMove1 = "This is a card";
    string explainMove2 = "To move, keep pressed while the card is selected, and move your head";
    string explainMove3 = "Try to move it there";

    string explainOptions1 = "By clicking on the card, you make a menu appear";    

    string explainPersonalSpace = "Here is your Personal Space, you can add documents to it, and its content is private, not seen by the collaborators";

    string explainCreateSurface = "Click this to create a surface.";
    string explainSurface = "A surface is placable at start by looking where you want to place it and click it.";
    string explainSurface2 = "A surface is a space where you can move cards, try to move this card on it";

    string explainBatchMovePS = "You can move the content of your Personal Space one at a time, or all at once with this option";
    string explainRelayout = "By clicking this button, you layout the content of the surface to organize it (exist for the wall)";
    string explainEmpty = "In a card options when in a surface, you can move every cards on suface to your personal space.";
    string explainExpand = "You can also move every card from your personal space to a new placable surface surface.";
    string explainBatchMove = "To finish, you can also move every cards from a surface to any other surfaces using this";
    string explainEnd = "Place those cards on the wall, and click to finish these explanations";

    ToolTip toolTipInstance;
    GameObject cardTarget;
    GameObject VirtualWall;
    Canvas pieMenuOptions;
    GameObject surfaceTarget;

    bool hasClicked = false;
    bool hasBatchMove = false;
    bool once = false;
    UnityAction ua;    

    protected enum State
    {
        ExplainMoveCard = 0,
        MoveCard = 1,
        OpenCardOptions = 2,
        ExplainCardOptions = 3,
        ExplainPS = 4,
        CreateSurface = 5,
        MoveToSurface = 6,
        BatchMovePersonalSpace = 7,
        Relayout = 8,
        Empty = 9,
        Expand = 10,
        BatchMoveSurfaceToWall = 11,
        EndOfTraining = 12
    };

    // Start is called before the first frame update
    void Start()
    {        
        int instanciatedSlates = 0;
        bool useViconFeed = SynchroManager.Instance.useVicon;
        bool isWall = SynchroManager.Instance.IsDevice() == Synchro.DeviceType.WALL;
        this.isHololens = SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS;

        this.wall = this.transform.Find("Wall").gameObject;
        this.VirtualWall = GameObject.Find("VirtualWall1");
        
        for(int i=0; i< this.slateNumber; i++)
        {            
            InstantiateBlankSlate(this.explainMove1, this.posImages[i], instanciatedSlates);
            instanciatedSlates++;
        }

        this.toolTipInstance = Instantiate(this.tooltip);
        this.toolTipInstance.ToolTipText = this.explainMove2;
        this.toolTipInstance.Anchor = this.slatesG[0];
        this.toolTipInstance.transform.position = this.slatesG[0].transform.position + new Vector3(0.6f, 0.3f, -0.1f);

        this.cardTarget = Instantiate(this.HighLightCard, SynchroManager.Instance.Wall.transform);
        this.cardTarget.transform.localPosition = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
        {
            UpdateStateMachine();
        }
    }

    void UpdateStateMachine()
    {
        switch (this.state)
        {
            case State.ExplainMoveCard:
                if (this.slatesG[0].transform.hasChanged)
                {
                    this.state = State.MoveCard;
                    this.toolTipInstance.Anchor = this.cardTarget;
                    this.toolTipInstance.ToolTipText = this.explainMove3;
                }
                break;
            case State.MoveCard:
                if (this.cardTarget.GetComponentInChildren<MeshRenderer>().bounds.Contains(this.slatesG[0].transform.position) && !this.slatesG[0].GetComponent<PieMenuClickInteraction>().IsMoving())
                {
                    this.state = State.OpenCardOptions;
                    this.toolTipInstance.Anchor = this.slatesG[0];
                    this.toolTipInstance.ToolTipText = this.explainOptions1;
                    Destroy(this.cardTarget);
                }
                break;
            case State.OpenCardOptions:
                if (this.slatesG[0].GetComponent<OrbitalPersonal>().enabled)
                {
                    this.state = State.ExplainPS;
                    this.toolTipInstance.ToolTipText = this.explainPersonalSpace + " Click me to continue";
                    CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
                }

                if (PieMenu.Instance.Exist())
                {
                    this.state = State.ExplainCardOptions;
                    this.pieMenuOptions = Instantiate(this.PieMenuOptions, this.slatesG[0].transform.position + new Vector3(0f,0f,-0.2f), this.slatesG[0].transform.rotation);
                    this.pieMenuOptions.transform.localScale = Vector3.one * 0.005F;
                }
                break;
            case State.ExplainCardOptions:                
                if (!PieMenu.Instance.Exist())
                {
                    this.toolTipInstance.Anchor = this.slatesG[0];

                    Destroy(this.pieMenuOptions.gameObject);
                    this.state = State.OpenCardOptions;
                } else
                {
                    this.toolTipInstance.Anchor = PieMenu.Instance.pieMenuInstanceManagement.buttons[3].gameObject;                    
                }
                break;
            case State.ExplainPS:
                this.toolTipInstance.PivotPosition = this.slatesG[0].transform.position + new Vector3(0f, 0.5f, 0f);
                if (this.hasClicked)
                {
                    CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);
                    this.state = State.CreateSurface;
                    this.toolTipInstance.ToolTipText = this.explainCreateSurface;
                    this.toolTipInstance.Anchor = WallGenerator.Instance.gameObject;
                    this.once = false;
                    this.hasClicked = false;
                }
                break;
            case State.CreateSurface:
                if(WallGenerator.Instance.CountWallCreated >= 2)
                {
                    this.toolTipInstance.PivotPosition = Camera.main.transform.position + Camera.main.transform.forward * 1f - Vector3.up * 0.1f;
                    this.toolTipInstance.ToolTipText = this.explainSurface;
                    if (!this.once)
                    {
                        this.once = true;
                        this.toolTipInstance.Anchor = WallGenerator.Instance.FloatingWindows[0];
                        CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
                    }
                }

                if (this.hasClicked)
                {
                    CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);
                    this.once = false;
                    this.state = State.MoveToSurface;                    
                }
                break;
            case State.MoveToSurface:
                this.toolTipInstance.Anchor = this.slatesG[1];
                this.toolTipInstance.ToolTipText = this.explainSurface2;
                if (this.slatesG[1].transform.parent == WallGenerator.Instance.FloatingWindows[0].transform)
                {
                    this.state = State.BatchMovePersonalSpace;
                    PieMenu.Instance.OnAction2 += ValidateBatchMovePS;
                    this.toolTipInstance.Anchor = this.slatesG[0];
                    this.toolTipInstance.PivotPosition = this.slatesG[0].transform.position + new Vector3(0f, 0.5f, 0f);
                    this.toolTipInstance.ToolTipText = this.explainBatchMovePS;
                }
                break;
            case State.BatchMovePersonalSpace:
                this.toolTipInstance.PivotPosition = this.slatesG[0].transform.position + new Vector3(0f, 0.5f, 0f);
                if (PieMenu.Instance.Exist())
                {
                    this.toolTipInstance.Anchor = PieMenu.Instance.pieMenuInstanceManagement.buttons[1].gameObject;
                }
                else if (this.hasBatchMove)
                {
                    this.toolTipInstance.PivotPosition = this.slatesG[0].transform.position + Vector3.up * 0.3f;
                    this.toolTipInstance.Anchor = WallGenerator.Instance.FloatingWindows[0];
                }
                else
                {
                    this.toolTipInstance.Anchor = this.slatesG[0].gameObject;
                }

                if (this.hasBatchMove && this.slatesG[0].transform.parent == WallGenerator.Instance.FloatingWindows[0].transform)
                {
                    this.ua = new UnityAction(() => ValidateRelayout());
                    this.toolTipInstance.Anchor = WallGenerator.Instance.FloatingWindows[0].transform.GetChild(0).GetChild(1).Find("ReLayout").gameObject;
                    Interactable interactable = WallGenerator.Instance.FloatingWindows[0].transform.GetChild(0).GetChild(1).Find("ReLayout").GetComponent<Interactable>();
                    interactable.OnClick.AddListener(this.ua);
                    this.toolTipInstance.ToolTipText = this.explainRelayout;
                    this.state = State.Relayout;
                }                    
                break;
            case State.Relayout:
                break;
            case State.Empty:
                this.toolTipInstance.Anchor = PieMenu.Instance.Exist() 
                    ? PieMenu.Instance.pieMenuInstanceManagement.buttons[2].gameObject 
                    : this.slatesG[0];

                this.toolTipInstance.PivotPosition = this.slatesG[0].transform.position + new Vector3(0f, 0.5f, 0f) - WallGenerator.Instance.FloatingWindows[0].transform.forward * 0.3f;
                break;
            case State.Expand:
                this.toolTipInstance.PivotPosition = this.slatesG[0].transform.position + new Vector3(0f, 0.5f, 0f);
                break;
            case State.BatchMoveSurfaceToWall:
                this.toolTipInstance.PivotPosition = this.slatesG[0].transform.position + new Vector3(0f, 0.5f, 0f) - WallGenerator.Instance.FloatingWindows[0].transform.forward * 0.3f;
                if (PieMenu.Instance.Exist())
                    this.toolTipInstance.Anchor = PieMenu.Instance.pieMenuInstanceManagement.buttons[1].gameObject;
                else
                {
                    this.toolTipInstance.Anchor = this.slatesG[0];
                    this.toolTipInstance.PivotPosition = this.slatesG[0].transform.position + new Vector3(0f, 0.5f, 0f) - WallGenerator.Instance.FloatingWindows[0].transform.forward * 0.3f;
                }
                break;
            case State.EndOfTraining:
                this.toolTipInstance.Anchor = this.slatesG[0];                

                if (this.once)
                {
                    CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
                    this.toolTipInstance.ToolTipText = this.explainEnd;
                } else
                {
                    this.toolTipInstance.PivotPosition = Camera.main.transform.position + Camera.main.transform.forward * 1f - Vector3.up * 0.25f;
                }


                if(this.hasClicked)
                    EndTraining();
                break;
            default:
                break;
        }
    }

    [Button]
    void GeneratePosition()
    {
        string tot = "";
        foreach (GameObject g in this.slatesG)
            tot += g.name + g.transform.position.ToString("F3") + '\n';

        Debug.Log(tot);
    }

    string[] ParseTextAsset(TextAsset ta)
    {
        return ta.text.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
    }

    string ParseFile(TextAsset ta, out int sumLines, bool verbose = false)
    {
        //line count done for TMP 34 font size
        sumLines = 0;
        string text = ta.text;
        string parsedText = "";
        if (text == "")
            Debug.Log(ta.name);
        string[] processingStep = text.Replace('\r', '\n').Split('\n');

        foreach (string s in processingStep)
        {
            if (verbose) Debug.Log("#" + s + "#");
            if (s != "")
            {
                sumLines += (int)(s.Length / 63);
                sumLines++;
                parsedText += s + '\n';
            }
        }

        parsedText = parsedText.Replace("Alderwood", "<color=#ff0000><b>Alderwood</b></color>");

        return parsedText;
    }

    Vector3 ParseVector3(string vec3)
    {
        string[] tmp = vec3.Replace("(", string.Empty).Replace(")", string.Empty).Replace("f", string.Empty).Split(',');
        return new Vector3(float.Parse(tmp[0]), float.Parse(tmp[1]), float.Parse(tmp[2]));
    }

    bool InstantiateBlankSlate(string content, Vector3 position, int slateInstanceNumber)
    {        
        GameObject s = Instantiate(this.slate, this.transform);

        this.slatesG.Add(s);
        s.transform.localPosition = position;

        s.transform.localRotation = Quaternion.identity;
        s.name = "Slate" + slateInstanceNumber;
        s.GetComponent<BoundingBox>().Target.name = "Plane" + slateInstanceNumber;

        VersioningNotifications v = s.GetComponent<VersioningNotifications>();
        v.notifMissing = this.isMissing;
        v.notifNew = this.isNew;
        v.basic = this.basic;
        v.invisible = this.invisible;
        v.isAttracted = true;
        v.isOnWall = true;

        PieMenuClickInteraction p = s.GetComponent<PieMenuClickInteraction>();
        p.icons = this.icons;
        p.MoveCard = this.MoveCard;
        p.MoveWindowCards = this.MoveWindowCards;
        p.MoveCardToPersonal = this.MoveCardToPersonal;
        p.MovePersonalCardsToWindow = this.MovePersonalCardsToWindow;
        p.MoveWindowCardsToPersonal = this.MoveWindowCardsToPersonal;
        p.ExpandPersonalCardsToWindow = this.ExpandPersonalCardsAsWindow;
        
        TextMeshPro textItem = s.transform.GetChild(0).Find("Content").GetComponent<TextMeshPro>();
        textItem.text = content;
        textItem.fontSize = 34;        

        s.transform.parent = this.wall.transform;

        s.GetComponent<SlateAuthorViewRights>().defaultPublic = true;

        if (this.isHololens)
        {
            s.GetComponent<InteractionScript>().Magnet = this.wall;
            s.GetComponent<InteractionScript>().isAttracted = true;
            s.transform.GetChild(0).GetComponent<Renderer>().enabled = false;
            s.GetComponent<InteractionScript>().isAttracted = true;
            v.SetPublicSelf();
        }
        else
        {
            Destroy(s.GetComponent<InteractionScript>());
        }
        SynchroManager.Instance.AddShared(s.GetComponent<BoundingBox>().Target);
        SynchroManager.Instance.AddShared(s);

        s.transform.hasChanged = false;

        return true;
    }

    bool InstantiateSlate(string obj, int slateInstanceNumber)
    {
        string[] linkAndPos = obj.Split(';');
        TextAsset ta = Resources.Load(linkAndPos[0]) as TextAsset;
        GameObject s = Instantiate(this.slate, this.transform);

        this.slatesG.Add(s);
        s.transform.localPosition = ParseVector3(linkAndPos[1]);

        s.transform.localRotation = Quaternion.identity;
        s.name = "Slate" + slateInstanceNumber;
        s.GetComponent<BoundingBox>().Target.name = "Plane" + slateInstanceNumber;

        VersioningNotifications v = s.GetComponent<VersioningNotifications>();
        v.notifMissing = this.isMissing;
        v.notifNew = this.isNew;
        v.basic = this.basic;
        v.invisible = this.invisible;
        v.isAttracted = true;
        v.isOnWall = true;

        PieMenuClickInteraction p = s.GetComponent<PieMenuClickInteraction>();
        p.icons = this.icons;
        p.MoveCard = this.MoveCard;
        p.MoveCardToPersonal = this.MoveCardToPersonal;
        p.MovePersonalCardsToWindow = this.MovePersonalCardsToWindow;
        p.MoveWindowCardsToPersonal = this.MoveWindowCardsToPersonal;
        p.ExpandPersonalCardsToWindow = this.ExpandPersonalCardsAsWindow;

        int sumLines;
        TextMeshPro textItem = s.transform.GetChild(0).Find("Content").GetComponent<TextMeshPro>();
        textItem.text = ParseFile(ta, out sumLines);
        textItem.fontSize = 34;

        // If more than one page (23 lines) expand
        if (sumLines > 23)
        {
            float factor = sumLines / 23f;
            s.transform.localScale = new Vector3(s.transform.localScale.x, factor * s.transform.localScale.y, s.transform.localScale.z);
            textItem.transform.localScale = new Vector3(textItem.transform.localScale.x, textItem.transform.localScale.y / factor, textItem.transform.localScale.z);
            textItem.GetComponent<RectTransform>().sizeDelta = new Vector2(textItem.GetComponent<RectTransform>().rect.width, textItem.GetComponent<RectTransform>().rect.height * factor);
        }

        s.transform.parent = this.wall.transform;

        s.GetComponent<SlateAuthorViewRights>().defaultPublic = true;

        if (this.isHololens)
        {
            s.GetComponent<InteractionScript>().Magnet = this.wall;
            s.GetComponent<InteractionScript>().isAttracted = true;
            s.transform.GetChild(0).GetComponent<Renderer>().enabled = false;
            s.GetComponent<InteractionScript>().isAttracted = true;
            v.SetPublicSelf();
        }
        else
        {
            Destroy(s.GetComponent<InteractionScript>());
        }
        SynchroManager.Instance.AddShared(s.GetComponent<BoundingBox>().Target);
        SynchroManager.Instance.AddShared(s);

        s.transform.hasChanged = false;

        return true;
    }

    bool InstantiatePrefabImages(GameObject Prefab, int imageInstanceNumber)
    {
        GameObject s = Instantiate(Prefab, this.transform);

        this.slatesG.Add(s);

        s.name = Prefab.name;
        s.transform.localPosition = this.posImages[imageInstanceNumber];

        s.transform.localRotation = Quaternion.identity;

        s.GetComponent<BoundingBox>().Target.name = "Plane" + Prefab.name;

        VersioningNotifications v = s.GetComponent<VersioningNotifications>();
        v.notifMissing = this.isMissing;
        v.notifNew = this.isNew;
        v.basic = this.basic;
        v.invisible = this.invisible;
        v.isAttracted = true;
        v.isOnWall = true;

        PieMenuClickInteraction p = s.GetComponent<PieMenuClickInteraction>();
        p.icons = this.icons;
        p.MoveCard = this.MoveCard;
        p.MoveCardToPersonal = this.MoveCardToPersonal;
        p.MovePersonalCardsToWindow = this.MovePersonalCardsToWindow;
        p.MoveWindowCardsToPersonal = this.MoveWindowCardsToPersonal;
        p.ExpandPersonalCardsToWindow = this.ExpandPersonalCardsAsWindow;


        s.transform.parent = this.wall.transform;

        s.GetComponent<SlateAuthorViewRights>().defaultPublic = true;

        if (this.isHololens)
        {
            s.GetComponent<InteractionScript>().Magnet = this.wall;
            s.GetComponent<InteractionScript>().isAttracted = true;
            s.transform.GetChild(0).GetComponent<Renderer>().enabled = false;
            s.GetComponent<InteractionScript>().isAttracted = true;
            v.SetPublicSelf();
        }
        else
        {
            Destroy(s.GetComponent<InteractionScript>());
        }
        SynchroManager.Instance.AddShared(s.GetComponent<BoundingBox>().Target);
        SynchroManager.Instance.AddShared(s);

        s.transform.hasChanged = false;

        return true;
    }

    bool InstantiateImage(string obj, int slateInstanceNumber)
    {
        string[] linkAndPos = obj.Split(';');
        Sprite ta = Resources.Load<Sprite>(linkAndPos[0]);
        GameObject s = Instantiate(this.imageSlate, this.wall.transform);

        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);

        this.slatesG.Add(s);
        s.transform.localPosition = ParseVector3(linkAndPos[1]);
        s.transform.localRotation = Quaternion.identity;
        s.transform.GetChild(0).localScale = new Vector3(0.095f, 0.095f, 0.1f);
        s.name = "Slate" + slateInstanceNumber;
        s.GetComponent<BoundingBox>().Target.name = "Plane" + slateInstanceNumber;

        // Add sprite and resize box collider depending on renderer bounds
        s.GetComponentInChildren<SpriteRenderer>().sprite = ta;

        BoxCollider bc = s.GetComponentInChildren<BoxCollider>();
        bounds.Encapsulate(ta.bounds);
        bc.size = bounds.size;


        VersioningNotifications v = s.GetComponent<VersioningNotifications>();
        v.notifMissing = this.isMissing;
        v.notifNew = this.isNew;
        v.basic = this.basic;
        v.invisible = this.invisible;
        v.isAttracted = true;
        v.isOnWall = true;


        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
        {
            PieMenuClickInteraction p = s.GetComponent<PieMenuClickInteraction>();
            p.icons = this.icons;
            p.MoveCard = this.MoveCard;
            p.MoveCardToPersonal = this.MoveCardToPersonal;
            p.MovePersonalCardsToWindow = this.MovePersonalCardsToWindow;
            p.MoveWindowCardsToPersonal = this.MoveWindowCardsToPersonal;
            p.ExpandPersonalCardsToWindow = this.ExpandPersonalCardsAsWindow;
            p.MoveWindowCards = this.MoveWindowCards;
        }

        s.GetComponent<SlateAuthorViewRights>().defaultPublic = true;

        if (this.isHololens)
        {
            s.GetComponent<InteractionScript>().Magnet = this.wall;
            s.GetComponent<InteractionScript>().isAttracted = true;
            s.transform.GetChild(0).GetComponent<Renderer>().enabled = false;
            s.GetComponent<InteractionScript>().isAttracted = true;
            v.SetPublicSelf();
        }
        else
        {
            Destroy(s.GetComponent<InteractionScript>());
        }
        SynchroManager.Instance.AddShared(s.GetComponent<BoundingBox>().Target);
        SynchroManager.Instance.AddShared(s);

        s.transform.hasChanged = false;

        return true;
    }

    void EndTraining()
    {
        SceneManager.LoadScene("BaseSetupHL");
    }

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    {
    }

    public void OnPointerDragged(MixedRealityPointerEventData eventData)
    {
    }

    public void OnPointerUp(MixedRealityPointerEventData eventData)
    {
    }

    public void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
        this.hasClicked = true;
    }

    public void ValidateBatchMovePS()
    {
        this.hasBatchMove = true;
        PieMenu.Instance.OnAction2 -= ValidateBatchMovePS;
    }

    public void ValidateRelayout()
    {

        Interactable interactable = WallGenerator.Instance.FloatingWindows[0].transform.GetChild(0).GetChild(1).Find("ReLayout").GetComponent<Interactable>();
        interactable.OnClick.RemoveListener(this.ua);
        this.state = State.Empty;

        PieMenu.Instance.OnAction3 += ValidateEmpty;
        this.toolTipInstance.Anchor = this.slatesG[0];
        this.toolTipInstance.ToolTipText = this.explainEmpty;
    }

    public void ValidateEmpty()
    {
        PieMenu.Instance.OnAction3 -= ValidateEmpty;
        this.state = State.Empty;
        PieMenu.Instance.OnAction3 += ValidateExpand;

        this.toolTipInstance.Anchor = this.slatesG[0];
        this.toolTipInstance.PivotPosition = this.slatesG[0].transform.position + new Vector3(0f, 0.5f, 0f);
        this.toolTipInstance.ToolTipText = this.explainExpand;
    }

    public void ValidateExpand()
    {
        this.state = State.BatchMoveSurfaceToWall;
        this.toolTipInstance.Anchor = this.slatesG[0];
        this.toolTipInstance.PivotPosition = this.slatesG[0].transform.position + new Vector3(0f, 0.5f, 0f);
        this.toolTipInstance.ToolTipText = this.explainBatchMove;
        PieMenu.Instance.OnAction2 += ValidateBatchMove;
    }

    public void ValidateBatchMove()
    {
        PieMenu.Instance.OnAction2 -= ValidateBatchMove;
        this.state = State.EndOfTraining;
        this.hasClicked = false;
        this.once = false;

        this.toolTipInstance.ToolTipText = this.explainEnd;
    }
}