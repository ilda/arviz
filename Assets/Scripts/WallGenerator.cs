﻿using Synchro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PermissionDisplayMode
{
    SlateDisplay,
    WindowIconDisplay,
    WindowIconFullDisplay,
    WindowColorDisplay
}

public class WallGenerator : Singleton<WallGenerator>
{
    public bool isMaster;
    public GameObject PlaceHolder;
    public GameObject FloatingWindowPrefab;
    public PermissionDisplayMode permissionMode;
    private PermissionDisplayMode permissionModeMemory;

    public List<GameObject> FloatingWindows = new List<GameObject>();
    private ChangePermissionDisplay cpd = new ChangePermissionDisplay();

    public int CountWallCreated = 0;

    // Start is called before the first frame update
    void Start()
    {
        this.permissionModeMemory = this.permissionMode;
        this.cpd.owner = SynchroManager.Instance.ownerId;
    }

    // Update is called once per frame
    void Update()
    {
        string str = "";
        foreach(GameObject g in this.FloatingWindows)
        {
            str += g.name + " " + g.transform.localPosition + " " + g.transform.position + '\n';
        }
    }

    public void CreateWall()
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
            Measurement.Instance.WindowCreatedPlus();

        Vector3 position = this.transform.position;
        position = new Vector3(position.x, Camera.main.transform.position.y, position.z);
        Quaternion rotation = Quaternion.Euler(0, this.transform.rotation.eulerAngles.y, 0);

        GameObject g = Instantiate(this.FloatingWindowPrefab, this.transform.position, rotation, this.PlaceHolder.transform);
        //g.transform.GetChild(0).localScale = new Vector3(1.925f, 1.925f, 1f);
        this.FloatingWindows.Add(g);
        
        position -= new Vector3(0f, g.transform.localScale.y*1.5f/2f, 0f);

        g.GetComponent<SurfaceAuthorViewRights>().ChangePermissionDisplay(this.permissionMode);
        g.name = "FloatingWindow" + this.CountWallCreated + "-" + SynchroManager.Instance.ownerId;        

        StartCoroutine(PlaceWindow(g, this.transform.position, position, 0.2f));
        this.CountWallCreated++;
    }

    private void OnValidate()
    {
        if (Application.isPlaying && !this.isMaster)
        {
            if (this.permissionModeMemory == PermissionDisplayMode.SlateDisplay && this.permissionMode != PermissionDisplayMode.SlateDisplay)
                SynchroManager.Instance.SlateToWindowDisplay();
            else if (this.permissionModeMemory != PermissionDisplayMode.SlateDisplay && this.permissionMode == PermissionDisplayMode.SlateDisplay)
                SynchroManager.Instance.WindowToSlateDisplay();


            switch (this.permissionMode)
            {
                case PermissionDisplayMode.SlateDisplay:                    
                    break;
                case PermissionDisplayMode.WindowColorDisplay:
                    PermissionColorMode();
                    break;
                case PermissionDisplayMode.WindowIconDisplay:
                    PermissionIconMode();
                    break;
                case PermissionDisplayMode.WindowIconFullDisplay:
                    PermissionIconFullMode();
                    break;
                default:
                    Debug.LogError("Invalid PermissionDisplayMode value");
                    break;
            }

            this.permissionModeMemory = this.permissionMode;
        }
        else if(Application.isPlaying && this.isMaster)
        {
            this.cpd.mode = (int)this.permissionMode;
            //SynchroServer.Instance.SendCommand("M", this.cpd);
            //SynchroManager.Instance.CommandInstance.Rpc_ChangePermissionDisplay(cpd.owner, cpd.mode);           
        }
    }

    public void RemoteModeChange(int newMode)
    {
        PermissionDisplayMode mode = (PermissionDisplayMode)newMode;

        if (this.permissionModeMemory == PermissionDisplayMode.SlateDisplay && this.permissionMode != PermissionDisplayMode.SlateDisplay)
            SynchroManager.Instance.SlateToWindowDisplay();
        else if (this.permissionModeMemory != PermissionDisplayMode.SlateDisplay && this.permissionMode == PermissionDisplayMode.SlateDisplay)
            SynchroManager.Instance.WindowToSlateDisplay();

        switch (mode)
        {
            case PermissionDisplayMode.SlateDisplay:
                break;
            case PermissionDisplayMode.WindowColorDisplay:
                PermissionColorMode();
                break;
            case PermissionDisplayMode.WindowIconDisplay:
                PermissionIconMode();
                break;
            case PermissionDisplayMode.WindowIconFullDisplay:
                PermissionIconFullMode();
                break;
            default:
                Debug.LogError("Invalid PermissionDisplayMode value");
                break;
        }

        this.permissionModeMemory = mode;
    }

    private void PermissionColorMode()
    {
        foreach (GameObject fw in this.FloatingWindows)
        {
            fw.GetComponent<SurfaceAuthorViewRights>().ChangePermissionDisplay(PermissionDisplayMode.WindowColorDisplay);
        }
    }

    private void PermissionIconMode()
    {
        foreach(GameObject fw in this.FloatingWindows)
        {
            fw.GetComponent<SurfaceAuthorViewRights>().ChangePermissionDisplay(PermissionDisplayMode.WindowIconDisplay);
        }
    }

    private void PermissionIconFullMode()
    {
        foreach (GameObject fw in this.FloatingWindows)
        {
            fw.GetComponent<SurfaceAuthorViewRights>().ChangePermissionDisplay(PermissionDisplayMode.WindowIconFullDisplay);
        }
    }

    IEnumerator PlaceWindow(GameObject window, Vector3 start, Vector3 end, float time)
    {
        float iterationNum = time * 30f;
        for (int i = 0; i < iterationNum; i++)
        {
            window.transform.position = Vector3.Lerp(start, end, i / iterationNum);
            yield return null;
        }
        window.transform.position = end;
        window.GetComponent<SurfaceAuthorViewRights>().Appear();
    }

    public void RemoveWindow(GameObject Window)
    {
        FloatingWindows.Remove(Window);
    }

    public void ResetState()
    {
        this.FloatingWindows.Clear();        
    }
}
