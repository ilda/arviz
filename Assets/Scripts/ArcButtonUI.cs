﻿
using UnityEngine;
using UnityEngine.UI;

public class ArcButtonUI : MaskableGraphic
{
    public float circleSize = 1f;
    public float precision = 10f;
    [Range(1,10)]
    public int segmentNumber;
    public int segmentId;

    [SerializeField]
    Texture m_Texture;
    // make it such that unity will trigger our ui element to redraw whenever we change the texture in the inspector
    public Texture texture
    {
        get
        {
            return m_Texture;
        }
        set
        {
            if (m_Texture == value)
                return;

            m_Texture = value;
            SetVerticesDirty();
            SetMaterialDirty();
        }
    }

    public override Texture mainTexture
    {
        get
        {
            return m_Texture == null ? s_WhiteTexture : m_Texture;
        }
    }

    void AddArc(VertexHelper vh, int precision, int partition, int totalPartition)
    {
        UIVertex vert = new UIVertex();
        vert.color = this.color;


        for (int i = 0; i <= precision; i++)
        {
            int current = vh.currentVertCount;


            float coeff = 2f * Mathf.PI * (partition - 1) / totalPartition;
            float delta = (2f * Mathf.PI / totalPartition ) * (float)i / (float)precision ;

            Vector2 pos = new Vector2(Mathf.Sin(coeff + delta), Mathf.Cos(coeff + delta));
            vert.position = pos * 1f;
            vert.uv0 = pos / 2f + Vector2.one * 0.5f;
            vh.AddVert(vert);

            vert.position = pos * 0.4f;
            vert.uv0 = (pos * 0.4f / 2f + Vector2.one * 0.5f);
            vh.AddVert(vert);

            if (i == 0)
                continue;

            vh.AddTriangle(current - 2, current, current - 1);
            vh.AddTriangle(current - 1, current, current + 1);
        }
    }

    // actually update our mesh
    protected override void OnPopulateMesh(VertexHelper vh)
    {
        // Let's make sure we don't enter infinite loops
        if (circleSize <= 0)
        {
            circleSize = 1f;
            Debug.LogWarning("GridCellSize must be positive number. Setting to 1 to avoid problems.");
        }

        // Clear vertex helper to reset vertices, indices etc.
        vh.Clear();
       
        AddArc(vh, 20, segmentId, segmentNumber);

        Debug.Log("Mesh was redrawn!");
    }

    protected override void OnRectTransformDimensionsChange()
    {
        base.OnRectTransformDimensionsChange();
        SetVerticesDirty();
        SetMaterialDirty();
    }
}
