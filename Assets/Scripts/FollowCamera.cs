﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Transform mainCamera;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (mainCamera)
        {
            this.transform.position = mainCamera.position;
            this.transform.rotation = mainCamera.rotation;
        }
    }
}
