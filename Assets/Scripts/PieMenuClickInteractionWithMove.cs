﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Microsoft.MixedReality.Toolkit;
using Synchro;
using Microsoft.MixedReality.Toolkit.Input.Utilities;
using NaughtyAttributes;

public class PieMenuClickInteractionWithMove : InteractionScript, IMixedRealitySourceStateHandler
{
    private GraphicRaycaster graphicRaycast;
    private EventSystem m_EventSystem;
    private PointerEventData m_PointerEventData;

    private bool activeMenu = false;
    private GameObject sphere;
    private GameObject sphere2;

    private bool waitForAction = false;

    List<RaycastResult> results = new List<RaycastResult>();
    IMixedRealityPointer pointer;

    Transform cameraTr;

    bool moveOrdered = false;
    bool moveOrderedCenterHead = false;
    bool hybridMoveOrdered = false;
    bool isLost = false;
    public bool isPersonal = false;

    Vector3 deltaDistance;
    float itemDistance;
    Vector3 batchOffset;

    public AudioClip PressClip;
    public AudioSource audio;


    public bool icons = false;
    public Material MoveCard;
    public Material MoveCardToPersonal;
    public Material MovePersonalCardsToWindow;
    public Material MoveWindowCardsToPersonal;
    public Material ExpandPersonalCardsToWindow;
    public Material MoveWindowCards;


    protected override void Start()
    {
        base.Start();
        this.changeSurfaceMessage.isWindow = false;
        this.cameraTr = Camera.main.transform;
    }

    protected override void Update()
    {
        base.Update();

        if (this.activeMenu)
        {

        }

        if (this.moveOrdered)
        {
            Vector3 targetPosition;
            Quaternion targetRotation = this.transform.rotation;
            if (this.isLost)
            {
                targetPosition = this.cameraTr.position + (this.gazeProvider.HitPosition - this.cameraTr.position).normalized * 0.6f;
                targetRotation = this.cameraTr.rotation;
            }
            else
            {
                this.deltaDistance = (this.pointer.Position - Camera.main.transform.position).normalized;
                targetPosition = this.pointer.Position + this.deltaDistance.normalized * this.itemDistance;
                targetRotation = Quaternion.Euler(0, this.pointer.Rotation.eulerAngles.y, 0);
            }


            // Apply magnetism effect when getting inside a defined effect area
            if (this.isMagnetic)
            {
                Magnetism(this.max_dist, ref targetPosition, ref targetRotation);
            }

            float lerpAmount = GetLerpAmount();
            Quaternion smoothedRotation = Quaternion.Lerp(this.hostTransform.rotation, targetRotation, lerpAmount);
            Vector3 smoothedPosition = Vector3.Lerp(this.hostTransform.position, targetPosition, lerpAmount);

            //Change SetPositionAndRotation to this :
            this.hostTransform.position = smoothedPosition;
            //hostTransform.rotation = smoothedRotation;

            this.hostTransform.localRotation = Vector3.Dot(this.Magnet.transform.forward, this.gazeProvider.transform.forward) >= 0
                ? Quaternion.identity
                : new Quaternion(0, 1, 0, 0);
        }

        if (this.moveOrderedCenterHead)
        {
            Quaternion targetRotation;
            Vector3 targetPosition;

            targetPosition = this.gazeProvider.HitPosition;
            targetRotation = this.cameraTr.rotation;

            Quaternion surfaceNormal = (this.Magnet != null) ? Quaternion.LookRotation(this.Magnet.transform.forward) : targetRotation;

            // Apply magnetism effect when getting inside a defined effect area
            if (this.isMagnetic)
            {
                Magnetism(this.max_dist, ref targetPosition, ref targetRotation);
            }

            float lerpAmount = GetLerpAmount();
            Quaternion smoothedRotation = Quaternion.Lerp(this.hostTransform.rotation, targetRotation, lerpAmount);
            Vector3 smoothedPosition = Vector3.Lerp(this.hostTransform.position, targetPosition + surfaceNormal * this.batchOffset, lerpAmount);
            this.hostTransform.SetPositionAndRotation(smoothedPosition, smoothedRotation);
        }

        if (this.hybridMoveOrdered)
        {
            Vector3 targetPosition;
            Quaternion targetRotation = this.transform.rotation;

            if (this.isLost)
            {
                targetPosition = this.gazeProvider.HitPosition;
                targetRotation = this.cameraTr.rotation;
            }
            else
            {
                this.deltaDistance = (this.pointer.Position - Camera.main.transform.position).normalized;
                targetPosition = this.pointer.Position + this.deltaDistance.normalized * this.itemDistance;

                targetRotation = Quaternion.Euler(0, this.pointer.Rotation.eulerAngles.y, 0);
            }

            // Apply magnetism effect when getting inside a defined effect area
            if (this.isMagnetic)
            {
                Magnetism(this.max_dist, ref targetPosition, ref targetRotation);
            }

            float lerpAmount = GetLerpAmount();
            Quaternion smoothedRotation = Quaternion.Lerp(this.hostTransform.rotation, targetRotation, lerpAmount);
            Vector3 smoothedPosition = Vector3.Lerp(this.hostTransform.position, targetPosition, lerpAmount);
            this.hostTransform.SetPositionAndRotation(smoothedPosition, smoothedRotation);
        }

    }

    public override void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        eventData.Use();
    }

    public override void OnPointerDragged(MixedRealityPointerEventData eventData)
    {
        eventData.Use();
    }

    public override void OnPointerUp(MixedRealityPointerEventData eventData)
    {
    }

    public override void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
        this.audio.PlayOneShot(this.PressClip);

        if (!this.activeMenu & !(this.moveOrdered || this.moveOrderedCenterHead || this.hybridMoveOrdered))
        {
            StartPieMenu(eventData);
        }
        else if (this.activeMenu & !(this.moveOrdered || this.moveOrderedCenterHead || this.hybridMoveOrdered))
        {
            this.activeMenu = false;
            this.waitForAction = true;
            StartCoroutine("WaitForAction");
        }
        else if (!this.activeMenu & (this.moveOrdered || this.moveOrderedCenterHead || this.hybridMoveOrdered))
        {
            if (this.moveOrderedCenterHead)
            {
                // In case it did not hit the right thing
                if (this.gazeProvider.HitInfo.collider.gameObject.layer != LayerMask.NameToLayer("Surface"))
                    return;

                Collider[] collisions = Physics.OverlapSphere(this.transform.position, this.transform.localScale.y / 2f);
                if (collisions.Length > 1)
                {
                    float zPos = 0f;
                    foreach (Collider c in collisions)
                    {
                        if (c.tag != "Slate")
                            continue;
                        zPos = Mathf.Min(c.transform.parent.localPosition.z, zPos);
                    }
                    this.transform.localPosition += new Vector3(0f, 0f, 0.001f + zPos - 0.00002f);
                }

                this.hostTransform.localRotation = Vector3.Dot(this.Magnet.transform.forward, this.gazeProvider.transform.forward) >= 0
                    ? Quaternion.identity
                    : new Quaternion(0, 1, 0, 0);
            }

            this.moveOrdered = false;
            this.moveOrderedCenterHead = false;
            this.hybridMoveOrdered = false;
            this.isPersonal = false;

            this.hostTransform.GetComponent<AuthorViewRights>().LetAuthoring(false);

            foreach (Collider c in this.hostTransform.GetComponentsInChildren<Collider>())
            {
                c.enabled = true;
            }

            CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);

            StopPieMenu();
        }
    }

    private void StartPieMenu(MixedRealityPointerEventData eventData)
    {

        string[] funcName = new string[] { "Move card", null, null, null };
        Material[] funcIcons = new Material[] { this.MoveCard, null, null, null };

        if (this.icons)
        {
            if (!this.isPersonal && this.Magnet.name != "Wall") funcIcons[1] = this.MoveWindowCards;
            if (this.isPersonal) funcIcons[1] = this.MovePersonalCardsToWindow;
            if (!this.isPersonal) funcIcons[3] = this.MoveCardToPersonal;
            if (this.isPersonal) funcIcons[2] = this.ExpandPersonalCardsToWindow;
            if (!this.isPersonal && this.Magnet.name != "Wall") funcIcons[2] = this.MoveWindowCardsToPersonal;
        }
        else
        {
            if (!this.isPersonal && this.Magnet.name != "Wall") funcName[1] = "Move Window cards";
            if (this.isPersonal) funcName[1] = "Move personal cards";
            if (!this.isPersonal) funcName[3] = "Move card to personal Space";
            if (this.isPersonal) funcName[2] = "Expand personal cards to Window";
            if (!this.isPersonal && this.Magnet.name != "Wall") funcName[2] = "Move Window cards to personal Space";
        }

        Vector3 menuScale;
        Vector3 menuPos;


        if (Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position) > 1.2f)
        {
            menuScale = Vector3.one * 0.007f * Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position) / 2.5f;
            menuPos = (this.gazeProvider.HitPosition + Camera.main.transform.position) / 2f;
        }
        else
        {
            menuScale = Vector3.one * 0.007f * (Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position) / 1.2f);
            menuPos = this.gazeProvider.HitPosition + Camera.main.transform.forward * (-0.05f);
        }


        if (this.icons)
        {
            if (!PieMenu.Instance.Call(menuPos, Camera.main.transform.rotation, menuScale, funcIcons))
                return;
            //PieMenu.Instance.Call(gazeProvider.HitPosition + Camera.main.transform.forward * (-0.05f), Camera.main.transform.rotation, Vector3.one * 0.007f * (Vector3.Distance(gazeProvider.HitPosition, Camera.main.transform.position) / 1.2f), funcName);
        }
        else
        {
            if (!PieMenu.Instance.Call(menuPos, Camera.main.transform.rotation, menuScale, funcName))
                return;
            //PieMenu.Instance.Call(gazeProvider.HitPosition + Camera.main.transform.forward * (-0.05f), Camera.main.transform.rotation, Vector3.one * 0.007f * (Vector3.Distance(gazeProvider.HitPosition, Camera.main.transform.position) / 1.2f), funcName);
        }

        this.activeMenu = true;

        PieMenu.Instance.OnAction1 += StartHeadMove;
        PieMenu.Instance.OnAction2 += BatchMoveCall;

        if (this.isPersonal)
            PieMenu.Instance.OnAction3 += ExpandPersonal;
        else
            PieMenu.Instance.OnAction3 += CallEmptyOnWindow;
        PieMenu.Instance.OnAction4 += SwitchToPersonal;

        this.pointer = eventData.InputSource.Pointers.First();

        CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
    }

    private void StopPieMenu()
    {
        CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= BatchMoveCall;
        PieMenu.Instance.OnAction3 -= ExpandPersonal;
        PieMenu.Instance.OnAction3 -= CallEmptyOnWindow;
        PieMenu.Instance.OnAction4 -= SwitchToPersonal;

        PieMenu.Instance.Destroy();
    }

    private void CancelPieMenu()
    {
        CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);

        this.activeMenu = false;
        this.moveOrdered = false;
        this.moveOrderedCenterHead = false;
        this.hybridMoveOrdered = false;
        this.pointer = null;

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= BatchMoveCall;
        PieMenu.Instance.OnAction3 -= CallEmptyOnWindow;
        PieMenu.Instance.OnAction3 -= ExpandPersonal;
        PieMenu.Instance.OnAction4 -= SwitchToPersonal;

        PieMenu.Instance.Destroy();
    }

    private void StartProximityMove()
    {
        this.waitForAction = false;
        this.hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();
        if (GetComponent<OrbitalPersonal>().enabled)
            OrbitalPersonalRing.Instance.RemoveCollection(GetComponent<OrbitalPersonal>());

        this.moveOrdered = true;
        this.hybridMoveOrdered = false;
        this.moveOrderedCenterHead = false;
        this.activeMenu = false;
        this.isPersonal = false;

        this.deltaDistance = (this.pointer.Position - Camera.main.transform.position).normalized;
        this.itemDistance = 0.3f;

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= BatchMoveCall;
        PieMenu.Instance.OnAction3 -= ExpandPersonal;
        PieMenu.Instance.OnAction3 -= CallEmptyOnWindow;
        PieMenu.Instance.OnAction4 -= SwitchToPersonal;

        PieMenu.Instance.Hide();

        CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
    }

    private void BatchMoveCall()
    {
        this.waitForAction = false;

        this.moveOrdered = false;
        this.hybridMoveOrdered = false;
        this.moveOrderedCenterHead = false;
        this.activeMenu = false;

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= BatchMoveCall;
        PieMenu.Instance.OnAction3 -= ExpandPersonal;
        PieMenu.Instance.OnAction3 -= CallEmptyOnWindow;
        PieMenu.Instance.OnAction4 -= SwitchToPersonal;

        StopPieMenu();

        if (this.isPersonal)
            OrbitalPersonalRing.Instance.BatchMove();
        else if (this.Magnet.name != "Wall")
            this.Magnet.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().BatchMove();

        this.isPersonal = false;
    }

    private void StartHeadMove()
    {
        this.waitForAction = false;
        this.batchOffset = Vector3.zero;

        this.hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();
        if (GetComponent<OrbitalPersonal>().enabled)
            OrbitalPersonalRing.Instance.RemoveCollection(GetComponent<OrbitalPersonal>());

        this.moveOrderedCenterHead = true;
        this.hybridMoveOrdered = false;
        this.moveOrdered = false;
        this.activeMenu = false;
        this.isPersonal = false;

        this.deltaDistance = Camera.main.transform.forward * 0.2f;
        this.itemDistance = Vector3.Distance(this.transform.position, this.cameraTr.position);



        foreach (Collider c in this.hostTransform.GetComponentsInChildren<Collider>())
        {
            c.enabled = false;
        }


        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= BatchMoveCall;
        PieMenu.Instance.OnAction3 -= ExpandPersonal;
        PieMenu.Instance.OnAction3 -= CallEmptyOnWindow;
        PieMenu.Instance.OnAction4 -= SwitchToPersonal;

        PieMenu.Instance.Hide();

        CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
    }

    private void HybridMove()
    {
        this.waitForAction = false;

        this.hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();
        if (GetComponent<OrbitalPersonal>().enabled)
            OrbitalPersonalRing.Instance.RemoveCollection(GetComponent<OrbitalPersonal>());

        this.hybridMoveOrdered = true;
        this.moveOrderedCenterHead = false;
        this.moveOrdered = false;
        this.activeMenu = false;
        this.isPersonal = false;

        this.deltaDistance = (this.pointer.Position - Camera.main.transform.position).normalized;
        this.itemDistance = 0.3f;

        foreach (Collider c in this.hostTransform.GetComponentsInChildren<Collider>())
        {
            c.enabled = false;
        }

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= BatchMoveCall;
        PieMenu.Instance.OnAction3 -= ExpandPersonal;
        PieMenu.Instance.OnAction3 -= CallEmptyOnWindow;
        PieMenu.Instance.OnAction4 -= SwitchToPersonal;

        PieMenu.Instance.Hide();

        CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
    }

    private void SwitchToPersonal()
    {
        this.hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= BatchMoveCall;
        PieMenu.Instance.OnAction3 -= CallEmptyOnWindow;
        PieMenu.Instance.OnAction3 -= ExpandPersonal;
        PieMenu.Instance.OnAction4 -= SwitchToPersonal;

        StopPieMenu();

        OrbitalPersonalRing.Instance.AddToCollection(GetComponent<OrbitalPersonal>());

        GetComponent<SlateAuthorViewRights>().SwitchSurface(this.Magnet, SynchroManager.Instance.myGameObjectPosition);
        this.isPersonal = true;
        this.isAttracted = true;
        this.Magnet = SynchroManager.Instance.myGameObjectPosition;

        this.changeSurfaceMessage.surfaceName = SynchroManager.Instance.myGameObjectPosition.name;
        //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
        SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
    }

    public void CallEmptyOnWindow()
    {
        StopPieMenu();
        this.transform.parent.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().Empty();
    }

    public void SwitchToPersonalBatch()
    {
        this.hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();

        OrbitalPersonalRing.Instance.AddToCollection(GetComponent<OrbitalPersonal>(), this.HostTransform.transform.localPosition);

        GetComponent<SlateAuthorViewRights>().SwitchSurface(this.Magnet, SynchroManager.Instance.myGameObjectPosition);
        this.isPersonal = true;
        this.isAttracted = true;
        this.Magnet = SynchroManager.Instance.myGameObjectPosition;

        this.changeSurfaceMessage.surfaceName = SynchroManager.Instance.myGameObjectPosition.name;
        //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
        SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
    }

    private void ExpandPersonal()
    {
        OrbitalPersonalRing.Instance.Expand();
    }

    public void BatchHeadMove(Vector3 vec3BatchOffset)
    {
        StartHeadMove();
        this.batchOffset = vec3BatchOffset;
    }

    public void ExpandAction(GameObject newMagnet)
    {
        GetComponent<SlateAuthorViewRights>().SwitchSurface(this.Magnet, newMagnet);
        this.isPersonal = false;
        this.isAttracted = true;
        this.Magnet = newMagnet;

        this.changeSurfaceMessage.surfaceName = newMagnet.name;
        //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
        SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
    }

    public void SwitchToPersonalRemote()
    {
        this.hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();

        //Do not remove, if you do, it crash and i don't know why
        //GetComponent<SlateAuthorViewRights>().OffSurface(Magnet);
        //

        this.isPersonal = true;
    }

    IEnumerator WaitForAction()
    {
        yield return new WaitForSeconds(0.5f);
        if (this.waitForAction)
            CancelPieMenu();
        this.waitForAction = false;
    }

    public void OnSourceDetected(SourceStateEventData eventData)
    {
        if (this.moveOrdered || this.moveOrderedCenterHead || this.hybridMoveOrdered)
        {
            this.isLost = false;
            this.pointer = eventData.InputSource.Pointers.First();
        }
    }

    public void OnSourceLost(SourceStateEventData eventData)
    {
        this.pointer = null;
        if (this.moveOrdered || this.moveOrderedCenterHead || this.hybridMoveOrdered)
            this.isLost = true;
    }

    public void SwitchSurfaceTo(GameObject newSurface)
    {
        if (this.Magnet == null)
            this.hostTransform.GetComponent<SlateAuthorViewRights>().OnSurface(newSurface);
        else
            this.hostTransform.GetComponent<SlateAuthorViewRights>().SwitchSurface(this.Magnet, newSurface);
        this.Magnet = newSurface;

        this.changeSurfaceMessage.surfaceName = newSurface.name;
        //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
        SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
    }

    public override void ChangeMagnet(GameObject g)
    {
        if (g == null)
        {
            PieMenuAdvancedClickSurfaceInteraction pmacsi = this.Magnet.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>();
            if (pmacsi != null)
            {
                pmacsi.UpdateContent(-1);
            }

            this.isAttracted = false;
            this.Magnet = null;

        }
        else
        {
            this.isAttracted = true;
            this.Magnet = g;

            PieMenuAdvancedClickSurfaceInteraction pmacsi = this.Magnet.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>();
            if (pmacsi != null)
            {
                pmacsi.UpdateContent(0);
            }
        }

    }
    protected override void Magnetism(float max_dist, ref Vector3 position, ref Quaternion rotation)
    {

        // Manage magnetism when multiple magnet exists 
        int minIndex = 0;
        float distance;
        Vector3 ClosestToCenter;

        GameObject[] Magnets = GameObject.FindGameObjectsWithTag("MagneticSurface");
        Vector3 OnMagnet = Magnets[0].GetComponentInChildren<Collider>().ClosestPoint(position);

        float minDist = Vector3.Distance(OnMagnet, position);

        // Find the magnet closest to the transform 
        for (int i = 1; i < Magnets.Length; i++)
        {
            OnMagnet = Magnets[i].GetComponentInChildren<Collider>().ClosestPoint(position);

            distance = Vector3.Distance(OnMagnet, position); ;
            if (distance < minDist)
            {
                minDist = distance;
                minIndex = i;
            }
        }

        // Calculate the true position for item
        GameObject ClosestMagnet = Magnets[minIndex];
        OnMagnet = ClosestMagnet.GetComponentInChildren<Collider>().ClosestPoint(position);
        ClosestToCenter = ClosestMagnet.transform.position - OnMagnet;

        Vector3 objOnMagnet = Vector3.Project(ClosestToCenter, ClosestMagnet.transform.forward) + OnMagnet;

        // Operate Magnet effect on the object
        if (Vector3.Distance(objOnMagnet, position) <= max_dist)
        {
            // Case where item goes off a surface
            if (this.isAttracted == false)
            {
                this.Magnet = ClosestMagnet;
                this.hostTransform.GetComponent<SlateAuthorViewRights>().OnSurface(ClosestMagnet);

                this.changeSurfaceMessage.surfaceName = ClosestMagnet.name;
                //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }
            // Case where item changes surface without going through the 3D space
            else if (this.isAttracted == true && ClosestMagnet != this.Magnet)
            {
                this.hostTransform.GetComponent<SlateAuthorViewRights>().SwitchSurface(this.Magnet, ClosestMagnet);
                this.Magnet = ClosestMagnet;

                this.changeSurfaceMessage.surfaceName = ClosestMagnet.name;
                //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }

            this.isAttracted = true;
            position = objOnMagnet - this.Magnet.transform.forward * 0.001f;

            rotation = (Vector3.Dot(ClosestMagnet.transform.forward, this.hostTransform.forward) > 0f) ? ClosestMagnet.transform.rotation : Quaternion.Inverse(ClosestMagnet.transform.rotation);
        }
        else
        {
            // Case where item is in 3D space and goes on a surface
            if (this.isAttracted == true)
            {
                this.hostTransform.GetComponent<SlateAuthorViewRights>().OffSurface(this.Magnet);

                this.Magnet = null;

                this.changeSurfaceMessage.surfaceName = null;
                //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }
            this.isAttracted = false;
        }
    }

    [Button]
    public void Status()
    {
        Debug.Log(" activeMenu " + this.activeMenu + " moveOrdered " + this.moveOrdered + " headMoveOrdered " + this.moveOrderedCenterHead + " isPErsonal " + this.isPersonal + " isLost " + this.isLost);
    }
}

