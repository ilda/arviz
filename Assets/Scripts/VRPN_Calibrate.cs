﻿using NaughtyAttributes;
using Synchro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UVRPN.Core;
using UVRPN.Utility;

public class VRPN_Calibrate : VRPN_Client
{

    #region Position Fields

    [SerializeField] [HideInInspector] private bool trackPosition = true;
    [SerializeField] [HideInInspector] private bool localPosition = true;
    [SerializeField] [HideInInspector] private InvertAxis  invertPos = new InvertAxis();
    [SerializeField] [HideInInspector] [Range(0, 100)] private float scale = 1;


    #endregion

    #region Rotation Fields

    [SerializeField] [HideInInspector] private bool trackRotation = true;
    [SerializeField] [HideInInspector] private bool localRotation = true;
    [SerializeField] [HideInInspector] private InvertAxis invertRot = new InvertAxis();

    #endregion

    #region Properties

    /// <summary>
    /// Returns true if this tracker has been found at least once.
    /// </summary>
    public bool InitiallyConnected
    {
        get { return initiallyConnected; }
    }

    [SerializeField] [HideInInspector] private bool initiallyConnected;


    #endregion

    public string Tracker
    {
        get { return tracker; }
        set { tracker = value; }
    }

    private UpdateCalibration uc = new UpdateCalibration();

    protected virtual void Start()
    {
        uc.name = this.name;
    }

    #region Position Functions

    private Vector3 GetPosition()
    {
        var sub_sub_output = host.GetPosition(tracker, channel);
        var sub_output = invertPos.Apply(host.GetPosition(tracker, channel));
        var output = Process(invertPos.Apply(host.GetPosition(tracker, channel)) * scale);
        return output;
    }

    private IEnumerator Position()
    {
        yield return StartCoroutine(WaitForInitialConnection());

        while (true)
        {
            var sub_sub_output = host.GetPosition(tracker, channel);
            var sub_output = invertPos.Apply(host.GetPosition(tracker, channel));
            var output = Process(invertPos.Apply(host.GetPosition(tracker, channel)) * scale);

            if (localPosition) transform.localPosition = output;
            else transform.position = output;

            yield return null;
        }
    }

    /// <summary>
    /// Takes the position vector right before it is applied to the transform. Allows inherited classes to 
    /// intervene by overriding the function.
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    protected virtual Vector3 Process(Vector3 input)
    {
        return input;
    }

    #endregion

    #region Rotation Functions

    private Quaternion GetRotation()
    {
        var output = Process(invertRot.Apply(host.GetRotation(tracker, channel)));
        return output;
    }

    private IEnumerator Rotation()
    {
        yield return StartCoroutine(WaitForInitialConnection());

        while (true)
        {
            var output = Process(invertRot.Apply(host.GetRotation(tracker, channel)));

            if (localRotation) transform.localRotation = output;
            else transform.rotation = output;

            yield return null;
        }
    }

    /// <summary>
    /// Takes the rotation quaternion right before it is applied to the transform. Allows inherited classes to 
    /// intervene by overriding the function.
    /// </summary>
    /// <param name="input"></param>
    protected virtual Quaternion Process(Quaternion input)
    {
        return input;
    }

    #endregion

    /// <summary>
    /// This runs until tracker data is received and sets 'initiallyConnected' to true afterwards.
    /// </summary>
    /// <returns></returns>
    private IEnumerator WaitForInitialConnection()
    {
        while (!InitiallyConnected)
        {
            var temp = host.GetPosition(tracker, channel);

            //Vector (505, 505, 505) is hard coded in native library
            if (Mathf.Abs(temp.x - (-505)) < 0.001f &&
                Mathf.Abs(temp.y - (-505)) < 0.001f &&
                Mathf.Abs(temp.z - (-505)) < 0.001f)
            {
                yield return null;
                continue;
            }

            initiallyConnected = true;
        }
    }
}
