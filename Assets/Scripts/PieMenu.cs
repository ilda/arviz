﻿using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Synchro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieMenu : Singleton<PieMenu>, IMixedRealityPointerHandler
{
    public ContextualPieMenu pieMenu;
    private GameObject pieMenuInstance;
    public ContextualPieMenu pieMenuInstanceManagement;    

    public delegate void Action1Delegate();
    public delegate void Action2Delegate();
    public delegate void Action3Delegate();
    public delegate void Action4Delegate();

    public event Action1Delegate OnAction1;
    public event Action2Delegate OnAction2;
    public event Action2Delegate OnAction3;
    public event Action2Delegate OnAction4;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool Call(Vector3 position, Quaternion rotation, Vector3 scale, string[] funcName)
    {
        // Hijack input for WAll only
        if (!SynchroManager.Instance.WallARCondition)
            return false;

        // Use to call the menu to use it, maybe input the buttons name of the context
        // Return if you could call it or not
        if (this.pieMenuInstance != null)
        {
            return false;
        }
        else
        {
            this.pieMenuInstanceManagement = Instantiate(this.pieMenu) as ContextualPieMenu;
            this.pieMenuInstance = this.pieMenuInstanceManagement.gameObject;
            this.pieMenuInstance.transform.position = position;
            this.pieMenuInstance.transform.rotation = rotation;
            this.pieMenuInstance.transform.localScale = scale;

            this.pieMenuInstanceManagement.buttons[0].OnUp += Action1;
            this.pieMenuInstanceManagement.buttons[1].OnUp += Action2;
            this.pieMenuInstanceManagement.buttons[2].OnUp += Action3;
            this.pieMenuInstanceManagement.buttons[3].OnUp += Action4;

            CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);

            for (int i = 0; i<4; i++) 
            {
                this.pieMenuInstanceManagement.buttons[i].SetLabel(funcName[i]);
                if (funcName[i] == null)
                    this.pieMenuInstanceManagement.buttons[i].interactable = false;
                this.pieMenuInstanceManagement.icons[i].enabled = false;
            }                                        
        }

        return true;
    }

    public bool Call(Vector3 position, Quaternion rotation, Vector3 scale, Material[] funcIcons)
    {
        // Hijack input for WAll only
        if (!SynchroManager.Instance.WallARCondition)
            return false;
        

        // Use to call the menu to use it, maybe input the buttons name of the context
        // Return if you could call it or not
        if (this.pieMenuInstance != null)
        {
            return false;
        }
        else
        {
            this.pieMenuInstanceManagement = Instantiate(this.pieMenu) as ContextualPieMenu;
            this.pieMenuInstance = this.pieMenuInstanceManagement.gameObject;
            this.pieMenuInstance.transform.position = position;
            this.pieMenuInstance.transform.rotation = rotation;
            this.pieMenuInstance.transform.localScale = scale;

            this.pieMenuInstanceManagement.buttons[0].OnUp += Action1;
            this.pieMenuInstanceManagement.buttons[1].OnUp += Action2;
            this.pieMenuInstanceManagement.buttons[2].OnUp += Action3;
            this.pieMenuInstanceManagement.buttons[3].OnUp += Action4;

            CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);

            for (int i = 0; i < 4; i++)
            {                                
                if (funcIcons[i] == null)
                {
                    this.pieMenuInstanceManagement.buttons[i].interactable = false;
                    this.pieMenuInstanceManagement.icons[i].enabled = false;
                }
                else
                {
                    this.pieMenuInstanceManagement.buttons[i].interactable = true;
                    this.pieMenuInstanceManagement.icons[i].enabled = true;
                    this.pieMenuInstanceManagement.icons[i].material = funcIcons[i];
                }
            }
            //Debug.Log(funcIcons[0] + " - " + funcIcons[1] + " - " + funcIcons[2] + " - " + funcIcons[3]);
        }

        return true;
    }

    public bool Exist()
    {
        return this.pieMenuInstance != null;
    }

    public bool IsHighlightedOrPressed()
    {
        bool a = this.pieMenuInstanceManagement.buttons[0].IsInteractable() && this.pieMenuInstanceManagement.buttons[0].IsHighlightedOrPressed();
        bool b = this.pieMenuInstanceManagement.buttons[1].IsInteractable() && this.pieMenuInstanceManagement.buttons[1].IsHighlightedOrPressed();
        bool c = this.pieMenuInstanceManagement.buttons[2].IsInteractable() && this.pieMenuInstanceManagement.buttons[2].IsHighlightedOrPressed();
        bool d = this.pieMenuInstanceManagement.buttons[3].IsInteractable() && this.pieMenuInstanceManagement.buttons[3].IsHighlightedOrPressed();

        return a || b || c || d;
    }

    public bool isOn()
    {
        return this.pieMenuInstanceManagement != null;
    }

    public void Hide()
    {
        // Use to hide the menu when actions is ongoing
        if (this.pieMenuInstance != null)
            this.pieMenuInstance.SetActive(false);

        CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);
    }

    public void Reveal()
    {
        this.pieMenuInstance.SetActive(true);
    }

    public void Discard()
    {
        // Use to say you don't need the menu anymore
        this.pieMenuInstanceManagement.buttons[0].OnUp -= Action1;
        this.pieMenuInstanceManagement.buttons[1].OnUp -= Action2;
        this.pieMenuInstanceManagement.buttons[2].OnUp -= Action3;
        this.pieMenuInstanceManagement.buttons[3].OnUp -= Action4;

        this.pieMenuInstance.SetActive(false);

        CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);
    }

    public void Destroy()
    {
        //Debug.Log("<color=red> Destroy");
        if (this.pieMenuInstance == null && this.pieMenuInstance == null)
            return;
        else if (this.pieMenuInstance == null || this.pieMenuInstance == null)
            Debug.Log("Error");

        this.pieMenuInstanceManagement.buttons[0].OnUp -= Action1;
        this.pieMenuInstanceManagement.buttons[1].OnUp -= Action2;
        this.pieMenuInstanceManagement.buttons[2].OnUp -= Action3;
        this.pieMenuInstanceManagement.buttons[3].OnUp -= Action4;

        // Destroy Menu
        Destroy(this.pieMenuInstance);
        this.pieMenuInstance = null;
    }

    public void Action1()
    {
        OnAction1();
    }

    public void Action2()
    {
        OnAction2();
    }

    public void Action3()
    {
        OnAction3();
    }

    public void Action4()
    {
        OnAction4();
    }

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    { }

    public void OnPointerDragged(MixedRealityPointerEventData eventData)
    { }

    public void OnPointerUp(MixedRealityPointerEventData eventData)
    {
        if (this.pieMenuInstanceManagement.buttons[0].IsHighlightedOrPressed())
        {
            OnAction1();
        }
        else if(this.pieMenuInstanceManagement.buttons[1].IsHighlightedOrPressed())
        {
            OnAction2();
        }
        else if (this.pieMenuInstanceManagement.buttons[2].IsHighlightedOrPressed())
        {
            OnAction3();
        }
        else if (this.pieMenuInstanceManagement.buttons[3].IsHighlightedOrPressed())
        {
            OnAction4();
        }
        CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);
    }

    public void OnPointerClicked(MixedRealityPointerEventData eventData)
    { }
}
