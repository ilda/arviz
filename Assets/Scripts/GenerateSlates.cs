﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using NaughtyAttributes;
using Synchro;
using TMPro;
using UnityEngine;

public class GenerateSlates : MonoBehaviour
{
    public GameObject slate;
    public GameObject imageSlate;
    public int slateNumber;

    [BoxGroup("Material Parameters")] public Material isMissing;
    [BoxGroup("Material Parameters")] public Material isNew;

    [BoxGroup("Material Parameters")] public Material basic;
    [BoxGroup("Material Parameters")] public Material invisible;

    public TextMeshProUGUI show_events;

    [BoxGroup("Content Parameters")] public bool useCaseDS;
    [BoxGroup("Content Parameters")] public TextAsset textCaseAsset;
    [BoxGroup("Content Parameters")] public TextAsset imageCaseAsset;
    [BoxGroup("Content Parameters")] public int imageAssetId;
    [BoxGroup("Content Parameters")] public List<TextAsset> imageAsset;    

    GameObject wall;
    bool isHololens;

    string[] nameList;
    /*
    string[] nameList = new string[] { "ShortenDataset/utf8_1101162945890",
        "ShortenDataset/utf8_scientificreport2",
        "ShortenDataset/utf8_1101163018612",
        "ShortenDataset/utf8_1101163977242",
        //"NoiseFiles/utf8_1101163272650",
        //"NoiseFiles/utf8_1101163941036",
        "ShortenDataset/utf8_1101162452451",

        "ShortenDataset/utf8_scientificreport3",
        "ShortenDataset/utf8_scientificreport1",

        "ShortenDataset/utf8_1101163356500",
        "ShortenDataset/utf8_1101163018599",
        "ShortenDataset/utf8_1101243338500",
        "ShortenDataset/utf8_1101163019147",
        "ShortenDataset/utf8_1101240332919",
        "ShortenDataset/utf8_1101631275108",
        "ShortenDataset/utf8_1101163661840",
        "ShortenDataset/utf8_scientificreport4",

        "ShortenDataset/utf8_1101163595600",
        "ShortenDataset/utf8_1101163726024",
        "ShortenDataset/utf8_1101162413930",
        "NoiseFiles/utf8_1101243051605",
        "ShortenDataset/utf8_1101163017765",
        "ShortenDataset/utf8_1101163356001",
        "ShortenDataset/utf8_1101163840166",
        "ShortenDataset/utf8_1101163727099",
        "ShortenDataset/utf8_1101163452222",
        "NoiseFiles/utf8_1101163592729",

        "NoiseFiles/utf8_1101163941036",
        "NoiseFiles/utf8_1101243073559"
        };

    List<Vector3> posVicon = new List<Vector3>() {
        new Vector3(-2.387f, 1.708f, -0.060f),
        new Vector3(-2.656f, 1.246f, -0.060f),
        new Vector3(0.748f, 1.298f, -0.060f),
        new Vector3(-1.450f, 1.708f, -0.060f),
        new Vector3(-1.137f, 1.708f, -0.060f),
        new Vector3(-0.825f, 1.708f, -0.060f),
        new Vector3(-0.512f, 1.708f, -0.060f),
        new Vector3(-0.200f, 1.708f, -0.060f),
        new Vector3(0.113f, 1.708f, -0.060f),
        new Vector3(0.425f, 1.708f, -0.060f),
        new Vector3(1.864f, 1.2f-0.655f, -0.060f),
        new Vector3(2.203f, 1.2f-0.650f, -0.060f),
        new Vector3(1.848f, 1.799f, -0.060f),
        new Vector3(2.160f, 1.789f, -0.060f),
        new Vector3(-0.527f, 1.2f-0.450f, -0.060f),
        new Vector3(0.782f, 1.2f-0.541f, -0.060f),
        new Vector3(2.570f, 1.805f, -0.060f),
        new Vector3(-2.145f, 1.709f, -0.060f),
        new Vector3(-1.870f, 1.2f-0.061f, -0.060f),
        new Vector3(-0.232f, 1.2f-0.062f, -0.060f),
        new Vector3(1.480f, 1.267f, -0.060f),
        new Vector3(2.525f, 1.2f-0.655f, -0.060f),
        new Vector3(-0.825f, 1.408f, -0.060f),
        new Vector3(1.341f, 1.2f-0.509f, -0.060f),
        new Vector3(1.050f, 1.2f-0.525f, -0.060f),
        new Vector3(1.16f, 1.2f-0.11f, 0.000f),
        new Vector3(-1.470f, 1.2f-0.37f, 0.000f),
        new Vector3(-0.974f, 1.2f-0.330f, -0.060f),
        new Vector3(-2.278f, 1.274f, -0.060f),
        new Vector3(1.400f, 1.801f, -0.060f),
        new Vector3(-2.262f, 1.2f-0.476f, -0.060f),
        new Vector3(0.851f, 1.817f, -0.060f),
        new Vector3(-0.064f, 1.2f-0.599f, -0.060f),
        new Vector3(0.399f, 1.2f-0.400f, -0.060f),
        new Vector3(2.225f, 1.203f, -0.060f)
    };
    */
    List<Vector3> posImages = new List<Vector3>()
    {
        new Vector3(-1.900f, 0.725f, 0.000f),
        new Vector3(-0.659f, -0.345f, 0.000f),
        new Vector3(-2.426f, -0.522f, 0.000f),
        new Vector3(-2.627f, 0.128f, 0.000f),
        new Vector3(1.400f, 0.601f, 0.000f),
        new Vector3(-2.053f, 0.101f, 0.000f),
        new Vector3(0.851f, 0.617f, 0.000f),
        new Vector3(-0.064f, -0.599f, 0.000f),
        new Vector3(0.399f, -0.560f, 0.000f),
        new Vector3(-0.736f, 0.587f, 0.000f),
        new Vector3(2.13f, 0.016f, 0.000f),
        new Vector3(1.295f, 0.070f, 0.000f)
    };


    [BoxGroup("Slate Parameters")] public bool icons;
    [BoxGroup("Slate Parameters")] public Material MoveCard;
    [BoxGroup("Slate Parameters")] public Material MoveCardToPersonal;
    [BoxGroup("Slate Parameters")] public Material MovePersonalCardsToWindow;
    [BoxGroup("Slate Parameters")] public Material MoveWindowCardsToPersonal;
    [BoxGroup("Slate Parameters")] public Material ExpandPersonalCardsAsWindow;
    [BoxGroup("Slate Parameters")] public Material MoveWindowCards;
    [BoxGroup("Slate Parameters")] public Material Select;
    [BoxGroup("Slate Parameters")] public Material UnSelect;
    [BoxGroup("Slate Parameters")] public Material MoveSelection;
    [BoxGroup("Slate Parameters")] public Material MoveSelectionToPersonal;


    private List<GameObject> slatesG = new List<GameObject>();


    private ChangeSurface cs = new ChangeSurface();
    private LetObject lo = new LetObject();
    private ChangePermission cp = new ChangePermission();
    private TransformsStatusUpdate tsu = new TransformsStatusUpdate();

    // Start is called before the first frame update
    void Start()
    {        
        int instanciatedSlates = 0;
        bool useViconFeed = SynchroManager.Instance.useVicon;
        bool isWall = SynchroManager.Instance.IsDevice() == Synchro.DeviceType.WALL;
        this.isHololens = SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS;

        List<UnityEngine.Object> images = new List<UnityEngine.Object>(Resources.LoadAll("ImagesPrefabs/"));

        if (!isWall && !isHololens)
        {
            WallLaunchDeploy wld = GameObject.FindObjectOfType<WallLaunchDeploy>();
            this.imageAssetId = (wld != null) ? wld.GetDatasetId() : this.imageAssetId;
        }
            

        this.wall = this.transform.Find("Wall").gameObject;

        if (this.useCaseDS)
        {
            this.nameList = ParseTextAsset(this.textCaseAsset);
            for (int i = 0; i < this.nameList.Length; i++)
            {
                //!InstantiateSlate(nameList[instanciatedSlates], i, useViconFeed && !isWall)
                if (!InstantiateSlate(this.nameList[instanciatedSlates], i))
                {
                    continue;
                }
                instanciatedSlates++;
            }

            GameObject g;
            for (int i = 0; i < images.Count; i++)
            {
                g = (GameObject)images[i];
                //InstantiatePrefabImages(g, i, useViconFeed && !isWall);
                InstantiatePrefabImages(g, i);
            }
        }
        else
        {
            instanciatedSlates = 0;
            this.nameList = ParseTextAsset(this.imageAsset[imageAssetId]);
            for (int i = 0; i < this.nameList.Length; i++)
            {
                if (!InstantiateImage(this.nameList[instanciatedSlates], i))
                {
                    continue;
                }
                instanciatedSlates++;
            }
        }
    }   

    [Button]
    void GenerateFileList()
    {
        List<UnityEngine.Object> importantArticles = new List<UnityEngine.Object>(Resources.LoadAll("ShortenDataset/"));
        TextAsset noiseIndex = (TextAsset)Resources.Load("NoiseFileIndex");
        string tot = "";

        List<string> noiseFileNames = new List<string>(noiseIndex.text.Split('\n'));
        int randomText;
        int randomNoise;
        int slateInstanceNumber = 0;

        for (int i = 0; i < this.slateNumber; i++)
        {
            randomText = UnityEngine.Random.Range(0, this.slateNumber - i - 1);

            TextAsset ta;

            if (randomText < importantArticles.Count)
            {
                tot += "ShortenDataset/" + importantArticles[randomText].name + '\n';
                importantArticles.RemoveAt(randomText);
            }
            else
            {
                randomNoise = UnityEngine.Random.Range(0, noiseFileNames.Count - 1);
                ta = (TextAsset)Resources.Load("NoiseFiles/" + noiseFileNames[randomNoise]);

                //Debug.Log(ta.name + " " + ta.text);

                tot += "NoiseFiles/" + noiseFileNames[randomNoise] + '\n';
                noiseFileNames.RemoveAt(randomNoise);
            }
            slateInstanceNumber++;
        }
        //Debug.Log(tot);
    }

    [Button]
    void GeneratePosition()
    {
        string tot = "";
        foreach (GameObject g in this.slatesG)
            tot += g.name + g.transform.position.ToString("F3")+'\n';

        //Debug.Log(tot);
    }

    string[] ParseTextAsset(TextAsset ta)
    {
        return ta.text.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
    }

    string ParseFile(TextAsset ta, out int sumLines, bool verbose = false)
    {
        //line count done for TMP 34 font size
        sumLines = 0;
        string text = ta.text;
        string parsedText = "";
        if (text == "" && verbose)
            Debug.Log(ta.name);
        string[] processingStep = text.Replace('\r', '\n').Split('\n');

        foreach (string s in processingStep)
        {
            if (verbose) Debug.Log("#"+s+"#");
            if (s != "")
            {
                sumLines += (int) (s.Length / 63);
                sumLines++;
                parsedText += s + '\n';
            }
        }

        parsedText = parsedText.Replace("Alderwood", "<color=#ff0000><b>Alderwood</b></color>");

        return parsedText;        
    }

    Vector3 ParseVector3(string vec3)
    {
        string[] tmp = vec3.Replace("(", string.Empty).Replace(")", string.Empty).Replace("f", string.Empty).Split(',');
        /*
        if (tmp[0].Contains("."))
        {
            tmp[0] = tmp[0].Replace('.', ',');
            tmp[1] = tmp[1].Replace('.', ',');
            tmp[2] = tmp[2].Replace('.', ',');
        }        
        */
        return new Vector3(float.Parse(tmp[0]), float.Parse(tmp[1]), float.Parse(tmp[2]));        
    }

    bool InstantiateSlate(string obj, int slateInstanceNumber)
    {
        string[] linkAndPos = obj.Split(';');
        TextAsset ta = Resources.Load(linkAndPos[0]) as TextAsset;
        GameObject s = Instantiate(this.slate, this.transform);

        this.slatesG.Add(s);        
        s.transform.localPosition = ParseVector3(linkAndPos[1]);

        s.transform.localRotation = Quaternion.identity;
        s.name = "Slate" + slateInstanceNumber;
        s.GetComponent<BoundingBox>().Target.name = "Plane" + slateInstanceNumber;

        VersioningNotifications v = s.GetComponent<VersioningNotifications>();
        v.notifMissing = this.isMissing;
        v.notifNew = this.isNew;
        v.basic = this.basic;
        v.invisible = this.invisible;
        v.isAttracted = true;
        v.isOnWall = true;

        PieMenuClickInteraction p = s.GetComponent<PieMenuClickInteraction>();
        p.icons = this.icons;
        p.MoveCard = this.MoveCard;
        p.MoveCardToPersonal = this.MoveCardToPersonal;
        p.MovePersonalCardsToWindow = this.MovePersonalCardsToWindow;
        p.MoveWindowCardsToPersonal = this.MoveWindowCardsToPersonal;
        p.ExpandPersonalCardsToWindow = this.ExpandPersonalCardsAsWindow;
        p.SelectCard = this.Select;
        p.UnSelectCard = this.UnSelect;
        p.MoveSelection = this.MoveSelection;
        p.MoveSelectionToPersonal = this.MoveSelectionToPersonal;

        int sumLines;
        TextMeshPro textItem = s.transform.GetChild(0).Find("Content").GetComponent<TextMeshPro>();
        textItem.text = ParseFile(ta, out sumLines);
        textItem.fontSize = 34;

        // If more than one page (23 lines) expand
        if (sumLines > 23)
        {
            float factor = sumLines / 23f;
            s.transform.localScale = new Vector3(s.transform.localScale.x, factor * s.transform.localScale.y, s.transform.localScale.z);
            textItem.transform.localScale = new Vector3(textItem.transform.localScale.x, textItem.transform.localScale.y / factor, textItem.transform.localScale.z);
            textItem.GetComponent<RectTransform>().sizeDelta = new Vector2(textItem.GetComponent<RectTransform>().rect.width, textItem.GetComponent<RectTransform>().rect.height * factor);
        }

        s.transform.parent = this.wall.transform;

        s.GetComponent<SlateAuthorViewRights>().defaultPublic = true;

        if (this.isHololens)
        {
            s.GetComponent<InteractionScript>().Magnet = this.wall;
            s.GetComponent<InteractionScript>().isAttracted = true;
            s.transform.GetChild(0).GetComponent<Renderer>().enabled = false;
            s.GetComponent<InteractionScript>().isAttracted = true;
            v.SetPublicSelf();
        }
        else
        {
            Destroy(s.GetComponent<InteractionScript>());
        }
        SynchroManager.Instance.AddShared(s.GetComponent<BoundingBox>().Target);
        SynchroManager.Instance.AddShared(s);

        s.transform.hasChanged = false;

        return true;
    }

    bool InstantiatePrefabImages(GameObject Prefab, int imageInstanceNumber)
    {
        GameObject s = Instantiate(Prefab, this.transform);

        this.slatesG.Add(s);

        s.name = Prefab.name;
        if (imageInstanceNumber < this.posImages.Count)
            s.transform.localPosition = this.posImages[imageInstanceNumber];
        else
            s.transform.localPosition = Vector3.zero;

        s.transform.localRotation = Quaternion.identity;

        s.GetComponent<BoundingBox>().Target.name = "Plane" + Prefab.name;

        VersioningNotifications v = s.GetComponent<VersioningNotifications>();
        v.notifMissing = this.isMissing;
        v.notifNew = this.isNew;
        v.basic = this.basic;
        v.invisible = this.invisible;
        v.isAttracted = true;
        v.isOnWall = true;

        PieMenuClickInteraction p = s.GetComponent<PieMenuClickInteraction>();
        p.icons = this.icons;
        p.MoveCard = this.MoveCard;
        p.MoveCardToPersonal = this.MoveCardToPersonal;
        p.MovePersonalCardsToWindow = this.MovePersonalCardsToWindow;
        p.MoveWindowCardsToPersonal = this.MoveWindowCardsToPersonal;
        p.ExpandPersonalCardsToWindow = this.ExpandPersonalCardsAsWindow;
        p.SelectCard = this.Select;
        p.UnSelectCard = this.UnSelect;
        p.MoveSelection = this.MoveSelection;
        p.MoveSelectionToPersonal = this.MoveSelectionToPersonal;

        s.transform.parent = this.wall.transform;

        s.GetComponent<SlateAuthorViewRights>().defaultPublic = true;

        if (this.isHololens)
        {
            s.GetComponent<InteractionScript>().Magnet = this.wall;
            s.GetComponent<InteractionScript>().isAttracted = true;
            s.transform.GetChild(0).GetComponent<Renderer>().enabled = false;
            s.GetComponent<InteractionScript>().isAttracted = true;
            v.SetPublicSelf();
        }
        else
        {
            Destroy(s.GetComponent<InteractionScript>());
        }
        SynchroManager.Instance.AddShared(s.GetComponent<BoundingBox>().Target);
        SynchroManager.Instance.AddShared(s);

        s.transform.hasChanged = false;

        return true;
    }

    bool InstantiateImage(string obj, int slateInstanceNumber)
    {
        string[] linkAndPos = obj.Split(';');
        Sprite ta = Resources.Load<Sprite>(linkAndPos[0]);
        GameObject s = Instantiate(this.imageSlate, this.wall.transform);        

        Bounds bounds = new Bounds(Vector3.zero, Vector3.zero);

        //Debug.Log(linkAndPos);
        this.slatesG.Add(s);
        s.transform.localPosition = ParseVector3(linkAndPos[1]);
        s.transform.localRotation = Quaternion.identity;
        s.transform.GetChild(0).localScale = new Vector3(0.095f, 0.095f, 0.1f);
        s.name = "Slate" + slateInstanceNumber;
        s.GetComponent<BoundingBox>().Target.name = "Plane" + slateInstanceNumber;

        // Add sprite and resize box collider depending on renderer bounds
        s.GetComponentInChildren<SpriteRenderer>().sprite = ta;
        
        BoxCollider bc = s.GetComponentInChildren<BoxCollider>();
        //Debug.Log(ta.bounds + " Bounds calculated " + s.name);
        bounds.Encapsulate(ta.bounds);
        bc.size = bounds.size;        
        

        VersioningNotifications v = s.GetComponent<VersioningNotifications>();
        v.notifMissing = this.isMissing;
        v.notifNew = this.isNew;
        v.basic = this.basic;
        v.invisible = this.invisible;
        v.isAttracted = true;
        v.isOnWall = true;


        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
        {
            PieMenuClickInteraction p = s.GetComponent<PieMenuClickInteraction>();
            p.icons = this.icons;
            p.MoveCard = this.MoveCard;
            p.MoveCardToPersonal = this.MoveCardToPersonal;
            p.MovePersonalCardsToWindow = this.MovePersonalCardsToWindow;
            p.MoveWindowCardsToPersonal = this.MoveWindowCardsToPersonal;
            p.ExpandPersonalCardsToWindow = this.ExpandPersonalCardsAsWindow;
            p.MoveWindowCards = this.MoveWindowCards;
            p.SelectCard = this.Select;
            p.UnSelectCard = this.UnSelect;
            p.MoveSelection = this.MoveSelection;
            p.MoveSelectionToPersonal = this.MoveSelectionToPersonal;
        }

        s.GetComponent<SlateAuthorViewRights>().defaultPublic = true;

        if (this.isHololens)
        {
            s.GetComponent<InteractionScript>().Magnet = this.wall;
            s.GetComponent<InteractionScript>().isAttracted = true;
            s.transform.GetChild(0).GetComponent<Renderer>().enabled = false;
            s.GetComponent<InteractionScript>().isAttracted = true;
            v.SetPublicSelf();
        }
        else
        {
            Destroy(s.GetComponent<InteractionScript>());
        }
        s.GetComponent<BoundingBox>().CreateRig();

        SynchroManager.Instance.AddShared(s.GetComponent<BoundingBox>().Target);
        SynchroManager.Instance.AddShared(s);

        s.transform.hasChanged = false;

        return true;
    }

    [Button]
    public void ResetSlates()
    {
        FileStream logFile;
        StreamWriter log;
        logFile = new FileStream("./Test.txt", FileMode.Create, FileAccess.Write);
        log = new StreamWriter(logFile);

        foreach (GameObject g in this.slatesG)
        {
            DestroyImmediate(g);
        }
        this.slatesG.Clear();
        Start();

        string topic = SynchroManager.Instance.topic;
        string owner = SynchroManager.Instance.ownerId;

        this.tsu = new TransformsStatusUpdate(owner, new List<string>(), new List<Vector3>(), new List<Quaternion>(), new List<Vector3>());

        foreach (GameObject g in this.slatesG)
        {
            // 0 - Surface  
            this.cs = new ChangeSurface(owner, g.name, SynchroManager.Instance.Wall.name, false);
            //SynchroServer.Instance.SendCommand(topic, this.cs);
            SynchroManager.Instance.CommandInstance.Rpc_ChangeSurface(this.cs.owner, this.cs.objectName, this.cs.surfaceName, this.cs.isWindow);
            log.WriteLine(this.cs.ToString());
            // 1 - LetAuthoring
            this.lo = new LetObject(owner, g.name, true);
            //SynchroServer.Instance.SendCommand(topic, this.lo);
            SynchroManager.Instance.CommandInstance.Rpc_LetObject(this.lo.owner, this.lo.objectName, this.lo.affectSelectionState);
            log.WriteLine(this.lo.ToString());
            // 2 - Author Rights
            this.cp = new ChangePermission(owner, g.name, Permissions.PrivacyConverter(PrivacyState.Public), SynchroManager.Instance.fullOwners);
            //SynchroServer.Instance.SendCommand(topic, this.cp);
            SynchroManager.Instance.CommandInstance.Rpc_ChangePermission(cp.owner, cp.objectName, cp.permissionState, cp.owners.ToArray());
            log.WriteLine(this.cp.ToString());
            // 3 - Positions            
            this.tsu.AddChange(g.name, g.transform.localPosition, g.transform.localRotation, g.transform.localScale);
        }

        // Launch reset scene here to have the slates reset, in particular the OrbitalPersonal script disabled
        //SynchroServer.Instance.SendCommand(SynchroManager.Instance.topic, new ResetScene(SynchroManager.Instance.ownerId));
        //SynchroServer.Instance.SendCommand(topic, this.tsu);

        SynchroManager.Instance.CommandInstance.Rpc_ResetScene(SynchroManager.Instance.ownerId);
        SynchroManager.Instance.CommandInstance.Rpc_TransformsStatusUpdate(tsu.owner, tsu.names.ToArray(), tsu.poses.ToArray(), tsu.rots.ToArray(), tsu.scales.ToArray());

        log.WriteLine(this.tsu.ToString());
        log.Close();
        logFile.Close();
    }


    [Button]
    public void TestRPC()
    {
        //SynchroManager.Instance.RpcTestRPC();
    }
}
