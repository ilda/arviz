﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Microsoft.MixedReality.Toolkit;
using Synchro;
using Microsoft.MixedReality.Toolkit.Input.Utilities;
using NaughtyAttributes;

public class PieMenuClickInteraction : InteractionScript, IMixedRealitySourceStateHandler, IMixedRealityFocusHandler
{
    private GraphicRaycaster graphicRaycast;
    private EventSystem m_EventSystem;
    private PointerEventData m_PointerEventData;

    private bool activeMenu = false;
    private GameObject sphere;
    private GameObject sphere2;

    private bool waitForAction = false;

    List<RaycastResult> results = new List<RaycastResult>();
    IMixedRealityPointer pointer;

    Transform cameraTr;

    bool moveOrdered = false;

    bool moveOrderedCenterHead = false;
    public bool MoveOrderedCenterHead
    {
        get
        {
            return this.moveOrderedCenterHead;
        }
        set
        {
            this.moveOrderedCenterHead = value;
        }
    }

    enum SlateState
    {
        Base,
        AwaitInput,
        PieMenu,
        Drag,
        DragGroup
    }

    enum PointerAction
    {
        Down,
        Drag,
        Up,
        OrderDrag,
        Reset
    }

    bool isLost = false;
    private bool ISPERSONAL = false;
    public bool isPersonal
    {
        get
        {
            return ISPERSONAL;
        }
        set
        {
            ISPERSONAL = value;
        }
    } 

    Vector3 deltaDistance;
    float itemDistance;
    Vector3 batchOffset;
    Vector3 adjustmentRatio;
    float initialHeight;
    float initialWidth;
    bool logBatch = false;
    bool isBatch = false;
    bool isAdjusted = false;
    int zId = 0;
    int logNumberOfElement = 0;
    Vector3 memoryPosition;

    public AudioClip PressClip;
#pragma warning disable CS0108 // Un membre masque un membre hérité ; le mot clé new est manquant
    public AudioSource audio;
#pragma warning restore CS0108 // Un membre masque un membre hérité ; le mot clé new est manquant

    float clickTimer;
    float deltaClickAction = 0.2f;
    float longPressThreshold = 0.65f;
    float angleForMove = 2f;
    bool hasPointDown = false;
    Vector3 preDragPos;
    Quaternion preDragRot;
    Transform preDragParent;
    private SlateState currentSlateState = SlateState.Base;
    private bool upActionBuffer = true;

    public bool icons = false;
    public Material MoveCard;
    public Material MoveCardToPersonal;
    public Material MovePersonalCardsToWindow;
    public Material MoveWindowCardsToPersonal;
    public Material ExpandPersonalCardsToWindow;
    public Material MoveWindowCards;
    public Material SelectCard;
    public Material UnSelectCard;
    public Material MoveSelection;
    public Material MoveSelectionToPersonal;

    private bool isRegistered = false;

    #region
    Vector3 var1;
    Vector3 var2;
    Vector3 var3;
    Vector3 var4;
    #endregion

    #region Monobehaviour Methods
    protected override void Start()
    {
        base.Start();
        this.changeSurfaceMessage.isWindow = false;
        this.cameraTr = Camera.main.transform;
    }

    protected override void Update()
    {
        base.Update();

        if (this.activeMenu)
        {

        }

        if (this.moveOrdered)
        {
            Vector3 targetPosition;
            Quaternion targetRotation = this.transform.rotation;
            if (this.isLost)
            {
                targetPosition = this.cameraTr.position + (this.gazeProvider.HitPosition - this.cameraTr.position).normalized * 0.6f;
                targetRotation = this.cameraTr.rotation;
            }
            else
            {
                this.deltaDistance = (this.pointer.Position - Camera.main.transform.position).normalized;
                targetPosition = this.pointer.Position + this.deltaDistance.normalized * this.itemDistance;
                targetRotation = Quaternion.Euler(0, this.pointer.Rotation.eulerAngles.y, 0);
            }


            // Apply magnetism effect when getting inside a defined effect area
            if (this.isMagnetic)
            {
                Magnetism(this.max_dist, ref targetPosition, ref targetRotation);
            }

            float lerpAmount = GetLerpAmount();
            Quaternion smoothedRotation = Quaternion.Lerp(this.hostTransform.rotation, targetRotation, lerpAmount);
            Vector3 smoothedPosition = Vector3.Lerp(this.hostTransform.position, targetPosition, lerpAmount);

            //Change SetPositionAndRotation to this :
            this.hostTransform.position = smoothedPosition;
            //hostTransform.rotation = smoothedRotation;

            this.hostTransform.localRotation = Vector3.Dot(this.Magnet.transform.forward, this.gazeProvider.transform.forward) >= 0
                ? Quaternion.identity
                : new Quaternion(0, 1, 0, 0);
        }

        if (this.currentSlateState == SlateState.Drag || this.currentSlateState == SlateState.DragGroup)
        {
            Quaternion targetRotation;
            Vector3 targetPosition;

            if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
            {
                if (this.logBatch)
                {
                    Measurement.Instance.ItemBatchDistanceAdding(this.hostTransform.position, this.logNumberOfElement);
                }
                else if (!this.isBatch)
                {
                    Measurement.Instance.ItemDistanceAdding(this.hostTransform.position);
                }
            }

            targetPosition = this.gazeProvider.HitPosition;
            targetRotation = this.cameraTr.rotation;

            Quaternion surfaceNormal = (this.Magnet != null) ? Quaternion.LookRotation(this.Magnet.transform.forward) : targetRotation;

            float lerpAmount = GetLerpAmount();
            Quaternion smoothedRotation;
            Vector3 fromCursorOffset; 
            Vector3 smoothedPosition; 

            // Apply magnetism effect when getting inside a defined effect area
            if (this.isMagnetic)
            {
                Magnetism(this.max_dist, ref targetPosition, ref targetRotation);

                if(this.Magnet != null && this.Magnet.tag == "MagneticSurface" && this.Magnet.name != "Wall")
                {
                    smoothedRotation = Quaternion.Lerp(this.hostTransform.rotation, targetRotation, lerpAmount);
                    fromCursorOffset = new Vector3(this.batchOffset.x * this.adjustmentRatio.x, this.batchOffset.y * this.adjustmentRatio.y, this.batchOffset.z * this.adjustmentRatio.z);
                    smoothedPosition = Vector3.Lerp(this.hostTransform.position, targetPosition + surfaceNormal * fromCursorOffset, lerpAmount);

                    //var3 = surfaceNormal * fromCursorOffset;
                }
                else
                {
                    smoothedRotation = Quaternion.Lerp(this.hostTransform.rotation, targetRotation, lerpAmount);
                    smoothedPosition = Vector3.Lerp(this.hostTransform.position, targetPosition + surfaceNormal * this.batchOffset, lerpAmount);
                }
            }
            else
            {
                smoothedRotation = Quaternion.Lerp(this.hostTransform.rotation, targetRotation, lerpAmount);
                fromCursorOffset = new Vector3(this.batchOffset.x * this.adjustmentRatio.x, this.batchOffset.y * this.adjustmentRatio.y, this.batchOffset.z * this.adjustmentRatio.z);
                smoothedPosition = Vector3.Lerp(this.hostTransform.position, targetPosition + surfaceNormal * fromCursorOffset, lerpAmount);

                //var3 = surfaceNormal * fromCursorOffset;
            }
            
            this.hostTransform.SetPositionAndRotation(smoothedPosition, smoothedRotation);
        }
    }
    #endregion

    #region IMixedRealityPointerHandler Methods

    public override void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        UpdateStateMachine(PointerAction.Down, eventData);
        eventData.Use();
    }

    public override void OnPointerDragged(MixedRealityPointerEventData eventData)
    {
        //Debug.Log("Drag");
        UpdateStateMachine(PointerAction.Drag, eventData);
        eventData.Use();
    }

    public override void OnPointerUp(MixedRealityPointerEventData eventData)
    {
        //Debug.Log("Up");
        UpdateStateMachine(PointerAction.Up, eventData);
        eventData.Use();
    }
    
    public override void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
        //Debug.Log("Click | Item is Selected : " + this.isSelected);      
        eventData.Use();
    }

    private void UpdateStateMachine(PointerAction action, MixedRealityPointerEventData eventData)
    {
        SlateState newState = this.currentSlateState;
        switch (newState)
        {
            case SlateState.Base:              
                switch (action)
                {
                    case PointerAction.Down:
                        this.clickTimer = Time.time;
                        this.memoryPosition = this.gazeProvider.HitPosition;
                        newState = SlateState.AwaitInput;
                        break;
                    case PointerAction.Reset:
                        newState = SlateState.Base;
                        break;
                    case PointerAction.OrderDrag:
                        newState = SlateState.DragGroup;
                        break;
                    default:
                        break;
                }
                break;
            case SlateState.AwaitInput:
                // Down triggers timer, drag triggers drag IF headmove, Up triggers select if short, Up and timers long openPieMenu
                switch (action)
                {
                    case PointerAction.Drag:
                        if (HeadDeviation(this.memoryPosition))
                        {
                            SetupDrag();
                            if (this.isSelected && this.selectionOwner != SynchroManager.Instance.ownerId)
                            {
                                newState = SlateState.Base;
                            }
                            else if (this.isSelected && this.selectionOwner == SynchroManager.Instance.ownerId)
                            {
                                BatchMoveCall();
                                newState = SlateState.DragGroup;
                            }
                            else 
                            {
                                StartHeadMove(true);
                                newState = SlateState.Drag;
                            }
                        }
                        else if(Time.time - this.clickTimer > this.longPressThreshold)
                        {
                            if (this.isSelected && this.selectionOwner != SynchroManager.Instance.ownerId)
                                newState = SlateState.Base;
                            else
                                newState = StartPieMenu(eventData) ? SlateState.PieMenu : SlateState.Base;

                        }
                        break;
                    case PointerAction.Up:
                        if (Time.time - this.clickTimer <= this.longPressThreshold)
                        {
                            if(!this.isPersonal)
                                SelectItem();
                            newState = SlateState.Base;
                        }
                        else
                        {
                            newState = StartPieMenu(eventData) ? SlateState.PieMenu : SlateState.Base;
                        }
                        break;
                    case PointerAction.Reset:
                        newState = SlateState.Base;
                        break;
                    case PointerAction.OrderDrag:
                        newState = SlateState.DragGroup;
                        break;
                    default:
                        break;
                }
                break;
            case SlateState.PieMenu:
                switch (action)
                {
                    case PointerAction.Up:
                        if (!PieMenu.Instance.Exist() || !PieMenu.Instance.IsHighlightedOrPressed() && !this.upActionBuffer)
                        {
                            CancelPieMenu();
                            newState = SlateState.Base;
                        }
                        this.upActionBuffer = false;
                        break;
                    case PointerAction.Reset:
                        newState = SlateState.Base;
                        break;
                    case PointerAction.OrderDrag:
                        newState = SlateState.DragGroup;
                        break;
                    default:
                        break;
                }
                break;
            case SlateState.Drag:
                switch (action)
                {
                    case PointerAction.Up:
                    case PointerAction.Reset:
                        PlaceSlate();
                        newState = SlateState.Base;
                        break;
                    default:
                        break;
                }
                break;
            case SlateState.DragGroup:
                switch (action)
                {
                    case PointerAction.Up:
                        PlaceSlate();
                        newState = SlateState.Base;
                        break;
                    default:
                        break;
                }
                break;
                
        }

        //Debug.Log("Slate : " + this.name + " | State : " + this.currentSlateState + " -> " + newState + " | action : " + action);

        this.currentSlateState = newState;
    }

    #endregion

    #region PieMenu Methods
    private bool StartPieMenu(MixedRealityPointerEventData eventData)
    {
        //Debug.Log(this.name + " " + !activeMenu + " <- activeMenu | exist -> " + PieMenu.Instance.Exist());        
        string[] funcName = new string[] { null, null, null, null };
        Material[] funcIcons = new Material[] { null, null, null, null };

        // Populate the menu Icons / Text PlaceHolder
        if (this.icons)
        {
            if (this.isPersonal && SynchroManager.Instance.WallARCondition)
            {
                funcIcons[1] = this.MovePersonalCardsToWindow;
                funcIcons[2] = this.ExpandPersonalCardsToWindow;
            }
            else
            {
                if (this.isSelected)
                {
                    //funcIcons[0] = this.UnSelectCard;
                    funcIcons[1] = this.MoveSelection;
                    if(SynchroManager.Instance.WallARCondition)
                        funcIcons[2] = this.MoveSelectionToPersonal;                    
                }
                else
                {
                    //funcIcons[0] = this.SelectCard;
                    if(SynchroManager.Instance.WallARCondition)
                        funcIcons[3] = this.MoveCardToPersonal;

                    if(this.Magnet.name != "Wall")
                    {
                        funcIcons[1] = this.MoveWindowCards;
                        if(SynchroManager.Instance.WallARCondition)
                            funcIcons[2] = this.MoveWindowCardsToPersonal;
                    }                   
                }
            }
        }
        else
        {
            if (this.isPersonal && SynchroManager.Instance.WallARCondition)
            {
                funcName[1] = "Move personal cards";
                funcName[2] = "Expand personal cards to Window";
            }
            else
            {
                if (this.isSelected)
                {
                    //funcName[0] = "Unselect";
                    funcName[1] = "Move selection";  
                    if(SynchroManager.Instance.WallARCondition)
                        funcName[2] = "Move selection to personal Space";                    
                }
                else
                {
                    //funcName[0] = "Select";
                    if(SynchroManager.Instance.WallARCondition)
                        funcName[3] = "Move card to personal Space";

                    if (this.Magnet.name != "Wall")
                    {
                        funcName[1] = "Move Window cards";
                        if(SynchroManager.Instance.WallARCondition)
                            funcName[2] = "Move Window cards to personal Space";
                    }
                }
            }            
        }

        Vector3 menuScale;
        Vector3 menuPos;


        if (Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position) > 1.2f)
        {
            menuScale = Vector3.one * 0.007f * Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position) / 2.5f;
            menuPos = (this.gazeProvider.HitPosition + Camera.main.transform.position) / 2f;
        }
        else
        {
            menuScale = Vector3.one * 0.007f * (Vector3.Distance(this.gazeProvider.HitPosition, Camera.main.transform.position) / 1.2f);
            menuPos = this.gazeProvider.HitPosition + Camera.main.transform.forward * (-0.05f);
        }


        if (this.icons)
        {
            if (!PieMenu.Instance.Call(menuPos, Camera.main.transform.rotation, menuScale, funcIcons))
                return false;
        }
        else
        {
            if (!PieMenu.Instance.Call(menuPos, Camera.main.transform.rotation, menuScale, funcName))
                return false;
        }

        this.activeMenu = true;

        // Bind the functions to the menu buttons
        //PieMenu.Instance.OnAction1 += StartHeadMove;
        //Debug.Log("<color=red>+ BOUTON</color>");
        PieMenu.Instance.OnAction2 += BatchMoveCall;
        if (this.isPersonal)
        {
            PieMenu.Instance.OnAction3 += ExpandPersonal;
        }
        else if (this.isSelected)
        {
            PieMenu.Instance.OnAction4 += SwitchToPersonal;
            PieMenu.Instance.OnAction3 += CallEmpty;
            //PieMenu.Instance.OnAction1 += SelectItemFromMenu;
        }
        else
        {
            PieMenu.Instance.OnAction4 += SwitchToPersonal;
            PieMenu.Instance.OnAction3 += CallEmpty;
            //PieMenu.Instance.OnAction1 += SelectItemFromMenu;
        }


        this.pointer = eventData.InputSource.Pointers.First();

        if (!this.isRegistered)
        {
            this.isRegistered = true;
            CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
        }

        this.upActionBuffer = true;
        return true;
    }

    private void StopPieMenu()
    {
        this.activeMenu = false;

        if (this.isRegistered)
        {
            this.isRegistered = false;
            CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);
        }

        ResetPieMenuOptions();

        PieMenu.Instance.Destroy();

        UpdateStateMachine(PointerAction.Reset, null);
    }

    private void CancelPieMenu()
    {
        if (this.isRegistered)
        {
            this.isRegistered = false;
            CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);
        }
        this.activeMenu = false;
        this.moveOrdered = false;
        this.MoveOrderedCenterHead = false;
        this.pointer = null;

        ResetPieMenuOptions();

        PieMenu.Instance.Destroy();
    }
    
    private void ResetPieMenuOptions()
    {
        //Debug.Log("<color=blue>- BOUTON</color>");
        PieMenu.Instance.OnAction1 -= SelectItemFromMenu;
        PieMenu.Instance.OnAction2 -= BatchMoveCall;
        PieMenu.Instance.OnAction3 -= CallEmpty;
        PieMenu.Instance.OnAction3 -= ExpandPersonal;
        PieMenu.Instance.OnAction4 -= SwitchToPersonal;
    }
    #endregion

    #region Action Methods    
    // Move Functions
    private void StartProximityMove()
    {
        this.waitForAction = false;
        if (GetComponent<OrbitalPersonal>().enabled)
        {
            OrbitalPersonalRing.Instance.RemoveCollection(GetComponent<OrbitalPersonal>());
        }
        else
        {
            //this.hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();
        }

        this.moveOrdered = true;
        this.MoveOrderedCenterHead = false;
        this.isPersonal = false;

        this.deltaDistance = (this.pointer.Position - Camera.main.transform.position).normalized;
        this.itemDistance = 0.3f;

        ResetPieMenuOptions();

        PieMenu.Instance.Hide();

        if (!this.isRegistered)
        {
            this.isRegistered = true;
            CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
        }
    }

    private void StartHeadMove(bool isDragged)
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS && isDragged)
            Measurement.Instance.MovePlus();

        this.waitForAction = false;
        this.batchOffset = Vector3.zero;
        this.adjustmentRatio = Vector3.one;

        if (GetComponent<OrbitalPersonal>().enabled)
        {
            OrbitalPersonalRing.Instance.RemoveCollection(GetComponent<OrbitalPersonal>());
        }
        else if(!this.isSelected)
        {
            this.hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();
        }

        this.MoveOrderedCenterHead = true;
        this.moveOrdered = false;

        this.deltaDistance = Camera.main.transform.forward * 0.2f;
        this.itemDistance = Vector3.Distance(this.transform.position, this.cameraTr.position);

        foreach (Collider c in this.hostTransform.GetComponentsInChildren<Collider>())
        {
            c.enabled = false;            
        }


        ResetPieMenuOptions();
        StopPieMenu();

        if (!(this.currentSlateState == SlateState.Drag) || this.activeMenu)
        {
            if (!this.isRegistered)
            {
                this.isRegistered = true;
                CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
            }
        }
    }

    private void BatchMoveCall()
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
        {
            if (this.isPersonal)
                Measurement.Instance.BatchMoveFromPSPlus();
            else if (this.isSelected)
                Measurement.Instance.BatchMoveSelectionPlus();
            else if (this.Magnet.name != "Wall")
                Measurement.Instance.BatchMoveSurfacePlus();
        }   

        this.waitForAction = false;

        this.moveOrdered = false;
        this.MoveOrderedCenterHead = false;      

        if (this.isPersonal)
            OrbitalPersonalRing.Instance.BatchMove();
        else if (this.isSelected)
            SelectionController.Instance.MoveSelection(this.gameObject);
        else if (this.Magnet.name != "Wall")
            this.Magnet.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().BatchMove();

        SelectionController.Instance.IsDragging(true);

        this.logBatch = true;
    }

    public void BatchHeadMove(Vector3 vec3BatchOffset, int numberOfElements, float initialHeight, float initialWidth, int zId)
    {
        this.initialHeight = initialHeight;
        this.initialWidth = initialWidth;
        this.adjustmentRatio = Vector3.one;
        this.zId = zId;

        SetupDrag();
        UpdateStateMachine(PointerAction.OrderDrag, null);
        this.isBatch = true;
        this.logNumberOfElement = numberOfElements;
        
        StartHeadMove(false);
        this.batchOffset = vec3BatchOffset;
    }

    // Personal Space Function
    private void SwitchToPersonal()
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
            Measurement.Instance.MoveToPSPlus();        

        this.hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();

        ResetPieMenuOptions();
        StopPieMenu();
        
        OrbitalPersonalRing.Instance.AddToCollection(GetComponent<OrbitalPersonal>());
        
        GetComponent<SlateAuthorViewRights>().SwitchSurface(this.Magnet, SynchroManager.Instance.myGameObjectPosition);
        
        this.isPersonal = true;
        this.isAttracted = true;
        this.Magnet = SynchroManager.Instance.myGameObjectPosition;
        
        this.changeSurfaceMessage.surfaceName = SynchroManager.Instance.myGameObjectPosition.name;
        this.hostTransform.GetComponent<VersioningNotifications>().CancelVisual();

        //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
        SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
    }

    public void SwitchToPersonalBatch()
    {
        if (this.isSelected)
            SelectItem();

        this.hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();

        OrbitalPersonalRing.Instance.AddToCollection(GetComponent<OrbitalPersonal>(), this.HostTransform.transform.localPosition);

        GetComponent<SlateAuthorViewRights>().SwitchSurface(this.Magnet, SynchroManager.Instance.myGameObjectPosition);

        this.isPersonal = true;
        this.isAttracted = true;
        this.Magnet = SynchroManager.Instance.myGameObjectPosition;

        this.changeSurfaceMessage.surfaceName = SynchroManager.Instance.myGameObjectPosition.name;
        //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
        SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
    }

    public void SwitchToPersonalRemote()
    {
        this.hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();

        OrbitalPersonalRing.Instance.AddToCollection(GetComponent<OrbitalPersonal>());

        this.isPersonal = true;
        this.isAttracted = true;
        this.Magnet = SynchroManager.Instance.myGameObjectPosition;

        this.isPersonal = true;
    }

    private void ExpandPersonal()
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
        {
            Measurement.Instance.ExpandPSPlus();
            Measurement.Instance.WindowCreatedPlus();
        }
        OrbitalPersonalRing.Instance.Expand();
    }

    // On Window functions
    public void CallEmpty()
    {       
        StopPieMenu();
        if (this.isSelected)
        {
            SelectionController.Instance.StoreSelectionToPS();
        }
        else
            this.transform.parent.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().Empty();
    }
    
    public void ExpandAction(GameObject newMagnet)
    {
        GetComponent<SlateAuthorViewRights>().SwitchSurface(this.Magnet, newMagnet);
        this.isPersonal = false;
        this.isAttracted = true;
        this.Magnet = newMagnet;

        this.changeSurfaceMessage.surfaceName = newMagnet.name;
        //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);

        this.Magnet.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().Deactivate();

        SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
    }

    private void SetupDrag()
    {
        this.preDragParent = this.hostTransform.parent;
        this.preDragPos = this.hostTransform.position;
        this.preDragRot = this.hostTransform.rotation;
    }

    private void PlaceSlate()
    {
        RaycastHit r;

        // In case it did not hit the right thing    
        if (!this.isAttracted)// || !Physics.Raycast(this.gazeProvider.GazeOrigin, this.gazeProvider.GazeDirection, Mathf.Infinity, LayerMask.NameToLayer("Surface") << LayerMask.NameToLayer("Surface")))
        {
            this.isAttracted = true;

            this.hostTransform.GetComponent<SlateAuthorViewRights>().OnSurface(this.preDragParent.gameObject);
            this.Magnet = this.preDragParent.gameObject;

            this.changeSurfaceMessage.surfaceName = this.preDragParent.name;
            //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
            SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);

            this.HostTransform.parent = this.preDragParent;

            if (this.isPersonal)
            {
                this.changeSurfaceMessage.surfaceName = SynchroManager.Instance.myGameObjectPosition.name;
                OrbitalPersonalRing.Instance.AddToCollection(GetComponent<OrbitalPersonal>());
            }
            else
            {
                StartCoroutine(ReplaceAtStart(this.preDragPos, this.preDragRot, 0.1f));
            }

            this.moveOrdered = false;
            this.MoveOrderedCenterHead = false;
        }
        else
        {
            if (this.Magnet != null && this.Magnet.name != "Wall")
            {
                this.Magnet.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().Deactivate();

                if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
                {
                    Measurement.Instance.ItemsInWindowPlus();
                    Measurement.Instance.MaxInWindow(this.Magnet.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().HostTransform.childCount - 1);
                }
            }

            if (this.isPersonal)
            {
                this.GetComponent<OrbitalPersonal>().OutOfBelt();
            }
            
            Collider[] collisions = Physics.OverlapSphere(this.transform.position, this.transform.localScale.y / 2f);
            if (collisions.Length > 1)
            {
                float zPos = 0f;
                foreach (Collider c in collisions)
                {
                    if (c.tag != "Slate")
                        continue;
                    zPos = Mathf.Min(c.transform.parent.localPosition.z, zPos);
                }
                this.transform.localPosition += new Vector3(0f, 0f, 0.001f + zPos - 0.00002f);
            }

            //Debug.Log(this.Magnet + "  " + this.gazeProvider);
            this.hostTransform.localRotation = Vector3.Dot(this.Magnet.transform.forward, this.gazeProvider.transform.forward) >= 0
                ? Quaternion.identity
                : new Quaternion(0, 1, 0, 0);

            this.adjustmentRatio = Vector3.one;
            this.moveOrdered = false;
            this.MoveOrderedCenterHead = false;
            this.isAdjusted = false;
            this.initialHeight = 0;
            this.initialWidth = 0;

            this.GetComponent<AuthorViewRights>().LetAuthoring(false);
        }

        foreach (Collider c in this.hostTransform.GetComponentsInChildren<Collider>())
        {
            c.enabled = true;
        }

        if (this.isRegistered)
        {
            this.isRegistered = false;
            CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);
        }

        if (this.isSelected)
        {
            SelectItem(false);
        }

        this.batchOffset = Vector3.zero;
        SelectionController.Instance.IsDragging(false);
    }

    private bool HeadDeviation(Vector3 memoryPosition)
    {
        // Set so this function detects head deviation superior to 2 degrees in amplitude
        return (Vector3.Angle(memoryPosition - Camera.main.transform.position, this.gazeProvider.HitPosition - Camera.main.transform.position) > this.angleForMove);
    }
    #endregion

    #region Coroutine Helpers
    IEnumerator WaitForAction()
    {
        yield return new WaitForSeconds(0.5f);
        if (this.waitForAction)
            CancelPieMenu();
        this.waitForAction = false;
    }

    IEnumerator ReplaceAtStart(Vector3 startPos, Quaternion startRot, float time)
    {
        float timeSpent = 0f;
        while (timeSpent < time)
        {
            float lerpAmount = GetLerpAmount();
            Quaternion smoothedRotation = Quaternion.Lerp(this.hostTransform.rotation, this.preDragRot, lerpAmount);
            Vector3 smoothedPosition = Vector3.Lerp(this.hostTransform.position, this.preDragPos, lerpAmount);
            this.hostTransform.SetPositionAndRotation(smoothedPosition, smoothedRotation);

            timeSpent += Time.deltaTime;
            yield return null;
        }

        this.hostTransform.SetPositionAndRotation(this.preDragPos, this.preDragRot);
        //this.hostTransform.GetComponent<AuthorViewRights>().LetAuthoring();
    }
    #endregion

    #region IMixedRealitySourceStateHandler Methods
    public void OnSourceDetected(SourceStateEventData eventData)
    {
        if (this.moveOrdered || this.MoveOrderedCenterHead)
        {
            this.isLost = false;
            this.pointer = eventData.InputSource.Pointers.First();
        }
    }

    public void OnSourceLost(SourceStateEventData eventData)
    {
        this.pointer = null;
        if (this.moveOrdered || this.MoveOrderedCenterHead)
            this.isLost = true;

        if (this.currentSlateState == SlateState.Drag && this.currentSlateState == SlateState.DragGroup)
        {
            this.isAttracted = true;

            this.hostTransform.GetComponent<SlateAuthorViewRights>().OnSurface(this.preDragParent.gameObject);
            this.Magnet = this.preDragParent.gameObject;

            this.changeSurfaceMessage.surfaceName = this.preDragParent.name;
            //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);
            SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);

            this.HostTransform.parent = this.preDragParent;

            if (this.isPersonal)
            {
                this.changeSurfaceMessage.surfaceName = SynchroManager.Instance.myGameObjectPosition.name;
                OrbitalPersonalRing.Instance.AddToCollection(GetComponent<OrbitalPersonal>());
            }
            else
            {
                StartCoroutine(ReplaceAtStart(this.preDragPos, this.preDragRot, 0.1f));
            }

            this.moveOrdered = false;
            this.MoveOrderedCenterHead = false;

            foreach (Collider c in this.hostTransform.GetComponentsInChildren<Collider>())
            {
                c.enabled = true;
            }
        }
    }
    #endregion

    #region IFocusHandler Methods
    public override void SelectItem()
    {
        base.SelectItem();
        if (this.isSelected)
        {
            Measurement.Instance.SelectPlus();
            this.GetComponent<VersioningNotifications>().HasSelection(SynchroManager.Instance.fullOwners.IndexOf(int.Parse(SynchroManager.Instance.ownerId)));
            this.selectionOwner = SynchroManager.Instance.ownerId;
        }
        else
            this.GetComponent<VersioningNotifications>().LoseSelection();
    }

    public override void SelectItem(bool selectState)
    {
        if (this.isSelected == selectState)
            return;
        SelectItem();
    }

    public override void RemoteSelectItem(bool selectState, string owner)
    {
        // 1 - > check by whom it was selected
        // 2 - > If we select check that if we override, we remove/add to corresponding Selection Control
        // 3 - > If we do not select : Remove the item from selection and make sure it's out of SelectionControl

        if (selectState)
        {
            if (this.isSelected && this.selectionOwner != owner && this.selectionOwner == SynchroManager.Instance.ownerId)
                SelectionController.Instance.RemoveFromSelection(this.gameObject);

            this.isSelected = true;
            this.GetComponent<VersioningNotifications>().HasSelection(SynchroManager.Instance.fullOwners.IndexOf(int.Parse(owner)));
            this.selectionOwner = owner;
        }
        else
        {
            if (this.isSelected && this.selectionOwner == SynchroManager.Instance.ownerId)
                SelectionController.Instance.RemoveFromSelection(this.gameObject);

            this.GetComponent<VersioningNotifications>().LoseSelection();
            this.isSelected = false;
        }
    }

    private void SelectItemFromMenu()
    {
        SelectItem();
        ResetPieMenuOptions();
        StopPieMenu();
    }

    public override void OnFocusChanged(FocusEventData eventData)
    {
        base.OnFocusChanged(eventData);

        this.hasFocus = eventData.NewFocusedObject != null && this.transform.Find(eventData.NewFocusedObject.name) != null 
            ? true 
            : false;

        if (this.hasFocus && PieMenu.Instance.Exist() && !this.activeMenu && !this.isRegistered)
        {
            this.isRegistered = true;
            CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
        }
        else if (!this.hasFocus && !this.activeMenu && this.isRegistered && !(this.currentSlateState == SlateState.DragGroup))
        {
            //OFF FOCUS DURING ACCESS TO PIE MENU FOR BATCH MOVE APPARENTLY SCREWS THE REGISTERING
            this.isRegistered = false;
            CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);
        }
    }

    public void OnFocusEnter(FocusEventData eventData)
    {
        //Debug.Log("Focus");       
        this.hostTransform.GetComponent<AuthorViewRights>().HasFocus();
        this.hasFocus = true;
    }

    public void OnFocusExit(FocusEventData eventData)
    {
        //if(!this.isSelected && !this.isPersonal) this.hostTransform.GetComponent<AuthorViewRights>().LetAuthoring();
        this.hostTransform.GetComponent<AuthorViewRights>().LoseFocus();
        if (this.isPersonal) this.hostTransform.GetComponent<VersioningNotifications>().CancelVisual();
        this.hasFocus = false;        
    }
    #endregion

    public void SwitchSurfaceTo(GameObject newSurface)
    {
        if (this.Magnet == null)
            this.hostTransform.GetComponent<SlateAuthorViewRights>().OnSurface(newSurface);
        else
            this.hostTransform.GetComponent<SlateAuthorViewRights>().SwitchSurface(this.Magnet, newSurface);
        this.Magnet = newSurface;

        this.changeSurfaceMessage.surfaceName = newSurface.name;
        //SynchroServer.Instance.SendCommand("H", this.changeSurfaceMessage);

        if(this.currentSlateState == SlateState.DragGroup && newSurface.tag == "MagneticSurface")
        {
            //Debug.Log("Switch " + newSurface.name);
        }

        SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
    }

    public override void ChangeMagnet(GameObject g)
    {
        if (g == null)
        {
            PieMenuAdvancedClickSurfaceInteraction pmacsi = this.Magnet?.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>();
            if(pmacsi != null)
            {
                pmacsi.UpdateContent(-1);
            }

            this.isAttracted = false;
            this.Magnet = null;
        }
        else
        {
            PieMenuAdvancedClickSurfaceInteraction pmacsi = this.Magnet?.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>();
            if (pmacsi != null)
            {
                pmacsi.UpdateContent(-1);
            }

            this.isAttracted = true;
            this.Magnet = g;

            pmacsi = this.Magnet?.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>();
            if (pmacsi != null)
            {
                pmacsi.UpdateContent(0);
            }
        }

    }

    protected override void Magnetism(float max_dist, ref Vector3 position, ref Quaternion rotation)
    {

        // Manage magnetism when multiple magnet exists 
        int minIndex = 0;
        float distance;
        Vector3 ClosestToCenter;

        GameObject[] Magnets = GameObject.FindGameObjectsWithTag("MagneticSurface");
        Vector3 OnMagnet = Magnets[0].GetComponentInChildren<Collider>().ClosestPoint(position);

        float minDist = Vector3.Distance(OnMagnet, position);

        // Find the magnet closest to the transform 
        for (int i = 1; i < Magnets.Length; i++)
        {
            OnMagnet = Magnets[i].GetComponentInChildren<Collider>().ClosestPoint(position);

            distance = Vector3.Distance(OnMagnet, position); ;
            if (distance < minDist)
            {
                minDist = distance;
                minIndex = i;
            }
        }

        // Calculate the true position for item
        GameObject ClosestMagnet = Magnets[minIndex];
        OnMagnet = ClosestMagnet.GetComponentInChildren<Collider>().ClosestPoint(position);
        ClosestToCenter = ClosestMagnet.transform.position - OnMagnet;

        Vector3 objOnMagnet = Vector3.Project(ClosestToCenter, ClosestMagnet.transform.forward) + OnMagnet;

        // Operate Magnet effect on the object
        if (Vector3.Distance(objOnMagnet, position) <= max_dist)
        {
            var4 = position;
            // Case where item goes on a surface
            if (this.isAttracted == false)
            {
                this.Magnet = ClosestMagnet;
                this.hostTransform.GetComponent<SlateAuthorViewRights>().OnSurface(ClosestMagnet);                
                this.changeSurfaceMessage.surfaceName = ClosestMagnet.name;
                
                if(this.currentSlateState == SlateState.DragGroup)
                {
                    isAdjusted = AdjustBatch(ClosestMagnet);
                }

                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);                
            }
            // Case where item changes surface without going through the 3D space
            else if (this.isAttracted == true && ClosestMagnet != this.Magnet)
            {
                this.hostTransform.GetComponent<SlateAuthorViewRights>().SwitchSurface(this.Magnet, ClosestMagnet);                
                this.Magnet = ClosestMagnet;                

                this.changeSurfaceMessage.surfaceName = ClosestMagnet.name;
                
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }

            this.isAttracted = true;
            if (this.Magnet.tag == "MagneticSurface" && this.Magnet.name != "Wall")
            {

                Vector3 cursorProjX = Vector3.Project(position, Magnet.transform.right) - Vector3.Project(Magnet.transform.position, Magnet.transform.right);
                Vector3 cursorProjY = Vector3.Project(position, Magnet.transform.up) - Vector3.Project(Magnet.transform.position, Magnet.transform.up);

                float capX = (Magnet.transform.GetChild(0).localScale.x - this.initialWidth - this.transform.GetChild(0).localScale.x * this.transform.GetComponentInChildren<BoxCollider>().size.x / 2f) / 2f;
                float capY = (Magnet.transform.GetChild(0).localScale.y - this.initialHeight - this.transform.GetChild(0).localScale.y * this.transform.GetComponentInChildren<BoxCollider>().size.y) / 2f;

                Vector3 computedX = (capX < 0) ? Vector3.zero : Mathf.Clamp(Vector3.Dot(cursorProjX, Magnet.transform.right), -capX, capX) * Magnet.transform.right;
                Vector3 computedY = (capY < 0) ? Vector3.zero : Mathf.Clamp(Vector3.Dot(cursorProjY, Magnet.transform.up), -capY, capY) * Magnet.transform.up;

                var3 = computedX;
                var4 = computedY;

                position = Magnet.transform.position + computedX + computedY - this.zId * Magnet.transform.forward * 0.001f;

            }
            else
            {
                position = objOnMagnet - this.Magnet.transform.forward * 0.001f;
            }            
            rotation = (Vector3.Dot(ClosestMagnet.transform.forward, this.hostTransform.forward) > 0f) ? ClosestMagnet.transform.rotation : Quaternion.Inverse(ClosestMagnet.transform.rotation);
        }
        else
        {
            // Case where item is in 3D space and goes off a surface
            if (this.isAttracted == true)
            {
                this.hostTransform.GetComponent<SlateAuthorViewRights>().OffSurface(this.Magnet);

                this.Magnet = null;
                this.changeSurfaceMessage.surfaceName = null;

                if (this.currentSlateState == SlateState.DragGroup)
                {
                    isAdjusted = AdjustBatch(null);
                }

                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }
            this.isAttracted = false;
        }

        //Debug.Log("Parent at all time " + this.HostTransform.parent + " dists : " + Vector3.Distance(objOnMagnet, position) + " distmax " + max_dist + " Closest mag " + ClosestMagnet.name + " this mag " + this.Magnet.name);
    }

    bool AdjustBatch(GameObject target)
    {
        if(target == null || target.name == "Wall") 
        {
            this.adjustmentRatio = new Vector3(1f, 1f, 0f);
        }            
        else if(target.tag == "MagneticSurface" && target.name != "Wall")
        {
            bool output = false;
            this.adjustmentRatio = new Vector3(1f, 1f, 1f);

            if (target.transform.GetChild(0).localScale.x < this.initialWidth)
            {
                this.adjustmentRatio.x = target.transform.GetChild(0).localScale.x / (this.initialWidth + transform.localScale.x);
                output = true;
            }

            if (target.transform.GetChild(0).localScale.y < this.initialWidth)
            {
                this.adjustmentRatio.y = target.transform.GetChild(0).localScale.y / (this.initialHeight + transform.localScale.y);
                output = true;
            }

            return output;
        }
        return false;
    }

    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        // var1 = objOnMagnet;
        Gizmos.color = Color.yellow;
        if (var1 != Vector3.zero) Gizmos.DrawSphere(var1, 0.1f);

        // var2 = smoothedPosition;
        Gizmos.color = Color.blue;
        if (this.Magnet != null && this.Magnet.transform.position != Vector3.zero) Gizmos.DrawSphere(this.Magnet.transform.position, 0.1f);

        // var3 = surfaceNormal * fromCursorOffset;
        Gizmos.color = Color.red;
        if (var3 != Vector3.zero) Gizmos.DrawSphere(var3, 0.1f);

        // var4 = targetPosition
        Gizmos.color = Color.green;
        if (var4 != Vector3.zero) Gizmos.DrawSphere(var4, 0.1f);

    }

    public bool IsMoving()
    {
        return this.MoveOrderedCenterHead;
    }

    [Button]
    public void Status()
    {
        Debug.Log(" activeMenu " + this.activeMenu + " moveOrdered " + this.moveOrdered + " headMoveOrdered " + this.MoveOrderedCenterHead + " isPErsonal " + this.isPersonal + " isLost " + this.isLost + " isSelected " + this.isSelected);
    }

    [Button]
    public void SelectBy()
    {
        SynchroClient.Instance.TESTOWNE(180, this.name);
    }  
}

