using Microsoft.MixedReality.Toolkit;
using Synchro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Measurement : Singleton<Measurement>
{
    string ownIp;

    #region private Properties
    // Time
    float timeStart = 0f;

    // Action variables
    int nbrMove = 0;
    int nbrMoveToPS = 0;

    // -- Action from Surface
    int nbrBatchMoveSurface = 0;
    int nbrEmptySurface = 0;

    // -- Action from Selection
    int nbrSelect = 0;
    int nbrBatchMoveSelection = 0;
    int nbrEmptySelection = 0;

    // -- Action from PS
    int nbrExpandPS = 0;
    int nbrBatchMoveFromPS = 0;


    // Window Action variables
    int nbrRelayout = 0;
    int nbrWindowCreated = 0;
    int nbrWindowDestroyed = 0;
     
    // Personal Space variables
    int totToPSMoves = 0;
    int totFromPSMoves = 0;
    int maxNbrItemInPS = 0;

    // Window *Stats* variables
    int totItemsInWindow = 0;
    int maxItemsInWindow = 0;

    // Movement data
    float totWalkDistance = 0;
    float totItemMoveDist = 0;
    float totWindowMoveDist = 0;
    float totCursorMoveDist = 0;
    float avgWallDist = 0;
    float avgPlayerDistance = 0f;

    // Movement data Helpers
    Vector3 oldWalkPos;
    Vector3 oldItemPos;
    Vector3 oldWindowPos;
    Vector3 oldCursorPos;

    float deltaMov = 0.05f;

    string history = "";
    #endregion

    #region Public Measurement Methods
    // Option Action Incrementers
    public void MovePlus()
    {
        this.nbrMove++;
        this.history += ownIp + " " + Time.time + " Move " + '\n';
    }
    public void MoveToPSPlus()
    {
        this.nbrMoveToPS++;
        this.history += ownIp + " " + Time.time + " MoveToPersonalSpace " + '\n';
    }

    // Selection Action Incrementers
    public void SelectPlus()
    {
        this.nbrSelect++;
        this.history += ownIp + " " + Time.time + " Select" + '\n';
    }
    public void BatchMoveSelectionPlus()
    {
        this.nbrBatchMoveSelection++;
        this.history += ownIp + " " + Time.time + " BatchMoveSelection" + '\n';
    }
    public void EmptySelectionPlus()
    {
        this.nbrEmptySelection++;
        this.history += ownIp + " " + Time.time + " EmptySelection " + '\n';
    }

    // Window Action Incrementers
    public void BatchMoveSurfacePlus()
    {
        this.nbrBatchMoveSurface++;
        this.history += ownIp + " " + Time.time + " BatchMoveSurface" + '\n';
    }
    public void EmptySurfacePlus()
    {
        this.nbrEmptySurface++;
        this.history += ownIp + " " + Time.time + " EmptySurface " + '\n';
    }
    public void RelayoutPlus()
    {
        this.nbrRelayout++;
        this.history += ownIp + " " + Time.time + " Relayout " + '\n';
    }
    public void WindowCreatedPlus()
    {
        this.nbrWindowCreated++;
        this.history += ownIp + " " + Time.time + " CreateWindow " + '\n';
    }
    public void WindowDestroyedPlus()
    {
        this.nbrWindowDestroyed++;
        this.history += ownIp + " " + Time.time + " DestroyWindow " + '\n';
    }

    // Personal Space Incrementers
    public void ToPSPlus()
    {
        this.totToPSMoves++;
    }
    public void FromPSPlus()
    {
        this.totFromPSMoves++;
    }
    public void MaxPSItems(int max)
    {
        if (max > this.maxNbrItemInPS)
            this.maxNbrItemInPS = max;
    }
    public void BatchMoveFromPSPlus()
    {
        this.nbrBatchMoveFromPS++;
        this.history += ownIp + " " + Time.time + " BatchMoveFromPersonalSpace " + '\n';
    }
    public void ExpandPSPlus()
    {
        this.nbrExpandPS++;
        this.history += ownIp + " " + Time.time + " ExpandPersonalSpace " + '\n';
    }

    // Window *Stats* Methods
    public void ItemsInWindowPlus()
    {
        this.totItemsInWindow++;
    }
    public void MaxInWindow(int max)
    {
        this.maxItemsInWindow = Mathf.Max(this.maxItemsInWindow, max);
    }

    // Movement Methods //TODO FROM HERE
    public void WalkDistanceAdding(Vector3 newPos)
    {
        float dist = Vector3.Distance(this.oldWalkPos, newPos);
        if (dist >= this.deltaMov)
            this.totWalkDistance += dist;
        this.oldWalkPos = newPos;
    }
    public void ItemDistanceAdding(Vector3 newPos)
    {
        float dist = Vector3.Distance(this.oldItemPos, newPos);
        if(dist >= this.deltaMov)
            this.totItemMoveDist += dist;
        this.oldItemPos = newPos;
    }
    public void ItemBatchDistanceAdding(Vector3 newPos, int numberOfItems)
    {
        float dist = Vector3.Distance(this.oldItemPos, newPos);
        if (dist >= this.deltaMov)
            this.totItemMoveDist += dist * numberOfItems;
        this.oldItemPos = newPos;
    }
    public void WindowDistanceAdding(Vector3 newPos)
    {
        float dist = Vector3.Distance(this.oldWindowPos, newPos);
        if (dist >= this.deltaMov)
            this.totWindowMoveDist += dist;
        this.oldWindowPos = newPos;
    }
    public void CursorDistanceAdding(Vector3 newPos)
    {
        float dist = Vector3.Distance(this.oldCursorPos, newPos);
        if (dist >= this.deltaMov)
            this.totCursorMoveDist += dist;
        this.oldCursorPos = newPos;
    }
    public void DistanceToWallAdding(float newDist)
    {
        avgWallDist += newDist / 10f;
    }
    public void DistanceInterPlayerAdding(Vector3 p1, Vector3 p2)
    {
        avgPlayerDistance += Vector3.Distance(p1, p2 ) / 10f;
    }

    // Movement Methods Helpers
    public void WalkDistanceStartSetter(Vector3 newPos)
    {
        this.oldWalkPos = newPos;
    }
    public void ItemDistanceStartSetter(Vector3 newPos)
    {
        this.oldItemPos = newPos;
    }
    public void WindowDistanceStartSetter(Vector3 newPos)
    {
        this.oldWindowPos = newPos;
    }
    public void CursorDistanceStartSetter(Vector3 newPos)
    {
        this.oldCursorPos = newPos;
    }
    public void DistanceToWallStartSetter()
    {
        this.avgWallDist = 0;
    }
    public void DistanceInterPlayerSetter()
    {
        avgPlayerDistance = 0f;
    }

    // Cursor System
    private void GetCursor()
    {
        CursorDistanceAdding(CoreServices.InputSystem.GazeProvider.GazeCursor.Position);        
    }
    #endregion

    #region LogFile Generation Methods
    // Writing Methods
    public string TitleGeneration()
    {
        string[] fixedParameters = { };
        string[] variables = { };        

        return string.Join(";", fixedParameters) + ";" +  string.Join(";", variables);
    }
    public string DataGeneration()
    {
        string[] fixedParametersValues = { };
        string[] variablesValues = { };

        return string.Join(";", fixedParametersValues) + ";" + string.Join(";", variablesValues);
    }
    #endregion

    #region MonoBehaviour Functions
    protected void Start()
    {
        this.ownIp = SynchroManager.Instance.ownerId;
        CursorDistanceStartSetter(CoreServices.InputSystem.GazeProvider.GazeCursor.Position);
        InvokeRepeating("GetCursor", 0f, 0.1f);
    }
    protected void Update()
    {
    }

    public override string ToString()
    {
        /*
        return "Measurement : " + " nbrMove = " + this.nbrMove + " nbrMoveToPS = " + this.nbrMoveToPS  + " nbrSelect = " + this.nbrSelect 
            + " nbrBatchMoveSelection = " + this.nbrBatchMoveSelection + " nbrBatchMoveSurface = " + this.nbrBatchMoveSurface + " nbrBatchMoveFromPS = " + this.nbrBatchMoveFromPS 
            + " nbrExpandPS = " + this.nbrExpandPS + " nrbEmptySurface = " + this.nbrEmptySurface + " nrbEmptySelection = " + this.nbrEmptySelection + "\n" 
            + " nbrRelayout = " + this.nbrRelayout + " nbrWindowCreated = " + this.nbrWindowCreated + " nbrWindowDestroyed = " + this.nbrWindowDestroyed 
            + " totToPSMoves = " + this.totToPSMoves + " totFromPSMoves = " + this.totFromPSMoves +  " maxNbrItemInPS = " + this.maxNbrItemInPS + "\n" 
            + " totItemsInWindow = " + this.totItemsInWindow + " maxItemsInWindow = " + this.maxItemsInWindow + "\n"
            + " totWalkDistance = " + this.totWalkDistance + " totItemMoveDistance = " + this.totItemMoveDist + " totWindowMoveDistance = " + this.totWindowMoveDist + " totCursorMoveDist = " + this.totCursorMoveDist;       
        */
        return this.ownIp + " " + this.nbrMove + " " + this.nbrMoveToPS + " " + this.nbrSelect + " " + this.nbrBatchMoveSelection + " " + this.nbrBatchMoveSurface + " " + this.nbrBatchMoveFromPS + " "
            + this.nbrExpandPS + " " + this.nbrEmptySurface + " " + this.nbrEmptySelection + " " + this.nbrRelayout + " " + this.nbrWindowCreated + " " + this.nbrWindowDestroyed + " "
            + this.totToPSMoves + " " + this.totFromPSMoves + " " + this.maxNbrItemInPS + " " + this.totItemsInWindow + " " + this.maxItemsInWindow + " "
            + this.totWalkDistance + " " + this.totItemMoveDist + " " + this.totWindowMoveDist + " " + this.totCursorMoveDist + " " + this.avgWallDist;
    }

    public void StartSession()
    {
        timeStart = Time.time;

        this.nbrMove = 0;
        this.nbrMoveToPS = 0;

        //-- Action from Surface
        this.nbrBatchMoveSurface = 0;
        this.nbrEmptySurface = 0;

        //-- Action from Selection
        this.nbrSelect = 0;
        this.nbrBatchMoveSelection = 0;
        this.nbrEmptySelection = 0;

        //-- Action from PS
        this.nbrExpandPS = 0;
        this.nbrBatchMoveFromPS = 0;


        // Window Action variables
        this.nbrRelayout = 0;
        this.nbrWindowCreated = 0;
        this.nbrWindowDestroyed = 0;

        // Personal Space variables
        this.totToPSMoves = 0;
        this.totFromPSMoves = 0;
        this.maxNbrItemInPS = 0;

        // Window *Stats* variables
        this.totItemsInWindow = 0;
        this.maxItemsInWindow = 0;

        // Movement data
        this.totWalkDistance = 0;
        this.totItemMoveDist = 0;
        this.totWindowMoveDist = 0;
        this.totCursorMoveDist = 0;
        this.avgWallDist = 0;

        WalkDistanceStartSetter(this.oldWalkPos);
        ItemDistanceStartSetter(this.oldItemPos);
        WindowDistanceStartSetter(this.oldWindowPos);
        CursorDistanceStartSetter(this.oldCursorPos);
        DistanceToWallStartSetter();
        DistanceInterPlayerSetter();
    }

    public void EndSession()
    {
        Debug.Log(avgWallDist + " " + (Time.time - timeStart));
        this.avgWallDist = this.avgWallDist / (Time.time - timeStart);
        Debug.Log(avgWallDist);
        SessionResults sr = new SessionResults(SynchroManager.Instance.ownerId, this.ToString(), this.history);
        SynchroManager.Instance.CommandInstance.Cmd_SessionResults(SynchroManager.Instance.ownerId, this.ToString(), this.history);
    }

    private void OnDestroy()
    {
        
    }
    #endregion
}
