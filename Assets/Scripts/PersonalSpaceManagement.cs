﻿using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using Synchro;
using System.Linq;
using Microsoft.MixedReality.Toolkit.UI;

public class PersonalSpaceManagement : OrbitalPersonalRing
{
    public bool isWindowAttached = false;
    private PieMenuAdvancedClickSurfaceInteraction personalWindow = null;
    public override PieMenuAdvancedClickSurfaceInteraction PersonalWindow
    {
        get { return this.personalWindow; }
        set
        {
            if (value != null)
            {
                if (!this.isWindowAttached)
                {
                    this.isWindowAttached = true;                    
                }
                else if (this.personalWindow != value)
                {
                    this.isWindowAttached = true;                    
                }
                else
                {
                    this.isWindowAttached = true;
                }
            }
            else
            {
                this.isWindowAttached = false;
            }
            this.personalWindow = value;
        }
    }

    private UnityEngine.Object template;

    private float startAngle = 0f;
    private float PSangle = 25f;
    private ActivatePersonalSpace aps;
    private bool isLookingDown = false;
    private bool IsLookingDown
    {
        get
        {
            return this.isLookingDown;
        }
        set
        {
            this.aps.activate = value;
            this.isLookingDown = value;
            //SynchroServer.Instance.SendCommand("H", aps);
            SynchroManager.Instance.CommandInstance.Cmd_ActivatePersonalSpace(aps.owner, aps.activate);
        }
    }
    private float relativePos;
    private List<Vector3> onPersonalWindowPos = new List<Vector3>();

    private void Start()
    {
        this.head = Camera.main.gameObject;
        this.template = Resources.Load("FloatingWindowQuad");

        this.relativePos = 1.5f;

        this.aps = new ActivatePersonalSpace(SynchroManager.Instance.ownerId, false);
    }

    private void Update()
    {
        if (!this.IsLookingDown && Vector3.Angle(this.head.transform.up, Vector3.up) > this.PSangle && this.personalCollection.Count > 0)
        {
            this.startAngle = Vector3.SignedAngle(Vector3.forward, Vector3.ProjectOnPlane(this.head.transform.forward, Vector3.up), Vector3.up);
            this.IsLookingDown = true;
            RepositionItems(this.deltaAngle, this.startAngle);
            //Debug.Log(this.startAngle);
        }
        else if (this.IsLookingDown && Vector3.Angle(this.head.transform.up, Vector3.up) <= this.PSangle)
        {
            this.IsLookingDown = false;
        }
    }

    public override void AddToCollection(OrbitalPersonal item)
    {
        if(SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
            Measurement.Instance.ToPSPlus();
        
        if(this.personalCollection.Count == 0)
        {
            this.deltaAngle = Quaternion.FromToRotation(Vector3.forward, (item.transform.position - this.transform.position).normalized).eulerAngles.y;
        }

        int number = this.onPersonalWindowPos.Count;
        int Xnum = Mathf.FloorToInt(number % (Mathf.Ceil(this.lineSize / this.stepX) + 1));
        int Ynum = Mathf.FloorToInt(number / (Mathf.Ceil(this.lineSize / this.stepX) + 1));
        Vector3 startPos = new Vector3(-this.lineSize / 2f + Xnum * this.stepX, this.heightSize / 2f - Ynum * this.stepY, 0f);
        startPos = Vector3.one;
        this.onPersonalWindowPos.Add(startPos);        
        
        item.enabled = true;
        item.InBelt();
        this.personalCollection.Add(item);
        RepositionItems(this.deltaAngle, this.startAngle);

        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
            Measurement.Instance.MaxPSItems(this.personalCollection.Count);
    }

    public override void AddToCollection(OrbitalPersonal item, Vector3 pos)
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
            Measurement.Instance.ToPSPlus();

        if (this.personalCollection.Count == 0)
        {
            this.deltaAngle = Quaternion.FromToRotation(Vector3.forward, (item.transform.position - this.transform.position).normalized).eulerAngles.y;
        }

        this.onPersonalWindowPos.Add(pos);     

        item.enabled = true;

        this.personalCollection.Add(item);
        RepositionItems(this.deltaAngle, this.startAngle);

        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
            Measurement.Instance.MaxPSItems(this.personalCollection.Count);
    }

    public override void RemoveCollection(OrbitalPersonal item)
    {
        item.enabled = false;

        int i = this.personalCollection.IndexOf(item);
        this.personalCollection.RemoveAt(i);
        this.onPersonalWindowPos.RemoveAt(i);

        RepositionItems(this.deltaAngle, this.startAngle);
    }    

    [Button]
    public override void Expand()
    {
        GameObject g = (GameObject)Instantiate(this.template);

        SurfaceAuthorViewRights surfaceavr = g.GetComponent<SurfaceAuthorViewRights>();
        PieMenuAdvancedClickSurfaceInteraction expandWindow = g.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>();        

        g.name = "FloatingWindow" + WallGenerator.Instance.CountWallCreated + "-" + SynchroManager.Instance.ownerId;
        g.transform.parent = SynchroManager.Instance.transform;        
        g.SetActive(false);

        surfaceavr.isPersonal = false;        
        surfaceavr.isGenerated = false;
        surfaceavr.AnticipateStart();

        this.relativePos = 1.5f;
        this.onPersonalWindowPos.Add(new Vector3(-this.lineSize / 2f, this.heightSize / 2f, 0f));
        
        expandWindow.DiffStart();
        expandWindow.HostTransform.position = this.relativePos * FlattenOnY(this.head.transform.forward) + this.head.transform.position;
        expandWindow.HostTransform.rotation = Quaternion.LookRotation(FlattenOnY(this.head.transform.forward), Vector3.up);
        expandWindow.HostTransform.gameObject.SetActive(true);

        DocumentLayout_2(g.transform);

        for(int i = 0; i < this.personalCollection.Count; i++)
        {
            this.personalCollection[i].enabled = false;
            this.personalCollection[i].OutOfBelt();
            this.personalCollection[i].transform.parent = expandWindow.HostTransform;
            this.personalCollection[i].transform.localRotation = Quaternion.identity;
            this.personalCollection[i].transform.localPosition = this.onPersonalWindowPos[i];
            this.personalCollection[i].GetComponent<PieMenuClickInteraction>().ExpandAction(expandWindow.HostTransform.gameObject);
        }
        this.isWindowAttached = false;

        this.personalCollection.Clear();
        this.onPersonalWindowPos.Clear();

        expandWindow.StartMove(expand: true);

        WallGenerator.Instance.CountWallCreated++;
    }

    private void ExpandForBatch()
    {

        // Calculate position of each items
        DocumentLayout_2(null);


        float minX = this.onPersonalWindowPos[0].x,
               minY = this.onPersonalWindowPos[0].y,
               maxX = this.onPersonalWindowPos[0].x,
               maxY = this.onPersonalWindowPos[0].y;

        for (int i = 0; i < this.onPersonalWindowPos.Count; i++)
        {
            Vector3 t = this.onPersonalWindowPos[i];

            minX = Mathf.Min(minX, t.x);
            minY = Mathf.Min(minY, t.y);
            maxX = Mathf.Max(maxX, t.x);
            maxY = Mathf.Max(maxY, t.y);
        }

        Vector3 cursorCentroidAlt = new Vector3((maxX + minX) / 2, (maxY + minY) / 2, 0);


        // Setup items to get out of the belt and be placed correctly
        for (int i = 0; i < this.personalCollection.Count; i++)
        {
            Vector3 memScale = this.personalCollection[i].transform.localScale;
            this.personalCollection[i].enabled = false;

            //Debug.Log(i + " " + personalCollection.Count);
            this.personalCollection[i].GetComponent<PieMenuClickInteraction>().BatchHeadMove(this.onPersonalWindowPos[i] - cursorCentroidAlt, this.personalCollection.Count, Mathf.Abs(maxY - minY), Mathf.Abs(maxX - minX), i);
            //Debug.Log("Start " + this.personalCollection[i].transform.parent.name);
            this.personalCollection[i].transform.parent = SynchroManager.Instance.transform;
            //Debug.Log("End " + this.personalCollection[i].transform.parent.name);
            this.personalCollection[i].transform.localRotation = Quaternion.identity;
            this.personalCollection[i].transform.localPosition = this.onPersonalWindowPos[i];
            this.personalCollection[i].transform.localScale = memScale / 10f ;            
        }        
    }

    private void DocumentLayout(Transform expandWindow)
    {        
        // Constitute list of items without position defined
        List<Transform> toPlace = new List<Transform>();
        for (int i = this.personalCollection.Count - 1; i >= 0; i--)
        {
            if (this.onPersonalWindowPos[i] == Vector3.one)
            {
                toPlace.Add(this.personalCollection[i].transform);
                this.personalCollection.RemoveAt(i);
                this.onPersonalWindowPos.RemoveAt(i);
            }
        }

        Transform windowToExpand = expandWindow?.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().transform.parent;
        // Give to the algorithm for placement
        ReLayout(this.personalCollection, this.onPersonalWindowPos, toPlace, windowToExpand);                
    }

    private void DocumentLayout_2(Transform expandWindow)
    {
        // Constitute list of items without position defined
        Transform[] toPlace = new Transform[this.personalCollection.Count];
        for (int i = 0; i < this.personalCollection.Count; i++)
        {
            int newIndex = (i % 2 == 0) ? (i / 2) : (this.personalCollection.Count - (i / 2) - 1);
            toPlace[newIndex] = this.personalCollection[i].transform;
        }

        Transform windowToExpand = expandWindow?.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>().transform.parent;
        ReLayout_2(new List<Transform>(toPlace), windowToExpand);
    }   
    
    public void ReLayout(List<OrbitalPersonal> sorted, List<Vector3> sortedPos, List<Transform> ToPlace, Transform expandWindow)
    {
        // Get all cards inside the window
        List<Transform> cards = new List<Transform>();
        foreach (OrbitalPersonal op in sorted)
            cards.Add(op.transform);

        ToPlace.AddRange(cards);
        cards.Clear();

        float w_window = (expandWindow == null) ? 1.925f : expandWindow.localScale.x;
        float h_window = (expandWindow == null) ? 1.925f : expandWindow.localScale.y;
        float w_step = OrbitalPersonalRing.Instance.stepX;
        float h_step = OrbitalPersonalRing.Instance.stepY;
        Vector3 baseOffset = new Vector3(w_window / 2f, h_window / 2f, 0f);

        int w_grid_count = Mathf.FloorToInt(w_window / w_step);
        int h_grid_count = Mathf.FloorToInt(h_window / h_step);
        float w_grid = w_window / w_grid_count;
        float h_grid = h_window / h_grid_count;
        int resize_factor = w_grid_count;

        float xPos, yPos;

        List<List<Transform>> grid = Enumerable.Repeat<List<Transform>>(null, h_grid_count).ToList();
        // Create a grid to welcome the cards in fixed places
        // This grid has a fixed height, and variable width
        // Calculate grid capacity to resize window if the capacity is too small

        int w_index = 0;
        int h_index = 0;

        for (h_index = 0; h_index < h_grid_count; h_index++)
        {
            grid[h_index] = new List<Transform>();

            // Get every elements fitting a line in the grid
            for (int i = cards.Count - 1; i >= 0; i--)
            {
                Vector3 testPos = sortedPos[i] + baseOffset;
                if ((h_grid_count - h_index) * h_grid > testPos.y && testPos.y > (h_grid_count - h_index - 1) * h_grid)
                {
                    grid[h_index].Add(cards[i]);
                    cards.RemoveAt(i);
                }
            }

            // Fit the max width of each line to find out the max, and sort each line
            w_grid_count = Mathf.Max(w_grid_count, grid[h_index].Count);
            grid[h_index] = grid[h_index].OrderBy(o => o.localPosition.x).ToList();
        }
        h_index--;

        // If some element are outside, fit them in the last line
        if (cards.Count > 0)
            grid[h_index].AddRange(cards);
        w_grid_count = Mathf.Max(w_grid_count, grid[h_index].Count);
        grid[h_index] = grid[h_index].OrderBy(o => o.localPosition.x).ToList();

        // Determine every cards right position
        List<List<Vector3>> gridPositions = Enumerable.Repeat<List<Vector3>>(null, w_grid_count).ToList();
        int maxSize = 0;
        for (h_index = 0; h_index < h_grid_count; h_index++)
        {
            w_index = 0;
            int xPosIndex = w_index;
            gridPositions[h_index] = new List<Vector3>();

            while (w_index < grid[h_index].Count)
            {
                xPos = (2 * xPosIndex + 1) * w_grid / 2f;
                yPos = (2 * h_index + 1) * h_grid / 2f;

                while (grid[h_index][w_index].localPosition.x > (xPosIndex + 1) * w_grid - baseOffset.x)
                {
                    if (ToPlace.Count > 0)
                    {
                        xPos = (2 * xPosIndex + 1) * w_grid / 2f;
                        yPos = (2 * h_index + 1) * h_grid / 2f;

                        grid[h_index].Insert(w_index, ToPlace[0]);
                        gridPositions[h_index].Add(new Vector3(xPos, -yPos, 0f));
                        ToPlace.RemoveAt(0);

                        w_index++;                        
                    }
                        
                    xPosIndex++;
                }                

                gridPositions[h_index].Add(new Vector3(xPos, -yPos, 0f));

                w_index++;
                xPosIndex++;
                maxSize = Mathf.Max(maxSize, xPosIndex);
            }
        }

        if (ToPlace.Count > 0)
        {
            h_index = 0;
            for(int i=0; i< ToPlace.Count; i++)
            {
                xPos = (2 * (grid[h_index].Count) + 1) * w_grid / 2f;
                yPos = (2 * h_index + 1) * h_grid / 2f;

                grid[h_index].Add(ToPlace[i]);
                gridPositions[h_index].Add(new Vector3(xPos, -yPos, 0f));
                h_index = (h_index + 1) % grid.Count;

                maxSize = Mathf.Max(maxSize, grid[h_index].Count - 1);
            }
            ToPlace.Clear();
        }

        // Resize the window
        if(expandWindow != null)
            expandWindow.localScale = new Vector3(((float)Mathf.Max(Mathf.FloorToInt(1.925f / w_step), maxSize) / resize_factor) * expandWindow.localScale.x, expandWindow.localScale.y, expandWindow.localScale.z);
        baseOffset = new Vector3(((float)Mathf.Max(Mathf.FloorToInt(1.925f / w_step), maxSize) / resize_factor) * baseOffset.x, -baseOffset.y, baseOffset.z);

        //Debug.Log((float)Mathf.Max(Mathf.FloorToInt(1.925f / w_step), maxSize) + " max offset " + resize_factor);
        this.personalCollection.Clear();
        this.onPersonalWindowPos.Clear();
        for (h_index = 0; h_index < h_grid_count; h_index++)
        {
            for (w_index = 0; w_index < grid[h_index].Count; w_index++)
            {
                this.personalCollection.Add(grid[h_index][w_index].GetComponent<OrbitalPersonal>());
                this.onPersonalWindowPos.Add(gridPositions[h_index][w_index] - baseOffset);
            }
            grid[h_index].Clear();
        }
        cards.Clear();
        grid.Clear();
    }

    public void ReLayout_2(List<Transform> ToPlace, Transform expandWindow)
    {
        float w_window = (expandWindow == null) ? 1.925f : expandWindow.localScale.x;
        float h_window = (expandWindow == null) ? 1.925f : expandWindow.localScale.y;
        float w_step = OrbitalPersonalRing.Instance.stepX;
        float h_step = OrbitalPersonalRing.Instance.stepY;
        Vector3 baseOffset = new Vector3(w_window / 2f, h_window / 2f, 0f);

        int w_grid_count = Mathf.FloorToInt(w_window / w_step);
        int h_grid_count = Mathf.FloorToInt(h_window / h_step);
        float w_grid = w_window / w_grid_count;
        float h_grid = h_window / h_grid_count;
        int resize_factor = w_grid_count;
        int maxSize = Mathf.Max(Mathf.CeilToInt(ToPlace.Count * 1f / h_grid_count * 1f), w_grid_count);
        int h_index, w_index;

        // Initialize the grid to Place objects
        List<List<Transform>> grid = Enumerable.Repeat<List<Transform>>(null, h_grid_count).ToList();        
        for(h_index = 0; h_index < h_grid_count; h_index++)
        {
            grid[h_index] = new List<Transform>();            
        }

        for(int i = 0; i < ToPlace.Count; i++)
        {
            h_index = Mathf.FloorToInt(i / maxSize);
            grid[h_index].Add(ToPlace[i]);
        }

        // Resize the window
        if (expandWindow != null)
            expandWindow.localScale = new Vector3(((float)Mathf.Max(Mathf.FloorToInt(1.925f / w_step), maxSize) / resize_factor) * expandWindow.localScale.x, expandWindow.localScale.y, expandWindow.localScale.z);
        baseOffset = new Vector3(((float)Mathf.Max(Mathf.FloorToInt(1.925f / w_step), maxSize) / resize_factor) * baseOffset.x, -baseOffset.y, baseOffset.z);

        this.personalCollection.Clear();
        this.onPersonalWindowPos.Clear();

        for (h_index = 0; h_index < h_grid_count; h_index++)
        {
            for (w_index = 0; w_index < maxSize; w_index++)
            {
                if (!(h_index * maxSize + w_index < ToPlace.Count))
                    break;
                this.personalCollection.Add(grid[h_index][w_index].GetComponent<OrbitalPersonal>());
                this.onPersonalWindowPos.Add(new Vector3((2 * w_index + 1) * w_grid / 2f, - (2 * h_index + 1) * h_grid / 2f, 0) - baseOffset);
            }
        }
        grid.Clear();
    }

    private bool CheckCollision(Vector3 testPosition, OrbitalPersonal toPlace, List<OrbitalPersonal> possibleCollisions, List<Vector3> possibleCollisionsPos)
    {
        Bounds b = toPlace.GetComponentInChildren<Collider>().bounds;
        b.center = testPosition;
        for(int i = 0; i < possibleCollisions.Count; i++)
        {
            Bounds testBounds = possibleCollisions[i].GetComponentInChildren<Collider>().bounds;
            testBounds.center = this.onPersonalWindowPos[i];
            if (b.Intersects(testBounds))
                return true;
        }
        return false;
    }

    [Button]
    public void Empty(PieMenuAdvancedClickSurfaceInteraction windowToEmpty)
    {
        this.relativePos = Vector3.Distance(Camera.main.transform.position, this.head.transform.position);
        List<OrbitalPersonal> newChildren = new List<OrbitalPersonal>(windowToEmpty.HostTransform.GetComponentsInChildren<OrbitalPersonal>());
        newChildren = newChildren.OrderBy(o => o.transform.localPosition.y).ThenByDescending(o => o.transform.localPosition.x).ToList();

        for (int i = 0; i < newChildren.Count; i++)
        {
            //Debug.Log("Empty");

            newChildren[i].GetComponentInChildren<PieMenuClickInteraction>().SwitchToPersonalBatch();
            newChildren[i].enabled = true;
            newChildren[i].InBelt();            
        }
        RepositionItems(this.deltaAngle, this.startAngle);

        this.isWindowAttached = true;        
    }

    public void EmptySelection(List<PieMenuClickInteraction> items)
    {
        this.relativePos = Vector3.Distance(Camera.main.transform.position, this.head.transform.position);
        List<OrbitalPersonal> newChildren = new List<OrbitalPersonal>();
        foreach(PieMenuClickInteraction p in items)
            newChildren.Add(p.GetComponent<OrbitalPersonal>());
        newChildren = newChildren.OrderBy(o => o.transform.localPosition.y).ThenByDescending(o => o.transform.localPosition.x).ToList();

        for (int i = 0; i < newChildren.Count; i++)
        {
            //Debug.Log("Empty");

            newChildren[i].GetComponentInChildren<PieMenuClickInteraction>().SwitchToPersonalBatch();
            newChildren[i].enabled = true;
            newChildren[i].InBelt();
        }
        RepositionItems(this.deltaAngle, this.startAngle);

        this.isWindowAttached = true;
    }

    [Button]
    public void Size()
    {
        for (int i = 0; i < this.personalCollection.Count; i++)
        {            
            Debug.Log(this.personalCollection[i].name + " " + this.onPersonalWindowPos[i]);
        }
    }

    private Vector3 FlattenOnY(Vector3 v)
    {
        return Vector3.ProjectOnPlane(v, new Vector3(0f, 1f, 0f)).normalized;
    }

    public override void BatchMove()
    {
        ExpandForBatch();

        // Do a moving with magnetism on the Window, but disable window features and magnet on windows
        //PersonalWindow.BatchMove();

        this.personalCollection.Clear();
        this.onPersonalWindowPos.Clear();
    }        
}