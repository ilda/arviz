﻿#region Assembly UnityEngine.UI, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// C:\Program Files\Unity\Hub\Editor\2018.3.11f1\Editor\Data\UnityExtensions\Unity\GUISystem\UnityEngine.UI.dll
#endregion

using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using TMPro;
using UnityEngine;

using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CustomButtons : Selectable
{
    public delegate void Enter();
    public delegate void Exit();
    public delegate void Down();
    public delegate void Up();

    public event Enter OnEnter;
    public event Exit OnExit;
    public event Down OnDown;
    public event Up OnUp;

    public TextMeshProUGUI label;
    private BaseEventData baseEvent;

    protected override void Start()
    {
        base.Start();        

        this.GetComponent<Image>().material.renderQueue = 4000;
        this.label.fontMaterial.renderQueue = 4000;
    }

    private void Update()
    {        
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        base.OnPointerUp(eventData);
        OnUp();
    }

    public bool IsHighlightedOrPressed()
    {
        return IsHighlighted(this.baseEvent) || IsPressed();
    }

    public void SetLabel(string text)
    {
        this.label.text = text;
    }
}