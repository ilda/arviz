﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetCursor : MonoBehaviour
{
    public GameObject Wall;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Move(Vector3 position, Quaternion rot, Vector3 scale)
    {
        this.transform.position = Wall.transform.position + position;
        this.transform.rotation = rot;
        this.transform.localScale = scale;
    }
}
