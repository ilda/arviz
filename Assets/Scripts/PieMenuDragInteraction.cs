﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input.Utilities;
using Synchro;

public class PieMenuDragInteraction : InteractionScript, IMixedRealitySourceStateHandler
{
    private GraphicRaycaster graphicRaycast;
    private EventSystem m_EventSystem;
    private PointerEventData m_PointerEventData;

    private bool activeMenu = false;
    private GameObject sphere;
    private GameObject sphere2;

    List<RaycastResult> results = new List<RaycastResult>();
    IMixedRealityPointer pointer;

    Transform cameraTr;

    bool moveOrdered = false;
    bool moveOrderedCenterHead = false;
    bool hybridMoveOrdered = false;
    bool isLost = false;

    Vector3 deltaDistance;
    float itemDistance;

    protected override void Start()
    {
        base.Start();
        cameraTr = Camera.main.transform;
    }

    protected override void Update()
    {
        base.Update();        

        if (moveOrdered)
        {
            Vector3 targetPosition;
            Quaternion targetRotation = transform.rotation;
            if (isLost)
            {
                targetPosition = cameraTr.position + (gazeProvider.HitPosition - cameraTr.position).normalized * 0.6f;
                targetRotation = cameraTr.rotation;
            }
            else
            {
                Debug.Log(pointer);

                deltaDistance = (pointer.Position - Camera.main.transform.position).normalized;
                targetPosition = pointer.Position + deltaDistance.normalized * itemDistance;

                targetRotation = Quaternion.Euler(0, pointer.Rotation.eulerAngles.y, 0);
            }

            // Apply magnetism effect when getting inside a defined effect area
            if (isMagnetic)
            {
                Magnetism(max_dist, ref targetPosition, ref targetRotation);
            }            

            float lerpAmount = GetLerpAmount();
            Quaternion smoothedRotation = Quaternion.Lerp(hostTransform.rotation, targetRotation, lerpAmount);
            Vector3 smoothedPosition = Vector3.Lerp(hostTransform.position, targetPosition, lerpAmount);
            hostTransform.SetPositionAndRotation(smoothedPosition, smoothedRotation);
        }        

        if (moveOrderedCenterHead)
        {
            Quaternion targetRotation = transform.rotation;
            Vector3 targetPosition;

            targetPosition = gazeProvider.HitPosition;
            targetRotation = cameraTr.rotation;

            // Apply magnetism effect when getting inside a defined effect area
            if (isMagnetic)
            {
                Magnetism(max_dist, ref targetPosition, ref targetRotation);
            }

            float lerpAmount = GetLerpAmount();
            Quaternion smoothedRotation = Quaternion.Lerp(hostTransform.rotation, targetRotation, lerpAmount);
            Vector3 smoothedPosition = Vector3.Lerp(hostTransform.position, targetPosition, lerpAmount);
            hostTransform.SetPositionAndRotation(smoothedPosition, smoothedRotation);
        }

        if (hybridMoveOrdered)
        {
            Vector3 targetPosition;
            Quaternion targetRotation = transform.rotation;

            if (isLost)
            {
                targetPosition = gazeProvider.HitPosition;
                targetRotation = cameraTr.rotation;
            }
            else
            {
                Debug.Log(pointer);

                deltaDistance = (pointer.Position - Camera.main.transform.position).normalized;
                targetPosition = pointer.Position + deltaDistance.normalized * itemDistance;

                targetRotation = Quaternion.Euler(0, pointer.Rotation.eulerAngles.y, 0);
            }

            // Apply magnetism effect when getting inside a defined effect area
            if (isMagnetic)
            {
                Magnetism(max_dist, ref targetPosition, ref targetRotation);
            }

            float lerpAmount = GetLerpAmount();
            Quaternion smoothedRotation = Quaternion.Lerp(hostTransform.rotation, targetRotation, lerpAmount);
            Vector3 smoothedPosition = Vector3.Lerp(hostTransform.position, targetPosition, lerpAmount);
            hostTransform.SetPositionAndRotation(smoothedPosition, smoothedRotation);
        }
    }

    public override void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        Vector3 initialGrabPoint;

        if (!allowFarManipulation && eventData.Pointer as IMixedRealityNearPointer == null)
        {
            return;
        }

        uint id = eventData.Pointer.PointerId;

        // If we only allow one handed manipulations, check there is no hand interacting yet. 
        if (manipulationType != HandMovementType.OneHandedOnly || pointerIdToPointerMap.Count == 0)
        {
            // Ignore poke pointer events
            if (!eventData.used
                && !pointerIdToPointerMap.ContainsKey(eventData.Pointer.PointerId))
            {
                if (pointerIdToPointerMap.Count == 0)
                {
                    rigidBody = GetComponent<Rigidbody>();
                    if (rigidBody != null)
                    {
                        wasKinematic = rigidBody.isKinematic;
                        rigidBody.isKinematic = true;
                    }
                }
                initialGrabPoint = Quaternion.Inverse(eventData.Pointer.Rotation) * (eventData.Pointer.Result.Details.Point - eventData.Pointer.Position);
                pointerIdToPointerMap.Add(id, new PointerData(eventData.Pointer, initialGrabPoint));
            }
        }

        if (pointerIdToPointerMap.Count > 0)
        {
            // Always mark the pointer data as used to prevent any other behavior to handle pointer events
            // as long as the ManipulationHandler is active.
            // This is due to us reacting to both "Select" and "Grip" events.
            eventData.Use();
        }

        if (moveOrdered)
        {
            hostTransform.GetComponent<AuthorViewRights>().LetAuthoring(false);

            moveOrdered = false;
            moveOrderedCenterHead = false;

            pointerIdToPointerMap.Remove(id);

            MixedRealityToolkit.InputSystem.Unregister(gameObject);
            return;
        }
        else if (moveOrderedCenterHead)
        {
            hostTransform.GetComponent<AuthorViewRights>().LetAuthoring(false);

            moveOrdered = false;
            moveOrderedCenterHead = false;

            foreach (Collider c in hostTransform.GetComponentsInChildren<Collider>())
            {
                c.enabled = true;
            }

            pointerIdToPointerMap.Remove(id);

            MixedRealityToolkit.InputSystem.Unregister(gameObject);
            return;
        }
        else if (hybridMoveOrdered)
        {
            hostTransform.GetComponent<AuthorViewRights>().LetAuthoring(false);

            moveOrdered = false;
            moveOrderedCenterHead = false;
            hybridMoveOrdered = false;

            foreach (Collider c in hostTransform.GetComponentsInChildren<Collider>())
            {
                c.enabled = true;
            }

            pointerIdToPointerMap.Remove(id);

            MixedRealityToolkit.InputSystem.Unregister(gameObject);
            return;
        }

        activeMenu = true;
        StartPieMenu(eventData);


        PieMenu.Instance.GetComponent<Canvas>().worldCamera = Camera.main;
        graphicRaycast = PieMenu.Instance.GetComponent<GraphicRaycaster>();
        m_EventSystem = PieMenu.Instance.GetComponent<EventSystem>();

        Debug.Assert(pointerIdToPointerMap.Count == 1);
        initialGrabPoint = Quaternion.Inverse(eventData.Pointer.Rotation) * (eventData.Pointer.Result.Details.Point - eventData.Pointer.Position);
        pointerIdToPointerMap.Add(id, new PointerData(eventData.Pointer, initialGrabPoint));

        m_PointerEventData = new PointerEventData(m_EventSystem);
        m_PointerEventData.position = Camera.main.WorldToScreenPoint(pointer.Position);
        graphicRaycast.Raycast(m_PointerEventData, results);

        MixedRealityToolkit.InputSystem.Register(gameObject);
    }

    public override void OnPointerDragged(MixedRealityPointerEventData eventData)
    {        
        if (activeMenu)
        {
            List<RaycastResult> newResults = new List<RaycastResult>();

            m_PointerEventData = new PointerEventData(m_EventSystem);
            m_PointerEventData.position = Camera.main.WorldToScreenPoint(pointer.Position);
            graphicRaycast.Raycast(m_PointerEventData, newResults);

            int j = 0;
            for (int i=0; i < results.Count; i++)
            {
                j = 0;
                while(j < newResults.Count && results[i].gameObject != newResults[j].gameObject)
                {
                    j++;
                }                
                if (j == newResults.Count)
                {
                    ExecuteEvents.Execute(results[i].gameObject, m_PointerEventData, ExecuteEvents.pointerExitHandler);
                }                
            }

            for (int i = 0; i < newResults.Count; i++)
            {
                j = 0;
                while (j < results.Count && newResults[i].gameObject != results[j].gameObject)
                {
                    j++;
                }
                if (j == results.Count)
                {
                    ExecuteEvents.Execute(newResults[i].gameObject, m_PointerEventData, ExecuteEvents.pointerEnterHandler);
                }
            }

            results.Clear();
            results.AddRange(newResults);
            newResults.Clear();

            eventData.Use();            
        } else
        {
            base.OnPointerDragged(eventData);
        }      
    }

    public override void OnPointerUp(MixedRealityPointerEventData eventData)
    {        
        uint id = eventData.Pointer.PointerId;
        if (pointerIdToPointerMap.ContainsKey(id))
        {
            if (pointerIdToPointerMap.Count == 1 && rigidBody != null)
            {
                rigidBody.isKinematic = wasKinematic;

                if (releaseBehavior.HasFlag(ReleaseBehaviorType.KeepVelocity))
                {
                    rigidBody.velocity = GetPointersVelocity();
                }

                if (releaseBehavior.HasFlag(ReleaseBehaviorType.KeepAngularVelocity))
                {
                    rigidBody.angularVelocity = GetPointersAngularVelocity();
                }

                rigidBody = null;
            }

            pointerIdToPointerMap.Remove(id);
        }

        if (activeMenu && results.Count == 0)
        {
            CancelMenu();
        }
        else if (activeMenu)
        {
            foreach(RaycastResult r in results)
            {
                ExecuteEvents.Execute(r.gameObject, new PointerEventData(m_EventSystem), ExecuteEvents.pointerUpHandler);
            }
            results.Clear();
        }

        //UpdateStateMachine();
        eventData.Use();
    }

    private void StartPieMenu(MixedRealityPointerEventData eventData)
    {
        string[] funcName = new string[] { "Move On Windows", null, null, "Move To Personal Space" };

        activeMenu = true;
        PieMenu.Instance.Call(eventData.Pointer.Position + Camera.main.transform.forward * 0.4f, Camera.main.transform.rotation, Vector3.one * 0.007f, funcName);


        PieMenu.Instance.OnAction1 += StartHeadMove;
        PieMenu.Instance.OnAction2 += StartProximityMove;
        PieMenu.Instance.OnAction3 += HybridMove;


        pointer = pointer = eventData.InputSource.Pointers.First();
    }

    private void CancelMenu()
    {
        activeMenu = false;
        moveOrdered = false;
        moveOrderedCenterHead = false;
        hybridMoveOrdered = false;
        pointer = null;
        Destroy(PieMenu.Instance.gameObject);
        MixedRealityToolkit.InputSystem.Unregister(gameObject);

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= StartProximityMove;
        PieMenu.Instance.OnAction3 -= HybridMove;

        UpdateStateMachine();
    }

    private void StartProximityMove()
    {
        hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();
        if (GetComponent<OrbitalPersonal>().enabled)
            OrbitalPersonalRing.Instance.RemoveCollection(GetComponent<OrbitalPersonal>());

        moveOrdered = true;
        moveOrderedCenterHead = false;
        hybridMoveOrdered = false;
        activeMenu = false;

        deltaDistance = (pointer.Position - Camera.main.transform.position).normalized;
        itemDistance = 0.3f;

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= StartProximityMove;
        PieMenu.Instance.OnAction3 -= HybridMove;
        PieMenu.Instance.OnAction4 -= SwitchToPersonal;

        Destroy(PieMenu.Instance.gameObject);
    }

    private void StartHeadMove()
    {
        hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();
        if (GetComponent<OrbitalPersonal>().enabled)
            OrbitalPersonalRing.Instance.RemoveCollection(GetComponent<OrbitalPersonal>());

        moveOrderedCenterHead = true;
        moveOrdered = false;
        hybridMoveOrdered = false;
        activeMenu = false;

        deltaDistance = Camera.main.transform.forward * 0.2f;
        itemDistance = Vector3.Distance(transform.position, cameraTr.position);

        foreach(Collider c in hostTransform.GetComponentsInChildren<Collider>())
        {
            c.enabled = false;
        }

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= StartProximityMove;
        PieMenu.Instance.OnAction3 -= HybridMove;
        PieMenu.Instance.OnAction4 -= SwitchToPersonal;

        Destroy(PieMenu.Instance.gameObject);
    }

    private void HybridMove()
    {
        hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();
        if (GetComponent<OrbitalPersonal>().enabled)
            OrbitalPersonalRing.Instance.RemoveCollection(GetComponent<OrbitalPersonal>());

        hybridMoveOrdered = true;
        moveOrderedCenterHead = false;
        moveOrdered = false;
        activeMenu = false;

        deltaDistance = (pointer.Position - Camera.main.transform.position).normalized;
        itemDistance = 0.3f;

        foreach (Collider c in hostTransform.GetComponentsInChildren<Collider>())
        {
            c.enabled = false;
        }

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= StartProximityMove;
        PieMenu.Instance.OnAction3 -= HybridMove;
        PieMenu.Instance.OnAction4 -= SwitchToPersonal;

        Destroy(PieMenu.Instance.gameObject);
    }

    private void SwitchToPersonal()
    {
        hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= StartProximityMove;
        PieMenu.Instance.OnAction3 -= HybridMove;
        PieMenu.Instance.OnAction4 -= SwitchToPersonal;

        Destroy(PieMenu.Instance.gameObject);

        GetComponent<SlateAuthorViewRights>().OffSurface(Magnet);
        isAttracted = false;
        Magnet = null;

        changeSurfaceMessage.surfaceName = null;
        //SynchroServer.Instance.SendCommand("H", changeSurfaceMessage);
        SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);

        OrbitalPersonalRing.Instance.AddToCollection(GetComponent<OrbitalPersonal>());    
        MixedRealityToolkit.InputSystem.Unregister(gameObject);
    }

    public void OnSourceDetected(SourceStateEventData eventData)
    {
        if (moveOrdered || moveOrderedCenterHead ||hybridMoveOrdered)
        {
            isLost = false;
            //Debug.Log(eventData.InputSource.Pointers.Count());
            pointer = eventData.InputSource.Pointers.First();
        }
    }

    public void OnSourceLost(SourceStateEventData eventData)
    {
        pointer = null;
        if (moveOrdered || moveOrderedCenterHead || hybridMoveOrdered)
            isLost = true;
    }

    public override void ChangeMagnet(GameObject g)
    {
        if (g == null)
        {
            PieMenuAdvancedClickSurfaceInteraction pmacsi = this.Magnet.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>();
            if (pmacsi != null)
            {
                pmacsi.UpdateContent(-1);
            }

            this.isAttracted = false;
            this.Magnet = null;

        }
        else
        {
            this.isAttracted = true;
            this.Magnet = g;

            PieMenuAdvancedClickSurfaceInteraction pmacsi = this.Magnet.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>();
            if (pmacsi != null)
            {
                pmacsi.UpdateContent(0);
            }
        }

    }
    protected override void Magnetism(float max_dist, ref Vector3 position, ref Quaternion rotation)
    {

        // Manage magnetism when multiple magnet exists 
        int minIndex = 0;
        float distance;
        Vector3 ClosestToCenter;

        GameObject[] Magnets = GameObject.FindGameObjectsWithTag("MagneticSurface");

        Vector3 OnMagnet = Magnets[0].GetComponent<Collider>().ClosestPoint(position);
        float minDist = Vector3.Distance(OnMagnet, position);

        // Find the magnet closest to the transform 
        for (int i = 1; i < Magnets.Length; i++)
        {
            OnMagnet = Magnets[i].GetComponent<Collider>().ClosestPoint(position);

            distance = Vector3.Distance(OnMagnet, position); ;
            if (distance < minDist)
            {
                minDist = distance;
                minIndex = i;
            }
        }

        // Calculate the true position for item
        GameObject ClosestMagnet = Magnets[minIndex];
        OnMagnet = ClosestMagnet.GetComponent<Collider>().ClosestPoint(position);
        ClosestToCenter = ClosestMagnet.transform.position - OnMagnet;

        Vector3 objOnMagnet = Vector3.Project(ClosestToCenter, ClosestMagnet.transform.forward) + OnMagnet;

        // Operate Magnet effect on the object
        if (Vector3.Distance(objOnMagnet, position) <= max_dist)
        {
            // Case where item goes off a surface
            if (isAttracted == false)
            {
                Magnet = ClosestMagnet;
                GetComponent<SlateAuthorViewRights>().OnSurface(ClosestMagnet);

                changeSurfaceMessage.surfaceName = ClosestMagnet.name;
                //SynchroServer.Instance.SendCommand("H", changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }
            // Case where item changes surface without going through the 3D space
            else if (isAttracted == true && ClosestMagnet != Magnet)
            {
                GetComponent<SlateAuthorViewRights>().SwitchSurface(Magnet, ClosestMagnet);
                Magnet = ClosestMagnet;

                changeSurfaceMessage.surfaceName = ClosestMagnet.name;
                //SynchroServer.Instance.SendCommand("H", changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }

            isAttracted = true;
            position = objOnMagnet;
            rotation = (Vector3.Dot(ClosestMagnet.transform.forward, hostTransform.forward) > 0f) ? ClosestMagnet.transform.rotation : Quaternion.Inverse(ClosestMagnet.transform.rotation);
        }
        else
        {
            // Case where item is in 3D space and goes on a surface
            if (isAttracted == true)
            {
                GetComponent<SlateAuthorViewRights>().OffSurface(Magnet);
                Magnet = null;

                changeSurfaceMessage.surfaceName = null;
                //SynchroServer.Instance.SendCommand("H", changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }
            isAttracted = false;
        }
    }
}
