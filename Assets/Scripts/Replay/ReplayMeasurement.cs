﻿using Microsoft.MixedReality.Toolkit;
using Synchro;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ReplayMeasurement : MonoBehaviour
{
    string ownIp;
    private string fileStamp;

    TextAsset actions;
    string[] replayActions;
    int countActions = 0;
    float offsetTime;
    float offsetConnection = 0f;

    Dictionary<string, GameObject> monitorPosition = new Dictionary<string, GameObject>();
    Dictionary<string, Vector3> oldItemPosition = new Dictionary<string, Vector3>();

    Dictionary<string, GameObject> monitorWindowPosition = new Dictionary<string, GameObject>();
    Dictionary<string, Vector3> oldWindowPosition = new Dictionary<string, Vector3>();

    List<string> InPS = new List<string>();

    Dictionary<string, List<string>> monitorSurfaces = new Dictionary<string, List<string>>();

    Dictionary<string, string> sessionValues = new Dictionary<string, string>()
    { { "S0AR", "1" }, { "S0Wall", "2" },
      { "S1Wall", "1" }, { "S1AR", "2" },
      { "S2Wall", "2" }, { "S2AR", "1" },
      { "S3AR", "2" }, { "S3Wall", "1" },
      { "S4AR", "1" }, { "S4Wall", "2" },
      { "S5Wall", "1" }, { "S5AR", "2" },
      { "S6Wall", "2" }, { "S6AR", "1" },
      { "S7AR", "2" }, { "S7Wall", "1" },
      { "S8AR", "1" }, { "S8Wall", "2" },
      { "S9Wall", "1" }, { "S9AR", "2" },
      { "S10Wall", "2" }, { "S10AR", "1" },
      { "S11AR", "2" }, { "S11Wall", "1" } };

    private string session;
    private string task;
    private string condition;

    #region private Properties
    // Time
    float timeStart = 0f;
    float firstAction = 0f;

    // Action variables
    int nbrMove = 0;
    int nbrMoveToPS = 0;

    // -- Action from Surface
    int nbrBatchMoveSurface = 0;
    int nbrEmptySurface = 0;

    // -- Action from Selection
    int nbrSelect = 0;
    int nbrBatchMoveSelection = 0;
    int nbrEmptySelection = 0;

    // -- Action from PS
    int nbrExpandPS = 0;
    int nbrBatchMoveFromPS = 0;


    // Window Action variables
    int nbrRelayout = 0;
    int nbrWindowCreated = 0;
    int nbrWindowDestroyed = 0;

    // Personal Space variables
    int totToPSMoves = 0;
    int totFromPSMoves = 0;
    int maxNbrItemInPS = 0;
    int actualPS = 0;

    // Window *Stats* variables
    int totItemsInWindow = 0;
    int maxItemsInWindow = 0;

    // Movement data
    public float totWalkDistance = 0;
    public float totItemMoveDist = 0;
    public float totWindowMoveDist = 0;
    public float totCursorMoveDist = 0;
    float avgWallDist = 0;
    float avgPlayerDistance = 0f;
    float avgCursorDistance = 0f;

    // Movement data Helpers
    Vector3 oldWalkPos;
    Vector3 oldItemPos;
    Vector3 oldWindowPos;
    Vector3 oldCursorPos;

    float deltaMov = 0.015f;

    string history = "";

    GameObject player1;
    GameObject player2;
    GameObject cursor1;
    GameObject cursor2;

    int countMeasure = 0;

    bool sessionStarted = false;
    bool sessionEnded = false;

    bool gizmoOn = false;
    #endregion

    #region Public Measurement Methods
    // Option Action Incrementers
    public void MovePlus()
    {
        this.nbrMove++;
        this.history += ownIp + " " + Time.time + " Move " + '\n';
    }
    public void MoveToPSPlus()
    {
        this.nbrMoveToPS++;
        this.history += ownIp + " " + Time.time + " MoveToPersonalSpace " + '\n';
    }

    // Selection Action Incrementers
    public void SelectPlus()
    {
        this.nbrSelect++;
        this.history += ownIp + " " + Time.time + " Select" + '\n';
    }
    public void BatchMoveSelectionPlus()
    {
        this.nbrBatchMoveSelection++;
        this.history += ownIp + " " + Time.time + " BatchMoveSelection" + '\n';
    }
    public void EmptySelectionPlus()
    {
        Debug.Log("EmptuSelec");
        this.nbrEmptySelection++;
        this.history += ownIp + " " + Time.time + " EmptySelection " + '\n';
    }

    // Window Action Incrementers
    public void BatchMoveSurfacePlus()
    {
        this.nbrBatchMoveSurface++;
        this.history += ownIp + " " + Time.time + " BatchMoveSurface" + '\n';
    }
    public void EmptySurfacePlus()
    {
        this.nbrEmptySurface++;
        this.history += ownIp + " " + Time.time + " EmptySurface " + '\n';
    }
    public void RelayoutPlus()
    {
        this.nbrRelayout++;
        this.history += ownIp + " " + Time.time + " Relayout " + '\n';
    }
    public void WindowCreatedPlus()
    {
        this.nbrWindowCreated++;
        this.history += ownIp + " " + Time.time + " CreateWindow " + '\n';
    }
    public void WindowDestroyedPlus()
    {
        this.nbrWindowDestroyed++;
        this.history += ownIp + " " + Time.time + " DestroyWindow " + '\n';
    }

    // Personal Space Incrementers
    public void ToPSPlus()
    {
        this.totToPSMoves++;
    }
    public void ToPSPlusMax()
    {
        this.totToPSMoves++;
        this.actualPS++;
        if (this.actualPS > this.maxNbrItemInPS)
            this.maxNbrItemInPS = this.totToPSMoves;
    }
    public void FromPSPlus()
    {
        this.totFromPSMoves++;
    }
    public void FromPSPlusMax()
    {
        this.totFromPSMoves++;
        this.actualPS--;
    }
    public void MaxPSItems(int max)
    {
        if (max > this.maxNbrItemInPS)
            this.maxNbrItemInPS = max;
    }
    public void BatchMoveFromPSPlus()
    {
        this.nbrBatchMoveFromPS++;
        this.history += ownIp + " " + Time.time + " BatchMoveFromPersonalSpace " + '\n';
    }
    public void ExpandPSPlus()
    {
        this.nbrExpandPS++;
        this.history += ownIp + " " + Time.time + " ExpandPersonalSpace " + '\n';
    }

    public void StartPSTracking(string name)
    {
        this.InPS.Add(name);
        ItemDistanceStopSetter(name);
    }
    public void StopPSTracking(string name)
    {
        if (InPS.Remove(name))
        {
            this.FromPSPlusMax();
            ItemDistanceStartSetter(name, SynchroManager.Instance.GetObject(name));
        }
    }

    // Window *Stats* Methods
    public void ItemsInWindowPlus()
    {
        this.totItemsInWindow++;
    }
    public void MaxInWindow(int max)
    {
        this.maxItemsInWindow = Mathf.Max(this.maxItemsInWindow, max);
    }
    
    public void MovedItemWindow(string owner, string objectName, string surfaceName)
    {
        if (!monitorSurfaces.ContainsKey(surfaceName))
        {
            monitorSurfaces.Add(surfaceName, new List<string>());
        }

        monitorSurfaces[surfaceName].Add(objectName);

        if (this.name.Contains(owner))
            ItemsInWindowPlus();

        foreach (KeyValuePair<string, List<string>> kv in monitorSurfaces)
            MaxInWindow(kv.Value.Count);
    }
    public void MovedItemNotWindow(string objectName)
    {
        foreach(KeyValuePair<string, List<string>> kv in monitorSurfaces)
        {
            kv.Value.Remove(objectName);
        }
    }

    public int GetMaxItemInWindow()
    {
        return this.maxItemsInWindow;
    }
    public void SetMaxItemInWindow(int value)
    {
        this.maxItemsInWindow = value;
    }
    public int GetNbrMoveSurface()
    {
        return this.totItemsInWindow;
    }
    public void SetNbrMoveSurface(int value)
    {
        this.totItemsInWindow = value;
    }

    // Movement Methods //TODO FROM HERE
    public void WalkDistanceAdding(Vector3 newPos)
    {
        float dist = Vector3.Distance(this.oldWalkPos, newPos);
        if (dist >= this.deltaMov)
            this.totWalkDistance += dist;

        this.oldWalkPos = newPos;
        // Debug.Log(this.name + " " + this.totWalkDistance);
    }
    public void ItemDistanceAdding(Vector3 newPos)
    {
        float dist = Vector3.Distance(this.oldItemPos, newPos);
        if (dist >= this.deltaMov)
            this.totItemMoveDist += dist;
        this.oldItemPos = newPos;
    }
    public void ItemDistanceAdding(string name)
    {
        float newDist = Vector3.Distance(this.monitorPosition[name].transform.position, this.oldItemPosition[name]);
        if(newDist >= this.deltaMov) { 
            this.totItemMoveDist += newDist;
        this.gizmoOn = true;
        }
        else
        {
            this.gizmoOn = false;
        }
        this.oldItemPosition[name] = this.monitorPosition[name].transform.position;
    }
    public void ItemBatchDistanceAdding(Vector3 newPos, int numberOfItems)
    {
        float dist = Vector3.Distance(this.oldItemPos, newPos);
        if (dist >= this.deltaMov)
            this.totItemMoveDist += dist * numberOfItems;
        this.oldItemPos = newPos;
    }
    public void WindowDistanceAdding(Vector3 newPos)
    {
        float dist = Vector3.Distance(this.oldWindowPos, newPos);
        if (dist >= this.deltaMov)
            this.totWindowMoveDist += dist;
        this.oldWindowPos = newPos;
    }
    public void WindowDistanceAdding(string name)
    {        
        float newDist = Vector3.Distance(this.monitorWindowPosition[name].transform.position, this.oldWindowPosition[name]);
        if (newDist >= this.deltaMov)
            this.totWindowMoveDist += newDist;
        this.oldWindowPosition[name] = this.monitorWindowPosition[name].transform.position;
    }

    public void CursorDistanceAdding(Vector3 newPos)
    {
        float dist = Vector3.Distance(this.oldCursorPos, newPos);
        if (dist >= this.deltaMov)
        {
            this.totCursorMoveDist += dist;
            //Debug.Log("Cursor dist = " + dist + " tot " + totCursorMoveDist + " " +countMeasure);
        }
        this.oldCursorPos = newPos;
    }
    public void DistanceToWallAdding(float newDist)
    {
        avgWallDist += newDist / 10f;
    }
    public void DistanceInterPlayerAdding()
    {
        //Debug.Log("Dist = " + Vector3.Distance(player1.transform.position, player2.transform.position) + " time " + Time.time + " " + this.name);
        avgPlayerDistance += Vector3.Distance(player1.transform.position, player2.transform.position) / 10f;
    }
    public void DistanceInterCursorAdding()
    {
        //Debug.Log("Dist = " + Vector3.Distance(player1.transform.position, player2.transform.position) + " time " + Time.time + " " + this.name);
        avgCursorDistance += Vector3.Distance(cursor1.transform.position, cursor2.transform.position) / 10f;
    }

    // Movement Methods Helpers
    public void WalkDistanceStartSetter(Vector3 newPos)
    {
        this.oldWalkPos = newPos;
    }
    public void ItemDistanceStartSetter(Vector3 newPos)
    {
        this.oldItemPos = newPos;
    }
    public void ItemDistanceStartSetter(string name, GameObject item)
    {
        this.monitorPosition[name] = item;
        this.oldItemPosition[name] = item.transform.position;
    }
    public void WindowDistanceStartSetter(Vector3 newPos)
    {
        this.oldWindowPos = newPos;
    }
    public void WindowDistanceStartSetter(string name, GameObject item)
    {
        this.monitorWindowPosition[name] = item;
        this.oldWindowPosition[name] = item.transform.position;
    }
    public void CursorDistanceStartSetter(Vector3 newPos)
    {
        this.oldCursorPos = newPos;
    }
    public void DistanceToWallStartSetter()
    {
        this.avgWallDist = 0;
    }
    public void DistanceInterPlayerSetter(GameObject g1, GameObject g2)
    {
        player1 = g1;
        player2 = g2;
        avgPlayerDistance = 0f;
        countMeasure = 0;
    }
    public void DistanceInterCursorSetter(GameObject g1, GameObject g2)
    {
        cursor1 = g1.transform.GetChild(0).gameObject;
        cursor2 = g2.transform.GetChild(0).gameObject;
        avgCursorDistance = 0f;
    }

    public void ItemDistanceStopSetter(string name)
    {
        this.monitorPosition.Remove(name);
        this.oldItemPosition.Remove(name);
        gizmoOn = false;
    }
    public void WindowDistanceStopSetter(string name)
    {
        this.monitorWindowPosition.Remove(name);
        this.oldWindowPosition.Remove(name);
    }

    // Cursor System
    private void GetCursor()
    {
        CursorDistanceAdding(this.transform.GetChild(0).position);
    }
    private void GetDist()
    {
        WalkDistanceAdding(this.transform.position);
        DistanceToWallAdding(Vector3.Distance(this.transform.position, Vector3.ProjectOnPlane(this.transform.position, SynchroManager.Instance.Wall.transform.forward)));
    }
    private void GetItemMove()
    {
        foreach (KeyValuePair<string, GameObject> kp in this.monitorPosition)
        {
            float dist = Vector3.Distance(this.monitorPosition[kp.Key].transform.position, this.oldItemPosition[kp.Key]);
            if (dist > 0.01f)  
            {
                ItemDistanceAdding(kp.Key);
            }
        }

        foreach (KeyValuePair<string, GameObject> kp in this.monitorWindowPosition)
        {
            float dist = Vector3.Distance(this.monitorWindowPosition[kp.Key].transform.position, this.oldWindowPosition[kp.Key]);
            if (dist > 0.01f)
            {
                WindowDistanceAdding(kp.Key);
            }
        }
    }
    #endregion

    #region LogFile Generation Methods
    // Writing Methods
    public string TitleGeneration()
    {
        string[] fixedParameters = { };
        string[] variables = { };

        return string.Join(";", fixedParameters) + ";" + string.Join(";", variables);
    }
    public string DataGeneration()
    {
        string[] fixedParametersValues = { };
        string[] variablesValues = { };

        return string.Join(";", fixedParametersValues) + ";" + string.Join(";", variablesValues);
    }
    #endregion

    #region MonoBehaviour Functions
    private void Start()
    {
        this.ownIp = this.name.Remove(0, 2);

        GameObject g = new GameObject();
        g.transform.parent = this.transform;
        Camera c = g.AddComponent<Camera>();
        c.clearFlags = CameraClearFlags.Color;
        c.backgroundColor = Color.black;
        int tar = 0;
        foreach(Camera cam in Camera.allCameras)
        {
            if (cam.targetDisplay >= tar){
                tar = cam.targetDisplay + 1;
            }
        }
        c.targetDisplay = tar;
    }
    private void FixedUpdate()
    {
        if (this.sessionStarted && !this.sessionEnded)
        {
            while (countActions < replayActions.Length && replayActions[countActions] != "" && float.Parse(replayActions[countActions].Split(' ')[0]) + offsetConnection + offsetTime < Time.time)
            {
                //Debug.Log(replayActions[countActions].Split(' ')[1]);
                switch (replayActions[countActions].Split(' ')[1])
                {
                    case "Move":
                        MovePlus();
                        break;
                    case "MoveToPersonalSpace":
                        MoveToPSPlus();
                        break;
                    case "Select":
                        SelectPlus();
                        break;
                    case "Relayout":
                        RelayoutPlus();
                        break;
                    case "BatchMoveSurface":
                        BatchMoveSurfacePlus();
                        break;
                    case "BatchMoveSelection":
                        BatchMoveSelectionPlus();
                        break;
                    case "BatchMoveFromPersonalSpace":
                        BatchMoveFromPSPlus();
                        break;
                    case "EmptySurface":
                        EmptySurfacePlus();
                        break;
                    case "EmptySelection":
                        EmptySelectionPlus();
                        break;
                    case "ExpandPersonalSpace":
                        ExpandPSPlus();
                        break;
                    case "CreateWindow":
                        WindowCreatedPlus();
                        break;
                    case "DestroyWindow":
                        WindowDestroyedPlus();
                        break;
                    default:
                        Debug.LogError("? ? ? " + replayActions[countActions].Split(' ')[1]);
                        break;
                }

                countActions++;
            }
            //            Debug.Log(this.name + " " + this.maxNbrItemInPS);
            if(Physics.Raycast(new Ray(this.transform.position, this.transform.forward), out RaycastHit hit, float.PositiveInfinity)){
                Transform target = hit.transform;

                while(!(target.tag == "MagneticSurface" || target.name.Contains("Op") || target.name == "Controller"))
                {
                    target = target.parent;                    
                }

                string surfaceName = (target.name == "Controller") ? "null" : target.name;
                using (StreamWriter log = new StreamWriter("./" + session + "_" + condition + "_" + task + "_" + this.transform.GetChild(0).name + "_" + ".csv", true))
                {
                    log.WriteLine(Time.time + " " + this.name.Remove(0,2) + " SpatialStatusCursor " + this.transform.GetChild(0).name + " " + this.transform.GetChild(0).position.ToString("F3").Replace(" ", "") + " " + surfaceName);
                }
            }
            else
            {
                using (StreamWriter log = new StreamWriter("./" + session + "_" + condition + "_" + task + "_" + this.transform.GetChild(0).name + "_" + ".csv", true))
                {
                    log.WriteLine(Time.time + " " + this.name.Remove(0, 2) + " SpatialStatusCursor " + this.transform.GetChild(0).name + " " + this.transform.GetChild(0).position.ToString("F3").Replace(" ", "") + " null");
                }
            }
        }
    }

    public override string ToString()
    {
        /*
        return "Measurement : " + " nbrMove = " + this.nbrMove + " nbrMoveToPS = " + this.nbrMoveToPS  + " nbrSelect = " + this.nbrSelect 
            + " nbrBatchMoveSelection = " + this.nbrBatchMoveSelection + " nbrBatchMoveSurface = " + this.nbrBatchMoveSurface + " nbrBatchMoveFromPS = " + this.nbrBatchMoveFromPS 
            + " nbrExpandPS = " + this.nbrExpandPS + " nrbEmptySurface = " + this.nbrEmptySurface + " nrbEmptySelection = " + this.nbrEmptySelection + "\n" 
            + " nbrRelayout = " + this.nbrRelayout + " nbrWindowCreated = " + this.nbrWindowCreated + " nbrWindowDestroyed = " + this.nbrWindowDestroyed 
            + " totToPSMoves = " + this.totToPSMoves + " totFromPSMoves = " + this.totFromPSMoves +  " maxNbrItemInPS = " + this.maxNbrItemInPS + "\n" 
            + " totItemsInWindow = " + this.totItemsInWindow + " maxItemsInWindow = " + this.maxItemsInWindow + "\n"
            + " totWalkDistance = " + this.totWalkDistance + " totItemMoveDistance = " + this.totItemMoveDist + " totWindowMoveDistance = " + this.totWindowMoveDist + " totCursorMoveDist = " + this.totCursorMoveDist;       
        */
        return this.ownIp + " " + this.nbrMove + " " + this.nbrMoveToPS + " " + this.nbrSelect + " " + this.nbrBatchMoveSelection + " " + this.nbrBatchMoveSurface + " " + this.nbrBatchMoveFromPS + " "
            + this.nbrExpandPS + " " + this.nbrEmptySurface + " " + this.nbrEmptySelection + " " + this.nbrRelayout + " " + this.nbrWindowCreated + " " + this.nbrWindowDestroyed + " "
            + this.totToPSMoves + " " + this.totFromPSMoves + " " + this.maxNbrItemInPS + " " + this.totItemsInWindow + " " + this.maxItemsInWindow + " "
            + this.totWalkDistance + " " + this.totItemMoveDist + " " + this.totWindowMoveDist + " " + this.totCursorMoveDist + " " + this.avgWallDist;
    }

    public void StartSession()
    {
        timeStart = Time.time;

        this.nbrMove = 0;
        this.nbrMoveToPS = 0;

        //-- Action from Surface
        this.nbrBatchMoveSurface = 0;
        this.nbrEmptySurface = 0;

        //-- Action from Selection
        this.nbrSelect = 0;
        this.nbrBatchMoveSelection = 0;
        this.nbrEmptySelection = 0;

        //-- Action from PS
        this.nbrExpandPS = 0;
        this.nbrBatchMoveFromPS = 0;


        // Window Action variables
        this.nbrRelayout = 0;
        this.nbrWindowCreated = 0;
        this.nbrWindowDestroyed = 0;

        // Personal Space variables
        this.totToPSMoves = 0;
        this.totFromPSMoves = 0;
        this.maxNbrItemInPS = 0;
        this.actualPS = 0;

        // Window *Stats* variables
        this.totItemsInWindow = 0;
        this.maxItemsInWindow = 0;

        // Movement data
        this.totWalkDistance = 0;
        this.totItemMoveDist = 0;
        this.totWindowMoveDist = 0;
        this.totCursorMoveDist = 0;
        this.avgWallDist = 0;

        CursorDistanceStartSetter(this.transform.GetChild(0).position);
        WalkDistanceStartSetter(this.oldWalkPos);
        ItemDistanceStartSetter(this.oldItemPos);
        WindowDistanceStartSetter(this.oldWindowPos);
        CursorDistanceStartSetter(this.oldCursorPos);
        DistanceToWallStartSetter();

        InvokeRepeating("GetCursor", 0f, 0.1f);
        InvokeRepeating("GetDist", 0f, 0.1f);
        InvokeRepeating("GetItemMove", 0f, 0.1f);
        InvokeRepeating("DistanceInterPlayerAdding", 0f, 0.1f);
        InvokeRepeating("DistanceInterCursorAdding", 0f, 0.1f);

        Debug.Log("Triggered on " + this.name);
    }

    public void StartMeasures(GameObject g1, GameObject g2, string fileStamp, TextAsset actions, float offsetTime, string task, string condition, string session)
    {
        if (sessionStarted)
            return;

        this.session = session;
        this.condition = condition;
        this.task = task;

        countActions = 0;
        sessionStarted = true;

        this.actions = actions;
        this.replayActions = ParseActions(this.actions);
        
        // Unroll every actions happening before start of session
        while (countActions < replayActions.Length && replayActions[countActions] != "" && float.Parse(replayActions[countActions].Split(' ')[0]) + offsetConnection + offsetTime < Time.time)
        {
            Debug.Log(countActions + " " +   replayActions[countActions]);
            countActions++; 
        }        
        this.firstAction = float.Parse(replayActions[countActions].Split(' ')[0]) + offsetConnection + offsetTime;
        //Debug.Log(offsetConnection + " " + offsetTime + " " + this.firstAction);

        this.fileStamp = fileStamp;
        StartSession();

        DistanceInterPlayerSetter(g1, g2);
        DistanceInterCursorSetter(g1, g2);
    }

    public void EndSession(float endFromFirstAction)
    {
        if (this.sessionEnded)
        {
            Debug.Log("Already Ended");
            return;
        }

        this.sessionEnded = true;
        float endTime = Time.time - timeStart;

        //Debug.Log(avgWallDist + " " + endTime + " " +countMeasure);
        this.avgWallDist = this.avgWallDist / endTime;
        //  Debug.Log(avgWallDist);
        SessionResults sr = new SessionResults(this.name.Remove(0, 2), this.ToString(), this.history);
        
        using (StreamWriter log = new StreamWriter("./Measurement_Redo_" + this.fileStamp + ".txt", true))
        {
            //Debug.Log("Debug measure  : " + this.avgPlayerDistance + " " + endTime + " " + countMeasure + " ");
            log.WriteLine(this.session + " " + this.condition +  " " + this.task + " " + this.sessionValues[this.session + "" + this.condition] + " " + this.ToString() + " " + (this.avgPlayerDistance / endTime ) + " " + endTime + " " + endFromFirstAction + " " + (this.avgCursorDistance / endTime));
        }
    }

    public float FinishSession()
    {
        return Time.time - (this.firstAction + this.offsetTime);
    }

    public void UserConnection(float connectTime)
    {
        this.offsetConnection = connectTime;
    }

    private void OnDestroy()
    {

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        if (gizmoOn)
            Gizmos.DrawCube(this.transform.position, Vector3.one * 0.2f);
        //Gizmos.DrawLine(this.transform.position, this.transform.GetChild(0).position);
    }

    private string[] ParseActions(TextAsset ta)
    {
        string[] commands = ta.text.Split(new string[] { System.Environment.NewLine, "\r\n", "\r", "\n" }, StringSplitOptions.None);
        for (int i = 0; i < commands.Length; i++)
        {
            if(commands[i] != "")
                commands[i] = commands[i].Remove(0, 4);
        }
        return commands;
    }
    #endregion
}