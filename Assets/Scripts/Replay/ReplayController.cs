﻿using NaughtyAttributes;
using Synchro;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class ReplayController : MonoBehaviour
{
    TextAsset CinematicFile;
    TextAsset Cinematic116;
    TextAsset Cinematic180;

    [BoxGroup("Session Parameters")]
    [Dropdown("SessionValues")]
    public string session;
    [BoxGroup("Session Parameters")]
    [Dropdown("ConditionValues")]
    public string condition;
    [BoxGroup("Session Parameters")]
    [Dropdown("TaskValues")]
    public string task;

    string[] SessionValues = new string[] { "S0", "S1", "S2", "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10", "S11" };
    string[] ConditionValues = new string[] { "AR", "Wall" };
    string[] TaskValues = new string[] { "Classification", "Story" };


    public CustomNetworkManager CommandServer;
    public float StartDelay = 0f;
    public float ReplaySpeed = 1f;
    public Slider Progress;
    public Text TimeProgress;

    private Queue<ReplayCommand> ReplayCmdQueue;

    private float StartTime = 0f;
    private bool start = false;
    private int cmdCounter = 1;

    private float endTime = 0f;
    private int cmdCount = 0;

    public bool showLogs = false;
    public bool redoMeasurement = false;

    private string fileStamp;
    private bool isPlaying = false;

    // Start is called before the first frame update
    void Start()
    {
        string path = "FinalCinematic/" + session + "/" + session + "_" + condition + "_" + task;
        this.CinematicFile = Resources.Load(path + "_Cinematic") as TextAsset;
        this.Cinematic116 = Resources.Load(path + "_Actions116") as TextAsset;
        this.Cinematic180 = Resources.Load(path + "_Actions180") as TextAsset;
        
        this.ReplayCmdQueue = new Queue<ReplayCommand>();
        ParseCinematic(this.CinematicFile);
        this.TimeProgress.text = "Loaded";
        this.fileStamp = this.CinematicFile.name;

        using (StreamWriter log = new StreamWriter("./Measurement_Redo_" + this.fileStamp + ".txt", true))
        {
            log.WriteLine(Header());
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            if (!this.start)
                StartReplay();
            else if (!this.isPlaying)
                UnPause();
            else if (this.isPlaying)
                Pause();
    }

    private void FixedUpdate()
    {
        if (!this.start && Time.time >= 3f)
            StartReplay();

        if (this.start)
        {
            ReplayCommand rc = this.ReplayCmdQueue.Peek();
            while (this.ReplayCmdQueue.Count > 0 && rc.triggerTime + this.StartTime < Time.time)
            {
                if (this.showLogs || rc.cmd.GetType() == typeof(EndSession))
                    Debug.Log(this.cmdCounter + " " + rc.cmd.ToString());
                this.CommandServer.ReplayCommandSent(rc.triggerTime, this.ReplayCmdQueue.Dequeue().cmd, this.redoMeasurement, this.fileStamp);
                this.cmdCounter++;
                
                if (rc.cmd.GetType() == typeof(SpatialStatus))
                {
                    SpatialStatus s = (SpatialStatus)rc.cmd;

                    /*
                    if (s.name.Contains("Cursor"))
                    {
                        GameObject g = SynchroManager.Instance.GetObject(s.name);
                        using (StreamWriter log = new StreamWriter(this.fileStamp + "_Global.txt", true))
                        {
                            log.WriteLine(rc.triggerTime + " " + g.name + " " + g.transform.position.ToString("F3").Replace(" ", "") + " " + g.transform.rotation.ToString("F3").Replace(" ", "") + " " + g.transform.localScale.ToString("F3").Replace(" ", ""));
                        }
                    }
                    GameObject g = SynchroManager.Instance.GetObject(s.name);
                    using (StreamWriter log = new StreamWriter(this.fileStamp + "_GlobalStrip.txt", true))
                    {
                        log.WriteLine(rc.triggerTime + " SpatialStatus " + g.name + " " + g.transform.position.ToString("F3").Replace(" ", "") + " " + g.transform.rotation.ToString("F3").Replace(" ", "") + " " + g.transform.localScale.ToString("F3").Replace(" ", ""));
                    }
                    */
                }
                else if(rc.cmd.GetType() == typeof(StartSession))
                {
                    /*
                    using (StreamWriter log = new StreamWriter(this.fileStamp + "_Global.txt", true))
                    {
                        log.WriteLine(rc.cmd.ToString());
                    }
                    using (StreamWriter log = new StreamWriter(this.fileStamp + "_GlobalStrip.txt", true))
                    {
                        log.WriteLine(rc.cmd.ToString());
                    }
                                        */

                }
                else if (rc.cmd.GetType() == typeof(EndSessionResults))
                {
                    /*
                    using (StreamWriter log = new StreamWriter(this.fileStamp + "_GlobalStrip.txt", true))
                    {
                        log.WriteLine(rc.cmd.ToString());
                    }
                    */
                }

                if (this.ReplayCmdQueue.Count == 0)
                {
                    Debug.Log("<color=#009900>End Of Replay</color>");
                    this.start = false;
                    continue;
                }

                rc = this.ReplayCmdQueue.Peek();
            }

            this.TimeProgress.text = (Time.time - this.StartTime).ToString() + " / " + this.endTime.ToString();
            this.Progress.value = this.cmdCounter;
        }
    }

    private void OnDestroy()
    {
        Time.timeScale = 1;
    }

    private void ParseCinematic(TextAsset ta)
    {
        string[] commands = ta.text.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
        int it = 0;

        foreach (string s in commands)
        {
            if (s.Length == 0)
                continue;

            string[] command = s.Split(' ');
            ISynchroCommand isc;

            List<int> ownerList = new List<int>();
            //Debug.Log(s);
            switch (command[2])
            {
                case "SpatialStatus":
                    isc = new SpatialStatus(command[3], ParseToVector3(command[4]), ParseToQuaternion(command[5]), ParseToVector3(command[6]));
                    break;
                case "TransformStatusUpdate":
                    isc = new SpatialStatus(command[3], ParseToVector3(command[4]), ParseToQuaternion(command[5]), ParseToVector3(command[6]));
                    break;
                case "SpawnObject":
                    foreach (string owner in command[10].Split('-'))
                        if (owner != "null")
                            ownerList.Add(int.Parse(owner));
                    isc = new SpawnObject(command[1], command[3], command[4], (command[5] == "") ? null : command[5], ParseToVector3(command[6]), ParseToQuaternion(command[7]), ParseToVector3(command[8]), int.Parse(command[9]), ownerList);
                    break;
                case "TakeOwnership":
                    isc = new TakeOwnership(command[3], command[1]);
                    break;
                case "MakePublic":
                    isc = new MakePublic(command[3], command[1]);
                    break;
                case "Connect":
                    isc = new Register(command[3], command[1]);
                    /*
                    player = (GameObject)Instantiate(CustomNetworkManager.singleton.playerPrefab, Vector3.zero, Quaternion.identity);
                    player.name = command[3];
                    player.transform.GetChild(0).name = "Cursor" + command[3].Substring(2);
                    Destroy(player.GetComponent<InitializePlayerOnNetwork>());

                    PersonAuthorViewRights pavr = player.GetComponent<PersonAuthorViewRights>();
                    //Debug.Log("Generated" + this.isGenerated);

                    pavr.SetPS(player.transform.GetChild(0).GetChild(0).gameObject);
                    pavr.MakeShared(new List<int>() { int.Parse(SynchroManager.Instance.ownerId) });

                    if (!this.isGenerated)
                    {
                        this.selfColor = SynchroManager.Instance.fullOwners.IndexOf(int.Parse(SynchroManager.Instance.ownerId));
                        this.transform.GetChild(0).name += SynchroManager.Instance.ownerId;
                        this.transform.GetChild(0).GetChild(0).name += SynchroManager.Instance.ownerId;
                    }
                    else
                    {
                        this.GetComponentInChildren<FollowCursor>().enabled = false;
                    }

                    // for debug on pc //
                    if (this.selfColor == -1)
                        this.selfColor = 0;

                    this.focusGreen = Resources.Load("Focus0") as Material;
                    this.focusRed = Resources.Load("Focus1") as Material;
                    this.focusYellow = Resources.Load("Focus2") as Material;

                    // color PS indicator with the right material
                    this.transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material = (this.selfColor == 0) ? this.focusGreen : ((this.selfColor == 1) ? this.focusRed : this.focusYellow);
                    this.GetComponentInChildren<FollowCursor>().transform.GetChild(0).gameObject.SetActive(false);

                    SynchroManager.Instance.AddShared(this.transform.GetChild(0).gameObject);

                    this.GetComponent<Renderer>().material.SetColor("_WireColor", this.colorPalette[this.selfColor]);
                    this.GetComponentInChildren<SpriteRenderer>().color = this.colorPalette[this.selfColor];

                    */

                    isc = new Connect(command[1]);
                    break;
                case "Disconnect":
                    // grey out item
                    isc = new DisConnect(command[1]);
                    break;
                case "Reconnect":
                    // relight item
                    isc = new Connect(command[1]);
                    break;
                case "Register":
                    isc = new Register(command[3], command[1]);
                    break;
                case "UpdatePresence":
                    List<string> nameList = new List<string>();
                    foreach (string owner in command[3].Split('-'))
                        if (owner != "null")
                            nameList.Add(owner);
                    isc = new UpdatePresence(nameList, command[1]);
                    break;
                case "ChangePermission":
                    foreach (string owner in command[5].Split('-'))
                        if (owner != "null")
                            ownerList.Add(int.Parse(owner));
                    isc = new ChangePermission(command[1], command[3], int.Parse(command[4]), ownerList);
                    break;
                case "ChangeSurface":
                    isc = new ChangeSurface(command[1], command[3], (command[4] == "") ? null : command[4], bool.Parse(command[5]));
                    break;
                case "DeleteObject":
                    isc = new DeleteObject(command[1], command[3]);
                    break;
                case "UpdateCalibration":
                    isc = new UpdateCalibration(command[1], ParseToVector3(command[3]), ParseToQuaternion(command[4]));
                    break;
                case "TakeObject":
                    isc = new TakeObject(command[1], command[3], command[4] == true.ToString());
                    break;
                case "LetObject":
                    isc = new LetObject(command[1], command[3], command[4] == true.ToString());
                    break;
                case "ActivatePersonalSpace":
                    isc = new ActivatePersonalSpace(command[1], command[3] == true.ToString());
                    break;
                case "EndSession":
                    //isc = new SpatialStatus();
                    continue;
                    break;
                case "StartSession":
                    isc = new StartSession(command[1], command[0], Cinematic116, Cinematic180, this.StartTime, this.task, this.condition, this.session);
                    break;
                case "SessionsResults":
                    isc = new EndSessionResults(command[1], command[0]);
                    break;                

                default:
                    //isc = new SpatialStatus();

                    Debug.LogError("Invalid command type REally ? at line : " + it + " " + s);
                    continue;
                    break;

            }


            it++;
            this.ReplayCmdQueue.Enqueue(new ReplayCommand(float.Parse(command[0]), isc));
        }

        this.cmdCount = this.ReplayCmdQueue.Count;
        this.endTime = float.Parse((commands[this.cmdCount - 1].Split(' '))[0]);
        Debug.Log((commands[this.cmdCount - 1].Split(' '))[0]);
        Debug.Log("Cinematic File parsed. Size : " + this.ReplayCmdQueue.Count);
    }

    [Button]
    public void StartReplay()
    {
        this.StartTime = Time.time - this.StartDelay;
        this.start = true;
        this.isPlaying = true;
        Time.timeScale = this.ReplaySpeed;

        this.Progress.minValue = 0;
        this.Progress.maxValue = this.cmdCount;
    }

    [Button]
    public void Pause()
    {
        this.isPlaying = false;
        Time.timeScale = 0f;
    }

    [Button]
    public void UnPause()
    {
        this.isPlaying = true;
        Time.timeScale = this.ReplaySpeed;
    }

    public static List<ISynchroCommand> StringToCommands(string commandBatch)
    {
        List<ISynchroCommand> commandList = new List<ISynchroCommand>();

        string[] commands = commandBatch.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);

        foreach (string s in commands)
        {
            string[] command = s.Split(' ');
            if(command.Length < 2)
            {
                continue;
            }


            ISynchroCommand isc = null;
            List<int> ownerList = new List<int>();
            GameObject player;

            switch (command[2])
            {
                case "SpatialStatus":
                    isc = new SpatialStatus(command[3], ParseToVector3(command[4]), ParseToQuaternion(command[5]), ParseToVector3(command[6]));
                    break;
                case "TransformStatusUpdate":
                    isc = new SpatialStatus(command[3], ParseToVector3(command[4]), ParseToQuaternion(command[5]), ParseToVector3(command[6]));
                    break;
                case "SpawnObject":
                    foreach (string owner in command[10].Split('-'))
                        if (owner != "null")
                            ownerList.Add(int.Parse(owner));
                    isc = new SpawnObject(command[1], command[3], command[4], (command[5] == "") ? null : command[5], ParseToVector3(command[6]), ParseToQuaternion(command[7]), ParseToVector3(command[8]), int.Parse(command[9]), ownerList);
                    break;
                case "TakeOwnership":
                    isc = new TakeOwnership(command[3], command[1]);
                    break;
                case "MakePublic":
                    isc = new MakePublic(command[3], command[1]);
                    break;
                case "Connect":
                    isc = new Register(command[3], command[1]);
                    /*
                    player = (GameObject)Instantiate(CustomNetworkManager.singleton.playerPrefab, Vector3.zero, Quaternion.identity);
                    player.name = command[3];
                    player.transform.GetChild(0).name = "Cursor" + command[3].Substring(2);
                    Destroy(player.GetComponent<InitializePlayerOnNetwork>());

                    PersonAuthorViewRights pavr = player.GetComponent<PersonAuthorViewRights>();
                    //Debug.Log("Generated" + this.isGenerated);

                    pavr.SetPS(player.transform.GetChild(0).GetChild(0).gameObject);
                    pavr.MakeShared(new List<int>() { int.Parse(SynchroManager.Instance.ownerId) });

                    if (!this.isGenerated)
                    {
                        this.selfColor = SynchroManager.Instance.fullOwners.IndexOf(int.Parse(SynchroManager.Instance.ownerId));
                        this.transform.GetChild(0).name += SynchroManager.Instance.ownerId;
                        this.transform.GetChild(0).GetChild(0).name += SynchroManager.Instance.ownerId;
                    }
                    else
                    {
                        this.GetComponentInChildren<FollowCursor>().enabled = false;
                    }

                    // for debug on pc //
                    if (this.selfColor == -1)
                        this.selfColor = 0;

                    this.focusGreen = Resources.Load("Focus0") as Material;
                    this.focusRed = Resources.Load("Focus1") as Material;
                    this.focusYellow = Resources.Load("Focus2") as Material;

                    // color PS indicator with the right material
                    this.transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material = (this.selfColor == 0) ? this.focusGreen : ((this.selfColor == 1) ? this.focusRed : this.focusYellow);
                    this.GetComponentInChildren<FollowCursor>().transform.GetChild(0).gameObject.SetActive(false);

                    SynchroManager.Instance.AddShared(this.transform.GetChild(0).gameObject);

                    this.GetComponent<Renderer>().material.SetColor("_WireColor", this.colorPalette[this.selfColor]);
                    this.GetComponentInChildren<SpriteRenderer>().color = this.colorPalette[this.selfColor];

                    */

                    isc = new Connect(command[1]);
                    break;
                case "Disconnect":
                    // grey out item
                    isc = new DisConnect(command[1]);
                    break;
                case "Reconnect":
                    // relight item
                    isc = new Connect(command[1]);
                    break;
                case "ChangePermission":
                    foreach (string owner in command[5].Split('-'))
                        if (owner != "null")
                            ownerList.Add(int.Parse(owner));
                    isc = new ChangePermission(command[1], command[3], int.Parse(command[4]), ownerList);
                    break;
                case "ChangeSurface":
                    isc = new ChangeSurface(command[1], command[3], (command[4] == "") ? null : command[4], bool.Parse(command[5]));
                    break;
                case "DeleteObject":
                    isc = new DeleteObject(command[1], command[3]);
                    break;
                case "UpdateCalibration":
                    isc = new UpdateCalibration(command[1], ParseToVector3(command[3]), ParseToQuaternion(command[4]));
                    break;
                case "TakeObject":
                    isc = new TakeObject(command[1], command[3], command[4] == true.ToString());
                    break;
                case "LetObject":
                    isc = new LetObject(command[1], command[3], command[4] == true.ToString());
                    break;
                case "ActivatePersonalSpace":
                    isc = new ActivatePersonalSpace(command[1], command[3] == true.ToString());
                    break;
                case "RecoverPersonalSpace":
                    isc = new RecoverPersonalSpace(command[1], command[3], command[4]);
                    break;
                case "EndSession":
                    break;
                case "StartSession":
                    isc = new StartSession(command[1], command[0]);
                    break;
                case "SessionsResults":
                    isc = new EndSessionResults(command[1], command[0]);
                    break;
                default:
                    Debug.LogError("Invalid command type REally ?");
                    break;

            }

            if(isc != null)
                commandList.Add(isc);
        }

        return commandList;
    }


    public string Header()
    {
        return "Session Condition Task DataSet IP nbrMove nbrMoveToPS nbrSelect nbrBatchMoveSelection nbrBatchMoveSurface nbrBatchMoveFromPS nbrExpandPS nrbEmptySurface nrbEmptySelection nbrRelayout nbrWindowCreated nbrWindowDestroyed totToPSMoves totFromPSMoves maxNbrItemInPS totItemsInWindow maxItemsInWindow totWalkDistance totItemMoveDistance totWindowMoveDistance totCursorMoveDist avgWallDistance interPlayerDistance timeToComplete timeFromFirstInteraction";
    }

    public static string CommandsToString(List<ISynchroCommand> commandBatch)
    {
        StringBuilder commandList = new StringBuilder();
        
        foreach (ISynchroCommand c in commandBatch)
        {
            commandList.AppendLine(c.ToString());
        }

        return commandList.ToString();
    }

    private static Vector3 ParseToVector3(string v)
    {
        string[] values = v.Trim(new char[] { '(', ')' }).Split(',');
        return new Vector3(float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2]));

    }

    private static Quaternion ParseToQuaternion(string q)
    {
        string[] values = q.Trim(new char[] { '(', ')' }).Split(',');
        return new Quaternion(float.Parse(values[0]), float.Parse(values[1]), float.Parse(values[2]), float.Parse(values[3]));
    }

}
public class ReplayCommand
{
    public float triggerTime;
    public ISynchroCommand cmd;

    public ReplayCommand(float triggerTime, ISynchroCommand cmd)
    {
        this.triggerTime = triggerTime;
        this.cmd = cmd;
    }

    public void Apply()
    {
        this.cmd.Apply();
    }

    public override string ToString()
    {
        return this.cmd.ToString();
    }
}