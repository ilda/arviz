﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.

using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using NaughtyAttributes;
using UnityEngine;

/// <summary>
/// This script allows for an object to be movable, scalable, and rotatable with one or two hands. 
/// You may also configure the script on only enable certain manipulations. The script works with 
/// both HoloLens' gesture input and immersive headset's motion controller input.
/// </summary>
public class TapWallSlate : InteractionScript, IMixedRealitySourceStateHandler
{ 
    private bool isLost = false;
    private GameObject follow;
    private float follow_distance;
    private float endDragTime;

    protected override void Update()
    {
        if (isLost)
        {
            transform.position = Camera.main.transform.position + Camera.main.transform.forward * follow_distance;
            if (isMagnetic)
            {
                Vector3 pos = transform.position;
                Quaternion rot = transform.rotation;
                Magnetism(max_dist, ref pos, ref rot);
            }
        }
     }

    public void OnSourceDetected(SourceStateEventData eventData)
    {
        if (isLost)
        {
            // Doesn't work, when hand found still attached
            // MixedRealityToolkit.InputSystem.Unregister(gameObject);
            // isLost = false;
            // Drop item as it means we found back our hand, or if possible make it go back to hand            
            transform.position = follow.transform.position;
            Destroy(follow);
        }        
    }

    public void OnSourceLost(SourceStateEventData eventData)
    {
        if (Time.time - endDragTime < 1.0f)
        {
            isLost = true;

            MixedRealityToolkit.InputSystem.Register(gameObject);
            // Allow item to follow gaze by adding a follower solver

            follow_distance = Vector3.Distance(transform.position, Camera.main.transform.position);
        }
    }


    public override void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        if (!isLost)
        {
            base.OnPointerDown(eventData);
        }
    }

    public override void OnPointerDragged(MixedRealityPointerEventData eventData)
    {
        if (!isLost)
        {
            base.OnPointerDragged(eventData);
        }
    }

    public override void OnPointerUp(MixedRealityPointerEventData eventData)
    {
        if (isLost)
        {
            MixedRealityToolkit.InputSystem.Unregister(gameObject);
            isLost = false;
        }
        else
        {
            base.OnPointerUp(eventData);
        }
    }

    [Button]
    public void buttonAction()
    {
        int pointer = pointerIdToPointerMap.Count;
        Debug.Log("Pointer number " + pointer);
    }    
}
