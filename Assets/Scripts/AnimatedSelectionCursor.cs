﻿// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License. See LICENSE in the project root for license information.

using Microsoft.MixedReality.Toolkit;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using Synchro;
using UnityEngine;

public class AnimatedSelectionCursor : BaseCursor
{
    [SerializeField]
    [Header("Animated Cursor State Data")]
    [Tooltip("Cursor state data to use for its various states.")]
    private AnimatedCursorStateData[] cursorStateData = null;

    [SerializeField]
    [Header("Animated Cursor Context Data")]
    [Tooltip("Cursor context data to use for its various contextual states.")]
    private AnimatedCursorContextData[] cursorContextData = null;

    [SerializeField]
    [Tooltip("Animator parameter to set when input is enabled.")]
    private AnimatorParameter inputEnabledParameter = default(AnimatorParameter);

    [SerializeField]
    [Tooltip("Animator parameter to set when input is disabled.")]
    private AnimatorParameter inputDisabledParameter = default(AnimatorParameter);

    [SerializeField]
    [Tooltip("Animator for the cursor")]
    private Animator cursorAnimator = null;

    public GameObject SelectionCursor;
    private Color[] colorArray = new Color[] {new Color(0, 1, 0), new Color(1, 0, 0), new Color(1, 1, 0) };

    bool selectionMode = false;
    string lastSelected = null;

    bool isDragging = false;
    float dragTime = 0.0f;
    float deltaClickAction = 0.3f;

    bool isBusy = false;
    IMixedRealityInputSystem InputSystem;

    protected override void Start()
    {
        base.Start();
        Color c;
        switch (SynchroManager.Instance.fullOwners.IndexOf(int.Parse(SynchroManager.Instance.ownerId)))
        {
            case 0:
                c = this.colorArray[0];
                break;
            case 1:
                c = this.colorArray[1];
                break;
            case 2:
                c = this.colorArray[2];
                break;
            default:
                c = this.colorArray[0];
                break;
        }
        this.SelectionCursor.GetComponent<Renderer>().material.color = c;
    }

    public void IsDragged(bool isBusy)
    {
        this.isBusy = isBusy;
    }

    protected override void UpdateCursorTransform()
    {
        base.UpdateCursorTransform();
    }

    public override void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        base.OnPointerDown(eventData);
        GameObject gazeTarget = CoreServices.InputSystem?.GazeProvider.GazeTarget;
        InputSystem = CoreServices.InputSystem;

        if (!this.isBusy && (gazeTarget == null || (gazeTarget != null && gazeTarget.layer == LayerMask.NameToLayer("Surface"))))
        {
            this.selectionMode = true;
            this.lastSelected = null;
            this.dragTime = Time.time;
        }
    }    

    public override void OnPointerDragged(MixedRealityPointerEventData eventData)
    {        
        base.OnPointerDragged(eventData);

        GameObject gazeTarget = CoreServices.InputSystem?.GazeProvider.GazeTarget;
        //Debug.Log(" gaze Target " + (gazeTarget != null) + " gazeTag " + gazeTarget.tag + " gazeName " + gazeTarget.name + " FocusLock " + InputSystem.GazeProvider.GazePointer.IsTargetPositionLockedOnFocusLock + " PieMenu " + PieMenu.Instance.isOn());
        if (this.selectionMode && (Time.time - this.dragTime) > this.deltaClickAction)
        {
            if (!this.cursorAnimator.GetBool("isDragging"))
            {
                InputSystem.GazeProvider.GazePointer.IsTargetPositionLockedOnFocusLock = false;

                this.cursorAnimator.SetBool("isDragging", true);
                OnCursorStateChange(CursorStateEnum.Contextual);
            }


            if (gazeTarget != null)
            {
                if (gazeTarget.tag == "Slate" && gazeTarget.name != this.lastSelected)
                {
                    gazeTarget.GetComponentInParent<InteractionScript>().SelectItem();
                    this.lastSelected = gazeTarget.name;
                }
                else if (gazeTarget.name != this.lastSelected)
                {
                    this.lastSelected = null;
                }
            }
        }
        else if (InputSystem.GazeProvider.GazePointer.IsTargetPositionLockedOnFocusLock && PieMenu.Instance.isOn())
        {
            //Debug.Log("menu " + PieMenu.Instance.enabled);
            InputSystem.GazeProvider.GazePointer.IsTargetPositionLockedOnFocusLock = false;
        }
    }

    public override void OnPointerUp(MixedRealityPointerEventData eventData)
    {
        base.OnPointerUp(eventData);

        if (this.selectionMode)
        {
            if (!this.cursorAnimator.GetBool("isDragging"))
            {
                SelectionController.Instance.ResetSelection();
            }
            else
            {
                this.cursorAnimator.SetBool("isDragging", false);
                OnCursorStateChange(CursorStateEnum.None);
            }
        }

        if (!InputSystem.GazeProvider.GazePointer.IsTargetPositionLockedOnFocusLock)
            InputSystem.GazeProvider.GazePointer.IsTargetPositionLockedOnFocusLock = true;

        this.selectionMode = false;
    }

    /// <summary>
    /// Change animation state when enabling input.
    /// </summary>
    public override void OnInputEnabled()
    {
        base.OnInputEnabled();
        SetAnimatorParameter(inputEnabledParameter);
    }

    /// <summary>
    /// Change animation state when disabling input.
    /// </summary>
    public override void OnInputDisabled()
    {
        base.OnInputDisabled();
        SetAnimatorParameter(inputDisabledParameter);
    }

    /// <summary>
    /// Override to set the cursor animation trigger.
    /// </summary>
    public override void OnFocusChanged(FocusEventData eventData)
    {
        base.OnFocusChanged(eventData);

        if (((Pointer is UnityEngine.Object) ? ((Pointer as UnityEngine.Object) != null) : (Pointer != null)) && Pointer.CursorModifier != null)
        {
            if ((Pointer.CursorModifier.CursorParameters != null) && (Pointer.CursorModifier.CursorParameters.Length > 0))
            {
                OnCursorStateChange(CursorStateEnum.Contextual);

                for (var i = 0; i < Pointer.CursorModifier.CursorParameters.Length; i++)
                {
                    SetAnimatorParameter(Pointer.CursorModifier.CursorParameters[i]);
                }
            }
        }
        else
        {
            OnCursorStateChange(CursorStateEnum.None);
        }
    }

    /// <summary>
    /// Override OnCursorState change to set the correct animation state for the cursor.
    /// </summary>
    /// <param name="state"></param>
    public override void OnCursorStateChange(CursorStateEnum state)
    {
        base.OnCursorStateChange(state);

        for (int i = 0; i < cursorStateData.Length; i++)
        {
            if (cursorStateData[i].CursorState == state)
            {
                SetAnimatorParameter(cursorStateData[i].Parameter);
            }
        }
    }

    /// <summary>
    /// Override OnCursorContext change to set the correct animation state for the cursor.
    /// </summary>
    /// <param name="context"></param>
    public override void OnCursorContextChange(CursorContextEnum context)
    {
        base.OnCursorContextChange(context);

        if (context == CursorContextEnum.Contextual) { return; }

        for (int i = 0; i < cursorContextData.Length; i++)
        {
            if (cursorContextData[i].CursorState == context)
            {
                SetAnimatorParameter(cursorContextData[i].Parameter);
            }
        }
    }

    /// <summary>
    /// Based on the type of animator state info pass it through to the animator
    /// </summary>
    /// <param name="animationParameter"></param>
    private void SetAnimatorParameter(AnimatorParameter animationParameter)
    {
        // Return if we do not have an animator
        if (cursorAnimator == null || !cursorAnimator.isInitialized)
        {
            return;
        }

        switch (animationParameter.ParameterType)
        {
            case AnimatorControllerParameterType.Bool:
                cursorAnimator.SetBool(animationParameter.NameHash, animationParameter.DefaultBool);
                break;
            case AnimatorControllerParameterType.Float:
                cursorAnimator.SetFloat(animationParameter.NameHash, animationParameter.DefaultFloat);
                break;
            case AnimatorControllerParameterType.Int:
                cursorAnimator.SetInteger(animationParameter.NameHash, animationParameter.DefaultInt);
                break;
            case AnimatorControllerParameterType.Trigger:
                cursorAnimator.SetTrigger(animationParameter.NameHash);
                break;
        }
    }
}
