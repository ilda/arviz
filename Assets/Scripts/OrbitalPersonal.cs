﻿using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using UnityEngine;

public class OrbitalPersonal : Solver
{
    public Vector3 deltaPosition;
    public float downDelta;
    public float localAngle;

    // Start is called before the first frame update
    void Start()
    {
    }

    public override void SolverUpdate()
    {

        Vector3 desiredPos = this.SolverHandler.TransformTarget != null ? this.SolverHandler.TransformTarget.position : Vector3.zero;       

        Quaternion desiredRot = this.SolverHandler.TransformTarget != null ? this.SolverHandler.TransformTarget.rotation : Quaternion.identity ;
        Vector3 desiredEuler = desiredRot.eulerAngles;
        
        this.GoalPosition = desiredPos + this.deltaPosition + new Vector3(0f, -this.downDelta, 0f);
        
        float angleAxis = this.localAngle;

        desiredRot = (Quaternion.AngleAxis(this.localAngle, Vector3.up) * Quaternion.Euler(desiredEuler.x, 0f, desiredEuler.z)).normalized;
        
        Vector3 noRotAxis = this.transform.forward;
        Vector3 orthonormal1 = this.transform.up;

        Vector3 transformed = desiredRot * orthonormal1;
        
        // Project transformed vector onto plane
        Vector3 flattened = transformed - (Vector3.Dot(transformed, noRotAxis) * noRotAxis);
        flattened.Normalize();

        /* WIP
        // Get angle between original vector and projected transform to get angle around normal
        float a = (float)Mathf.Acos(Vector3.Dot(orthonormal1, flattened));
        Debug.Log(a * Mathf.Rad2Deg);
        GoalRotation = (desiredRot * Quaternion.AngleAxis(-a * Mathf.Rad2Deg, noRotAxis)).normalized;
        */
        
        this.GoalRotation = desiredRot;
        
        UpdateWorkingPositionToGoal();
        UpdateWorkingRotationToGoal();
        
    }

    public void OutOfBelt()
    {
        this.GetComponent<PieMenuClickInteraction>().isPersonal = false;
    }

    public void InBelt()
    {
        this.GetComponent<PieMenuClickInteraction>().isPersonal = true;
    }
}
