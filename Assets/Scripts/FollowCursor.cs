﻿using Microsoft.MixedReality.Toolkit.Input;
using Synchro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCursor : MonoBehaviour
{
    public GameObject cursor;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponentInChildren<SpriteRenderer>().enabled = false;
        if (this.cursor)
        {
            this.transform.position = this.cursor.transform.position;
            this.transform.rotation = this.cursor.transform.rotation;
        }
        else
        {
            this.cursor = Camera.main.GetComponent<GazeProvider>()?.GazeCursor.GameObjectReference;
        }
    }
}
