﻿using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using Synchro;
using UnityEngine;

public class PersonAuthorViewRights : AuthorViewRights
{
    public int selfColor;
    private Color[] colorPalette = new Color[] { Color.green, Color.red, Color.yellow };

    Material focusGreen;
    Material focusRed;
    Material focusYellow;

    GameObject PS;

    public override void MakeForeign()
    {
        throw new System.NotImplementedException();
    }

    public override void MakePrivate()
    {
        this.viewPermissions.SwitchPrivate();
    }

    public override void MakePublic()
    {
        throw new System.NotImplementedException();
    }

    public override void MakeShared(List<int> owners)
    {
        this.viewPermissions.SwitchShared(owners);
    }

    public override void RemoteMakeForeign()
    {
        throw new System.NotImplementedException();
    }

    public override void RemoteMakePrivate()
    {
        throw new System.NotImplementedException();
    }

    public override void RemoteMakePublic()
    {
        throw new System.NotImplementedException();
    }

    public override void RemoteMakeShared(List<int> owners)
    {
        throw new System.NotImplementedException();
    }

    public override void SlatePermissionDisplay(bool value)
    {
        throw new System.NotImplementedException();
    }

    protected override void Awake()
    {
        this.viewPermissions = new Permissions();
        this.changePermission = new ChangePermission
        {
            owner = ((CustomNetworkManager)CustomNetworkManager.singleton).GetOwnerId()
        };
    }

    // Start is called before the first frame update
    void Start()
    {
        this.PS = this.transform.GetChild(0).GetChild(0).gameObject;


        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS && this.name.Contains(SynchroManager.Instance.ownerId))
        {
            Measurement.Instance.WalkDistanceStartSetter(this.transform.position);
            InvokeRepeating("GetDist", 0f, 0.1f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKey(KeyCode.P))
            this.GetComponent<MeshRenderer>().enabled = !this.GetComponent<MeshRenderer>().enabled;
        if (Input.GetKey(KeyCode.C))
            this.CatchUp();
            */
    }


    private void GetDist()
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
        {
            Debug.Log("ok " + this.name);

            Measurement.Instance.WalkDistanceAdding(this.transform.position);
            Measurement.Instance.DistanceToWallAdding(Vector3.Distance(this.transform.position, Vector3.ProjectOnPlane(this.transform.position, SynchroManager.Instance.Wall.transform.forward)));
        }
    }

    public void ActivatePS(bool value)
    {
        this.PS.SetActive(value);
    }

    [Button]
    public void ReCalibrate()
    {
        SynchroManager.Instance.OrderCalibration(this.name);
    }

    [Button]
    public void CatchUp()
    {
        CatchUp catchUp = new CatchUp();

        string topic = SynchroManager.Instance.topic;
        string owner = SynchroManager.Instance.ownerId;

        List<ISynchroCommand> spO = new List<ISynchroCommand>();
        List<ISynchroCommand> cs = new List<ISynchroCommand>();
        List<ISynchroCommand> lo = new List<ISynchroCommand>();
        List<ISynchroCommand> cp = new List<ISynchroCommand>();

        TransformsStatusUpdate tsu = new TransformsStatusUpdate(SynchroManager.Instance.ownerId, new List<string>(), new List<Vector3>(), new List<Quaternion>(), new List<Vector3>());

        SurfaceAuthorViewRights[] surfaces = FindObjectsOfType<SurfaceAuthorViewRights>();
        SlateAuthorViewRights[] slates = FindObjectsOfType<SlateAuthorViewRights>();

        foreach(SurfaceAuthorViewRights s in surfaces)
        {
            spO.Add(new SpawnObject(SynchroManager.Instance.ownerId, s.name, "FloatingWindowQuad", s.transform.parent.name, 
                s.transform.localPosition, s.transform.localRotation, s.transform.localScale,
                Permissions.PrivacyConverter(s.viewPermissions.PermissionState()), s.viewPermissions.GetCollaborators()));
            //Debug.Log(spO[spO.Count-1].ToString());
        }

        tsu = new TransformsStatusUpdate(owner, new List<string>(), new List<Vector3>(), new List<Quaternion>(), new List<Vector3>());

        foreach (SlateAuthorViewRights sl in slates)
        {
            // 0 - Surface  
            cs.Add(new ChangeSurface(owner, sl.name, sl.transform.parent.name, false));            
            // 1 - Author Rights
            cp.Add(new ChangePermission(owner, sl.name, Permissions.PrivacyConverter(sl.viewPermissions.PermissionState()), sl.viewPermissions.GetCollaborators()));
            // 2 - Positions            
            tsu.AddChange(sl.name, sl.transform.localPosition, sl.transform.localRotation, sl.transform.localScale);
        }

        foreach(ObjectLockOwnership o in SynchroManager.Instance.GetLockedObjects())
        {
            if (o.owner == this.name.Remove(0, 2))
            {
                lo.Add(new RecoverPersonalSpace(SynchroManager.Instance.ownerId, o.owner, o.objectName));
            }
            else 
            {
                lo.Add(new TakeObject(o.owner, o.objectName, false));
            }
        }

        catchUp.owner = SynchroManager.Instance.ownerId;
        catchUp.target = this.name.Remove(0, 2);
        catchUp.toSpawn = ReplayController.CommandsToString(spO);
        catchUp.toChangeSurface = ReplayController.CommandsToString(cs);
        catchUp.toLetObject = ReplayController.CommandsToString(lo);
        catchUp.toChangePermission = ReplayController.CommandsToString(cp);
        catchUp.toPosition = ReplayController.CommandsToString(new List<ISynchroCommand> { tsu });

        //Debug.Log(SynchroManager.Instance.GetLockedObjects().Count);
        //Debug.Log(catchUp.toLetObject);

        SynchroManager.Instance.CommandInstance.Cmd_CatchUp(catchUp.owner, catchUp.target, catchUp.toSpawn, catchUp.toChangeSurface, catchUp.toLetObject, catchUp.toChangePermission, catchUp.toPosition);
        //SynchroManager.Instance.GetComponent<MasterRedirection>().LaunchUpdatePresence();
    }

    public void SetPS(GameObject g)
    {
        this.PS = g;
    }
}
