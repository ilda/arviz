﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Microsoft.MixedReality.Toolkit;
using Synchro;
using Microsoft.MixedReality.Toolkit.Input.Utilities;
using NaughtyAttributes;

public class PieMenuClickSurfaceInteraction : InteractionScript, IMixedRealitySourceStateHandler
{
    private GraphicRaycaster graphicRaycast;
    private EventSystem m_EventSystem;
    private PointerEventData m_PointerEventData;

    private bool activeMenu = false;
    private GameObject sphere;
    private GameObject sphere2;

    private bool waitForAction = false;

    List<RaycastResult> results = new List<RaycastResult>();
    IMixedRealityPointer pointer;

    Transform cameraTr;

    bool moveOrdered = false;
    bool moveOrderedCenterHead = false;
    bool hybridMoveOrdered = false;
    bool isLost = false;

    Vector3 deltaDistance;
    float itemDistance;
    DeleteObject delObj = new DeleteObject();

    public AudioClip PressClip;
    private AudioSource audio;

    protected override void Start()
    {
        base.Start();
        changeSurfaceMessage.isWindow = true;
        cameraTr = Camera.main.transform;
        delObj.owner = SynchroManager.Instance.ownerId;
        delObj.name = hostTransform.name;

        audio = gameObject.AddComponent<AudioSource>();
    }

    protected override void Update()
    {
        base.Update();


        if (activeMenu)
        {

        }

        if (moveOrdered)
        {
            Vector3 targetPosition;
            Quaternion targetRotation = transform.rotation;
            if (isLost)
            {
                targetPosition = cameraTr.position + (gazeProvider.HitPosition - cameraTr.position).normalized * 0.6f;
                targetRotation = cameraTr.rotation;
            }
            else
            {
                deltaDistance = (pointer.Position - Camera.main.transform.position).normalized;
                targetPosition = pointer.Position + deltaDistance.normalized * itemDistance;

                targetRotation = Quaternion.Euler(0, pointer.Rotation.eulerAngles.y, 0);
            }


            // Apply magnetism effect when getting inside a defined effect area
            if (isMagnetic)
            {
                Magnetism(max_dist, ref targetPosition, ref targetRotation);
            }

            float lerpAmount = GetLerpAmount();
            Quaternion smoothedRotation = Quaternion.Lerp(hostTransform.rotation, targetRotation, lerpAmount);
            Vector3 smoothedPosition = Vector3.Lerp(hostTransform.position, targetPosition, lerpAmount);
            hostTransform.SetPositionAndRotation(smoothedPosition, smoothedRotation);
        }

        if (moveOrderedCenterHead)
        {
            Quaternion targetRotation = transform.rotation;
            Vector3 targetPosition;

            targetPosition = gazeProvider.HitPosition;
            targetRotation = cameraTr.rotation;

            // Apply magnetism effect when getting inside a defined effect area
            if (isMagnetic)
            {
                Magnetism(max_dist, ref targetPosition, ref targetRotation);
            }

            float lerpAmount = GetLerpAmount();
            targetPosition = targetPosition - new Vector3(0f, hostTransform.transform.localScale.y * 1.925f / 2f, 0f);
            Quaternion smoothedRotation = Quaternion.Lerp(hostTransform.rotation, targetRotation, lerpAmount);
            Vector3 smoothedPosition = Vector3.Lerp(hostTransform.position, targetPosition, lerpAmount);            
            hostTransform.SetPositionAndRotation(smoothedPosition, smoothedRotation);
        }

        if (hybridMoveOrdered)
        {
            Vector3 targetPosition;
            Quaternion targetRotation = transform.rotation;

            if (isLost)
            {
                targetPosition = gazeProvider.HitPosition;
                targetRotation = cameraTr.rotation;
            }
            else
            {
                deltaDistance = (pointer.Position - Camera.main.transform.position).normalized;
                targetPosition = pointer.Position + deltaDistance.normalized * itemDistance;

                targetRotation = Quaternion.Euler(0, pointer.Rotation.eulerAngles.y, 0);
            }

            // Apply magnetism effect when getting inside a defined effect area
            if (isMagnetic)
            {
                Magnetism(max_dist, ref targetPosition, ref targetRotation);
            }

            float lerpAmount = GetLerpAmount();
            Quaternion smoothedRotation = Quaternion.Lerp(hostTransform.rotation, targetRotation, lerpAmount);
            Vector3 smoothedPosition = Vector3.Lerp(hostTransform.position, targetPosition, lerpAmount);
            hostTransform.SetPositionAndRotation(smoothedPosition, smoothedRotation);
        }

    }

    public override void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        eventData.Use();
    }

    public override void OnPointerDragged(MixedRealityPointerEventData eventData)
    {

        eventData.Use();
    }

    public override void OnPointerUp(MixedRealityPointerEventData eventData)
    {
        eventData.Use();
        eventData.Use();
    }

    public override void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
        audio.PlayOneShot(PressClip);

        if (!activeMenu & !(moveOrdered || moveOrderedCenterHead || hybridMoveOrdered))
        {
            StartPieMenu(eventData);
        }
        else if (activeMenu & !(moveOrdered || moveOrderedCenterHead || hybridMoveOrdered))
        {
            activeMenu = false;
            waitForAction = true;
            StartCoroutine("WaitForAction");
        }
        else if (!activeMenu & (moveOrdered || moveOrderedCenterHead || hybridMoveOrdered))
        {
            moveOrdered = false;
            moveOrderedCenterHead = false;
            hybridMoveOrdered = false;

            foreach (Collider c in hostTransform.GetComponentsInChildren<Collider>())
            {
                c.enabled = true;
            }

            CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);

            StopPieMenu();
        }
        eventData.Use();
    }

    private void StartPieMenu(MixedRealityPointerEventData eventData)
    {
        string[] funcName = new string[] { "Move On Boundaries", null, null, null };
        if (hostTransform.childCount <= 1)
        {
            funcName[1] = "Delete";
        }

        activeMenu = true;


        //PieMenu.Instance.Call(gazeProvider.HitPosition + Camera.main.transform.forward * (-0.05f), Camera.main.transform.rotation, Vector3.one * 0.007f * (Vector3.Distance(gazeProvider.HitPosition, Camera.main.transform.position) / 1.2f), funcName);
        Vector3 menuScale;
        Vector3 menuPos;

        if(Vector3.Distance(gazeProvider.HitPosition, Camera.main.transform.position) > 1.2f)
        {
            menuScale = Vector3.one * 0.007f * Vector3.Distance(gazeProvider.HitPosition, Camera.main.transform.position) / 2.5f;
            menuPos = (gazeProvider.HitPosition + Camera.main.transform.position) / 2f;
        }
        else
        {
            menuScale = Vector3.one * 0.007f * (Vector3.Distance(gazeProvider.HitPosition, Camera.main.transform.position) / 1.2f);
            menuPos = gazeProvider.HitPosition + Camera.main.transform.forward * (-0.05f);
        }

        PieMenu.Instance.Call(menuPos, Camera.main.transform.rotation, menuScale, funcName);
        //PieMenu.Instance.Call(eventData.Pointer.Position + Camera.main.transform.forward * 0.4f, Camera.main.transform.rotation, Vector3.one * 0.007f, funcName);

        PieMenu.Instance.OnAction1 += StartHeadMove;
        PieMenu.Instance.OnAction2 += Delete;
        PieMenu.Instance.OnAction3 += HybridMove;

        pointer = pointer = eventData.InputSource.Pointers.First();

        CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
    }

    private void StopPieMenu()
    {
        CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= Delete;
        PieMenu.Instance.OnAction3 -= HybridMove;

        PieMenu.Instance.Destroy();
    }

    private void CancelPieMenu()
    {
        CoreServices.InputSystem.UnregisterHandler<IMixedRealityPointerHandler>(this);

        activeMenu = false;
        moveOrdered = false;
        moveOrderedCenterHead = false;
        hybridMoveOrdered = false;
        pointer = null;

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= Delete;
        PieMenu.Instance.OnAction3 -= HybridMove;

        PieMenu.Instance.Destroy();
    }

    private void StartProximityMove()
    {
        waitForAction = false;
        hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();
        

        moveOrdered = true;
        hybridMoveOrdered = false;
        moveOrderedCenterHead = false;
        activeMenu = false;

        deltaDistance = (pointer.Position - Camera.main.transform.position).normalized;
        itemDistance = 0.3f;

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= Delete;
        PieMenu.Instance.OnAction3 -= HybridMove;

        PieMenu.Instance.Hide();

        CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
    }

    private void StartHeadMove()
    {
        waitForAction = false;

        hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();

        moveOrderedCenterHead = true;
        hybridMoveOrdered = false;
        moveOrdered = false;
        activeMenu = false;

        deltaDistance = Camera.main.transform.forward * 0.2f;
        itemDistance = Vector3.Distance(transform.position, cameraTr.position);

        foreach (Collider c in hostTransform.GetComponentsInChildren<Collider>())
        {
            c.enabled = false;
        }

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= Delete;
        PieMenu.Instance.OnAction3 -= HybridMove;

        PieMenu.Instance.Hide();

        CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
    }

    private void HybridMove()
    {
        waitForAction = false;
        

        hostTransform.GetComponent<AuthorViewRights>().GetAuthoring();

        hybridMoveOrdered = true;
        moveOrderedCenterHead = false;
        moveOrdered = false;
        activeMenu = false;

        deltaDistance = (pointer.Position - Camera.main.transform.position).normalized;
        itemDistance = 0.3f;

        foreach (Collider c in hostTransform.GetComponentsInChildren<Collider>())
        {
            c.enabled = false;
        }

        PieMenu.Instance.OnAction1 -= StartHeadMove;
        PieMenu.Instance.OnAction2 -= Delete;
        PieMenu.Instance.OnAction3 -= HybridMove;
        

        PieMenu.Instance.Hide();

        CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
    }

    // Add a delete action    
    private void Delete()
    {
        StopPieMenu();

        //SynchroServer.Instance.SendCommand("H", delObj);
        SynchroManager.Instance.CommandInstance.Cmd_DeleteObject(delObj.owner, delObj.name);
        Destroy(hostTransform.gameObject);
    }

    public void DeleteRemote()
    {
        StopPieMenu();

        WallGenerator.Instance.RemoveWindow(hostTransform.gameObject);
        Destroy(hostTransform.gameObject);
    }

    IEnumerator WaitForAction()
    {
        yield return new WaitForSeconds(0.5f);
        if (waitForAction)
            CancelPieMenu();
        waitForAction = false;
    }

    public void OnSourceDetected(SourceStateEventData eventData)
    {
        if (moveOrdered || moveOrderedCenterHead || hybridMoveOrdered)
        {
            isLost = false;
            pointer = eventData.InputSource.Pointers.First();
        }
    }

    public void OnSourceLost(SourceStateEventData eventData)
    {
        pointer = null;
        if (moveOrdered || moveOrderedCenterHead || hybridMoveOrdered)
            isLost = true;
    }

    public override void ChangeMagnet(GameObject g)
    {
        if (g == null)
        {
            PieMenuAdvancedClickSurfaceInteraction pmacsi = this.Magnet.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>();
            if (pmacsi != null)
            {
                pmacsi.UpdateContent(-1);
            }

            this.isAttracted = false;
            this.Magnet = null;

        }
        else
        {
            this.isAttracted = true;
            this.Magnet = g;

            PieMenuAdvancedClickSurfaceInteraction pmacsi = this.Magnet.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>();
            if (pmacsi != null)
            {
                pmacsi.UpdateContent(0);
            }
        }

    }
    protected override void Magnetism(float max_dist, ref Vector3 position, ref Quaternion rotation)
    {

        // Manage magnetism when multiple magnet exists 
        int minIndex = 0;
        float distance;
        Vector3 ClosestToCenter;

        GameObject[] Magnets = GameObject.FindGameObjectsWithTag("MagneticBoundary");
        Vector3 OnMagnet = Magnets[0].GetComponent<Collider>().ClosestPoint(position);
        float minDist = Vector3.Distance(OnMagnet, position);

        // Find the magnet closest to the transform 
        for (int i = 1; i < Magnets.Length; i++)
        {
            OnMagnet = Magnets[i].GetComponent<Collider>().ClosestPoint(position);

            distance = Vector3.Distance(OnMagnet, position); ;
            if (distance < minDist)
            {
                minDist = distance;
                minIndex = i;
            }
        }


        // Calculate the true position for item
        GameObject ClosestMagnet = Magnets[minIndex];
        OnMagnet = ClosestMagnet.GetComponent<Collider>().ClosestPoint(position);
        ClosestToCenter = ClosestMagnet.transform.position - OnMagnet;

        Vector3 objOnMagnet = Vector3.Project(ClosestToCenter, ClosestMagnet.transform.forward) + OnMagnet;

        // Operate Magnet effect on the object
        if (Vector3.Distance(objOnMagnet, position) <= max_dist)
        {
            // Case where item goes off a surface
            if (isAttracted == false)
            {
                Magnet = ClosestMagnet;
                hostTransform.GetComponent<SurfaceAuthorViewRights>().OnSurface(ClosestMagnet);

                changeSurfaceMessage.surfaceName = ClosestMagnet.name;
                //changeSurfaceMessage.isWindow = true;
                //SynchroServer.Instance.SendCommand("H", changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }
            // Case where item changes surface without going through the 3D space
            else if (isAttracted == true && ClosestMagnet != Magnet)
            {
                hostTransform.GetComponent<SurfaceAuthorViewRights>().SwitchSurface(Magnet, ClosestMagnet);
                Magnet = ClosestMagnet;

                changeSurfaceMessage.surfaceName = ClosestMagnet.name;
                //changeSurfaceMessage.isWindow = true;
                //SynchroServer.Instance.SendCommand("H", changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }

            isAttracted = true;
            position = objOnMagnet + (Camera.main.transform.position - Magnet.transform.position).normalized * 0.01f;
            rotation = (Vector3.Dot(ClosestMagnet.transform.forward, hostTransform.forward) > 0f) ? ClosestMagnet.transform.rotation : Quaternion.Inverse(ClosestMagnet.transform.rotation);
        }
        else
        {
            // Case where item is in 3D space and goes on a surface
            if (isAttracted == true)
            {
                hostTransform.GetComponent<SurfaceAuthorViewRights>().OffSurface(Magnet);

                Magnet = null;

                changeSurfaceMessage.surfaceName = null;
                //changeSurfaceMessage.isWindow = true;
                //SynchroServer.Instance.SendCommand("H", changeSurfaceMessage);
                SynchroManager.Instance.CommandInstance.Cmd_ChangeSurface(changeSurfaceMessage.owner, changeSurfaceMessage.objectName, changeSurfaceMessage.surfaceName, changeSurfaceMessage.isWindow);
            }
            isAttracted = false;
        }
    }
}

