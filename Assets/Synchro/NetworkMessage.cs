﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class NetworkMessage : NetworkBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(this.isLocalPlayer)
            InvokeRepeating("Cmd_TestMessage", 1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [ClientRpc]
    private void Rpc_TestMessage()
    {
        Debug.Log("Sent to clients");
    }

    [Command]
    private void Cmd_TestMessage()
    {
        Rpc_TestMessage();
        Debug.Log("Sent to server");
    }
}
