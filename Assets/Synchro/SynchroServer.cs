using System;
using System.Collections.Concurrent;
using MessagePack;
using UnityEngine;

namespace Synchro
{
    [RequireComponent(requiredComponent: typeof(NetMqPublisher))]
    public class SynchroServer : Singleton<SynchroServer>
    {
        private NetMqPublisher pub;
        
        private readonly ConcurrentQueue<SynchroMessage> commandQueue = new ConcurrentQueue<SynchroMessage>();

        public TMPro.TextMeshProUGUI text;

        protected void Start()
        {
            this.pub = this.gameObject.GetComponent<NetMqPublisher>();                           
        }

        private void LateUpdate()
        {
            while (!this.commandQueue.IsEmpty)
            {
                if (this.commandQueue.TryDequeue(out var message))
                {
                    Debug.Log("send");
                    byte[] serialized = MessagePackSerializer.Serialize(message.Command);
                    this.pub.Send(message.Topic,serialized);
                }
                else
                {
                    break;
                }
            }
        }

        public void SendCommand(string topic, ISynchroCommand cmd)
        {
            this.commandQueue.Enqueue(new SynchroMessage(topic, cmd));            
        }
    }
    
    
    public struct SynchroMessage
    {
        public string Topic;
        public ISynchroCommand Command;

        public SynchroMessage(string topic, ISynchroCommand cmd)
        {
            this.Topic = topic;
            this.Command = cmd;
        }

    }
}