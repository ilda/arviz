using MessagePack;
using Synchro.Test;

namespace Synchro
{
    /*
    [MessagePack.Union(0, typeof(TestCommand))]
    [MessagePack.Union(1, typeof(SpatialStatus))]
    [MessagePack.Union(2, typeof(ColorStatus))]
    [MessagePack.Union(3, typeof(TransformsStatusUpdate))]
    [MessagePack.Union(4, typeof(MakePublic))]
    [MessagePack.Union(5, typeof(TakeOwnership))]
    [MessagePack.Union(6, typeof(SpawnObject))]
    [MessagePack.Union(7, typeof(Register))]
    [MessagePack.Union(8, typeof(UpdatePresence))]
    [MessagePack.Union(9, typeof(ChangePermission))]
    [MessagePack.Union(10, typeof(ChangeSurface))]
    [MessagePack.Union(11, typeof(ChangePermissionDisplay))]
    [MessagePack.Union(12, typeof(DeleteObject))]
    [MessagePack.Union(13, typeof(UpdateCalibration))]
    [MessagePack.Union(14, typeof(TakeObject))]
    [MessagePack.Union(15, typeof(LetObject))]
    [MessagePack.Union(16, typeof(ReCalibrate))]
    [MessagePack.Union(17, typeof(ResetScene))]
    [MessagePack.Union(18, typeof(CatchUp))]
    [MessagePack.Union(19, typeof(Ping))]
    [MessagePack.Union(20, typeof(RecoverPersonalSpace))]
    [MessagePack.Union(21, typeof(ActivatePersonalSpace))]
    [MessagePack.Union(22, typeof(EndSession))]
    [MessagePack.Union(23, typeof(SessionResults))]
    [MessagePack.Union(24, typeof(KillApp))]
    
    [MessagePack.Union(25, typeof(Connect))]
    [MessagePack.Union(26, typeof(DisConnect))]
    [MessagePack.Union(27, typeof(ReConnect))]
    */

    public interface ISynchroCommand
    {
        void Apply();
    }
}