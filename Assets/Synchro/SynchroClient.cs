using System;
using System.Collections;
using System.Collections.Generic;
using MessagePack;
using NaughtyAttributes;
using UnityEngine;

namespace Synchro
{
    public class SynchroClient : Singleton<SynchroClient>
    {
        [ReorderableList]
        public List<NetMqSubscriber> Subscribers;
        public float lastCmd = 0f;

        protected void Start()
        {
            foreach (NetMqSubscriber sub in this.Subscribers)
               sub.MessageReceived += OnMessageReceived;
        }

        public override void OnDestroy()
        {
            foreach (NetMqSubscriber sub in this.Subscribers)
                sub.MessageReceived -= OnMessageReceived;
            base.OnDestroy();
        }

        void OnMessageReceived(object sender, NetMqMessageEventArgs e)
        {
            var cmd = MessagePackSerializer.Deserialize<ISynchroCommand>(e.Content);    
            this.lastCmd = Time.time;
            cmd.Apply();
        }

        public virtual void CheckIfUp(float uptime)
        {

        }

        public void TESTOWNE(int owner, string caller)
        {
            TakeObject to = new TakeObject(owner.ToString(), caller, true);
            to.Apply();

            //StartCoroutine(TESTOWNECOROUTINE(owner, caller));

        }

        IEnumerator TESTOWNECOROUTINE(int owner, string caller)
        {
            TakeObject to = new TakeObject(owner.ToString(), caller, true);
            to.Apply();
            yield return new WaitForSeconds(1);

            LetObject lo = new LetObject(owner.ToString(), caller, false);
            lo.Apply();
        }

        [Button]
        public void TestInstanceWindow()
        {
            SpawnObject so = new SpawnObject();
            so.owner = "116";
            so.name = "TestWindow";
            so.parentName = null;
            so.prefabName = "FloatingWindowQuad";
            so.startPos = transform.up;
            so.startRot = Quaternion.identity;
            so.startScale = Vector3.one;
            so.privacy = Permissions.PrivacyConverter(PrivacyState.Private);
            so.owners = new List<int>();
            so.owners.Add(166);

            ChangePermission cp = new ChangePermission();
            cp.owner = "116";
            cp.objectName = "Op116";
            cp.owners = new List<int>(SynchroManager.Instance.fullOwners);
            cp.permissionState = Permissions.PrivacyConverter(PrivacyState.Public);

            so.Apply();
            cp.Apply();
        }
    }
}