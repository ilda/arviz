using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System.Net.NetworkInformation;
using System.Net.Sockets;

using UVRPN.Core;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.IO;

#if !UNITY_STANDALONE_LINUX
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities;
using Microsoft.MixedReality.Toolkit;
#endif

#if WINDOWS_UWP
using Windows.Networking.Sockets;
using Windows.Networking.Connectivity;
using Windows.Networking;
using System.Linq;
#endif

namespace Synchro
{
    public enum ADDRESSFAM
    {
        IPv4, IPv6
    }
    public enum DeviceType
    {
        MASTER,
        WALL,
        HOLOLENS,
        OBSERVER
    }

    public struct ObjectLockOwnership
    {
        public string objectName;
        public string owner;
    }
    public struct ObjectOwnershipStatus
    {
        public GameObject obj;
        public string prefabName;
        public string owner;
    }

    public class SynchroManager : Singleton<SynchroManager>
    {
        [BoxGroup("Network")]
        public float NetworkFps = 60;
        [BoxGroup("Network")]
        public string topic;
        [BoxGroup("Network")]
        public Canvas WaitForReconnection;
        [BoxGroup("Network")]
        public Slider LoadingBar;
        [BoxGroup("Network")]
        public CommandMessages CommandInstance;

        [BoxGroup("System")]
        public DeviceType ownType;
        [BoxGroup("System")]
        [ShowIf("CheckTypeHololens")]
        public GameObject Anchor;
        public bool CheckTypeHololens() { return this.ownType == DeviceType.HOLOLENS; }
        [BoxGroup("System")]
        public bool useVicon;

        private float timeSinceLastNetworkUpdate = 0;
        private float networkFrameTime;

        [BoxGroup("System")]
        public string ownerId;
        //public List<int> fullOwners { get; private set; } = new List<int>() { 180, 133, 190 };
        //public List<int> fullOwners { get; private set; } = new List<int>() { 42, 43, 166 };
        //public List<int> fullOwners { get; private set; } = new List<int>() { 133, 180, 116 };
        public List<int> fullOwners { get; private set; } = new List<int>() { 133, 116, 180, 146 };
        public Color[] colorPalette = new Color[] { Color.green, Color.red, Color.yellow };
        //public List<int> fullOwners { get; private set; } = new List<int>() { 133, 14, 116 };

        [BoxGroup("Scene")]
        public GameObject myGameObjectPosition;
        [BoxGroup("Scene")]
        public GameObject Wall;

        [BoxGroup("Scene")]
        public GameObject[] startItems = new GameObject[] { };
        [BoxGroup("Scene")]
        public GameObject[] wallItems = new GameObject[] { };
        [BoxGroup("Scene")]
        public GameObject[] otherItems = new GameObject[] { };

        protected Dictionary<string, ObjectOwnershipStatus> ownItems = new Dictionary<string, ObjectOwnershipStatus>();
        protected Dictionary<string, ObjectOwnershipStatus> sharedItems = new Dictionary<string, ObjectOwnershipStatus>();
        protected Dictionary<string, ObjectOwnershipStatus> foreignItems = new Dictionary<string, ObjectOwnershipStatus>();
        protected List<ObjectLockOwnership> lockedObjects = new List<ObjectLockOwnership>();

        [BoxGroup("Parameters")]
        public bool standAlone = false;
        [BoxGroup("Parameters")]
        public bool hasStarted = false;
        [BoxGroup("Parameters")]
        public bool WallARCondition = true;
        [BoxGroup("Parameters")]
        public GameObject[] ARItems = new GameObject[] { };

        private ReCalibrate r = new ReCalibrate();
        private EventSystem eventSystem;

        private float pingLapsTime = 1f;

        private string task;
        private int dataSet;

#if !UNITY_STANDALONE_LINUX
        private IMixedRealityInputSystem inputSystem;
#endif

        // Cas ou HoloLens 
        private TransformsStatusUpdate transformsStatusUpdate = new TransformsStatusUpdate();

        [BoxGroup("Testing")]
        public TextMesh text;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        static void OnBeforeSceneLoadRuntimeMethod()
        {
            MessagePack.Resolvers.CompositeResolver.RegisterAndSetAsDefault(
                // use generated resolver first, and combine many other generated/custom resolvers
                MessagePack.Resolvers.GeneratedResolver.Instance,

                // finally, use builtin/primitive resolver(don't use StandardResolver, it includes dynamic generation)
                MessagePack.Unity.UnityResolver.Instance,
                MessagePack.Resolvers.BuiltinResolver.Instance,
                MessagePack.Resolvers.AttributeFormatterResolver.Instance,
                MessagePack.Resolvers.PrimitiveObjectResolver.Instance,
                MessagePack.Resolvers.StandardResolver.Instance
            );
        }

        public virtual void Awake()
        {
            this.ownerId = GetOwnerId();

            this.ownItems = new Dictionary<string, ObjectOwnershipStatus>();
            this.sharedItems = new Dictionary<string, ObjectOwnershipStatus>();
            this.foreignItems = new Dictionary<string, ObjectOwnershipStatus>();

            if (this.startItems.Length != 0)
            {
                for (int i = 0; i < this.startItems.Length; i++)
                {
                    this.ownItems.Add(this.startItems[i].name, new ObjectOwnershipStatus { obj = this.startItems[i], prefabName = null, owner = null });
                }
            }

            if (this.wallItems != null)
            {
                for (int i = 0; i < this.wallItems.Length; i++)
                {
                    this.sharedItems.Add(this.wallItems[i].name, new ObjectOwnershipStatus { obj = this.wallItems[i], prefabName = null, owner = null });
                }
            }

            if (this.otherItems != null)
            {
                for (int i = 0; i < this.otherItems.Length; i++)
                {
                    this.foreignItems.Add(this.otherItems[i].name, new ObjectOwnershipStatus { obj = this.otherItems[i], prefabName = null, owner = null });
                }
            }
            Debug.Log("Device state " + IsDevice());
        }

        public virtual void Start()
        {
            this.networkFrameTime = 1.0f/ this.NetworkFps;
            this.synchroEventArgs = new SynchroEventArgs();

            Debug.Log("MY SUPER IP " + GetLocalIp());

            this.eventSystem = EventSystem.current;

            if (this.useVicon)
            {
                if(this.ownType == DeviceType.HOLOLENS)
                {
                    foreach (int item in this.fullOwners)
                    {
                        GameObject following = Instantiate(Resources.Load<GameObject>("Person"));
                        Destroy(following.GetComponent<FollowCamera>());
                        following.GetComponent<PersonAuthorViewRights>().selfColor = this.fullOwners.IndexOf(item);

                        following.transform.parent = this.transform;
                        following.transform.localScale = Vector3.one * 0.1f;
                        following.name = "Op" + item.ToString();

                        if (item.ToString() == this.ownerId)
                        {
                            Camera.main.gameObject.AddComponent<BlockCamera>().CameraVicon = following.transform;
                            following.GetComponent<Renderer>().enabled = false;
                            following.transform.parent = null;
                        }

                        this.sharedItems.Add(following.name, new ObjectOwnershipStatus { obj = following, prefabName = null, owner = ownerId });                     
                    } 
                }
                else if(this.ownType == DeviceType.MASTER || this.ownType == DeviceType.OBSERVER)
                {
                    int i = 1;
                    // Use the created boundaries to detect if HL is in or not
                    foreach (int item in this.fullOwners)
                    {                        
                        GameObject following = Instantiate(Resources.Load<GameObject>("Person"));
                        Destroy(following.GetComponent<FollowCamera>());
                        following.GetComponent<AuthorViewRights>().isGenerated = true;

                        following.transform.parent = this.transform;
                        following.transform.localScale = Vector3.one * 0.1f;
                        following.name = "Op" + item.ToString();
                        VRPN_Calibrate tracker = following.AddComponent<VRPN_Calibrate>();
                        tracker.Host = this.GetComponent<VRPN_Manager>();
                        tracker.Tracker = "Hololens" + i;

                        this.sharedItems.Add(following.name, new ObjectOwnershipStatus { obj = following, prefabName = null, owner = ownerId });
                        i++;                        
                    }
                    //*/
                }
                else
                {                    
                    foreach (int item in this.fullOwners)
                    {
                        GameObject following = Instantiate(Resources.Load<GameObject>("Person"));
                        Destroy(following.GetComponent<FollowCamera>());
                        Destroy(following.GetComponent<VRPN_Calibrate>());

                        following.transform.parent = this.transform;
                        following.transform.localScale = Vector3.one * 0.1f;
                        following.name = "Op" + item.ToString();
                        following.transform.localPosition += Vector3.one;
                        this.sharedItems.Add(following.name, new ObjectOwnershipStatus { obj = following, prefabName = null, owner = ownerId });                                                
                    }
                }
            }
            else
            {

                if (this.ownType == DeviceType.HOLOLENS)
                {
                    NetworkUpdate += OnSynchroUpdate;
                    this.transformsStatusUpdate.owner = this.ownerId;                   
                }
                else if(this.ownType == DeviceType.MASTER || this.ownType == DeviceType.OBSERVER)
                {
                    //InvokeRepeating("PingDevices", 1f, pingLapsTime);
                }
            }

            this.r.owner = this.ownerId.ToString();

            Debug.Log("Device state " + IsDevice());

            WallLaunchDeploy wld = GameObject.FindObjectOfType<WallLaunchDeploy>();
            if(wld != null)
            {
                this.dataSet = wld.GetDatasetId();
                this.task = wld.GetTask();
            }

            using (StreamWriter log = new StreamWriter("./Measurement_" + CinematicLogWriter.Instance.timestamp + ".txt", true))
            {
                log.WriteLine("Task DataSet IP nbrMove nbrMoveToPS nbrSelect nbrBatchMoveSelection nbrBatchMoveSurface nbrBatchMoveFromPS nbrExpandPS nrbEmptySurface nrbEmptySelection nbrRelayout nbrWindowCreated nbrWindowDestroyed " +
                    "totToPSMoves totFromPSMoves maxNbrItemInPS totItemsInWindow maxItemsInWindow totWalkDistance totItemMoveDistance totWindowMoveDistance totCursorMoveDist avgWallDistance interPlayerDistance timeToComplete");
            }
        }

        private void Update()
        {
            this.timeSinceLastNetworkUpdate += Time.deltaTime;
            if (this.timeSinceLastNetworkUpdate > this.networkFrameTime)
            {
                this.synchroEventArgs.ElapsedTime = this.timeSinceLastNetworkUpdate;
                NetworkUpdate?.Invoke(this, this.synchroEventArgs);
                this.timeSinceLastNetworkUpdate = 0;                
            }

            if (Input.GetKeyUp(KeyCode.E))
            {
                CinematicLogWriter.Instance.StopTime();
                EndGlobalSession();
            }
        }


        void OnSynchroUpdate(object sender, SynchroManager.SynchroEventArgs e)
        {
            
            if (!this.hasStarted)
                return;

            this.transformsStatusUpdate.Reset();
            bool hasChanged = false;
            foreach (KeyValuePair<string, ObjectOwnershipStatus> de in this.sharedItems)
            {
                GameObject gameObject = de.Value.obj;
                if (gameObject.transform.hasChanged)
                {
                    //Debug.Log(gameObject.name + "hasChanged");
                    hasChanged = true;
                    gameObject.transform.hasChanged = false;
                    this.transformsStatusUpdate.AddChange(gameObject.name, gameObject.transform.localPosition,
                        gameObject.transform.localRotation, gameObject.transform.localScale);

                }
            }

            if (hasChanged)
            {
                //SynchroServer.Instance.SendCommand(this.topic, this.transformsStatusUpdate);
                CommandInstance.Cmd_TransformsStatusUpdate(transformsStatusUpdate.owner, 
                    transformsStatusUpdate.names.ToArray(), 
                    transformsStatusUpdate.poses.ToArray(), 
                    transformsStatusUpdate.rots.ToArray(), 
                    transformsStatusUpdate.scales.ToArray());
            }
            
        }

        /// <summary>
        /// When Unity quits, it destroys objects in a random order.
        /// In principle, a Singleton is only destroyed when application quits.
        /// If any script calls Instance after it have been destroyed, 
        ///   it will create a buggy ghost object that will stay on the Editor scene
        ///   even after stopping playing the Application. Really bad!
        /// So, this was made to be sure we're not creating that buggy ghost object.
        /// </summary>
        public virtual void OnDestroy()
        {
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
            {            
                //SynchroServer.Instance.SendCommand("M", new KillApp(null));
                CommandInstance.Rpc_KillApp(null);
            }
        }

        public event EventHandler<SynchroEventArgs> NetworkUpdate;

        public class SynchroEventArgs : EventArgs
        {
            public float ElapsedTime { get; set; }

        }
 
        private SynchroEventArgs synchroEventArgs;

        public DeviceType IsDevice()
        {
            return this.ownType;
        }

        public string GetLocalIp()
        {           
            if (this.ownType == DeviceType.HOLOLENS)
            {
#if WINDOWS_UWP
                var icp = NetworkInformation.GetInternetConnectionProfile();

                if (icp?.NetworkAdapter == null) return null;
                var hostname =
                    NetworkInformation.GetHostNames()
                        .FirstOrDefault(
                            hn =>
                                hn.Type == HostNameType.Ipv4 &&
                                hn.IPInformation?.NetworkAdapter != null &&
                                hn.IPInformation.NetworkAdapter.NetworkAdapterId == icp.NetworkAdapter.NetworkAdapterId);

                // the ip address
                return hostname?.CanonicalName;

#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
                ADDRESSFAM Addfam = ADDRESSFAM.IPv4;
                //Return null if ADDRESSFAM is Ipv6 but Os does not support it
                if (Addfam == ADDRESSFAM.IPv6 && !Socket.OSSupportsIPv6)
                {
                    return null;
                }

                string output = "";

                foreach (NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces())
                {
                    NetworkInterfaceType _type1 = NetworkInterfaceType.Wireless80211;
                    NetworkInterfaceType _type2 = NetworkInterfaceType.Ethernet;

                    if ((item.NetworkInterfaceType == _type1 || item.NetworkInterfaceType == _type2) && item.OperationalStatus == OperationalStatus.Up)
                    {
                        foreach (UnicastIPAddressInformation ip in item.GetIPProperties().UnicastAddresses)
                        {
                            //IPv4
                            if (Addfam == ADDRESSFAM.IPv4)
                            {
                                if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                                {
                                    output = ip.Address.ToString();
                                }
                            }

                            //IPv6
                            else if (Addfam == ADDRESSFAM.IPv6)
                            {
                                if (ip.Address.AddressFamily == AddressFamily.InterNetworkV6)
                                {
                                    output = ip.Address.ToString();
                                }
                            }
                        }
                    }
                }
                return output;
#else
                return "127.0.0.1";
#endif
            }
            else if(this.ownType == DeviceType.MASTER || this.ownType == DeviceType.OBSERVER)
            {
                return "127.0.0.300";
            }
            else
            {
                return "127.0.0.0";
            }
        }

        private string GetOwnerId()
        {
            string ip = GetLocalIp();
            string[] ipNumbers = ip.Split('.');
            return ipNumbers[3];
        }

#region Privacy Methods
        // Change access list Somehow not working when used, check why
        public bool OwnToShared(string name)
        {
            if(this.ownItems.TryGetValue(name, out ObjectOwnershipStatus o))
            {
                this.sharedItems.Add(name, o);
                this.ownItems.Remove(name);
                return true;
            }
            return false;
        }
        public bool SharedToOwn(string name)
        {
            if (this.sharedItems.TryGetValue(name, out ObjectOwnershipStatus o))
            {
                this.ownItems.Add(name, o);
                this.sharedItems.Remove(name);
                return true;
            }
            return false;
        }
        public bool ForeignToShared(string name)
        {
            if (this.foreignItems.TryGetValue(name, out ObjectOwnershipStatus o))
            {
                this.sharedItems.Add(name, o);
                this.foreignItems.Remove(name);
                o.obj.SetActive(true);
                return true;
            }
            return false;
        }
        public bool SharedToForeign(string name)
        {
            if (this.sharedItems.TryGetValue(name, out ObjectOwnershipStatus o))
            {
                this.foreignItems.Add(name, o);
                this.sharedItems.Remove(name);
                return true;
            }
            return false;
        }
        public bool OwnToForeign(string name)
        {
            if (this.ownItems.TryGetValue(name, out ObjectOwnershipStatus o))
            {
                this.foreignItems.Add(name, o);
                this.ownItems.Remove(name);
                return true;
            }
            return false;
        }
        public bool ForeignToOwn(string name)
        {
            if (this.foreignItems.TryGetValue(name, out ObjectOwnershipStatus o))
            {
                this.ownItems.Add(name, o);
                this.foreignItems.Remove(name);
                return true;
            }
            return false;
        }

        // change access list using previous permission
        public void ToOwn(string name, PrivacyState ps)
        {
            bool isFound = false;
            switch(ps){
                case PrivacyState.Foreign:
                    isFound = ForeignToOwn(name);
                    break;
                case PrivacyState.Private:
                    isFound = true;
                    break;
                case PrivacyState.Public:
                case PrivacyState.Shared:
                    isFound = SharedToOwn(name);
                    break;
            }
            if (!isFound)
                Debug.LogError("Object " + name + " not found for privacyState " + ps);
        }
        public void ToForeign(string name, PrivacyState ps)
        {
            bool isFound = false;
            switch (ps)
            {
                case PrivacyState.Foreign:
                    isFound = true;
                    break;
                case PrivacyState.Private:
                    isFound = OwnToForeign(name);
                    break;
                case PrivacyState.Public:
                case PrivacyState.Shared:
                    isFound = SharedToForeign(name);
                    break;
            }
            if (!isFound)
                Debug.LogError("Object " + name + " not found for privacyState " + ps);
        }
        public void ToShared(string name, PrivacyState ps)
        {
            bool isFound = false;
            switch (ps)
            {
                case PrivacyState.Foreign:
                    isFound = ForeignToShared(name);
                    break;
                case PrivacyState.Private:
                    isFound = OwnToShared(name);
                    break;
                case PrivacyState.Public:
                case PrivacyState.Shared:
                    isFound = true;
                    break;
            }
            if (!isFound)
                Debug.LogError("Object " + name + " not found for privacyState " + ps);
        }

        // Get Object
        public GameObject GetOwn(string name)
        {
            if (this.ownItems.TryGetValue(name, out ObjectOwnershipStatus o))
            {
                return o.obj;
            }
            else
            {
                return null;
            }
        }
        public GameObject GetShared(string name)
        {
            if (this.sharedItems.TryGetValue(name, out ObjectOwnershipStatus o))
            {
                return o.obj;
            }
            else
            {
                return null;
            }
        }
        public GameObject GetForeign(string name)
        {
            if (this.foreignItems.TryGetValue(name, out ObjectOwnershipStatus o))
            {
                return o.obj;
            }
            else
            {
                return null;
            }
        }
        public GameObject GetObject(string name)
        {
            GameObject obj = GetOwn(name);
            if (obj == null)
            {
                obj = GetShared(name);
                if(obj == null)
                {
                    obj = GetForeign(name);                    
                }                    
            }
            if (obj == null)
                Debug.Log("<color=red>[WARNING] Object " + name + " not found</color>");

            return obj;
        }
        public GameObject GetObject(string name, PrivacyState ps)
        {
            GameObject obj = null;
            switch (ps)
            {
                case PrivacyState.Foreign:
                    obj = GetForeign(name);
                    break;
                case PrivacyState.Private:
                    obj = GetOwn(name);
                    break;
                case PrivacyState.Public:                                        
                case PrivacyState.Shared:
                    obj = GetShared(name);
                    break;
            }
            if (obj == null)
                Debug.Log("<color=red>[WARNING] Object " + name + " not found</color>");

            return obj;
        }
        public void AddOwn(GameObject g)
        {
            this.ownItems.Add(g.name, new ObjectOwnershipStatus { obj = g, prefabName = null, owner = null });
        }
        public void AddShared(GameObject g)
        {
            //Debug.Log(g.name + " add shared ");
            this.sharedItems.Add(g.name, new ObjectOwnershipStatus { obj = g, prefabName = null, owner = null });            
        }
        public void AddForeign(GameObject g)
        {
            this.foreignItems.Add(g.name, new ObjectOwnershipStatus { obj = g, prefabName = null, owner = null });
        }
        public void RemoveObject(string name)
        {
            GameObject obj = GetOwn(name);
            if (obj == null)
            {
                obj = GetShared(name);
                if (obj == null)
                {
                    obj = GetForeign(name);
                }
                else
                {
                    this.sharedItems.Remove(name);
                }
            } else
            {
                this.ownItems.Remove(name);
            }
            if (obj == null)
                Debug.Log("<color=red>[WARNING] Object " + name + " not found</color>");
            else
            {
                this.foreignItems.Remove(name);
            }
        }
#endregion

#region Lock Methods
        public bool Lock(string name, string ownerObj)
        {
            //Debug.Log("Lock");
            bool prevState = IsLocked(name);
            if (prevState)
            {
                foreach (ObjectLockOwnership olo in this.lockedObjects)
                {
                    if (olo.objectName == name)
                    {
                        this.lockedObjects.Remove(olo);
                        break;
                    }
                }
            }
            this.lockedObjects.Add(new ObjectLockOwnership
            {
                objectName = name,
                owner = ownerObj
            });
            return prevState;
        }
        public bool Unlock(string name)
        {
            bool prevState = IsLocked(name);
            if (prevState)
            {
                foreach (ObjectLockOwnership olo in this.lockedObjects)
                {
                    if (olo.objectName == name)
                    {
                        this.lockedObjects.Remove(olo);
                        break;
                    }
                }
            }
            return prevState;
        }
        public bool Unlock(string name, string ownerObj)
        {
            return Unlock(name);
        }

        public bool IsLocked(string name)
        {
            foreach (ObjectLockOwnership olo in this.lockedObjects)
            {
                if (olo.objectName == name)
                    return true;
            }
            return false;
        }
        public bool IsLocked(string name, string ownerObj)
        {
            foreach(ObjectLockOwnership olo in this.lockedObjects)
            {
                if (olo.objectName == name && olo.owner == ownerObj)
                    return true;
            }
            return false;
        }
        public List<ObjectLockOwnership> GetLockedObjects()
        {
            return this.lockedObjects;
        }
#endregion

        public void SpawnObject(string name, string prefabName, string parentName, Vector3 position, Quaternion rotation, Vector3 scale)
        {
            SpawnObject msg = new SpawnObject
            {
                name = name,
                prefabName = prefabName,
                parentName = parentName,
                startPos = position,
                startRot = rotation,
                startScale = scale
            };
            
            //Create locally
            msg.Apply();
            
            //Send message
            //SynchroServer.Instance.SendCommand(this.topic,msg);
            CommandInstance.Cmd_SpawnObject(msg.owner, msg.name, msg.prefabName, msg.parentName, msg.startPos, msg.startRot, msg.startScale, msg.privacy, msg.owners.ToArray());
        }

        public void TakeOwnership(string objectName, string owner)
        {
            GameObject g;

            Lock(objectName, owner);
            if (this.ownType == DeviceType.MASTER || this.ownType == DeviceType.WALL ||this.ownType == DeviceType.OBSERVER)
            {
                g = GetShared(objectName);
                if (g == null)
                {
                    Debug.LogWarning("<color=red>Object Called " + objectName + " unknown take</color>");
                    return;
                }

                SharedToForeign(objectName);
                g.GetComponent<VersioningNotifications>().Remove();
            }
            else if(this.ownType == DeviceType.HOLOLENS && this.ownerId != owner)
            {
#if WINDOWS_UWP || UNITY_EDITOR_WIN || WINDOWS_WSA
                g = GetShared(objectName);
                if (g == null)
                {
                    Debug.LogWarning("<color=red>Object Called " + objectName + " unknown take</color>");
                    return;
                }

                SharedToForeign(objectName);
                g.GetComponent<VersioningNotifications>().SetForeign(owner);                       
#endif
            }
        }

        public void MakePublic(string objectName, string owner)
        {
            GameObject g;

            if (this.ownType == DeviceType.MASTER || this.ownType == DeviceType.WALL || this.ownType == DeviceType.OBSERVER)
            {
                g = GetForeign(objectName);
                if (g == null)
                {
                    Debug.Log("<color=red>Object Called " + objectName + " unknown make public</color>");
                    return;
                }

                ForeignToShared(objectName);
                g.GetComponent<VersioningNotifications>().Add();
            }
            else if (this.ownType == DeviceType.HOLOLENS && this.ownerId != owner)
            {
#if WINDOWS_UWP || UNITY_EDITOR_WIN || WINDOWS_WSA

                g = GetForeign(objectName);
                if (g == null)
                {
                    Debug.Log("<color=red>Object Called " + objectName + " unknown make public</color>");
                    return;
                }

                ForeignToShared(objectName);
                g.GetComponent<VersioningNotifications>().SetPublic(owner);                       
#endif
            }
        }

        public void SlateToWindowDisplay()
        {
            AuthorViewRights avr;
            foreach (KeyValuePair<string, ObjectOwnershipStatus> o in this.ownItems)
            {
                avr = o.Value.obj.GetComponent<AuthorViewRights>();
                if(avr != null)
                    avr.SlatePermissionDisplay(false);
            }
            foreach (KeyValuePair<string, ObjectOwnershipStatus> o in this.sharedItems)
            {
                avr = o.Value.obj.GetComponent<AuthorViewRights>();
                if (avr != null)
                    avr.SlatePermissionDisplay(false);
            }
            foreach (KeyValuePair<string, ObjectOwnershipStatus> o in this.foreignItems)
            {
                avr = o.Value.obj.GetComponent<AuthorViewRights>();
                if (avr != null)
                    avr.SlatePermissionDisplay(false);
            }
        }

        public void WindowToSlateDisplay()
        {
            AuthorViewRights avr;
            foreach (KeyValuePair<string, ObjectOwnershipStatus> o in this.ownItems)
            {
                avr = o.Value.obj.GetComponent<AuthorViewRights>();
                if (avr != null)
                    avr.SlatePermissionDisplay(true);
            }
            foreach (KeyValuePair<string, ObjectOwnershipStatus> o in this.sharedItems)
            {
                avr = o.Value.obj.GetComponent<AuthorViewRights>();
                if (avr != null)
                    avr.SlatePermissionDisplay(true);
            }
            foreach (KeyValuePair<string, ObjectOwnershipStatus> o in this.foreignItems)
            {
                avr = o.Value.obj.GetComponent<AuthorViewRights>();
                if (avr != null)
                    avr.SlatePermissionDisplay(true);
            }
        }

        public void DestroyItem(string name)
        {
            GameObject item = GetObject(name);

            if(item == null)
            {
                Debug.LogError(" Item " + name + " to destroy do not exist");   
            }

            Destroy(item);
        }
        
        public void ReCalibrate()
        {
#if !(UNITY_STANDALONE_LINUX || UNITY_EDITOR)
            this.Anchor.GetComponent<FreezeTrackingHandler>().enabled = true;
            this.Anchor.GetComponent<FreezeTrackingHandler>().Enable(this.transform);
#endif
        }

        public void OrderCalibration(string name)
        {
            this.r.target = name.Remove(0, 2);
            //SynchroServer.Instance.SendCommand(this.topic, this.r);
            CommandInstance.Rpc_ReCalibrate(r.owner, r.target);
        }

        public void PublishResults(string o, string content, string history)
        {
            using (StreamWriter log = new StreamWriter("./Measurement_"+ CinematicLogWriter.Instance.timestamp + ".txt", true))
            {
                log.WriteLine(this.task + " " + this.dataSet + " " + content + " " + (CinematicLogWriter.Instance.avgInterPlayerDistance / CinematicLogWriter.Instance.sessionTime) + " " + CinematicLogWriter.Instance.sessionTime);
            }

            using (StreamWriter log = new StreamWriter("./Actions_" + o + CinematicLogWriter.Instance.timestamp + ".txt", true))
            {
                log.WriteLine(history);
            }

            Debug.Log("EndSession for " + o);
        }

        [Button]
        public void ResetScene()
        {
            GetComponent<GenerateSlates>().ResetSlates();

            DeleteObject delObj = new DeleteObject(this.ownerId, "");
            SurfaceAuthorViewRights[] toDestroy  = this.GetComponentsInChildren<SurfaceAuthorViewRights>();

            foreach (SurfaceAuthorViewRights s in toDestroy)
            {
                delObj.name = s.name;
                //SynchroServer.Instance.SendCommand(this.topic, delObj);
                CommandInstance.Rpc_DeleteObject(delObj.owner, delObj.name);
                Destroy(s.gameObject);
            }            
        }

        public void ResetOrdered()
        {
            if (IsDevice() == DeviceType.HOLOLENS)
            {
                WallGenerator.Instance.ResetState();
                OrbitalPersonalRing.Instance.ResetState();
            }
        }

        public void CatchUp(CatchUp cu)
        {
            Debug.Log("Catching Up");
            List<ISynchroCommand> commandList = ReplayController.StringToCommands(cu.toSpawn);
            commandList.AddRange(ReplayController.StringToCommands(cu.toChangeSurface));
            commandList.AddRange(ReplayController.StringToCommands(cu.toLetObject));
            commandList.AddRange(ReplayController.StringToCommands(cu.toChangePermission));
            commandList.AddRange(ReplayController.StringToCommands(cu.toPosition));

            StartCoroutine(ASyncCommandLauch(commandList));
        }

        IEnumerator ASyncCommandLauch(List<ISynchroCommand> commands)
        {
            this.LoadingBar.transform.parent.gameObject.SetActive(true);
            this.LoadingBar.value = 0;
            this.LoadingBar.minValue = 0;
            this.LoadingBar.maxValue = commands.Count;

            foreach (ISynchroCommand cmd in commands)
            {
                //Debug.Log(cmd.ToString());
                cmd.Apply();
                this.LoadingBar.value++;
                yield return new WaitForEndOfFrame();
            }
            this.LoadingBar.transform.parent.gameObject.SetActive(false);
        }

        /* 
        [Button]
        public void BlockInput()
        {
#if !UNITY_STANDALONE_LINUX

            isInteractionOn = false;
            eventSystem = EventSystem.current;
            eventSystem.enabled = false;
            MixedRealityServiceRegistry.TryGetService<IMixedRealityInputSystem>(out inputSystem);
            inputSystem.GazeProvider.Enabled = false;
            WaitForReconnection.gameObject.SetActive(true);
#endif
        }

        [Button]
        public void ReleaseInput()
        {
#if !UNITY_STANDALONE_LINUX

            isInteractionOn = true;
            eventSystem.enabled = true;
            MixedRealityServiceRegistry.TryGetService<IMixedRealityInputSystem>(out inputSystem);
            inputSystem.GazeProvider.Enabled = true;
            WaitForReconnection.gameObject.SetActive(false);
#endif
        }


    */

        [Button]
        public void Interrupt()
        {

        }

        [Button]
        public void Locked()
        {
            string s = "";
            foreach (ObjectLockOwnership olo in this.lockedObjects)
                s += olo.objectName + " " + olo.owner + "\n";
            Debug.Log(s);
        }

        [Button]
        public void EndGlobalSession()
        {         
            CommandInstance.Rpc_EndSession(SynchroManager.Instance.ownerId);
        }

        [Button]
        public void KillAllApp()
        {
            SynchroServer.Instance.SendCommand("M", new KillApp(null));
        }

    }
}
