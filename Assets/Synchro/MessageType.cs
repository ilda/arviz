﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MessagePack;

namespace Synchro
{
    public class MessageType
    {
    }

    ////[MessagePackObject]
    public class SpatialStatus : ISynchroCommand
    {
        [Key(0)] public string name { get; set; }
        [Key(1)] public Vector3 pos { get; set; }
        [Key(2)] public Quaternion rot { get; set; }
        [Key(3)] public Vector3 scale { get; set; }


        public SpatialStatus()
        {
        }

        public SpatialStatus(string name, Vector3 pos, Quaternion rot, Vector3 scale)
        {
            this.name = name;
            this.pos = pos;
            this.rot = rot;
            this.scale = scale;
        }

        public void Apply()
        {
            GameObject current = SynchroManager.Instance.GetObject(this.name);
            if (SynchroManager.Instance.IsLocked(this.name, SynchroManager.Instance.ownerId))
                return;


            if (current == null)
            {
                Debug.LogError("Can't find " + this.name);
                return;
            }

            bool changeMemory = current.transform.hasChanged;

            current.transform.localPosition = this.pos;
            current.transform.localRotation = this.rot;
            current.transform.localScale = this.scale;
            current.transform.hasChanged = changeMemory;
        }

        public override string ToString()
        {
            return Time.time + " SpatialStatus " + this.name + " " + this.pos.ToString("F3").Replace(" ", "") + " " + this.rot.ToString("F3").Replace(" ", "") + " " + this.scale.ToString("F3").Replace(" ", "");
        }
    }

    //[MessagePackObject]
    public class ColorStatus : ISynchroCommand
    {
        [Key(0)] public string name { get; set; }
        [Key(1)] public Color color { get; set; }

        public ColorStatus()
        {

        }

        public ColorStatus(string name, Color color)
        {
            this.name = name;
            this.color = color;
        }

        public void Apply()
        {
            GameObject actif;
            if (SynchroManager.Instance.GetShared(this.name) != null)
                actif = SynchroManager.Instance.GetShared(this.name);
            else if (SynchroManager.Instance.GetForeign(this.name) != null)
                actif = SynchroManager.Instance.GetForeign(this.name);
            else
            {
                return;
            }

            actif.GetComponent<Renderer>().material.color = this.color;
        }

        public override string ToString()
        {

            return " Color Status ";
        }
    }

    //[MessagePackObject]
    public class TransformsStatusUpdate : ISynchroCommand
    {
        [Key(0)] public string owner;
        [Key(1)] public List<string> names { get; set; } = new List<string>();
        [Key(2)] public List<Vector3> poses { get; set; } = new List<Vector3>();
        [Key(3)] public List<Quaternion> rots { get; set; } = new List<Quaternion>();
        [Key(4)] public List<Vector3> scales { get; set; } = new List<Vector3>();


        public TransformsStatusUpdate()
        {
        }

        public TransformsStatusUpdate(string owner, List<string> names, List<Vector3> poses, List<Quaternion> rots, List<Vector3> scales)
        {
            this.owner = owner;
            this.names = names;
            this.poses = poses;
            this.rots = rots;
            this.scales = scales;
        }

        public void AddChange(string name, Vector3 pos, Quaternion rot, Vector3 scale)
        {
            this.names.Add(name);
            this.poses.Add(pos);
            this.rots.Add(rot);
            this.scales.Add(scale);
        }

        public void Apply()
        {
            if (this.owner == SynchroManager.Instance.ownerId)
                return;

            for (int i = 0; i < this.names.Count; i++)
            {
                GameObject current = SynchroManager.Instance.GetObject(this.names[i]);
                if (SynchroManager.Instance.IsLocked(this.names[i], SynchroManager.Instance.ownerId))
                    continue;


                if (current == null)
                {
                    Debug.LogError("Can't find " + this.names[i]);
                    continue;
                }

                bool changeMemory = current.transform.hasChanged;

                current.transform.localPosition = this.poses[i];
                current.transform.localRotation = this.rots[i];
                current.transform.localScale = this.scales[i];
                current.transform.hasChanged = changeMemory;
            }
        }

        public override string ToString()
        {
            string totalMov = "";
            for (int i = 0; i < this.names.Count; i++)
            {
                totalMov += Time.time + " " + this.owner + " TransformStatusUpdate " + this.names[i] + " " + this.poses[i].ToString("F3").Replace(" ", "") + " " + this.rots[i].ToString("F3").Replace(" ", "") + " " + this.scales[i].ToString("F3").Replace(" ", "");
                if (i != this.names.Count - 1) totalMov += System.Environment.NewLine;
            }
            return totalMov;
        }

        public void Reset()
        {
            this.names.Clear();
            this.poses.Clear();
            this.rots.Clear();
            this.scales.Clear();
        }
    }

    //[MessagePackObject]
    public class SpawnObject : ISynchroCommand
    {
        [Key(0)] public string owner;
        [Key(1)] public string name { get; set; }
        [Key(2)] public string prefabName { get; set; }
        [Key(3)] public string parentName { get; set; }
        [Key(4)] public Vector3 startPos { get; set; }
        [Key(5)] public Quaternion startRot { get; set; }
        [Key(6)] public Vector3 startScale { get; set; }

        [Key(7)] public int privacy;
        [Key(8)] public List<int> owners;


        public SpawnObject()
        {
        }

        public SpawnObject(string owner, string name, string prefabName, string parentName, Vector3 startPos, Quaternion startRot, Vector3 startScale, int privacy, List<int> owners)
        {
            this.owner = owner;
            this.name = name;
            this.prefabName = prefabName;
            this.parentName = parentName;
            this.startPos = startPos;
            this.startRot = startRot;
            this.startScale = startScale;
            this.privacy = privacy;
            this.owners = owners;
        }

        public void Apply()
        {
            //Debug.Log(this.ToString());
            if (SynchroManager.Instance.ownerId == this.owner || SynchroManager.Instance.GetObject(this.name) != null)
                return;

            if (SynchroManager.Instance.IsDevice() == DeviceType.WALL && this.prefabName == "FloatingWindowQuad")
            {
                this.prefabName = "FloatingWindowNetwork";
            }
            else if(SynchroManager.Instance.IsDevice() == DeviceType.MASTER && this.prefabName == "FloatingWindowQuad")
            {
                this.prefabName = "FloatingWindowMaster";
            }

            GameObject g = Object.Instantiate((GameObject)Resources.Load(this.prefabName));
            g.GetComponent<AuthorViewRights>().isGenerated = true;

            g.transform.parent = (this.parentName == null || this.parentName == "") ? SynchroManager.Instance.transform : SynchroManager.Instance.GetObject(this.parentName).transform;
            g.transform.localPosition = this.startPos;
            g.transform.localRotation = this.startRot;
            g.transform.localScale = this.startScale;
            g.name = this.name;
            g.GetComponent<AuthorViewRights>().AnticipateStart();

            if (this.owners.Count > 0 && this.owners.Contains(int.Parse(SynchroManager.Instance.ownerId)))
            {
                switch (Permissions.PrivacyConverter(this.privacy))
                {
                    case PrivacyState.Shared:
                        g.GetComponent<AuthorViewRights>().RemoteMakeShared(this.owners);
                        break;
                    case PrivacyState.Public:
                        g.GetComponent<AuthorViewRights>().RemoteMakePublic();
                        break;
                    default:
                        Debug.LogError("Impossible case of Foreign or Private for changePermission with state : " + this.privacy);
                        break;
                }
            }
            else
            {
                g.GetComponent<AuthorViewRights>().RemoteMakeForeign();
            }
        }

        public override string ToString()
        {
            string ownerList = "";
            foreach (int o in this.owners) ownerList += o + "-";
            ownerList += "null";
            return Time.time + " " + this.owner + " SpawnObject " + this.name + " " + this.prefabName + " " + this.parentName + " " + this.startPos.ToString("F3").Replace(" ", "") + " " + this.startRot.ToString().Replace(" ", "") + " " + this.startScale.ToString().Replace(" ", "") + " " + this.privacy + " " + ownerList;
        }
    }

    //[MessagePackObject]
    public class TakeOwnership : ISynchroCommand
    {
        [Key(0)] public string objectName { get; set; }
        [Key(1)] public string owner { get; set; }


        public TakeOwnership()
        {
        }

        public TakeOwnership(string objectName, string owner)
        {
            this.objectName = objectName;
            this.owner = owner;
        }

        public void Apply()
        {
            SynchroManager.Instance.TakeOwnership(this.objectName, this.owner);
        }

        public override string ToString()
        {

            return Time.time + " " + this.owner + " TakeOwnership " + this.objectName;
        }
    }

    //[MessagePackObject]
    public class MakePublic : ISynchroCommand
    {
        [Key(0)] public string objectName { get; set; }
        [Key(1)] public string owner { get; set; }


        public MakePublic()
        {
        }

        public MakePublic(string objectName, string owner)
        {
            this.objectName = objectName;
            this.owner = owner;
        }

        public void Apply()
        {
            SynchroManager.Instance.MakePublic(this.objectName, this.owner);
        }

        public override string ToString()
        {

            return Time.time + " " + this.owner + " MakePublic " + this.objectName;
        }
    }


    //Message sent by HoloLens to MAsterRedirection in order to connect
    //[MessagePackObject]
    public class Register : ISynchroCommand
    {
        [Key(0)] public string name { get; set; }
        [Key(1)] public string owner { get; set; }


        public Register()
        {
        }

        public Register(string name, string owner)
        {
            this.name = name;
            this.owner = owner;
        }

        public void Apply()
        {
            if (this.owner == SynchroManager.Instance.ownerId)
                return;

            GameObject p = Object.Instantiate((GameObject)Resources.Load("Person"));
            p.transform.localScale = Vector3.one;
            p.GetComponent<PersonAuthorViewRights>().isGenerated = true;
            p.GetComponent<PersonAuthorViewRights>().selfColor = SynchroManager.Instance.fullOwners.IndexOf(int.Parse(this.owner));
            p.name = this.name;
            p.transform.GetChild(0).name += int.Parse(this.owner);
            p.transform.GetChild(0).GetChild(0).name += int.Parse(this.owner);
            p.transform.parent = SynchroManager.Instance.transform;
            p.GetComponent<FollowCamera>().enabled = false;
            SynchroManager.Instance.AddShared(p);
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " Register " + this.name;
        }
    }

    // Message for HoloLenses when someone connects, sent by MasterRedirection
    //[MessagePackObject]
    public class UpdatePresence : ISynchroCommand
    {
        [Key(0)] public List<string> name { get; set; }
        [Key(1)] public string owner { get; set; }


        public UpdatePresence()
        {
        }

        public UpdatePresence(List<string> name, string owner)
        {
            this.name = name;
            this.owner = owner;
        }

        public void Apply()
        {
            for (int i = 0; i < this.name.Count; i++)
            {
                if (GameObject.Find("Op" + this.name[i]) == null)
                {
                    GameObject p = Object.Instantiate((GameObject)Resources.Load("Person"));
                    p.GetComponent<PersonAuthorViewRights>().isGenerated = true;
                    p.GetComponent<PersonAuthorViewRights>().selfColor = SynchroManager.Instance.fullOwners.IndexOf(int.Parse(this.name[i]));
                    p.name = "Op" + this.name[i];
                    p.transform.GetChild(0).name += int.Parse(this.name[i]);
                    p.transform.GetChild(0).GetChild(0).name += int.Parse(this.name[i]);
                    p.transform.parent = SynchroManager.Instance.transform;
                    p.GetComponent<FollowCamera>().enabled = false;
                    SynchroManager.Instance.AddShared(p);
                }
            }

            if (SynchroManager.Instance.IsDevice() == DeviceType.HOLOLENS && !SynchroManager.Instance.hasStarted)
            {
                SynchroManager.Instance.hasStarted = true;
            }
        }

        public override string ToString()
        {
            string nameList = "";
            foreach (string n in this.name) nameList += n + "-";
            nameList += "null";
            return Time.time + " " + this.owner + " UpdatePresence " + " " + nameList;
        }
    }

    //[MessagePackObject]
    public class ChangePermission : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }
        [Key(1)] public string objectName { get; set; }
        [Key(2)] public int permissionState { get; set; }
        [Key(3)] public List<int> owners { get; set; }


        public ChangePermission()
        {
        }

        public ChangePermission(string owner, string objectName, int permissionState, List<int> owners)
        {
            this.owner = owner;
            this.objectName = objectName;
            this.permissionState = permissionState;
            this.owners = owners;
        }

        public void Apply()
        {
            if (this.owner == SynchroManager.Instance.ownerId)
                return;

            GameObject obj = SynchroManager.Instance.GetObject(this.objectName);
            if (obj == null)
                return;

            if (SynchroManager.Instance.IsDevice() != DeviceType.HOLOLENS || this.owners.Contains(int.Parse(SynchroManager.Instance.ownerId)))
            {
                switch (Permissions.PrivacyConverter(this.permissionState))
                {
                    case PrivacyState.Shared:
                        obj.GetComponent<AuthorViewRights>().RemoteMakeShared(this.owners);
                        break;
                    case PrivacyState.Public:
                        obj.GetComponent<AuthorViewRights>().RemoteMakePublic();
                        break;
                    default:
                        Debug.LogError("Impossible case of Foreign or Private for changePermission with state : " + Permissions.PrivacyConverter(this.permissionState));
                        break;
                }
            }
            else
            {
                obj.GetComponent<AuthorViewRights>().RemoteMakeForeign();
            }
        }

        public override string ToString()
        {
            string ownersListed = "";
            foreach (int o in this.owners) ownersListed += o.ToString() + "-";
            ownersListed += "null";
            return Time.time + " " + this.owner + " ChangePermission " + this.objectName + " " + this.permissionState + " " + ownersListed;
        }
    }

    //[MessagePackObject]
    public class ChangeSurface : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }
        [Key(1)] public string objectName { get; set; }
        [Key(2)] public string surfaceName { get; set; }
        [Key(3)] public bool isWindow { get; set; }


        public ChangeSurface()
        {
        }

        public ChangeSurface(string owner, string objectName, string surfaceName, bool isWindow)
        {
            this.owner = owner;
            this.objectName = objectName;
            this.surfaceName = surfaceName;
            this.isWindow = isWindow;
        }

        public void Apply()
        {
            if (this.owner == SynchroManager.Instance.ownerId)
                return;

            if (SynchroManager.Instance.IsDevice() == DeviceType.HOLOLENS)
            {
                GameObject g = SynchroManager.Instance.GetObject(this.objectName);
                InteractionScript IS = this.isWindow ? g.GetComponentInChildren<PieMenuAdvancedClickSurfaceInteraction>() : (InteractionScript)g.GetComponent<PieMenuClickInteraction>();

                if (this.surfaceName == null || this.surfaceName == "")
                {
                    IS.ChangeMagnet(null);
                    g.transform.parent = SynchroManager.Instance.Wall.transform;
                }
                else
                {
                    g.transform.parent = SynchroManager.Instance.GetObject(this.surfaceName).transform;
                    IS.ChangeMagnet(SynchroManager.Instance.GetObject(this.surfaceName));
                }
                g.transform.hasChanged = false;
            }
            else
            {
                GameObject g = SynchroManager.Instance.GetObject(this.objectName);

                g.transform.parent = (this.surfaceName == null || this.surfaceName == "")
                    ? SynchroManager.Instance.transform
                    : SynchroManager.Instance.GetObject(this.surfaceName).transform;
                g.transform.hasChanged = false;
            }
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " ChangeSurface " + this.objectName + " " + this.surfaceName + " " + this.isWindow;
        }
    }

    //[MessagePackObject]
    public class ChangePermissionDisplay : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }
        [Key(1)] public int mode { get; set; }

        public ChangePermissionDisplay()
        {

        }

        public ChangePermissionDisplay(string owner, int mode)
        {
            this.owner = owner;
            this.mode = mode;
        }

        public void Apply()
        {
            if (this.owner != SynchroManager.Instance.ownerId && SynchroManager.Instance.IsDevice() == DeviceType.HOLOLENS)
                WallGenerator.Instance.RemoteModeChange(this.mode);
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " ChangePermissionDisplay " + this.mode;
        }
    }

    //[MessagePackObject]
    public class DeleteObject : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }
        [Key(1)] public string name { get; set; }


        public DeleteObject()
        {
        }

        public DeleteObject(string owner, string name)
        {
            this.owner = owner;
            this.name = name;
        }

        public void Apply()
        {
            if (SynchroManager.Instance.ownerId != this.owner)
                SynchroManager.Instance.DestroyItem(this.name);

        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " DeleteObject " + this.name;
        }
    }

    //[MessagePackObject]
    public class UpdateCalibration : ISynchroCommand
    {
        public string name;
        public Vector3 position;
        public Quaternion rotation;


        public UpdateCalibration()
        {
        }

        public UpdateCalibration(string name, Vector3 position, Quaternion rotation)
        {
            this.name = name;
            this.position = position;
            this.rotation = rotation;
        }

        public void Apply()
        {
            Vector3 originalOffset = new Vector3(0f, 0.125f, 0.018f);
            if (SynchroManager.Instance.ownerId == this.name)
            {
                SynchroManager.Instance.gameObject.transform.position = Camera.main.transform.position = this.position - originalOffset;
                SynchroManager.Instance.gameObject.transform.rotation = Camera.main.transform.rotation * this.rotation;
            }
        }

        public override string ToString()
        {
            return Time.time + " UpdateCalibration " + this.name + " " + this.position.ToString() + " " + this.rotation.ToString();
        }

    }

    //[MessagePackObject]
    public class TakeObject : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }
        [Key(1)] public string objectName { get; set; }
        [Key(2)] public bool selectionState { get; set; }

        public TakeObject()
        {
        }

        public TakeObject(string owner, string objectName, bool selectionState)
        {
            this.owner = owner;
            this.objectName = objectName;
            this.selectionState = selectionState;
        }

        public void Apply()
        {
            if (this.owner == SynchroManager.Instance.ownerId)
                return;

            GameObject g = SynchroManager.Instance.GetObject(this.objectName);
            if (g != null)
            {
                g.GetComponentInChildren<AuthorViewRights>().RemoteGetAuthoring(int.Parse(this.owner), this.selectionState);
            }
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " TakeObject " + this.objectName + " " + this.selectionState;
        }

    }

    //[MessagePackObject]
    public class LetObject : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }
        [Key(1)] public string objectName { get; set; }
        [Key(2)] public bool affectSelectionState { get; set; }


        public LetObject()
        {
        }

        public LetObject(string owner, string objectName, bool affectSelectionState)
        {
            this.owner = owner;
            this.objectName = objectName;
            this.affectSelectionState = affectSelectionState;
        }

        public void Apply()
        {
            if (this.owner == SynchroManager.Instance.ownerId)
                return;

            GameObject g = SynchroManager.Instance.GetObject(this.objectName);
            if (g != null)
            {
                g.GetComponentInChildren<SlateAuthorViewRights>().RemoteLetAuthoring(int.Parse(this.owner), this.affectSelectionState);
            }
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " LetObject " + this.objectName + " " + this.affectSelectionState;
        }
    }

    //[MessagePackObject]
    public class ReCalibrate : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }
        [Key(1)] public string target { get; set; }

        public ReCalibrate()
        {

        }

        public ReCalibrate(string owner, string target)
        {
            this.owner = owner;
            this.target = target;
        }

        public void Apply()
        {
            if (SynchroManager.Instance.ownerId == this.target)
            {
                SynchroManager.Instance.ReCalibrate();
            }
        }
    }

    //[MessagePackObject]
    public class ResetScene : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }

        public ResetScene()
        {

        }

        public ResetScene(string owner)
        {
            this.owner = owner;
        }

        public void Apply()
        {
            SynchroManager.Instance.ResetOrdered();
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " ResetScene";
        }
    }

    //[MessagePackObject]
    public class CatchUp : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }
        [Key(1)] public string target { get; set; }
        [Key(2)] public string toSpawn { get; set; }
        [Key(3)] public string toChangeSurface { get; set; }
        [Key(4)] public string toLetObject { get; set; }
        [Key(5)] public string toChangePermission { get; set; }
        [Key(6)] public string toPosition { get; set; }

        public CatchUp()
        {

        }

        public CatchUp(string owner, string target, string toSpawn, string toChangeSurface, string toLetObject, string toChangePermission, string toPosition)
        {
            this.owner = owner;
            this.target = target;
            this.toSpawn = toSpawn;
            this.toChangeSurface = toChangeSurface;
            this.toLetObject = toLetObject;
            this.toChangePermission = toChangePermission;
            this.toPosition = toPosition;
        }

        public void Apply()
        {
            Debug.Log(this.owner + "Ask to catchup to " + this.target);
            if (SynchroManager.Instance.ownerId == this.target)
            {
                SynchroManager.Instance.CatchUp(this);
            }
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " CatchUp"; ;
        }
    }

    //[MessagePackObject]
    public class Ping : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }

        public Ping()
        {

        }

        public Ping(string ownerId)
        {
            this.owner = ownerId;
        }

        public void Apply()
        {

        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " Ping"; ;
        }
    }

    //[MessagePackObject]
    public class RecoverPersonalSpace : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }
        [Key(1)] public string objectOwner { get; set; }
        [Key(2)] public string objectName { get; set; }

        public RecoverPersonalSpace()
        {

        }

        public RecoverPersonalSpace(string owner, string objectOwner, string objectName)
        {
            this.owner = owner;
            this.objectOwner = objectOwner;
            this.objectName = objectName;
        }

        public void Apply()
        {
            if (this.objectOwner == SynchroManager.Instance.ownerId)
            {
                GameObject g = SynchroManager.Instance.GetObject(this.objectName);
                g.GetComponentInChildren<PieMenuClickInteraction>().SwitchToPersonalRemote();
            }
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " RecoverPersonalSpace " + this.objectOwner + " " + this.objectName;
        }
    }

    //[MessagePackObject]
    public class ActivatePersonalSpace : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }
        [Key(1)] public bool activate { get; set; }

        public ActivatePersonalSpace() { }

        public ActivatePersonalSpace(string owner, bool activate)
        {
            this.owner = owner;
            this.activate = activate;
        }

        public void Apply()
        {
            if (this.owner == SynchroManager.Instance.ownerId)
                return;

            SynchroManager.Instance.GetObject("Op"+this.owner)?.GetComponent<PersonAuthorViewRights>().ActivatePS(this.activate);
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " ActivatePersonalSpace " + this.activate;
        }
    }

    //[MessagePackObject]
    public class StartSession : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }
        public string time { get; set; }
        public TextAsset action116 { get; set; }
        public TextAsset action180 { get; set; }
        public float offsetTime { get; set; }

        public string condition { get; set; }
        public string task { get; set; }
        public string session { get; set; }

        public StartSession()
        {

        }

        public StartSession(string owner, string time)
        {
            this.owner = owner;
            this.time = time;
        }

        public StartSession(string owner, string time, TextAsset action116, TextAsset action180, float offsetTime, string task, string condition, string session)
        {
            this.owner = owner;
            this.time = time;
            this.action116 = action116;
            this.action180 = action180;
            this.offsetTime = offsetTime;
            this.task = task;
            this.condition = condition;
            this.session = session;
        }

        public void Apply()
        {
            Debug.Log("<color=#ffaa00>Start session : " + this.time + "</color>");
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " StartSession";
        }
    }

    public class EndSessionResults : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }
        public string time { get; set; }

        public EndSessionResults()
        {

        }

        public EndSessionResults(string owner, string time)
        {
            this.owner = owner;
            this.time = time;
        }

        public void Apply()
        {
            Debug.Log("<color=#009900>End session : " + this.time + "</color>");
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " EndSessionResults";
        }
    }

    //[MessagePackObject]
    public class EndSession : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }

        public EndSession()
        {

        }

        public EndSession(string owner)
        {
            this.owner = owner;
        }

        public void Apply()
        {
            if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.HOLOLENS)
                Measurement.Instance.EndSession();
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " EndSession";
        }
    }

    //[MessagePackObject]
    public class SessionResults : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }
        [Key(1)] public string content { get; set; }
        [Key(2)] public string history { get; set; }

        public SessionResults()
        {

        }

        public SessionResults(string owner, string content, string history)
        {
            this.owner = owner;
            this.content = content;
            this.history = history;
        }

        public void Apply()
        {
            SynchroManager.Instance.PublishResults(this.owner, this.content, this.history);
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " SessionsResults";
        }
    }
    
    //[MessagePackObject]
    public class KillApp : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }

        public KillApp()
        {

        }

        public KillApp(string owner)
        {
            this.owner = owner;
        }

        public void Apply()
        {
            Debug.Log("Ciao");
            Application.Quit();
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " KillApp";
        }
    }

    public class Connect : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }

        public Connect()
        {

        }

        public Connect(string owner)
        {
            this.owner = owner;
        }

        public void Apply()
        {
            if (this.owner == SynchroManager.Instance.ownerId)
                return;

            GameObject g = SynchroManager.Instance.GetObject("Op" + owner);
            if (g != null)
            {
                g.GetComponent<Renderer>().material.color = SynchroManager.Instance.colorPalette[SynchroManager.Instance.fullOwners.IndexOf(int.Parse(owner))];
                g.transform.GetChild(0).GetComponent<Renderer>().material.color = SynchroManager.Instance.colorPalette[SynchroManager.Instance.fullOwners.IndexOf(int.Parse(owner))];
            }
            else
            {
                GameObject player;
                player = (GameObject)GameObject.Instantiate(CustomNetworkManager.singleton.playerPrefab, Vector3.zero, Quaternion.identity);
                player.name = "Op" + owner;
                player.transform.GetChild(0).name = "Cursor" + owner;
                player.GetComponent<InitializePlayerOnNetwork>().isNew = true;
                player.GetComponent<InitializePlayerOnNetwork>().personName = "Op" + owner;
                player.GetComponent<InitializePlayerOnNetwork>().selfColor = SynchroManager.Instance.fullOwners.IndexOf(int.Parse(owner));
                player.GetComponent<InitializePlayerOnNetwork>().parentName = "Controller";
                player.GetComponent<InitializePlayerOnNetwork>().selfScale = Vector3.one * 0.1f;
                SynchroManager.Instance.AddShared(player);
                SynchroManager.Instance.AddShared(player.transform.GetChild(0).gameObject);
                Debug.Log("Update");
            }
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " Connect";
        }
    }

    public class DisConnect : ISynchroCommand
    {
        [Key(0)] public string owner { get; set; }

        public DisConnect()
        {

        }

        public DisConnect(string owner)
        {
            this.owner = owner;
        }

        public void Apply()
        {
            if (this.owner == SynchroManager.Instance.ownerId)
                return;

            GameObject g = SynchroManager.Instance.GetObject("Op" + owner);
            if (g != null)
            {
                g.GetComponent<Renderer>().material.color = g.GetComponent<Renderer>().material.color + new Color(0.2f, 0.2f, 0.2f);
                g.transform.GetComponent<Renderer>().material.color = g.GetComponent<Renderer>().material.color;
            }
            else
            {
                Debug.LogError("Disconnecting not existing player !");
            }
        }

        public override string ToString()
        {
            return Time.time + " " + this.owner + " DisConnect";
        }
    }
}