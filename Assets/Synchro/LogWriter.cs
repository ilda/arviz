﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;
using System.Reflection;
using System;

public class LogWriter 
{
    private string m_exePath;

    public LogWriter(string logMessage)
    {
        LogWrite(logMessage);
    }

    public LogWriter(string logMessage, string logFilename)
    {
        this.m_exePath = Application.dataPath + "/" + logFilename;
        LogWrite(logMessage);
    }

    public void LogWrite(string logMessage)
    {
        try
        {
            using (StreamWriter w = File.AppendText(this.m_exePath))
            {
                Log(logMessage, w);
            }
        }
        catch (Exception ex)
        {
            Debug.LogWarning("Errir" + ex);
        }
    }

    public void Log(string logMessage, TextWriter txtWriter)
    {
        try
        {
            txtWriter.WriteLine(Time.time + " " + logMessage);
        }
        catch (Exception ex)
        {
        }
    }
}
