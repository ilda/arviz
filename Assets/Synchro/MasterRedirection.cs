﻿using System;
using System.Collections.Generic;
using System.IO;
using MessagePack;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Synchro
{
    [RequireComponent(requiredComponent: typeof(NetMqSubscriber))]
    public class MasterRedirection : Singleton<SynchroClient>
    {
        public Dictionary<string, float> presentPersons { get; set; } = new Dictionary<string, float>();
        public RectTransform IPList;

        private string cinematicName;

        [ReorderableList]
        public List<NetMqSubscriber> Subscribers;
        protected void Start()
        {
            this.cinematicName = "./Cinematic_" + DateTime.Now.ToString("s").Replace(':', '-') + ".txt";
            UpdateView();
            foreach (NetMqSubscriber sub in this.Subscribers)
                sub.MessageReceived += OnMessageReceived;

            InvokeRepeating("CheckOpConnection", 1f, 1f);
        }

        public override void OnDestroy()
        {
            foreach (NetMqSubscriber sub in this.Subscribers)
                sub.MessageReceived -= OnMessageReceived;
            base.OnDestroy();
        }

        private void Update()
        {}

        void OnMessageReceived(object sender, NetMqMessageEventArgs e)
        {
            var cmd = MessagePackSerializer.Deserialize<ISynchroCommand>(e.Content);

            using (StreamWriter log = new StreamWriter(this.cinematicName, true))
            {
                log.WriteLine(cmd.ToString());
            }

            if (cmd.GetType() == typeof(Register))
            {
                Debug.Log(((Register)cmd).ToString() + "Connect");

                if (!this.presentPersons.ContainsKey(((Register)cmd).owner))
                { 
                    cmd.Apply();

                    this.presentPersons.Add(((Register)cmd).owner, Time.time);

                    UpdateView();
                    List<string> nameList = new List<string>(this.presentPersons.Keys);
                    //SynchroServer.Instance.SendCommand("M", new UpdatePresence() { name = nameList, owner = ((Register)cmd).owner });
                    SynchroManager.Instance.CommandInstance.Rpc_UpdatePresence(nameList.ToArray(), ((Register)cmd).owner);
                }
                return;
            }

            if (cmd.GetType().Equals(typeof(TransformsStatusUpdate)))
            {
                TransformsStatusUpdate t = (TransformsStatusUpdate)cmd;

                //TRY OF TIMER FOR TIMEOUT
                this.presentPersons[t.owner] = Time.time;
                //Debug.Log(t.owner + " " + " owner !!!");
            }

            //SynchroServer.Instance.SendCommand("M", cmd);
            SynchroManager.Instance.CommandInstance.Rpc_Register(((Register)cmd).owner, ((Register)cmd).name);
            cmd.Apply();
        }

        private void CheckOpConnection()
        {
            foreach (int i in SynchroManager.Instance.fullOwners)
            {
                if (this.presentPersons.ContainsKey(i.ToString()) && (Time.time - this.presentPersons[i.ToString()]) > 1f)
                    Debug.Log(i + " is deconnected");
            }
        }

        void UpdateView()
        {
            List<int> owners = SynchroManager.Instance.fullOwners;
            for (int i = 0; i < owners.Count; i++) {
                this.IPList.GetChild(i).GetComponentInChildren<TextMeshProUGUI>().text = "Op" + owners[i].ToString();
                this.IPList.GetChild(i).GetComponentInChildren<Image>().color = (this.presentPersons.ContainsKey("Op" + owners[i].ToString()) ? Color.green : Color.red);
            }
        }

        public void CheckIfUp(float uptime)
        {
            List<int> owners = SynchroManager.Instance.fullOwners;

            foreach (KeyValuePair<string, float> k in this.presentPersons)
            {
                string t = k.Key.Remove(0, 2);
                this.IPList.GetChild(owners.IndexOf(int.Parse(t))).GetComponentInChildren<Image>().color = k.Value + uptime < Time.time
                    ? Color.red
                    : Color.green;
            }
        }

        public void ReplayCommandSent(ISynchroCommand cmd)
        {
            if (cmd.GetType() == typeof(Register))
            {
                cmd.Apply();
                this.presentPersons.Add(((Register)cmd).name, Time.time);
                UpdateView();
                List<string> nameList = new List<string>(this.presentPersons.Keys);
                //SynchroServer.Instance.SendCommand("M", new UpdatePresence() { name = nameList, owner = ((Register)cmd).owner });
                SynchroManager.Instance.CommandInstance.Rpc_UpdatePresence(nameList.ToArray(), ((Register)cmd).owner);
                return;
            }

            cmd.Apply();
        }

        public void LaunchUpdatePresence()
        {
            List<string> nameList = new List<string>(this.presentPersons.Keys);
            SynchroServer.Instance.SendCommand("M", new UpdatePresence() { name = nameList, owner = SynchroManager.Instance.ownerId });
            SynchroManager.Instance.CommandInstance.Rpc_UpdatePresence(nameList.ToArray(), SynchroManager.Instance.ownerId);
        }

        [Button]
        public void GetConnected()
        {
            foreach (var v in this.presentPersons.Keys) {
                Debug.Log(v);
            }
            
        }
    }
}