﻿using NaughtyAttributes;
using Synchro;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

#if WINDOWS_UWP
using Windows.Networking.Sockets;
using Windows.Networking.Connectivity;
using Windows.Networking;
using System.Linq;
#endif

public class CustomNetworkManager : NetworkManager
{
    public bool is_host = false;
    public List<int> fullOwners { get; private set; } = new List<int>() { 133, 116, 180, 146 };
    public List<string> connected = new List<string>();
    public List<string> disconnected = new List<string>();

    public GameObject serverCommandPrefab;
    private GameObject serverCommand;

    public bool hasStarted;

    public bool monitoring;
    public RectTransform IPList;

    public bool localDebug = false;

    private List<ReplayMeasurement> measures = new List<ReplayMeasurement>();

    private void Awake()
    {

    }

    private void Start()
    {
        //is_host = GetLocalIPAddress() == this.serverBindAddress;
        Debug.Log("Host" + is_host);

        if (is_host)
        {
            NetworkClient nc = StartHost();
            //nc.RegisterHandler(MsgType.Connect,  OnConnectedClient);
            //nc.RegisterHandler(MsgType.Disconnect, OnDisconnectedClient);
            //nc.RegisterHandler(MsgType.Error, OnError);

            serverCommand = Instantiate(serverCommandPrefab);
            SynchroManager.Instance.CommandInstance = serverCommand.GetComponent<CommandMessages>();

            NetworkServer.Spawn(serverCommand);
            hasStarted = true;
            
            InvokeRepeating("GetInterPlayerDist", 0f, 0.1f);
        }
        else
        {
            StartClient();
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        Debug.Log(conn.address + " -> ID: " + GetIPId(conn.address) + " " + (disconnected.Contains(GetIPId(conn.address)) ? " Reconnects" : ""));
        string id = GetIPId(conn.address);
        if ((id == "localClient" ||  int.Parse(id) < 50 && !localDebug))
            return;


        if (disconnected.Contains(id))
        {
            disconnected.Remove(id);
            connected.Add(id);
            SynchroManager.Instance.CommandInstance.ReconnectClient(id);
            GameObject ghostPlayer = SynchroManager.Instance.GetObject("Op" + id);
            ghostPlayer?.GetComponent<NetworkIdentity>().AssignClientAuthority(conn);
            if (monitoring)
            {
                UpdateView();
            }
            return;
        }

        connected.Add(id);
        SynchroManager.Instance.CommandInstance.ConnectClient(id);

        if (monitoring)
        {
            UpdateView();
        }

        GameObject player = (GameObject)Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
        player.name = "Op" + id;
        player.transform.GetChild(0).name = "Cursor" + id;
        player.GetComponent<InitializePlayerOnNetwork>().isNew = true;
        player.GetComponent<InitializePlayerOnNetwork>().personName = "Op" + id;
        player.GetComponent<InitializePlayerOnNetwork>().selfColor = SynchroManager.Instance.fullOwners.IndexOf(int.Parse(id));
        player.GetComponent<InitializePlayerOnNetwork>().parentName = "Controller";
        player.GetComponent<InitializePlayerOnNetwork>().selfScale = Vector3.one * 0.1f;
        Debug.Log("Update");
        NetworkServer.SpawnWithClientAuthority(player, conn);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        NetworkServer.DestroyPlayersForConnection(conn);
        if (conn.lastError != NetworkError.Ok)
        {
            if (LogFilter.logError) { Debug.LogError("ServerDisconnected due to error: " + conn.lastError); }
        }
        Debug.Log("A client disconnected from the server: " + conn.address);

        List<string> ncsAddr = new List<string>();
        foreach(NetworkConnection nc in NetworkServer.connections)
        {
            if (nc == null)
                continue;
            ncsAddr.Add(GetIPId(nc.address));
        }
        foreach(string c in connected)
        {
            if (!ncsAddr.Contains(c))
            {
                disconnected.Add(c);
                connected.Remove(c);
                SynchroManager.Instance.CommandInstance.DisconnectClient(c);
                break;
            }
        }

        if (monitoring)
        {
            UpdateView();
        }
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        Debug.Log(conn.address + " " + this.networkAddress);
        if(conn.address == this.networkAddress)
        {
            Debug.Log("OK");
            Application.Quit();
        }
    }

    public static string GetLocalIPAddress()
    {
#if WINDOWS_UWP
            var icp = NetworkInformation.GetInternetConnectionProfile();

            if (icp?.NetworkAdapter == null) return null;
            var hostname =
                NetworkInformation.GetHostNames()
                    .FirstOrDefault(
                        hn =>
                            hn.Type == HostNameType.Ipv4 &&
                            hn.IPInformation?.NetworkAdapter != null &&
                            hn.IPInformation.NetworkAdapter.NetworkAdapterId == icp.NetworkAdapter.NetworkAdapterId);

            // the ip address
            return hostname?.CanonicalName;

#elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            ADDRESSFAM Addfam = ADDRESSFAM.IPv4;
            //Return null if ADDRESSFAM is Ipv6 but Os does not support it
            if (Addfam == ADDRESSFAM.IPv6 && !Socket.OSSupportsIPv6)
            {
                return null;
            }

            string output = "";

            foreach (NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces())
            {
                NetworkInterfaceType _type1 = NetworkInterfaceType.Wireless80211;
                NetworkInterfaceType _type2 = NetworkInterfaceType.Ethernet;

                if ((item.NetworkInterfaceType == _type1 || item.NetworkInterfaceType == _type2) && item.OperationalStatus == OperationalStatus.Up)
                {
                    foreach (UnicastIPAddressInformation ip in item.GetIPProperties().UnicastAddresses)
                    {
                        //IPv4
                        if (Addfam == ADDRESSFAM.IPv4)
                        {
                            if (ip.Address.AddressFamily == AddressFamily.InterNetwork)
                            {
                                output = ip.Address.ToString();
                            }
                        }

                        //IPv6
                        else if (Addfam == ADDRESSFAM.IPv6)
                        {
                            if (ip.Address.AddressFamily == AddressFamily.InterNetworkV6)
                            {
                                output = ip.Address.ToString();
                            }
                        }
                    }
                }
            }
            return output;
#else
            return "127.0.0.1";
#endif
        
    }
    public string GetOwnerId()
    {
        string ip = GetLocalIPAddress();
        string[] ipNumbers = ip.Split('.');
        return ipNumbers[3];
    }
    public string GetIPId(string ip)
    {
        string[] ipNumbers = ip.Split('.');
        return (ip == "localClient") ? "localClient" : ipNumbers[3];
    }

    public void UpdateView()
    {
        List<int> owners = SynchroManager.Instance.fullOwners;
        for (int i = 0; i < owners.Count; i++)
        {
            this.IPList.GetChild(i).GetComponentInChildren<TextMeshProUGUI>().text = "Op" + owners[i].ToString();
            this.IPList.GetChild(i).GetComponentInChildren<Image>().color = (this.connected.Contains(owners[i].ToString()) ? Color.green : Color.red);
        }
    }

    public void ReplayCommandSent(float replayTime, ISynchroCommand cmd, bool redoMeasurement, string fileStamp)
    {
        if(cmd.GetType() == typeof(Connect))
        {
            cmd.Apply();
            this.connected.Add(((Connect)cmd).owner);
            UpdateView();

            if (redoMeasurement)
            {
                measures.Add(GameObject.Find("Op" + ((Connect)cmd).owner).AddComponent<ReplayMeasurement>());
                measures[measures.Count - 1].UserConnection(replayTime);
            }

            return;
        }

        if(cmd.GetType() == typeof(StartSession))
        {
            StartSession s = (StartSession)cmd;
            TextAsset action0;
            TextAsset action1;

            if (measures[0].name.Contains("180"))
            {
                action0 = s.action180;
                action1 = s.action116;
            }
            else
            {
                action0 = s.action116;
                action1 = s.action180;
            }

            measures[0].StartMeasures(measures[0].gameObject, measures[1].gameObject, fileStamp, action0, s.offsetTime, s.task, s.condition, s.session);
            measures[1].StartMeasures(measures[1].gameObject, measures[0].gameObject, fileStamp, action1, s.offsetTime, s.task, s.condition, s.session);

            Debug.Log("Compte : " + this.connected.Count + " " + fileStamp);
        }

        if(cmd.GetType() == typeof(EndSessionResults))
        {

            float timeFromFirstAction = Mathf.Max(measures[0].FinishSession(), measures[1].FinishSession());
            measures[0].EndSession(timeFromFirstAction);
            measures[1].EndSession(timeFromFirstAction);

            Debug.Log("Saved to : " + fileStamp);
        }

        if(cmd.GetType() == typeof(TakeObject))
        {
            TakeObject t = (TakeObject)cmd;

            if (!t.objectName.Contains("Op"))
            {
                if (!t.objectName.Contains("Window"))
                {
                    if (measures[0].name.Contains(t.owner))
                    {
                        measures[0].ItemDistanceStartSetter(t.objectName, SynchroManager.Instance.GetObject(t.objectName));
                    }
                    else
                    {
                        measures[1].ItemDistanceStartSetter(t.objectName, SynchroManager.Instance.GetObject(t.objectName));
                    }
                }
                else 
                {
                    if (measures[0].name.Contains(t.owner))
                    {
                        measures[0].WindowDistanceStartSetter(t.objectName, SynchroManager.Instance.GetObject(t.objectName));
                    }
                    else
                    {
                        measures[1].WindowDistanceStartSetter(t.objectName, SynchroManager.Instance.GetObject(t.objectName));
                    }
                }
            }
        }

        if(cmd.GetType() == typeof(LetObject))
        {
            LetObject l = (LetObject)cmd;
            if (!l.objectName.Contains("Op"))
            {
                if (!l.objectName.Contains("Window"))
                {
                    if (measures[0].name.Contains(l.owner))
                    {
                        measures[0].ItemDistanceStopSetter(l.objectName);
                        measures[0].StopPSTracking(l.objectName);
                    }
                    else
                    {
                        measures[1].ItemDistanceStopSetter(l.objectName);
                        measures[1].StopPSTracking(l.objectName);
                    }
                }
                else
                {
                    if (measures[0].name.Contains(l.owner))
                    {
                        measures[0].WindowDistanceStopSetter(l.objectName);
                    }
                    else
                    {
                        measures[1].WindowDistanceStopSetter(l.objectName);
                    }
                }
            }
        }

        if(cmd.GetType() == typeof(ChangePermission))
        {
            ChangePermission cp = (ChangePermission)cmd;

            if (cp.objectName.Contains("Window"))
            {
                if (measures[0].name.Contains(cp.owner))
                {
                    measures[0].WindowDistanceStartSetter(cp.objectName, SynchroManager.Instance.GetObject(cp.objectName));
                }
                else
                {
                    measures[1].WindowDistanceStartSetter(cp.objectName, SynchroManager.Instance.GetObject(cp.objectName));
                }
            }
            else if (cp.permissionState == 1 && cp.owners.Count < 3)
            {
                if (measures[0].name.Contains(cp.owner))
                {
                    measures[0].ToPSPlusMax();
                    measures[0].StartPSTracking(cp.objectName);
                }
                else
                {
                    measures[1].ToPSPlusMax();
                    measures[1].StartPSTracking(cp.objectName);
                }
            }
        }

        if(cmd.GetType() == typeof(ChangeSurface))
        {
            ChangeSurface cs = (ChangeSurface)cmd;

            if (!cs.isWindow)
            {
                measures[0].MovedItemNotWindow(cs.objectName);
                measures[1].MovedItemNotWindow(cs.objectName);

                if (cs.surfaceName != null && cs.surfaceName != "" && cs.surfaceName.Contains("Window"))
                {
                    measures[0].MovedItemWindow(cs.owner, cs.objectName, cs.surfaceName);
                    measures[1].MovedItemWindow(cs.owner, cs.objectName, cs.surfaceName);
                }
            }
        }

        if(cmd.GetType() == typeof(DeleteObject))
        {
            DeleteObject d = (DeleteObject)cmd;
            measures[0].WindowDistanceStopSetter(d.name);
            measures[1].WindowDistanceStopSetter(d.name);
        }

        cmd.Apply();    
    }

    public void GetInterPlayerDist()
    {
        if (this.connected.Count == 2)
        {
            PersonAuthorViewRights[] players = GameObject.FindObjectsOfType<PersonAuthorViewRights>();
            CinematicLogWriter.Instance.avgInterPlayerDistance += (Vector3.Distance(players[0].transform.position, players[1].transform.position) / 10f);
        }
    }
}
