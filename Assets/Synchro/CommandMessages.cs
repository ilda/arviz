using NaughtyAttributes;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace Synchro
{
    public class CommandMessages : NetworkBehaviour
    {
        private bool sessionStarted = false;

        private void Start()
        {
        }

        private void Update()
        {
            if (sessionStarted && this.isServer && Input.GetKeyUp(KeyCode.Space))
            {
                Debug.Log("Not Authorized to start again");
            }

            if (!sessionStarted && this.isServer && Input.GetKeyUp(KeyCode.Space))
            {
                sessionStarted = true;
                Rpc_StartSession(SynchroManager.Instance.ownerId);
                CinematicLogWriter.Instance.StartSession(SynchroManager.Instance.ownerId);
            }


        }

        public override void OnStartAuthority()
        {
            
        }

        public override void OnNetworkDestroy()
        {
            CinematicLogWriter.Instance.Close();
        }

        void TestAuthority()
        {
            Debug.Log(this.localPlayerAuthority);
        }

        [ClientRpc]
        public void Rpc_SpatialStatus(string name, Vector3 pos, Quaternion rot, Vector3 scale)
        {
            SpatialStatus spatialstatus = new SpatialStatus(name, pos, rot, scale);
            spatialstatus.Apply();
        }
        [Command]
        public void Cmd_SpatialStatus(string name, Vector3 pos, Quaternion rot, Vector3 scale)
        {
            SpatialStatus spatialstatus = new SpatialStatus(name, pos, rot, scale);
            spatialstatus.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(spatialstatus.ToString());
            
            Rpc_SpatialStatus(name, pos, rot, scale);
        }

        [ClientRpc]
        public void Rpc_ColorStatus(string name, Color color)
        {
            ColorStatus colorstatus = new ColorStatus(name, color);
            colorstatus.Apply();
        }
        [Command]
        public void Cmd_ColorStatus(string name, Color color)
        {
            ColorStatus colorstatus = new ColorStatus(name, color);
            colorstatus.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(colorstatus.ToString());

            Rpc_ColorStatus(name, color);
        }

        [ClientRpc]
        public void Rpc_TransformsStatusUpdate(string owner, string[] names, Vector3[] poses, Quaternion[] rots, Vector3[] scales)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            TransformsStatusUpdate transformsstatusupdate = new TransformsStatusUpdate(owner, new List<string>(names), new List<Vector3>(poses), new List<Quaternion>(rots), new List<Vector3>(scales));
            transformsstatusupdate.Apply();
        }
        [Command]
        public void Cmd_TransformsStatusUpdate(string owner, string[] names, Vector3[] poses, Quaternion[] rots, Vector3[] scales)
        {
            TransformsStatusUpdate transformsstatusupdate = new TransformsStatusUpdate(owner, new List<string>(names), new List<Vector3>(poses), new List<Quaternion>(rots), new List<Vector3>(scales));
            transformsstatusupdate.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(transformsstatusupdate.ToString());
            Rpc_TransformsStatusUpdate(owner, names, poses, rots, scales);
        }

        [ClientRpc]
        public void Rpc_SpawnObject(string owner, string name, string prefabName, string parentName, Vector3 startPos, Quaternion startRot, Vector3 startScale, int privacy, int[] owners)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            SpawnObject spawnobject = new SpawnObject(owner, name, prefabName, parentName, startPos, startRot, startScale, privacy, new List<int>(owners));
            spawnobject.Apply();
        }
        [Command]
        public void Cmd_SpawnObject(string owner, string name, string prefabName, string parentName, Vector3 startPos, Quaternion startRot, Vector3 startScale, int privacy, int[] owners)
        {
            SpawnObject spawnobject = new SpawnObject(owner, name, prefabName, parentName, startPos, startRot, startScale, privacy, new List<int>(owners));
            spawnobject.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(spawnobject.ToString());
            Rpc_SpawnObject(owner, name, prefabName, parentName, startPos, startRot, startScale, privacy, owners);
        }

        [ClientRpc]
        public void Rpc_TakeOwnership(string objectName, string owner)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            TakeOwnership takeownership = new TakeOwnership(objectName, owner);
            takeownership.Apply();
        }
        [Command]
        public void Cmd_TakeOwnership(string objectName, string owner)
        {
            TakeOwnership takeownership = new TakeOwnership(objectName, owner);
            takeownership.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(takeownership.ToString());
            Rpc_TakeOwnership(objectName, owner);
        }

        [ClientRpc]
        public void Rpc_MakePublic(string objectName, string owner)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            MakePublic makepublic = new MakePublic(objectName, owner);
            makepublic.Apply();
        }
        [Command]
        public void Cmd_MakePublic(string objectName, string owner)
        {
            MakePublic makepublic = new MakePublic(objectName, owner);
            makepublic.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(makepublic.ToString());
            Rpc_MakePublic(objectName, owner);
        }

        [ClientRpc]
        public void Rpc_Register(string name, string owner)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            Register register = new Register(name, owner);
            register.Apply();
        }
        [Command]
        public void Cmd_Register(string name, string owner)
        {
            Register register = new Register(name, owner);
            register.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(register.ToString());
            Rpc_Register(name, owner);
        }

        [ClientRpc]
        public void Rpc_UpdatePresence(string[] name, string owner)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            UpdatePresence updatepresence = new UpdatePresence(new List<string>(name), owner);
            updatepresence.Apply();
        }
        [Command]
        public void Cmd_UpdatePresence(string[] name, string owner)
        {
            UpdatePresence updatepresence = new UpdatePresence(new List<string>(name), owner);
            updatepresence.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(updatepresence.ToString());
            Rpc_UpdatePresence(name, owner);
        }

        [ClientRpc]
        public void Rpc_ChangePermission(string owner, string objectName, int permissionState, int[] owners)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            ChangePermission changepermission = new ChangePermission(owner, objectName, permissionState, new List<int>(owners));
            changepermission.Apply();
        }
        [Command]
        public void Cmd_ChangePermission(string owner, string objectName, int permissionState, int[] owners)
        {
            ChangePermission changepermission = new ChangePermission(owner, objectName, permissionState, new List<int>(owners));
            changepermission.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(changepermission.ToString());
            Rpc_ChangePermission(owner, objectName, permissionState, owners);
        }

        [ClientRpc]
        public void Rpc_ChangeSurface(string owner, string objectName, string surfaceName, bool isWindow)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            ChangeSurface changesurface = new ChangeSurface(owner, objectName, surfaceName, isWindow);
            changesurface.Apply();
        }
        [Command]
        public void Cmd_ChangeSurface(string owner, string objectName, string surfaceName, bool isWindow)
        {
            ChangeSurface changesurface = new ChangeSurface(owner, objectName, surfaceName, isWindow);
            changesurface.Apply();            
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(changesurface.ToString());
            Rpc_ChangeSurface(owner, objectName, surfaceName, isWindow);
        }

        [ClientRpc]
        public void Rpc_ChangePermissionDisplay(string owner, int mode)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            ChangePermissionDisplay changepermissiondisplay = new ChangePermissionDisplay(owner, mode);
            changepermissiondisplay.Apply();
        }
        [Command]
        public void Cmd_ChangePermissionDisplay(string owner, int mode)
        {
            ChangePermissionDisplay changepermissiondisplay = new ChangePermissionDisplay(owner, mode);
            changepermissiondisplay.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(changepermissiondisplay.ToString());
            Rpc_ChangePermissionDisplay(owner, mode);
        }

        [ClientRpc]
        public void Rpc_DeleteObject(string owner, string name)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            DeleteObject deleteobject = new DeleteObject(owner, name);
            deleteobject.Apply();
        }
        [Command]
        public void Cmd_DeleteObject(string owner, string name)
        {
            DeleteObject deleteobject = new DeleteObject(owner, name);
            deleteobject.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(deleteobject.ToString());
            Rpc_DeleteObject(owner, name);
        }

        [ClientRpc]
        public void Rpc_UpdateCalibration()
        {
            UpdateCalibration updatecalibration = new UpdateCalibration();
            updatecalibration.Apply();
        }
        [Command]
        public void Cmd_UpdateCalibration()
        {
            UpdateCalibration updatecalibration = new UpdateCalibration();
            updatecalibration.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(updatecalibration.ToString());
            Rpc_UpdateCalibration();
        }

        [ClientRpc]
        public void Rpc_TakeObject(string owner, string objectName, bool selectionState)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            TakeObject takeobject = new TakeObject(owner, objectName, selectionState);
            takeobject.Apply();
        }
        [Command]
        public void Cmd_TakeObject(string owner, string objectName, bool selectionState)
        {
            TakeObject takeobject = new TakeObject(owner, objectName, selectionState);
            takeobject.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(takeobject.ToString());
            Rpc_TakeObject(owner, objectName, selectionState);
        }

        [ClientRpc]
        public void Rpc_LetObject(string owner, string objectName, bool affectSelectionState)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            LetObject letobject = new LetObject(owner, objectName, affectSelectionState);
            letobject.Apply();
        }
        [Command]
        public void Cmd_LetObject(string owner, string objectName, bool affectSelectionState)
        {
            LetObject letobject = new LetObject(owner, objectName, affectSelectionState);
            letobject.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(letobject.ToString());
            Rpc_LetObject(owner, objectName, affectSelectionState);
        }

        [ClientRpc]
        public void Rpc_ReCalibrate(string owner, string target)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            ReCalibrate recalibrate = new ReCalibrate(owner, target);
            recalibrate.Apply();
        }
        [Command]
        public void Cmd_ReCalibrate(string owner, string target)
        {
            ReCalibrate recalibrate = new ReCalibrate(owner, target);
            recalibrate.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(recalibrate.ToString());
            Rpc_ReCalibrate(owner, target);
        }

        [ClientRpc]
        public void Rpc_ResetScene(string owner)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            ResetScene resetscene = new ResetScene(owner);
            resetscene.Apply();
        }
        [Command]
        public void Cmd_ResetScene(string owner)
        {
            ResetScene resetscene = new ResetScene(owner);
            resetscene.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(resetscene.ToString());
            Rpc_ResetScene(owner);
        }

        [ClientRpc]
        public void Rpc_CatchUp(string owner, string target, string toSpawn, string toChangeSurface, string toLetObject, string toChangePermission, string toPosition)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            CatchUp catchup = new CatchUp(owner, target, toSpawn, toChangeSurface, toLetObject, toChangePermission, toPosition);
            catchup.Apply();
        }
        [Command]
        public void Cmd_CatchUp(string owner, string target, string toSpawn, string toChangeSurface, string toLetObject, string toChangePermission, string toPosition)
        {
            CatchUp catchup = new CatchUp(owner, target, toSpawn, toChangeSurface, toLetObject, toChangePermission, toPosition);
            catchup.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(catchup.ToString());
            Rpc_CatchUp(owner, target, toSpawn, toChangeSurface, toLetObject, toChangePermission, toPosition);
        }


        // TODO : Remove Ping and registration procedures
        [ClientRpc]
        public void Rpc_Ping(string owner)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            Ping ping = new Ping(owner);
            ping.Apply();
        }
        [Command]
        public void Cmd_Ping(string owner)
        {
            Ping ping = new Ping(owner);
            ping.Apply();
            Rpc_Ping(owner);
        }

        [ClientRpc]
        public void Rpc_RecoverPersonalSpace(string owner, string objectOwner, string objectName)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            RecoverPersonalSpace recoverpersonalspace = new RecoverPersonalSpace(owner, objectOwner, objectName);
            recoverpersonalspace.Apply();
        }
        [Command]
        public void Cmd_RecoverPersonalSpace(string owner, string objectOwner, string objectName)
        {
            RecoverPersonalSpace recoverpersonalspace = new RecoverPersonalSpace(owner, objectOwner, objectName);
            recoverpersonalspace.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(recoverpersonalspace.ToString());
            Rpc_RecoverPersonalSpace(owner, objectOwner, objectName);
        }

        [ClientRpc]
        public void Rpc_ActivatePersonalSpace(string owner, bool activate)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            ActivatePersonalSpace activatepersonalspace = new ActivatePersonalSpace(owner, activate);
            activatepersonalspace.Apply();
        }
        [Command]
        public void Cmd_ActivatePersonalSpace(string owner, bool activate)
        {
            ActivatePersonalSpace activatepersonalspace = new ActivatePersonalSpace(owner, activate);
            activatepersonalspace.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(activatepersonalspace.ToString());
            Rpc_ActivatePersonalSpace(owner, activate);
        }

        [ClientRpc]
        public void Rpc_StartSession(string owner)
        {
            Measurement.Instance.StartSession();
        }

        [ClientRpc]
        public void Rpc_EndSession(string owner)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            EndSession endsession = new EndSession(owner);
            endsession.Apply();
        }
        [Command]
        public void Cmd_EndSession(string owner)
        {
            EndSession endsession = new EndSession(owner);
            endsession.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(endsession.ToString());
            Rpc_EndSession(owner);
        }

        [ClientRpc]
        public void Rpc_SessionResults(string owner, string content, string history)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            SessionResults sessionresults = new SessionResults(owner, content, history);
            sessionresults.Apply();
        }
        [Command]
        public void Cmd_SessionResults(string owner, string content, string history)
        {
            SessionResults sessionresults = new SessionResults(owner, content, history);
            sessionresults.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(sessionresults.ToString());
            //Rpc_SessionResults(owner, content, history);
        }

        [ClientRpc]
        public void Rpc_KillApp(string owner)
        {
            if (owner == SynchroManager.Instance.ownerId)
                return;

            KillApp killapp = new KillApp(owner);
            killapp.Apply();
        }
        [Command]
        public void Cmd_KillApp(string owner)
        {
            KillApp killapp = new KillApp(owner);
            killapp.Apply();
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(killapp.ToString());
            Rpc_KillApp(owner);
        }

        [ClientRpc]
        public void Rpc_Test()
        {
            Debug.Log("Test Client");
        }
        [Command]
        public void Cmd_Test()
        {
            Debug.Log("Test Serveur");
            Rpc_Test();
        }

        public void ConnectClient(string name)
        {
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(Time.time + " " + name + " Connect Op" + name);
        }

        public void DisconnectClient(string name)
        {
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(Time.time + " " + name + " Disconnect Op" + name);

        }

        public void ReconnectClient(string name)
        {
            if (SynchroManager.Instance.IsDevice() == DeviceType.MASTER)
                CinematicLogWriter.Instance.LogMessage(Time.time + " " + name + " Reconnect Op" + name);

        }

    }
}