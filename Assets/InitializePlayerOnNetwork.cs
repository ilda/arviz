﻿using Synchro;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class InitializePlayerOnNetwork : NetworkBehaviour
{
    [SyncVar(hook = "OnNameChange")]
    public string personName;

    [SyncVar(hook = "OnParentChange")]
    public string parentName;

    [SyncVar(hook = "OnColorChange")]
    public int selfColor;    
    private Color[] colorPalette = new Color[] { Color.green, Color.red, Color.yellow, Color.blue };

    [SyncVar(hook = "OnScaleChange")]
    public Vector3 selfScale;

    [SyncVar]
    public bool isNew;

    Material focusGreen;
    Material focusRed;
    Material focusYellow;
    Material focusBlue;

    GameObject PS; 

    bool secondSceneReady = false;
    bool memoryRegister = false;    

    public override void OnStartClient()
    {
        Debug.Log("StartCLient " + this.name);

        this.GetComponent<PersonAuthorViewRights>().SetPS(this.transform.GetChild(0).GetChild(0).gameObject);

        if (this.name != "Person(Clone)")
        {
            if (SynchroManager.Instance.GetObject(this.name) != null)
            {
                SynchroManager.Instance.RemoveObject(this.name);
                SynchroManager.Instance.RemoveObject(this.transform.GetChild(0).name);
            }

            SynchroManager.Instance.AddShared(this.gameObject);
            SynchroManager.Instance.AddShared(this.transform.GetChild(0).gameObject);
        }
        else if(this.personName != "Person(Clone)" && this.personName != "")
        {
            OnNameChange(this.personName);
            OnColorChange(this.selfColor);
            OnParentChange(this.parentName);
            OnScaleChange(this.selfScale);
        }

        if (!hasAuthority || this.isServer)
        {
            this.GetComponentInChildren<FollowCursor>().enabled = false;
        }
    }
    public override void OnStartAuthority()
    {
        Debug.Log("StartAuthority " + this.name);
        if (hasAuthority)
        {
            SynchroManager.Instance.CommandInstance = this.GetComponent<CommandMessages>();
            SynchroManager.Instance.myGameObjectPosition = this.gameObject;

            this.GetComponent<AuthorViewRights>().MakeShared(new List<int>() { int.Parse(SynchroManager.Instance.ownerId) });

            this.GetComponent<FollowCamera>().mainCamera = Camera.main.transform;
            this.transform.GetChild(0).GetComponent<FollowCursor>().enabled = true;
            
            if (this.name != "Person(Clone)")
            {
                SynchroManager.Instance.Lock(this.name, SynchroManager.Instance.ownerId);
                SynchroManager.Instance.Lock(this.transform.GetChild(0).name, SynchroManager.Instance.ownerId);
            }
        }
    }
    public override void OnStartLocalPlayer()
    {
        Debug.Log("StartLocalPlayer " + this.name);
    }

    private void Awake()
    {
        this.focusGreen = Resources.Load("Focus0") as Material;
        this.focusRed = Resources.Load("Focus1") as Material;
        this.focusYellow = Resources.Load("Focus2") as Material;
        this.focusBlue = Resources.Load("Focus3") as Material;
    }

    private void OnDestroy()
    {
        Debug.Log("Destroy by network");        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnNameChange(string name)
    {
        if (name == this.name)
            return;
        this.name = name;
        this.personName = name;
        this.transform.GetChild(0).name = "Cursor" + name.Substring(2);
        if(this.name != "Person(Clone)" || SynchroManager.Instance.GetObject(name) == null)
        {
            SynchroManager.Instance.AddShared(this.gameObject);
            SynchroManager.Instance.AddShared(this.transform.GetChild(0).gameObject);
            if (this.hasAuthority)
            {
                SynchroManager.Instance.Lock(this.name, SynchroManager.Instance.ownerId);
            }
        }
    }
    void OnColorChange(int selfColor)
    {
        this.selfColor = selfColor == -1 ? 0 : selfColor;
        this.GetComponent<Renderer>().material.SetColor("_WireColor", this.colorPalette[this.selfColor]);
        this.GetComponentInChildren<SpriteRenderer>().color = this.colorPalette[this.selfColor];
        this.transform.GetChild(0).GetChild(0).GetComponent<Renderer>().material = (this.selfColor == 0) ? this.focusGreen : ((this.selfColor == 1) ? this.focusRed : ((this.selfColor == 2) ? this.focusYellow : this.focusBlue));
    }
    void OnParentChange(string g)
    {
        if (GameObject.Find(this.parentName))
        {
            this.transform.parent = GameObject.Find(this.parentName).transform;
        }
        else
        {
            Invoke("WaitForParent", 0.2f);
        }

    }
    void OnScaleChange(Vector3 newScale)
    {
        this.selfScale = newScale;
        this.transform.localScale = newScale;
    }

    public void WaitForParent()
    {
        if (GameObject.Find(this.parentName))
        {
            this.transform.parent = GameObject.Find(this.parentName).transform;
        }
        else
        {
            Invoke("WaitForParent", 0.2f);
        }
    }

}
