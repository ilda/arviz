Experts question reliability of meat import inspections 

<mark=#ffff00aa>November 3, 2002 </mark>Milwaukee Journal Sentinel by Mark Johnson and John 
Fauber 


What Roger Viadero saw in the spring of 2001 made him wonder just how airtight the nation's defense against mad cow disease really is. 
Six months before Viadero left his job as inspector general of the U.S. Department of Agriculture, a shipment of 650,000 pounds of foreign pork arrived on the East Coast. Government inspectors apparently approved the shipment and sent the meat on its way to America's heartland. 

At its destination in Iowa, a meat inspector noticed a serious problem. The pork came from a country that was already under a U.S. embargo because of the threat of foot-and-mouth disease. Viadero won't say which country was the source. "The mere fact that it got in here," he said. "That's the big issue." 

The meat ended up in the largest pork-producing state in the nation, the one place that could least afford an outbreak of foot-and-mouth disease. 

As it turned out, the shipment did not bring foot-and-mouth to our shores. But an investigation by Viadero's office found two more shipments of meat -- each one comparable in size to the Iowa shipment -- that entered the United States from embargoed countries. 

The inspector general's investigation was detailed in a report highlighting flaws in the inspection system intended to guard America's food supply from diseases abroad, such as bovine spongiform encephalopathy, or BSE, the official name for mad cow. 

"If you can get embargoed products in here from an embargoed country, you can definitely get beef products in here," said Viadero, who now works for Ernst & Young. 

Nevertheless Viadero stressed that he remains confident U.S. meat is free of mad cow disease, "so confident that I still eat meat, and I still eat beef." 

Ed Curlett, a spokesman for the USDA's Animal and Plant Health Inspection Service, explained that the foot-and-mouth embargo covered all nations in the European Union and went into effect at a time when products were already on the ocean bound for the U.S. As a result, ships pulled into port carrying containers with goods that had been embargoed and others that had not. All required inspection. 

"It just created a mountain of product," Curlett said. "In the future, we'd just turn all of the ships around. . . . The bottom line is that foot-and-mouth disease never made it into this country, and we prevented it." 

But reports by the U.S. General Accounting Office suggest that stronger measures may be needed to keep the U.S. safe from mad cow disease and the devastating impact it would have on the nation's $56 billion beef industry. 

Other countries are stricter 

A 2002 GAO report said, "Federal actions do not sufficiently ensure that all BSE-infected animals or products are kept out or that if BSE were found it would be detected promptly and not spread to other cattle through animal feed or enter the human food supply." 

Among the report's findings: 

-- The U.S. is not testing nearly enough cattle to be confident of detecting any case of mad cow disease. In the last fiscal year, the USDA more than tripled the number of cow brains tested, to 17,800 as of the end of August, but this is still just a small fraction of the 37 million cattle slaughtered each year. 

-- The U.S. Food and Drug Administration is not stringently enforcing its 1997 feed ban, a measure taken to prevent cattle from being fed contaminated meat and bone meal, widely believed to have triggered the outbreak of mad cow disease in Great Britain. The GAO found firms that were out of compliance and had not been reinspected in at least two years. 

-- The U.S. feed ban "is more permissive" than the bans other countries have imposed, allowing cattle to be fed proteins from horses and pigs. This is a concern because rendered pig products -- potentially from pigs that have eaten cattle -- can then be fed back to cows, said Michael Hansen, a biologist and staff scientist with the Consumers Union. 

The GAO added that eating nerve tissue would not pose a safety concern unless it was from an infected animal. 

"To my knowledge, there's no evidence that pigs can get a spongiform encephalopathy," said Stephen Sundlof, director of the FDA's Center for Veterinary Medicine. 

Nevertheless, research has suggested that some animals may be "silent carriers," unaffected by mad cow disease but able to spread it to other affected animals. 

Sundlof said his agency had early problems enforcing the feed ban, because it had never regulated the rendering industry. 

"We basically had a brand new industry we had to bring into compliance in a very short amount of time" he said. 

The FDA was candid in bringing enforcement problems to the attention of GAO officials and has since made great strides in addressing them, Sundlof said. 

"As of today we have better than 95 percent of firms in compliance (with the feed ban), and the quality of reporting has increased dramatically," he said, adding that about 100 to 150 firms are not yet in compliance. 

As for USDA, the agency has defended its testing, saying it targets the cattle at greatest risk of having the disease: adult cows with central nervous system problems and cows that are unable to walk. The vast majority of cows slaughtered in the U.S. are less than 2 years old, younger than any cow ever found to have the disease. 

The test program is designed to find the disease even if it infects only one cow in a million. 

But the USDA's targeted testing isn't sufficient, Hansen said. The younger cows that are not being tested may be incubating mad cow disease, just at very low levels, and may be perfectly capable of passing on the disease, he said. 

The U.S. should be testing all fallen and downer cows for the disease, as Switzerland now does, Hansen said. Both he and Viadero said the nation needs to be testing far more of its cattle. 

Shortly after the GAO released its critical report this year, Agriculture Secretary Ann M. Veneman countered, saying the report contained scientific errors and didn't take into account sufficiently a Harvard University study that found a very low risk of mad cow in the U.S. 

Erin Lansburgh, assistant director of the GAO's study, said "We stand by the facts in the report and the recommendations that we made." 

Lansburgh noted that the USDA has taken steps to respond to the report, including testing more downer cattle. 

Still, the economic consequences of a mad cow outbreak would be devastating. 

The severity of the impact would depend on how many cattle were infected and whether infected beef reached the food supply. But the beef industry alone could lose billions of dollars. 

And the effect could be seen as close as a grocery store. In 1998, Britain outlawed the sale of beef on the bone, because it might include nervous-system tissue, where the infectious agent that causes mad cow could be present. 

SILENT CARRIERS 

Some scientists are concerned that a U.S. ban on feeding certain animal products to cattle is not stringent enough. 

They point to research that suggests some animals may be "silent carriers" of mad cow disease, unaffected by it but able to spread it to other animals. 
