﻿using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonForMouse : MonoBehaviour, IMixedRealityPointerHandler
{
    public Interactable button;

    public void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
        button.TriggerOnClick();
    }

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    { }

    public void OnPointerDragged(MixedRealityPointerEventData eventData)
    { }

    public void OnPointerUp(MixedRealityPointerEventData eventData)
    { }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
