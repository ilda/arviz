﻿using System.Linq;
using Microsoft.MixedReality.Toolkit.Input;
using UnityEngine;
using Microsoft.MixedReality.Toolkit;

public class HandPos : MonoBehaviour, IMixedRealitySourceStateHandler
{
    IMixedRealityPointer pointer;
    GameObject handPos;
    bool isHandSeen = false;

    // Start is called before the first frame update
    void Start()
    {
        CoreServices.InputSystem.RegisterHandler<IMixedRealityPointerHandler>(this);
        this.handPos = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        this.handPos.transform.localScale = Vector3.one * 0.02f;

        this.handPos.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.isHandSeen)
            this.handPos.transform.position = this.pointer.Position;
    }

    public void OnSourceDetected(SourceStateEventData eventData)
    {
        this.isHandSeen = true;
        this.pointer = eventData.InputSource.Pointers.First();

        this.handPos.SetActive(true);
    }

    public void OnSourceLost(SourceStateEventData eventData)
    {
        this.isHandSeen = false;

        this.handPos.SetActive(false);
    }
}
