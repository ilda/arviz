﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MessagePack;
using Synchro;
using NaughtyAttributes;

#if WINDOWS_UWP
using Windows.Networking.Sockets;
using Windows.Networking.Connectivity;
using Windows.Networking;
using System.Linq;
#endif

// Change to put cursor on wall
public class SendingTest : MonoBehaviour
{    
    private ColorStatus cr;
    private SpatialStatus ss;
    private Vector3 color_control;
    
    private float xPos;
    private float yPos;
    // Start is called before the first frame update
    void Start()
    {
        string ip = GetLocalIp();
        
        cr = new ColorStatus
        {
            color = Color.red,
            name = "Cube"
        };

        ss = new SpatialStatus
        {
            name = "Cube",
            rot = Quaternion.identity,
            scale = Vector3.one
        };
        
        SynchroManager.Instance.NetworkUpdate += SendStateCube;
    }

    private void OnDestroy()
    {
        SynchroManager.Instance.NetworkUpdate -= SendStateCube;
    }

    public static string GetLocalIp()
    {
#if WINDOWS_UWP
        var icp = NetworkInformation.GetInternetConnectionProfile();

        if (icp?.NetworkAdapter == null) return null;
        var hostname =
            NetworkInformation.GetHostNames()
                .FirstOrDefault(
                    hn =>
                        hn.Type == HostNameType.Ipv4 &&
                        hn.IPInformation?.NetworkAdapter != null &&
                        hn.IPInformation.NetworkAdapter.NetworkAdapterId == icp.NetworkAdapter.NetworkAdapterId);

        // the ip address
        return hostname?.CanonicalName;
#else
        return "127.0.0.1";
#endif

    }

    // Update is called once per frame
    void Update()
    {
        xPos = Mathf.Sin(Time.time);
        yPos = Mathf.Cos(Time.time);
        
    }


    void  SendStateCube(object sender, SynchroManager.SynchroEventArgs e)
    {
        ss.pos = new Vector3(xPos, yPos, 2.0f);
        SynchroManager.Instance.CommandInstance.Cmd_SpatialStatus(ss.name, ss.pos, ss.rot, ss.scale);
        SynchroServer.Instance.SendCommand("M", ss);
    }
}
