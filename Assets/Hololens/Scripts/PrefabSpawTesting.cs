﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Synchro;
using NaughtyAttributes;

#if UNITY_WSA
using UnityEngine.XR.WSA.Input;
#endif

public class PrefabSpawTesting : MonoBehaviour
{
    private int it = 0;
#if UNITY_WSA
    private GestureRecognizer gr;
#endif

    // Start is called before the first frame update
    void Start()
    {
#if UNITY_WSA
        gr = new GestureRecognizer();
        gr.StartCapturingGestures();

        gr.Tapped += OnTapp;
#endif
    }

    // Update is called once per frame
    void Update()
    {

    }

#if UNITY_WSA
    void OnTapp(TappedEventArgs tea)
    {
        Vector3 position = new Vector3(Random.Range(-3f, 3f), Random.Range(-1f, 1f), 2f);
        Vector3 scale =  Vector3.one * 0.05f;
        string name = "Prefab" + it.ToString();
        it++;

        SynchroManager.Instance.SpawnObject(name, "Cube", "Controller", position, Quaternion.identity, scale);
        SynchroManager.Instance.GetShared(name).AddComponent<Rotate>();
    }
#endif
    [Button]
    void OnClick()
    {
        Vector3 position = new Vector3(Random.Range(-3f, 3f), Random.Range(-1f, 1f), 2f);
        Vector3 scale = Vector3.one * 0.05f;
        string name = "Prefab" + it.ToString();
        it++;

        SynchroManager.Instance.SpawnObject(name, "Cube", "Controller", position, Quaternion.identity, scale);
        SynchroManager.Instance.GetShared(name).AddComponent<Rotate>();
    }

    private void OnDestroy()
    {
#if UNITY_WSA
        gr.Tapped -= OnTapp;
        gr.StopCapturingGestures();
#endif 
    }

}
