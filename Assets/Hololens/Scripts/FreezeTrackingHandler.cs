﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class FreezeTrackingHandler : DefaultTrackableEventHandler
{
    public Transform Controller;
    public Transform CameraTransform;
    private bool recalibrating = false;


    protected override void OnTrackingFound()
    {
        base.OnTrackingFound();

        this.Controller.parent = null;
        this.CameraTransform.parent = this.Controller;
        if(this.CameraTransform.GetComponentInChildren<VuforiaMonoBehaviour>())
            this.CameraTransform.GetComponentInChildren<VuforiaMonoBehaviour>().enabled = false;
        this.enabled = false;

        this.mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (this.mTrackableBehaviour)
            this.mTrackableBehaviour.UnregisterTrackableEventHandler(this);

        this.Controller.hasChanged = false;
        Transform[] hierarchy = this.Controller.GetComponentsInChildren<Transform>();
        foreach (Transform t in hierarchy)
            t.hasChanged = false;
    }

    public void Enable(Transform controller)
    {
        this.Controller = controller;
        this.Controller.parent = this.transform;
        this.Controller.transform.localPosition = Vector3.zero;
        this.Controller.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);

        this.CameraTransform.parent = null;
        this.CameraTransform.GetComponentInChildren<VuforiaMonoBehaviour>().enabled = true;

        this.recalibrating = true;

        this.mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (this.mTrackableBehaviour)
            this.mTrackableBehaviour.RegisterTrackableEventHandler(this);

        this.Controller.hasChanged = false;
        Transform[] hierarchy = GetComponentsInChildren<Transform>();
        foreach (Transform t in hierarchy)
            t.hasChanged = false;
    }

    private void Update()
    {
        //Debug.Log(" Tracker " + this.m_PreviousStatus.ToString() + " " + this.m_NewStatus.ToString());
    }

    public override void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
    {
        if(this.recalibrating && newStatus == TrackableBehaviour.Status.TRACKED)
        {
            this.m_PreviousStatus = previousStatus;
            this.m_NewStatus = newStatus;
            this.recalibrating = false;
            return;
        }

        base.OnTrackableStateChanged(previousStatus, newStatus);
    }
}
