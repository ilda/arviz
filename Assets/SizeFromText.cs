﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SizeFromText : MonoBehaviour
{
    public TextMeshPro text;

    // Start is called before the first frame update
    void Start()
    {
        // Resize plane size in case of long text
        int textSize = text.textInfo.pageCount;
        Debug.Log("Text size " + textSize);
        if (textSize > 23)
        {
            transform.localScale = new Vector3(transform.localScale.y, transform.localScale.y * textSize / 23, transform.localScale.z);
            text.textBounds.Expand(3f);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
