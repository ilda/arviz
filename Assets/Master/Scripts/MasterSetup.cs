﻿using System.Collections.Generic;
using Synchro;
using UnityEngine;
using UnityEngine.SceneManagement;
using NaughtyAttributes;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

public class MasterSetup : Singleton<MasterSetup>
{
    public string default_wall_config;
    public List<Hololens> hololenses;

    [HideInInspector] public Wall wall;

    // Start is called before the first frame update
    void Start()
    {
        SceneManager.LoadSceneAsync("MainMenu", LoadSceneMode.Additive);
        this.hololenses = new List<Hololens>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetupWall(List<Screen> ls, int h_size, int v_size, float h_real_size, float v_real_size, string setupName)
    {
        this.wall = new Wall();
        this.wall.AddScreenRange(ls);
        this.wall.SetTotalSize(h_size, v_size);
        this.wall.SetRealSize(h_real_size, v_real_size);
        this.wall.SetupWallDimensions();
        Debug.Log(this.wall.ToString());
        SaveSetup(setupName);
    }

    public void DeployWall(string password)
    {
        string sshKeyPath = Path.Combine(Application.streamingAssetsPath, "ssh\\wall_rsa");
        this.wall.Deploy(sshKeyPath, password);
    }

    public void LaunchWall(string password, int dataset)
    {
        if (this.wall.IsEmpty())
        {
            LoadConfig(this.default_wall_config + ".dat");
        }
        string sshKeyPath = Path.Combine(Application.dataPath, "Network\\ssh\\wall_rsa");
        this.wall.Start(sshKeyPath, password, dataset);
    }


    public void LaunchTrainingWall(string password)
    {
        if (this.wall.IsEmpty())
        {
            LoadConfig(this.default_wall_config + ".dat");
        }
        string sshKeyPath = Path.Combine(Application.dataPath, "Network\\ssh\\wall_rsa");
        this.wall.StartTraining(sshKeyPath, password);
    }

    public void SetupHololens(List<Hololens> hl_list)
    {
        this.hololenses = hl_list;
        string result = "";
        for(int i = 0; i < this.hololenses.Count; i++)
        {
            result += this.hololenses[i].ToString() + "\n";
            
            NetMqSubscriber nmqs = this.gameObject.AddComponent<NetMqSubscriber>();
            nmqs.HostIpAddress = this.hololenses[i].IPAddress;
            nmqs.IpPort = int.Parse(this.hololenses[i].port);
            nmqs.Topic = "H";
            this.gameObject.AddComponent<SynchroClient>();
        }
        Debug.Log(result);
        SaveSetupHL("hololens_config");
    }

    public string GetHololensName(string IP)
    {
        for(int i = 0; i < this.hololenses.Count; i++)
        {
            if (this.hololenses[i].IPAddress == IP)
                return this.hololenses[i].name;
        }
        throw new System.Exception("Unknown Hololens IP address");
    }

    [Button]
    public void resetDim()
    {
        this.wall.SetupWallDimensions();
        SaveSetup("wilder2");
    }

    [Button]
    public void ShowDataPath()
    {
        Debug.Log(Application.streamingAssetsPath);
        Debug.Log(Application.dataPath + "/Network/ssh/wall_rsa");
        Debug.Log(Path.Combine(Application.dataPath, "Network\\ssh\\walll_rsa"));
        Debug.Log(File.ReadAllText(Path.Combine(Application.dataPath, "Network/ssh\\wall_rsa")));
    }


    public void SaveSetup(string filename)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = File.Create(Path.Combine(Application.streamingAssetsPath, "configs/wall/", filename));
        Debug.Log("Saved to : " + Path.Combine(Application.streamingAssetsPath, "configs/wall/", filename));
        SurrogateSelector ss = new SurrogateSelector();

        // Adding Serialization surrogate to allow serializing Unity's structure that we use
        Vector2SerializationSurrogate vector2SerializationSurrogate = new Vector2SerializationSurrogate();
        Vector3SerializationSurrogate vector3SerializationSurrogate = new Vector3SerializationSurrogate();
        Vector4SerializationSurrogate vector4SerializationSurrogate = new Vector4SerializationSurrogate();
        ss.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), vector2SerializationSurrogate);
        ss.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), vector3SerializationSurrogate);
        ss.AddSurrogate(typeof(Vector4), new StreamingContext(StreamingContextStates.All), vector4SerializationSurrogate);
        bf.SurrogateSelector = ss;

        bf.Serialize(fs, this.wall);
        fs.Close();

        Debug.Log(this.wall.ToString());
    }

    public void SaveSetupHL(string filename)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = File.Create(Path.Combine(Application.streamingAssetsPath, "configs/", filename + ".dat"));
        Debug.Log("Saved to : " + Path.Combine(Application.streamingAssetsPath, "configs/", filename + ".dat"));
        SurrogateSelector ss = new SurrogateSelector();

        bf.SurrogateSelector = ss;

        bf.Serialize(fs, this.hololenses);
        fs.Close();

        string hls = "";
        foreach (var h in this.hololenses)
            hls += h.ToString() + "\n";
        Debug.Log(hls);
    }

    [Button]
    public void LoadSetup()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = File.Open(Path.Combine(Application.streamingAssetsPath, "configs/wall_config.dat"), FileMode.Open, FileAccess.Read);
        Debug.Log("Reading to : " + Path.Combine(Application.streamingAssetsPath, "configs/wall_config.dat"));
        SurrogateSelector ss = new SurrogateSelector();

        // Adding Serialization surrogate to allow serializing Unity's structure that we use
        Vector2SerializationSurrogate vector2SerializationSurrogate = new Vector2SerializationSurrogate();
        Vector3SerializationSurrogate vector3SerializationSurrogate = new Vector3SerializationSurrogate();
        Vector4SerializationSurrogate vector4SerializationSurrogate = new Vector4SerializationSurrogate();
        ss.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), vector2SerializationSurrogate);
        ss.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), vector3SerializationSurrogate);
        ss.AddSurrogate(typeof(Vector4), new StreamingContext(StreamingContextStates.All), vector4SerializationSurrogate);
        bf.SurrogateSelector = ss;

        fs.Close();
    }

    public void LoadConfig(string name)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = File.Open(Path.Combine(Application.streamingAssetsPath, "configs/wall/", name), FileMode.Open, FileAccess.Read);
        Debug.Log("Reading to : " + Path.Combine(Application.streamingAssetsPath, "configs/wall/", name));
        SurrogateSelector ss = new SurrogateSelector();

        // Adding Serialization surrogate to allow serializing Unity's structure that we use
        Vector2SerializationSurrogate vector2SerializationSurrogate = new Vector2SerializationSurrogate();
        Vector3SerializationSurrogate vector3SerializationSurrogate = new Vector3SerializationSurrogate();
        Vector4SerializationSurrogate vector4SerializationSurrogate = new Vector4SerializationSurrogate();
        ss.AddSurrogate(typeof(Vector2), new StreamingContext(StreamingContextStates.All), vector2SerializationSurrogate);
        ss.AddSurrogate(typeof(Vector3), new StreamingContext(StreamingContextStates.All), vector3SerializationSurrogate);
        ss.AddSurrogate(typeof(Vector4), new StreamingContext(StreamingContextStates.All), vector4SerializationSurrogate);
        bf.SurrogateSelector = ss;

        this.wall = (Wall)bf.Deserialize(fs);
        fs.Close();

        Debug.Log(this.wall.ToString());
    }

    public void LoadConfigHL(string name)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = File.Open(Path.Combine(Application.streamingAssetsPath, "configs/", name), FileMode.Open, FileAccess.Read);
        Debug.Log("Reading to : " + Path.Combine(Application.streamingAssetsPath, "configs/", name));
        SurrogateSelector ss = new SurrogateSelector();

        // Adding Serialization surrogate to allow serializing Unity's structure that we use
        bf.SurrogateSelector = ss;

        this.hololenses = (List<Hololens>)bf.Deserialize(fs);
        fs.Close();

        string hls = "";
        foreach (var h in this.hololenses)
            hls += h.ToString() + "\n";
        Debug.Log(hls);
    }

    [Button]
    public void SetSub()
    {
        this.gameObject.AddComponent<SynchroClient>();
    }

    [Button]
    public void IsEmpty()
    {
        Debug.Log(this.wall.IsEmpty());
    }
}
