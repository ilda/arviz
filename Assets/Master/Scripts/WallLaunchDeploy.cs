﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WallLaunchDeploy : MonoBehaviour
{
    string password;
    int dataset = 0;
    string task = "Classification";

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangePassword(string password)
    {
        this.password = password;
    }

    public void DeployOnWall()
    {
        MasterSetup.Instance.DeployWall(password);
    }

    public void LaunchOnWall()
    {        
        MasterSetup.Instance.LaunchWall(password, dataset);
        StartCoroutine(LoadYourAsyncScene());
    }

    public void TrainingOnWall()
    {
        MasterSetup.Instance.LaunchTrainingWall(password);
        StartCoroutine(LoadYourTrainingAsyncScene());
    }

    IEnumerator LoadYourAsyncScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("BaseSetupMaster", LoadSceneMode.Additive);        

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("BaseSetupMaster"));        
    }

    IEnumerator LoadYourTrainingAsyncScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("TrainingMaster", LoadSceneMode.Additive);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("TrainingMaster" +
            ""));
    }

    public void GoToSetup()
    {
        SceneManager.UnloadSceneAsync("WallLaunch");
        SceneManager.LoadScene("WallSetup", LoadSceneMode.Additive);
    }

    public void GoBack()
    {
        SceneManager.UnloadSceneAsync("WallLaunch");
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Additive);
    }

    public void UpdateDataset(string value)
    {
        if (value == "")
            return;
        Debug.Log("Ok " + value);
        dataset = int.Parse(value);
    }

    public int GetDatasetId()
    {
        return this.dataset;
    }

    public string GetTask()
    {
        return this.task;
    }

    public void SetTask(int value)
    {
        this.task = (value == 0) ? "Classification" : "Story";
    }
}
