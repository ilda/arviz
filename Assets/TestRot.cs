﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Input;
using TMPro;
using Microsoft.MixedReality.Toolkit;

public class TestRot : MonoBehaviour, IMixedRealityPointerHandler, IMixedRealityInputHandler
{
    private Quaternion rotationReferential = new Quaternion(0.707107f, 0.0f, 0.0f, 0.707107f);
    public TextMeshProUGUI textAsset;
    bool md = false;

    // Start is called before the first frame update
    void Start()
    {
        textAsset.text = "";
        var pointers = new HashSet<IMixedRealityPointer>();

        // Find all valid pointers
        foreach (var inputSource in CoreServices.InputSystem.DetectedInputSources)
        {
            foreach (var pointer in inputSource.Pointers)
            {
                textAsset.text += pointer.PointerName;
                if (pointer.IsInteractionEnabled && !pointers.Contains(pointer))
                {
                    pointers.Add(pointer);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        /*
        textAsset.text = "";
        var pointers = new HashSet<IMixedRealityPointer>();

        // Find all valid pointers
        foreach (var inputSource in CoreServices.InputSystem.DetectedInputSources)
        {
            foreach (var pointer in inputSource.Pointers)
            {
                textAsset.text += pointer.PointerName;
                if (pointer.IsInteractionEnabled && !pointers.Contains(pointer))
                {
                    pointers.Add(pointer);
                }
            }
        }
        */
        /*
        Debug.Log(Input.GetMouseButtonDown(0));
        if (Input.GetMouseButtonDown(0) || md)
        {
            textAsset.text += "true";
            md = true;
        }
        */

    }


    [Button]
    public void QuaternionOut()
    {
        this.transform.localPosition += Vector3.one;
        Debug.Log(this.transform.rotation.ToString("F6"));
    }

    [Button]
    public void QuaternionMinus()
    {
        this.transform.localRotation = this.transform.localRotation * this.rotationReferential;
        Debug.Log(this.transform.rotation.ToString("F6"));
    }

    [Button]
    public void QuaternionInverse()
    {
        Debug.Log(this.transform.rotation.ToString("F6"));

        this.transform.localPosition += Vector3.one;
        this.transform.localRotation = this.transform.localRotation * Quaternion.Inverse(this.rotationReferential);
        Debug.Log(this.transform.rotation.ToString("F6"));
    }

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        if (!eventData.used)
        {
            Debug.Log("Down");
            textAsset.text = "Down";
        }
    }

    public void OnPointerDragged(MixedRealityPointerEventData eventData)
    {
        if (!eventData.used)
        {
            Debug.Log("Drag");
            textAsset.text = "Drag";
        }
    }

    public void OnPointerUp(MixedRealityPointerEventData eventData)
    {
        if (!eventData.used)
        {
            Debug.Log("Up");
            textAsset.text = "Up";
        }
    }

    public void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
    }

    public void OnInputUp(InputEventData eventData)
    {
        Debug.Log("UpCube");
    }

    public void OnInputDown(InputEventData eventData)
    {
        Debug.Log("DownCube");
    }
}
