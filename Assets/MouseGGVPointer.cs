﻿using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Physics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit;
using UnityEngine.EventSystems;
using Microsoft.MixedReality.Toolkit.Utilities;

public class MouseGGVPointer : MonoBehaviour, IMixedRealityInputHandler
{
    public bool IsInteractionEnabled = true;
    IMixedRealityInputSystem inputSystem;
    IMixedRealityGazeProvider gazeProvider;
    IMixedRealityPointer gazePointer;
    IMixedRealityInputSource InputSourceParent;
    IMixedRealityInputSystem InputSystem;
    private bool isSelectPressed;
    InputEventData inputEventData;
    bool isPressed = false;

    private void Start()
    {
        MixedRealityServiceRegistry.TryGetService<IMixedRealityInputSystem>(out InputSystem);        
        gazeProvider = InputSystem.GazeProvider;
        gazePointer = gazeProvider.GazePointer;
        InputSourceParent = gazeProvider.GazeInputSource;

        inputEventData = new InputEventData(EventSystem.current);
        inputEventData.Initialize(gazePointer.InputSourceParent, Handedness.Any, MixedRealityInputAction.None);
    }

    private void Update()
    {
        if (isPressed)
        {
            isSelectPressed = true;
            if (IsInteractionEnabled)
            {
                BaseCursor c = gazeProvider.GazePointer.BaseCursor as BaseCursor;
                InputSystem.RaisePointerDragged(gazePointer, MixedRealityInputAction.None, Handedness.Any);
            }             
        }

        if (Input.GetMouseButtonDown(0))
        {
            isPressed = true;
            OnInputDown(inputEventData);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isPressed = false;
            OnInputUp(inputEventData);
        }        
    }

    #region IMixedRealityInputHandler Implementation

    /// <inheritdoc />
    public void OnInputUp(InputEventData eventData)
    {        
        if (eventData.SourceId == InputSourceParent.SourceId)
        {
            if (eventData.MixedRealityInputAction == MixedRealityInputAction.None)
            {
                isSelectPressed = false;
                if (IsInteractionEnabled)
                {
                    BaseCursor c = gazeProvider.GazePointer.BaseCursor as BaseCursor;
                    if (c != null)
                    {
                        c.SourceDownIds.Remove(eventData.SourceId);
                    }
                    InputSystem.RaisePointerClicked(gazePointer, MixedRealityInputAction.None, 0, Handedness.Any);
                    InputSystem.RaisePointerUp(gazePointer, MixedRealityInputAction.None, Handedness.Any);

                    // For GGV, the gaze pointer does not set this value itself. 
                    // See comment in OnInputDown for more details.
                    //gazeProvider.GazePointer.IsFocusLocked = false;
                }
            }
        }        
    }

    /// <inheritdoc />
    public void OnInputDown(InputEventData eventData)
    {
        if (eventData.SourceId == InputSourceParent.SourceId)
        {
            if (eventData.MixedRealityInputAction == MixedRealityInputAction.None)
            {
                isSelectPressed = true;
                if (IsInteractionEnabled)
                {
                    BaseCursor c = gazeProvider.GazePointer.BaseCursor as BaseCursor;
                    if (c != null)
                    {
                        c.SourceDownIds.Add(eventData.SourceId);
                    }
                    InputSystem.RaisePointerDown(gazePointer, MixedRealityInputAction.None, Handedness.Any);

                    // For GGV, the gaze pointer does not set this value itself as it does not receive input 
                    // events from the hands. Because this value is important for certain gaze behavior, 
                    // such as positioning the gaze cursor, it is necessary to set it here.
                    //gazeProvider.GazePointer.IsFocusLocked = (gazeProvider.GazePointer.Result?.Details.Object != null);
                }
            }
        }
        
    }

    #endregion  IMixedRealityInputHandler Implementation

}
