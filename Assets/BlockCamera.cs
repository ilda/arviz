﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class BlockCamera : MonoBehaviour
{
    public Transform CameraVicon;
    // Start is called before the first frame update
    void Start()
    {
        XRDevice.DisableAutoXRCameraTracking(Camera.main, true);
    }

    // Update is called once per frame
    void Update()
    {
        if (CameraVicon != null)
        {
            Vector3 tmp = CameraVicon.rotation.eulerAngles;
            Quaternion corectedVicon = Quaternion.Euler(tmp.x, tmp.y, tmp.z);

            /* use rotation referential because of hat, now not useful */
            //Vector3 eulertmp = (rotationReferential * CameraVicon.rotation).eulerAngles;
            
            Vector3 eulertmp = (CameraVicon.rotation).eulerAngles;

            Quaternion newAngle = Quaternion.Euler(-eulertmp.x, eulertmp.z, -eulertmp.y);
            transform.position = newAngle * (-CameraVicon.position);
            transform.rotation = newAngle;            
        }
    }

    private void LateUpdate()
    {
    }
}
