﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SyncPosRot : MonoBehaviour
{
    public Transform model;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = model.localPosition;
        transform.localRotation = model.localRotation;
    }
}
