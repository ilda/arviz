﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftRigh : MonoBehaviour
{
    public Vector3 left = Vector3.left;
    public Vector3 right = Vector3.right;

    bool way = false;
    float timeStep = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (way) {
            transform.position = Vector3.Lerp(left, right, Time.time - Mathf.Floor(Time.time));
        }
        else
        {
            transform.position = Vector3.Lerp(right, left, Time.time - Mathf.Floor(Time.time));
        }

        way = (((int)Time.time) % 2) == 0;
    }
}
