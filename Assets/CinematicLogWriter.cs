﻿using Synchro;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class CinematicLogWriter : Singleton<CinematicLogWriter>
{
    private string cinematicName;
    public string timestamp;
    StreamWriter log;
    float timer = 0;

    public float sessionTime = 0;
    public float avgInterPlayerDistance = 0f;

    private void Awake()
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.MASTER)
        {
            timestamp = DateTime.Now.ToString("s").Replace(':', '-') + ".txt";
            this.cinematicName = "./Cinematic_" + timestamp;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.MASTER)
        {
            timestamp = DateTime.Now.ToString("s").Replace(':', '-') + ".txt";
            this.cinematicName = "./Cinematic_" + timestamp;
            log = new StreamWriter(this.cinematicName, true);
            log.AutoFlush = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            timer = Time.time;
            avgInterPlayerDistance = 0f;
            using (StreamWriter stream = new StreamWriter("./ManualEvents_" + timestamp + ".txt", true))
            {
                stream.WriteLine(Time.time + " TimerStart " + (Time.time - timer));
            }
        }

        if (Input.GetKeyUp(KeyCode.P))
        {
            using (StreamWriter manual = new StreamWriter("./Manual_Events_" + timestamp + ".txt", true))
            {
#if UNITY_EDITOR
                manual.WriteLine(Time.time + " Storage " + Selection.activeGameObject?.name);
#endif
                Debug.Log("Storage");
            }
        }
        else if (Input.GetKeyUp(KeyCode.O))
        {
            using (StreamWriter manual = new StreamWriter("./Manual_Events_" + timestamp + ".txt", true))
            {
#if UNITY_EDITOR

                manual.WriteLine(Time.time + " PersonalUse " + Selection.activeGameObject?.name);
#endif
                Debug.Log("Personal");
            }
        }
        else if (Input.GetKeyUp(KeyCode.I))
        {
            using (StreamWriter manual = new StreamWriter("./Manual_Events_" + timestamp + ".txt", true))
            {
#if UNITY_EDITOR

                manual.WriteLine(Time.time + " SharedUse " + Selection.activeGameObject?.name);
#endif
                Debug.Log("Shared");
            }
        }
    }

    private void OnDestroy()
    {
        log.Close();
    }

    public void StartSession(string owner)
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.MASTER)
            log.WriteLine(Time.time + " " + owner + " StartSession ");
    }

    public void LogMessage(ISynchroCommand msg)
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.MASTER)
            log.WriteLine(msg.ToString());
    }

    public void LogMessage(string msg)
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.MASTER)
        {
            log.WriteLine(msg);

            //Debug.Log(msg);
        }
    }

    public void LogSession(string msg)
    {
        if (SynchroManager.Instance.IsDevice() == Synchro.DeviceType.MASTER)
            log.WriteLine(msg);
    }

    public void StopTime()
    {
        using (StreamWriter stream = new StreamWriter("./ManualEvents_" + timestamp + ".txt", true))
        {
            stream.WriteLine(Time.time + " TimeSample " + (Time.time - timer));
        }
        sessionTime = Time.time - timer;
        timer = Time.time;
    }

    public void Close()
    {
       log.Close();
    }
}
