# ARViz project: Extending Wall Displays with AR

This project is working on Unity 2018.4.36f1. It requires Linux Build support, UWP build support, Vuforia Augmented Reality Support, and IL2CPP Windows build support. It needs the package Windows Mixed Reality version 1.0.19. Download the project 

## Replay participant session

To replay the sessions from the participants experiment:
1. Open the ReplayScene under the "Assets" > "Scenes" folder. 
2. Click on the Replay Controller object in the opened scene on the left side of the interface.
3. In the Replay Controller script, on the right side of the interface. You will gain access to different parameters
	1. Session: The collaboration session (Between S0 and S11)
	2. Condition: The condition (Between AR and Wall)
	3. Task: The task (Between Story and Classification)
	
	$`\textcolor{red}{\text{Warning: the combination S8/AR/Classification does not have log, launching it will result in not showing anything}}`$ 
4. Once the session parameters are set, click play to launch the replay. You can view the scene either through the camera, or through the Scene window, where you can move the camera at will using the mouse.
5. The replay can be paused by pressing SPACE or hitting the Pause button in the Replay Controller script. When paused, you can change the Replay Speed of the scene by adding a multiplier. Hit SPACE again or the UnPause button to resume the replay.

![A unity 3D interface](Interface.png "Steps to replay a session")
